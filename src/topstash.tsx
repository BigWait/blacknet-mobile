import React, { Component } from "react";
import { StyleSheet,  View, Text, Platform } from 'react-native'
import { stashStore } from "./stores/stash";
import colors from "./style/colors";
import * as RootNavigation from './rootNavigation';
import { HomeRoutes } from "./routes";
import { TouchableView } from "./components/common/touchable-view";
import i18n from "./services/i18n";
import { LANGUAGE_KEYS } from "./constants/language";
import { observer } from "mobx-react";
import RootSibling from 'react-native-root-siblings'
import { platform } from "process";
import { optionStore } from "./stores/option";
import { observable } from "mobx";

@observer export default class TopStash extends Component {
    sibling: any;

    constructor(props: any) {
        super(props)
        this.sibling = undefined;
    }

    render() {
        const { styles } = obStyles
        if(stashStore.pendingLength < 1 && stashStore.isDisplay){
            this.sibling && this.sibling.destroy()
            this.sibling = undefined
            return null
        }
        
        this.sibling = new RootSibling(<View style={styles.container}>
            <TouchableView style={styles.touch} onPress={()=>{
                stashStore.refresh();
                RootNavigation.navigate(HomeRoutes.Stash, {});
            }}>
                <Text>
                <Text style={styles.text}>{stashStore.pendingLength}</Text>
                <Text style={[{paddingLeft: 4, paddingRight: 4}, styles.text]}>{i18n.t(LANGUAGE_KEYS.TX_TRANSACTIONS)}</Text>
                <Text style={styles.text}>{i18n.t(LANGUAGE_KEYS.TX_WAITING_CONFIRMED)}</Text>
                </Text>
            </TouchableView>
        </View>)
        return null;
    }
}
// const viewHeight = Platform.OS == 'android' ? 30 : 60;
const obStyles = observable({
    get styles() {
      return StyleSheet.create({
            container: {
                position: 'absolute',
                top: 0,
                right: 0,
                elevation: 99999,
                left: 0,
                height: 53,
                backgroundColor: optionStore.darkTheme ? colors.primary : "#27892f",
                zIndex: 1000,
                
                
            },
            touch: {
                height: 53,
                paddingBottom: 2,
                paddingTop: 2,
                flex:1,
                textAlign: 'center',
                alignItems: 'center',
                justifyContent: 'flex-end',
            },
            text: {
                color: optionStore.darkTheme ? colors.deep : colors.pure, fontSize: 12
            }
        })
    }
})








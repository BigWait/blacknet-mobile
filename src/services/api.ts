/**
 * Gravatar service
 * @file Gravatar
 * @module app/services/gravatar
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { blnnodeAPI, blnscanAPI } from '@app/config'
import { IBlnTransaction, IBlnScanContact, IBlnLease, IBlnScanStatistics, IBlnBlock, IBlnScanAccount, IBlnScanMessage, IBlnScanForumPost, BlnTransaction, ICoinCap, IBlnGroup } from '@app/types/bln'
import { IRequestParams, ITxResultPaginate } from '@app/types/http'
import BigNumber from 'bignumber.js';
import {stashStore }from '@app/stores/stash'
import Blacknetjs from 'blacknetjs';
import fetch from './fetch'
import { accountStore } from '@app/stores/account';
import transactionStore from '@app/pages/wallet/stores/transaction';
import md5 from 'md5';
import { chatGroupStore } from '@app/pages/chat/stores/group';
import { chatKeyStore } from '@app/pages/chat/stores/key';
const devFundAddress = 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e'
// const blacknetjs = new Blacknetjs(new Blacknetjs.Config(blnnodeAPI))
const blacknetjs = new Blacknetjs()
blacknetjs.jsonrpc.endpoint = blnnodeAPI;

export default class {
    // blacknet price
    
    

    static fetchLeaseList(address?: string, params?: IRequestParams): Promise<ITxResultPaginate<IBlnLease[]>>{
        return fetch.get(`${blnscanAPI}/api/outleases/${address}`, params)
        .then((res)=>res.json())
        .then((json)=>{
            return {
                txns: json,
                params: params,
                pagination: {
                    current_page: 1,
                    per_page: 0,
                    txns_len: json.length
                }
            } as ITxResultPaginate<IBlnLease[]>
        })
    }

    static fetchTransactions(address?: string, params?: IRequestParams): Promise<ITxResultPaginate<IBlnTransaction[]>>{
        return fetch.get(`${blnscanAPI}/api/v3/account/${address}`, params)
        .then((res)=>res.json())
        .then((json)=>{
            if(json.txns && json.txns.length){
                for (const key in json.txns) {
                    if (Object.prototype.hasOwnProperty.call(json.txns, key)) {
                        const tx = json.txns[key];
                        tx.to = tx.data.to
                        tx.amount = tx.data.amount
                        tx.blockHeight = tx.data.blockHeight
                        tx.message = tx.data.message
                        json.txns[key] = tx
                    }
                }
            }
            json.pagination = {
                current_page: json.page | 0,
                per_page: (json.page | 0) - 1,
                txns_len: json.txns ? json.txns.length : 0
            }
            json.params = params
            return json
        })
    }
     

    
     static async transfer(mnemonic: string, amountstr: string, from: string, to: string, message?: string, encrypted?: boolean): Promise<IBlnTransaction> {
        return new Promise((resolve, reject)=>{
            const amount = new BigNumber(amountstr).multipliedBy(1e8)
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amount.toNumber(),
                message: message,
                from: from,
                to: to,
                encrypted: encrypted ? 1 : 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromTx(res.body, from, to, amount.toString(), message)
                    stashStore.add(from, tx)
                    transactionStore.upsertTx(from, tx)
                    resolve(tx)
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                reject(err);
            })  
        })
     }

     static async addContact(mnemonic: string, myAddress: string, contactAdress: string, name: string): Promise<any> {
        return new Promise((resolve, reject)=>{
            const message = 'new contact: ' + Blacknetjs.Encrypt(mnemonic, myAddress, name)
            const amount = new BigNumber(0.1).multipliedBy(1e8)
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amount.toNumber(),
                from: myAddress,
                to: contactAdress,
                message: message,
                encrypted: 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromTx(res.body, myAddress, contactAdress, amount.toString(), message)
                    stashStore.add(myAddress, tx)
                    transactionStore.upsertTx(myAddress, tx)
                    resolve({
                        txid: res.body,
                        message: message
                    })
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
     }

     static async lease(mnemonic: string, amountStr: string, from: string, to: string): Promise<IBlnTransaction> {
        return new Promise((resolve, reject)=>{
            const amount = new BigNumber(amountStr).multipliedBy(1e8)
            return blacknetjs.jsonrpc.lease(mnemonic, {
                fee: 0.001*1e8,
                amount: amount.toNumber(),
                from: from,
                to: to
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromLease(res.body, from, to, amount.toString())
                    stashStore.add(from, tx)
                    transactionStore.upsertTx(from, tx)
                    resolve(tx)
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
     }

     static async cancelLease(mnemonic: string, amountStr: string, from: string, to: string, height: number): Promise<any> {
        return new Promise((resolve, reject)=>{
            const amount = new BigNumber(amountStr).multipliedBy(1e8)
            return blacknetjs.jsonrpc.cancelLease(mnemonic, {
                fee: 0.001*1e8,
                amount: amount.toNumber(),
                from: from,
                to: to,
                height: height
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromCancelLease(res.body, from, to, amount.toString(), height)
                    stashStore.add(from, tx)
                    resolve(res.body)
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
     }

     // explorer statistics
     static blnScanStatistics(): Promise<IBlnScanStatistics>{
        return fetch.get(`${blnscanAPI}/api/statistics`)
        .then((res)=>{
            try {
                return res.json()
            } catch (error) {
                return Promise.reject(error)   
            }
        })
        .then((json)=>{
            if(json.overview && json.overview.supply){
                json.supply = json.overview.supply
                json.blocks = json.overview.height
            }
            return json
        }).catch((err)=>{
            console.log(err);
        })
    }

    static blnScanRecentTransactions(): Promise<IBlnTransaction[]>{
        return fetch.get(`${blnscanAPI}/api/recent_transactions`)
        .then((res)=>{
            try {
                return res.json()
            } catch (error) {
                return Promise.reject(error)   
            }
        })
        .then((json)=>{
            if(json.rtxns && json.rtxns.length){
                for (const key in json.rtxns) {
                    if (Object.prototype.hasOwnProperty.call(json.rtxns, key)) {
                        const tx = json.rtxns[key];
                        tx.to = tx.data.to
                        tx.amount = tx.data.amount
                        tx.blockHeight = tx.data.blockHeight
                        tx.message = tx.data.message
                        json.rtxns[key] = tx
                    }
                }
            }
            return json.rtxns || []
        }).catch((err)=>{
            console.log(err);
        })
    }
    static blnScanRecentBlocks(): Promise<IBlnBlock[]>{
        return fetch.get(`${blnscanAPI}/api/recent_blocks`)
        .then((res)=>{
            try {
                return res.json()
            } catch (error) {
                return Promise.reject(error)   
            }
        })
        .catch((err)=>{
            console.log(err);
        })
    }
    static blnScanTopAccounts(): Promise<IBlnScanAccount[]>{
        return fetch.get(`${blnscanAPI}/api/top_accounts`)
        .then((res)=>{
            try {
                return res.json()
            } catch (error) {
                return Promise.reject(error)   
            }
        })
        .catch((err)=>{
            console.log(err);
        })
    }
    static blnScanAccount(address: string): Promise<IBlnScanAccount>{
        return fetch.get(`${blnscanAPI}/api/account/ledger/${address}`)
        .then((res)=>{
            try {
                return res.json()
            } catch (error) {
                return Promise.reject(error)   
            }
        })
        .catch((err)=>{
            console.log(err);
        })
    }
    static blnScanBlock(block: string | number): Promise<IBlnBlock>{
        return fetch.get(`${blnscanAPI}/api/block/${block}`)
        .then((res)=>{
            try {
                return res.json()
            } catch (error) {
                return Promise.reject(error)   
            }
        })
        .then((json)=>{
            if(json && json.length){
                return json[0]
            }
            return json
        })
        .then((json)=>{
            if(json.transactions && json.transactions.length){
                json.transactions = json.transactions.reverse();
                for (const key in json.transactions) {
                    if (Object.prototype.hasOwnProperty.call(json.transactions, key)) {
                        const tx = json.transactions[key];
                        tx.to = tx.data.to
                        tx.amount = tx.data.amount
                        tx.blockHeight = tx.data.blockHeight
                        tx.message = tx.data.message
                        json.transactions[key] = tx
                    }
                }
            }
            return json
        })
        .catch((err)=>{
            console.log(err);
        })
    }
    static blnScanTransaction(txid: string): Promise<IBlnTransaction>{
        return fetch.get(`${blnscanAPI}/api/tx/${txid}`)
        .then((res)=>{
            try {
                return res.json()
            } catch (error) {
                return Promise.reject(error)   
            }
        })
        .then((tx)=>{
            tx.to = tx.data.to
            tx.amount = tx.data.amount
            tx.blockHeight = tx.data.blockHeight
            tx.message = tx.data.message
            return tx
        }).catch((err)=>{
            console.log(err);
        })
    }
    static blnFeedback(params: any): Promise<boolean>{
        return fetch.get(`${blnscanAPI}/api/v2/feedback`, params)
        .then((res)=>res.json())
        .then((json)=>{
            if(json.code === 0){
                return true
            }
            return false
        })
    }
    static blnScanContact(address: any): Promise<Array<IBlnScanContact>>{
        return fetch.get(`${blnscanAPI}/api/contact/${address}`)
        .then((res)=>res.json())
    }

    static blnScanUpdateProfile(mnemonic: string, myAddress: string, contactAdress: string, data: any): Promise<any>{
        return new Promise((resolve, reject)=>{
            const message = 'profile: ' + Buffer.from(JSON.stringify(data), 'utf8').toString('base64')
            const amount = new BigNumber(0.1).multipliedBy(1e8).toNumber()
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amount,
                from: myAddress,
                to: contactAdress,
                message: message,
                encrypted: 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromTx(res.body, myAddress, contactAdress, amount.toString(), message)
                    stashStore.add(myAddress, tx)
                    transactionStore.upsertTx(myAddress, tx)
                    resolve({
                        txid: res.body,
                        amount: amount,
                        message: message
                    })
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
    }

    static setNickname(mnemonic: string, myAddress: string, contactAdress: string, name: string): Promise<any> {
        return new Promise((resolve, reject)=>{
            const message = 'nickname: ' + Buffer.from(name, 'utf8').toString('base64')
            const amount = new BigNumber(0.01).multipliedBy(1e8)
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amount.toNumber(),
                from: myAddress,
                to: contactAdress,
                message: message,
                encrypted: 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromTx(res.body, myAddress, contactAdress, amount.toString(), message)
                    stashStore.add(myAddress, tx)
                    resolve({
                        txid: res.body,
                        message: message
                    })
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
     }
     static setSignmessage(mnemonic: string, myAddress: string, contactAdress: string, name: string): Promise<any> {
        return new Promise((resolve, reject)=>{
            const message = 'signmessage: ' + Buffer.from(name, 'utf8').toString('base64')
            const amount = new BigNumber(0.02).multipliedBy(1e8)
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amount.toNumber(),
                from: myAddress,
                to: contactAdress,
                message: message,
                encrypted: 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromTx(res.body, myAddress, contactAdress, amount.toString(), message)
                    stashStore.add(myAddress, tx)
                    resolve({
                        txid: res.body,
                        message: message
                    })
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
     }
    static blnScanNickname(address: any): Promise<string|undefined>{
        return fetch.get(`${blnscanAPI}/api/nickname/${address}`)
        .then((res)=>res.json())
        .then((json)=>{
            if(json.text){
                return Buffer.from(json.text.replace("nickname: ", ''), 'base64').toString()
            }
            return undefined
        })
        .catch((err)=>{
            console.log(err)
            return undefined;
        })
    }
    
    static blnScanSignmessage(address: any): Promise<string|undefined>{
        return fetch.get(`${blnscanAPI}/api/signmessage/${address}`)
        .then((res)=>res.json())
        .then((json)=>{
            if(json.text){
                return Buffer.from(json.text.replace("signmessage: ", ''), 'base64').toString()
            }
            return undefined
        })
        // .then((json)=>{
        //     json.message = json.data.message
        //     const tx = json as IBlnTransaction;
        //     if(tx.message){
        //         return Buffer.from(tx.message.replace("signmessage: ", ''), 'base64').toString()
        //     }
        //     return tx.message
        // })
        .catch((err)=>{
            console.log(err)
            return undefined;
        })
    }

   

    static blnScanUsers(): Promise<Array<IBlnScanAccount>>{
        return fetch.get(`${blnscanAPI}/api/users`)
        .then((res)=>res.json())
        .catch((err)=>{
            console.log(err)
            return [];
        })
    }

    static blnScanMessages(address: string, params: {with: string, page?: number} & IRequestParams): Promise<IBlnScanMessage[]>{
        return fetch.get(`${blnscanAPI}/api/messages/${address}`, params)
        .then((res)=>res.json())
        .then((res: IBlnScanMessage[])=>{
            if(res && res.length){
                for (const key in res) {
                    if (Object.prototype.hasOwnProperty.call(res, key)) {
                        res[key].received = true
                        res[key].sent = true
                        res[key].pending = false
                    }
                }
            }
            return res
        })
    }

    // static blnScanGroupMessages(address: string, params: {page?: number} & IRequestParams): Promise<IBlnScanMessage[]>{
    //     return fetch.get(`${blnscanAPI}/api/messages/group/${address}`, params)
    //     .then((res)=>res.json())
    //     .then((res: IBlnScanMessage[])=>{
    //         if(res && res.length){
    //             for (const key in res) {
    //                 if (Object.prototype.hasOwnProperty.call(res, key)) {
    //                     res[key].received = true
    //                     res[key].sent = true
    //                     res[key].pending = false
    //                 }
    //             }
    //         }
    //         return res
    //     })
    // }

    static blnScanGroupMessages(txid: string, params: {page?: number} & IRequestParams): Promise<IBlnScanMessage[]>{
        return fetch.get(`${blnscanAPI}/api/message/type/groupMessage`, {txid: txid, ...params})
        .then((res)=>res.json())
        .then((res: IBlnScanMessage[])=>{
            if(res && res.length){
                for (const key in res) {
                    if (Object.prototype.hasOwnProperty.call(res, key)) {
                        res[key].received = true
                        res[key].sent = true
                        res[key].pending = false
                        res[key].group = true
                    }
                }
            }
            return res
        })
    }

    

    static sendMessage(mnemonic: string, from: string, to: string, msg: string, quote?: string): Promise<IBlnScanMessage> {
        
        return this.sendEncryptMessage('chat', to, msg, 0.005, function(res:any,  from: string, amountText: string, message: string, resolve:any){

            const tx = BlnTransaction.fromTx(res.body, from, to, amountText.toString(), message)
            stashStore.add(from, tx)
            transactionStore.upsertTx(from, tx)
            resolve({
                from: from,
                to: to,
                txid: res.body,
                time: (new Date().getTime() / 1000 | 0).toString(),
                text: message
            })
        }, quote);
        return new Promise((resolve, reject)=>{
            const message = 'chat: ' + Blacknetjs.Encrypt(mnemonic, to, msg)
            const amount = new BigNumber(0.005).multipliedBy(1e8)
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amount.toNumber(),
                from: from,
                to: to,
                message: message,
                encrypted: 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromTx(res.body, from, to, amount.toString(), message)
                    stashStore.add(from, tx)
                    transactionStore.upsertTx(from, tx)
                    resolve({
                        from: from,
                        to: to,
                        txid: res.body,
                        time: (new Date().getTime() / 1000 | 0).toString(),
                        text: message
                    })
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
    }

    static sendEncryptMessage(type: string, to: string, msg: string, amount: number, success: Function, quote?: string): Promise<IBlnScanMessage> {

        let mnemonic = accountStore.currentMnemonic;
        let from = accountStore.currentAddress;

        return new Promise((resolve, reject)=>{
            const message =  type + ': ' + Blacknetjs.Encrypt(mnemonic, to, msg) + `${quote ? quote : ''}`
            const amountText = new BigNumber(0.005).multipliedBy(1e8)
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amountText.toNumber(),
                from: from,
                to: to,
                message: message,
                encrypted: 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    success(res, from, amountText, message, resolve)
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
    }

    static sendGroupMessage(mnemonic: string, from: string, txid: string, msg: string, isPrivate: boolean, quote?: string): Promise<IBlnScanMessage> {
        return new Promise((resolve, reject)=>{
            // const message = 'chat: ' + Buffer.from(msg, 'utf8').toString('base64') + `${quote ? quote : ''}`
            let text 
            if(isPrivate){
                const groupSK = chatKeyStore.getSK(mnemonic, from, txid)
                if(!groupSK){
                    return reject("sk not found")
                }
                const groupPK = Blacknetjs.Address(groupSK)
                text = Blacknetjs.Encrypt(groupSK, groupPK, msg)
            } else {
                text = Buffer.from(msg, 'utf8').toString('base64')
            }
            let msgJson: {[key: string]: any}= {
                txid: txid, 
                text: text
            }
            if(quote){
                msgJson['text'] = msgJson['text'] + quote
            }
            const message = `groupMessage: ${JSON.stringify(msgJson)}`
            const amount = new BigNumber(0.01).multipliedBy(1e8)    
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amount.toNumber(),
                from: from,
                to: devFundAddress,
                message: message,
                encrypted: 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    console.log(res.body)
                    const tx = BlnTransaction.fromTx(res.body, from, devFundAddress, amount.toString(), message)
                    stashStore.add(from, tx)
                    transactionStore.upsertTx(from, tx)
                    resolve({
                        from: from,
                        to: devFundAddress,
                        txid: res.body,
                        group: true,
                        time: (new Date().getTime() / 1000 | 0).toString(),
                        text: message
                    })
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
    }

     static addPost(mnemonic: string, from: string, to: string, data: any): Promise<IBlnScanForumPost> {
        return new Promise((resolve, reject)=>{
            const message = 'post: ' + Buffer.from(JSON.stringify({
                channel: "default",
                ...data
            }), 'utf8').toString('base64')
            const amount = new BigNumber(0.03).multipliedBy(1e8)
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amount.toNumber(),
                from: from,
                to: to,
                message: message,
                encrypted: 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromTx(res.body, from, to, amount.toString(), message)
                    stashStore.add(from, tx)
                    transactionStore.upsertTx(from, tx)
                    resolve({
                        from: from,
                        to: to,
                        txid: res.body,
                        time: (new Date().getTime() / 1000 | 0).toString(),
                        amount: amount.toNumber(),
                        text: message,
                        data: data
                    })
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
    }

    static addReplyPost(mnemonic: string, from: string, to: string, data: any): Promise<IBlnScanForumPost> {
        return new Promise((resolve, reject)=>{
            const message = 'post: ' + Buffer.from(JSON.stringify({
                channel: "default",
                ...data
            }), 'utf8').toString('base64')
            const amount = new BigNumber(0.0031).multipliedBy(1e8)
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amount.toNumber(),
                from: from,
                to: to,
                message: message,
                encrypted: 0
            }).then((res:any)=>{
                if (res.code === 200) {
                    const tx = BlnTransaction.fromTx(res.body, from, to, amount.toString(), message)
                    stashStore.add(from, tx)
                    transactionStore.upsertTx(from, tx)
                    resolve({
                        from: from,
                        to: to,
                        txid: res.body,
                        time: (new Date().getTime() / 1000 | 0).toString(),
                        amount: amount.toNumber(),
                        text: message,
                        data: data
                    })
                } else {
                    reject(res.body)
                }
            }).catch((err:any)=>{
                console.log(err)
            })  
        })
    }

    static fetchPosts(params?: IRequestParams): Promise<ITxResultPaginate<IBlnScanForumPost[]>>{
        return fetch.get(`${blnscanAPI}/api/post`, params)
        .then((res)=>res.json())
        .then((json)=>{
            let page = 1
            if(params && params.page){
                page = parseInt(params.page.toString())
            }
            return {
                txns: json,
                pagination: {
                    current_page: page,
                    per_page: page - 1,
                    txns_len: json.length
                },
                params: params
            }
        })
    }

    static fetchPostDetail(txid: string, params?: IRequestParams): Promise<ITxResultPaginate<IBlnScanForumPost[]>>{
        return fetch.get(`${blnscanAPI}/api/post/list/${txid}`, params)
        .then((res)=>res.json())
        .then((json)=>{
            let page = 1
            if(params && params.page){
                page = parseInt(params.page.toString())
            }
            return {
                txns: json,
                pagination: {
                    current_page: page,
                    per_page: page - 1,
                    txns_len: json.length
                },
                params: params
            }
        })
    }

    static fetchPostComment(txid: string, params?: IRequestParams): Promise<IBlnScanForumPost[]>{
        return fetch.get(`${blnscanAPI}/api/post/comment/${txid}`, params)
        .then((res)=>res.json())
    }


    static getPortfolio(address: any): Promise<IBlnScanForumPost>{
        return fetch.get(`${blnscanAPI}/api/portfolio/${address}`)
        .then((res)=>res.json())
        .catch((err)=>{
            console.log(err)
            return undefined;
        })
    }

    static fetchCoinList(page: number = 1, params?: IRequestParams): Promise<ITxResultPaginate<ICoinCap[]>>{
        let start = (page - 1) * 100 + 1
        return fetch.get(`https://web-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?aux=circulating_supply,max_supply,total_supply&cryptocurrency_type=all&sort=market_cap&sort_dir=desc&start=${start}`, {convert: 'USD', limit: 100, ...params})
        .then((res)=>res.json())
        .then((json)=>{
            if(json.status && json.status.error_code !== 0){
                return Promise.reject(json.status.error_message)
            }
            if(params && params.page){
                page = parseInt(params.page.toString())
            }
            for (const key in json.data) {
                if (Object.prototype.hasOwnProperty.call(json.data, key)) {
                    json.data[key].url = `https://s2.coinmarketcap.com/static/img/coins/64x64/${json.data[key].id}.png`
                }
            }
            return {
                txns: json.data,
                pagination: {
                    current_page: page,
                    per_page: page - 1,
                    total: json.status.total_count,
                    txns_len: json.data.length
                },
                params: params
            }
        })
    }

    static upload(path: string, mime: string, name?: string){
        if(!name){
            name = md5(path)
        }
        const data = new FormData();
        data.append("photo", {
            name: name,
            type: mime,
            uri: path,
        });
        let options = {
            method: "POST",
            body: data,
            headers: { "Content-Type": "multipart/form-data" },
        };
        return fetch.upload("https://blnscan.loqunbai.com/api/upload", options)
            .then((response) => response.json())
            // .then((response) => {
            //     let url = `https://ipfs.loqunbai.com/ipfs/${response.cid}`;
            // })
    }
}

export const getIPFSURL = (cid: string)=>{
    return `https://ipfs.loqunbai.com/ipfs/${cid}`;
}
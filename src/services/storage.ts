/**
 * Storage service
 * @file 本地存储服务
 * @module app/services/storage
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import AsyncStorage from '@react-native-community/async-storage'
import { STORAGE } from '@app/constants/storage'

const memcached = new Map();
export const get = <T>(key: STORAGE | string): Promise<T> => {
  return new Promise((resolve, reject)=>{

    if(memcached.has(key)) {
      let data = memcached.get(key);
      resolve(data ? JSON.parse(data) : data)
      return 
    }

    AsyncStorage.getItem(key, (err, data)=>{
      if(err){
        reject(err)
      }else{
        resolve(data ? JSON.parse(data) : data)
      }
    })
  })
  // return AsyncStorage.getItem(key).then(data => {
  //   return data ? JSON.parse(data) : data
  // })
}

export const set = (key: STORAGE | string, value: any): Promise<void> => {
  let data = JSON.stringify(value);
  memcached.set(key, data)

  return AsyncStorage.setItem(key, data)
}

export const remove = (key: STORAGE | string): Promise<void> => {
  memcached.delete(key)
  return AsyncStorage.removeItem(key)
}

export const clear = (): Promise<void> => {
  return AsyncStorage.clear()
}

export default {
  get,
  set,
  remove,
  clear
}

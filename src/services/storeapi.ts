/**
 * Gravatar service
 * @file Gravatar
 * @module app/services/gravatar
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { blnnodeAPI, blnscanAPI, IS_IOS, androidID, appleID } from '@app/config'
import { IBlnBalance, IBlnTransaction, IBlnBlock, IBlnScanMessage, ICoinCapTicker, IBlnGroup } from '@app/types/bln'
import { IRequestParams, ITxResultPaginate } from '@app/types/http'
import BigNumber from 'bignumber.js';
import Blacknetjs from 'blacknetjs';
import fetch from './fetch'
import { isObject } from 'lodash';

// const blacknetjs = new Blacknetjs(new Blacknetjs.Config(blnnodeAPI))
const blacknetjs = new Blacknetjs()
blacknetjs.jsonrpc.endpoint = blnnodeAPI;
const devFundAddress = 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e'
const gorupPublicFee = 1000
const gorupPrivateFee = 10000
const groupApplyFee = 10
export default class {
    // adminRemoveGroup
    static adminRemoveGroup(mnemonic: string, from: string, to: string, isPrivate: boolean, txid: string, address: string) {
        let updateMsg
        let amount = new BigNumber(groupApplyFee).multipliedBy(1e8)
        const data = {
            address: address,
            txid: txid
        }
        try {
            if (isPrivate) {
                updateMsg = `removeGroup: ${JSON.stringify(data)}`
            } else {
                updateMsg = `removeGroup: ${JSON.stringify(data)}`
            }
        } catch (error) {
            return Promise.reject(error)
        }
        return blacknetjs.jsonrpc.transfer(mnemonic, {
            amount: amount.toNumber(),
            from: from,
            to: devFundAddress,
            message: updateMsg,
            encrypted: 0
        }).then((res: any) => {
            if (res.code === 200) {
                return res.body //群的txid
            } else {
                return Promise.reject(res.body)
            }
        })
    }
    // adminAgreeGroup 发送给申请人
    // @params txid 群组txid
    // @params to 申请人地址
    static adminAgreeGroup(mnemonic: string, from: string, to: string, isPrivate: boolean, txid: string, groupSK?: string) {
        let message
        let amount = new BigNumber(GroupOpFee.GroupAgree).multipliedBy(1e8)
        try {
            if (isPrivate) {
                message = `agreeGroup: ${JSON.stringify({
                    txid: txid,
                    privateKey: Blacknetjs.Encrypt(mnemonic, to, groupSK)
                })}`
            } else {
                message = `agreeGroup: ${JSON.stringify({
                    txid: txid
                })}`
            }
        } catch (error) {
            return Promise.reject(error)
        }
        return blacknetjs.jsonrpc.transfer(mnemonic, {
            amount: amount.toNumber(),
            from: from,
            to: to,
            message: message,
            encrypted: 0
        }).then((res: any) => {
            if (res.code === 200) {
                return res.body
            } else {
                return Promise.reject(res.body)
            }
        })
    }
    // adminRejectGroup
    static adminRejectGroup(mnemonic: string, from: string, to: string, isPrivate: boolean, txid: string) {
        let message
        let amount = new BigNumber(groupApplyFee).multipliedBy(1e8)
        try {
            if (isPrivate) {
                message = `rejectGroup: ${JSON.stringify({
                    txid: txid
                })}`
            } else {
                message = `rejectGroup: ${JSON.stringify({
                    txid: txid
                })}`
            }
        } catch (error) {
            return Promise.reject(error)
        }
        return blacknetjs.jsonrpc.transfer(mnemonic, {
            amount: amount.toNumber(),
            from: from,
            to: devFundAddress,
            message: message,
            encrypted: 0
        }).then((res: any) => {
            if (res.code === 200) {
                return res.body //群的txid
            } else {
                return Promise.reject(res.body)
            }
        })
    }

    static updateGroup(mnemonic: string, from: string, isPrivate: boolean, txid: string, data: {
        name?: string,
        description?: string
    }, groupSK?: string) {
        let message
        let amount = new BigNumber(GroupOpFee.GroupUpdate).multipliedBy(1e8)
        try {
            if (isPrivate && groupSK) {
                const groupPK = Blacknetjs.Address(groupSK)
                message = `updateGroup: ${JSON.stringify({ txid: txid, ...data, 
                    name: Blacknetjs.Encrypt(groupSK, groupPK, data.name),
                    description: Blacknetjs.Encrypt(groupSK, groupPK, data.description)
                })}`
            } else {
                message = `updateGroup: ${JSON.stringify({ txid: txid, ...data })}`
            }
        } catch (error) {
            return Promise.reject(error)
        }
        return blacknetjs.jsonrpc.transfer(mnemonic, {
            amount: amount.toNumber(),
            from: from,
            to: devFundAddress,
            message: message,
            encrypted: 0
        }).then((res: any) => {
            if (res.code === 200) {
                // txid 这里是否需要stashstore 和  transactionstore
                // return res.body
                return res.body //群的txid
            } else {
                return Promise.reject(res.body)
            }
        })
    }

    static quiteGroup(mnemonic: string, from: string, to: string, isPrivate: boolean, txid: string) {
        let message
        let amount = new BigNumber(groupApplyFee).multipliedBy(1e8)
        try {
            if (isPrivate) {
                message = `quiteGroup: ${JSON.stringify({
                    txid: txid
                })}`
            } else {
                message = `quiteGroup: ${JSON.stringify({
                    txid: txid
                })}`
            }
        } catch (error) {
            return Promise.reject(error)
        }
        return blacknetjs.jsonrpc.transfer(mnemonic, {
            amount: amount.toNumber(),
            from: from,
            to: devFundAddress,
            message: message,
            encrypted: 0
        }).then((res: any) => {
            if (res.code === 200) {
                // txid 这里是否需要stashstore 和  transactionstore
                // return res.body
                return res.body //群的txid
            } else {
                return Promise.reject(res.body)
            }
        })
    }

    static removeMember(mnemonic: string, from: string, to: string, isPrivate: boolean, txid: string) {
        let message
        let amount = new BigNumber(groupApplyFee).multipliedBy(1e8)
        try {
            if (isPrivate) {
                message = `removeMember: ${JSON.stringify({
                    txid: txid
                })}`
            } else {
                message = `removeMember: ${JSON.stringify({
                    txid: txid
                })}`
            }
        } catch (error) {
            return Promise.reject(error)
        }
        return blacknetjs.jsonrpc.transfer(mnemonic, {
            amount: amount.toNumber(),
            from: from,
            to: devFundAddress,
            message: message,
            encrypted: 0
        }).then((res: any) => {
            if (res.code === 200) {
                return res.body //群的txid
            } else {
                return Promise.reject(res.body)
            }
        })
    }

    // 发送消息给群组创建人
    // @params txid 群组txid
    // @params to 申请人地址
    static applyGroup(mnemonic: string, from: string, to: string, isPrivate: boolean, txid: string, text?: string) {
        let message
        let amount = new BigNumber(GroupOpFee.GroupApply).multipliedBy(1e8)
        try {
            if (isPrivate) {
                message = `applyGroup: ${JSON.stringify({
                    txid: txid,
                    text: Blacknetjs.Encrypt(mnemonic, to, text)
                })}`
            } else {
                message = `addPublicGroup: ${JSON.stringify({
                    txid: txid,
                    text: text
                })}`
            }
        } catch (error) {
            return Promise.reject(error)
        }
        return blacknetjs.jsonrpc.transfer(mnemonic, {
            amount: amount.toNumber(),
            from: from,
            to: to,
            message: message,
            encrypted: 0
        }).then((res: any) => {
            if (res.code === 200) {
                return res.body
            } else {
                return Promise.reject(res.body)
            }
        })
    }

    static createGroup(mnemonic: string, from: string, isPrivate: boolean, groupInfo: {
        name: string,
        description: string,
        logo: string
    }) {
        let amount
        let message
        try {
            if (isPrivate) {
                amount = new BigNumber(GroupOpFee.GroupCreate).multipliedBy(1e8)
                const groupSK = Blacknetjs.Mnemonic()
                const groupPK = Blacknetjs.Address(groupSK)
                message = `createGroup: ${JSON.stringify({
                    isPrivate: 1,
                    name: Blacknetjs.Encrypt(groupSK, groupPK, groupInfo.name),
                    description: Blacknetjs.Encrypt(groupSK, groupPK, groupInfo.description),
                    logo: Blacknetjs.Encrypt(groupSK, groupPK, groupInfo.logo),
                    privateKey: Blacknetjs.Encrypt(mnemonic, from, groupSK)
                })}`
            } else {
                amount = new BigNumber(GroupOpFee.GroupCreatePublic).multipliedBy(1e8)
                message = `createGroup: ${JSON.stringify({ ...groupInfo, isPrivate: 0 })}`
            }
        } catch (error) {
            return Promise.reject(error)
        }
        return blacknetjs.jsonrpc.transfer(mnemonic, {
            amount: amount.toNumber(),
            from: from,
            to: devFundAddress,
            message: message,
            encrypted: 0
        }).then((res: any) => {
            if (res.code === 200) {
                return res.body
            } else {
                return Promise.reject(res.body)
            }
        })
    }

    static sendEncryptMessage(mnemonic: string, from: string, type: string, to: string, msg: string, amount: number): Promise<string> {

        return new Promise((resolve, reject) => {
            const message = type + ': ' + Blacknetjs.Encrypt(mnemonic, to, msg)
            const amountText = new BigNumber(amount).multipliedBy(1e8)
            return blacknetjs.jsonrpc.transfer(mnemonic, {
                amount: amountText.toNumber(),
                from: from,
                to: to,
                message: message,
                encrypted: 0
            }).then((res: any) => {
                if (res.code === 200) {
                    resolve(res.body);
                } else {
                    reject(res.body)
                }
            }).catch((err: any) => {
                console.log(err)
            })
        })
    }
    static sendPortfolio(mnemonic: string, account: string, msg: string): Promise<string> {
        return this.sendEncryptMessage(mnemonic, account, 'portfolio', account, msg, 0.1);
    }

    // blacknet price
    static fetchTX(txid: string, params?: IRequestParams): Promise<IBlnTransaction> {
        return fetch.get(`${blnscanAPI}/api/tx/${txid?.toLocaleLowerCase()}`, params)
            .then((res) => {
                try {
                    return res.json()
                } catch (error) {

                    console.log(res)
                    if (res.text.toString().indexOf('Transaction rejected') == 0)
                        return error
                }
            })
            .then((json) => {
                if (json == "tx not found") {
                    return Promise.reject(json)
                }
                if (json.status === "error") {
                    return Promise.reject(json.msg)
                }
                if (!json.txid) {
                    return Promise.reject(json)
                }
                if (json.data && json.data.blockHeight) {
                    json.blockHeight = json.data.blockHeight
                    json.message = json.data.message
                }
                return json
            })
            .then((json) => {
                if (isObject(json.message)) {
                    json.message = json.message.message
                }
                return json
            })
            .catch((err) => {
                console.log(err);
            })
    }

    static getBlnPrice(): Promise<ICoinCapTicker> {
        return fetch.post(`https://api.aex.zone/v3/ticker.php`, {
            "mk_type": "cnc",
            "coinname": "bln"
        })
            .then(res => res.json())
            .then((json) => {
                if (json.code !== 20000) {
                    return Promise.reject(json)
                }
                return {
                    price: json.data.ticker.last,
                    volume_24h: json.data.ticker.vol,
                    percent_change_1h: json.data.ticker.range * 100,
                    percent_change_24h: json.data.ticker.range * 100,
                    percent_change_7d: json.data.ticker.range * 100,
                    market_cap: 0,
                    last_updated: new Date().toString()
                } as ICoinCapTicker
            })
    }
    static blnScanProfile(address: any): Promise<any> {
        return fetch.get(`${blnscanAPI}/api/user/${address}`)
            .then((res) => res.json())
            .then((json) => {
                if (!json.profile) {
                    return {}
                }
                return json.profile
            })
            .catch((err) => {
                console.log(err)
                return {};
            })
    }
    static getMoreBalance(address: string | undefined): Promise<IBlnBalance> {
        if (!address) {
            return Promise.reject(`address: ${address}`)
        }
        return fetch.get(`${blnscanAPI}/api/account/ledger/${address}`).then((res) => {
            if (res.ok) {
                return res.json()
            }
            return res.text().then(Promise.reject)
        }).catch((err) => {
            console.log(err);
        })
    }


    static getBalance(address: string | undefined): Promise<IBlnBalance> {
        if (!address) {
            return Promise.resolve({
                seq: 0,
                balance: "",
                confirmedBalance: "",
                stakingBalance: "",
                outLeasesBalance: "",
                inLeasesBalance: ""
            });
        }
        return fetch.get(`${blnnodeAPI}/api/v2/account/${address}`).then((res) => {
            if (res.ok) {
                return res.json()
            }
            return res.text().then(Promise.reject)
        }).catch((err) => {
            console.log(err);
        })
    }

    static blnAPIBlock(hash: string): Promise<IBlnBlock> {
        return fetch.get(`${blnnodeAPI}/api/v2/blockdb/get/${hash}/true`)
            .then((res) => {
                try {
                    return res.json()
                } catch (error) {
                    return Promise.reject(error)
                }
            })
            .then((json) => {
                if (json && json.length) {
                    return json[0]
                }
                return json
            })
            .then((json) => {
                if (json.transactions && json.transactions.length) {
                    json.transactions = json.transactions.reverse();
                    for (const key in json.transactions) {
                        if (Object.prototype.hasOwnProperty.call(json.transactions, key)) {
                            const tx = json.transactions[key];
                            tx.to = tx.data.to
                            tx.amount = tx.data.amount
                            tx.blockHeight = tx.data.blockHeight
                            if (isObject(tx.data.message)) {
                                tx.message = tx.data.message.message
                            } else {
                                tx.message = tx.data.message
                            }
                            json.transactions[key] = tx
                        }
                    }
                }
                return json
            })
            .catch((err) => {
                console.log(err);
            })
    }

    static blnScanChatMap(address: any): Promise<IBlnScanMessage[]> {
        return fetch.get(`${blnscanAPI}/api/chatmap/${address}`)
            .then((res) => res.json())
            .then((json) => {
                const messages: IBlnScanMessage[] = []
                if (Object.keys(json.groups).length) {
                    for (const key in json.groups) {
                        if (Object.prototype.hasOwnProperty.call(json.groups, key)) {
                            const contact = json.groups[key];
                            messages.push({
                                from: contact.sender,
                                to: key,
                                time: contact.time,
                                group: true,
                                text: contact.message
                            })
                        }
                    }
                }
                if (json.address && Object.keys(json.contacts).length) {
                    for (const key in json.contacts) {
                        if (Object.prototype.hasOwnProperty.call(json.contacts, key)) {
                            const contact = json.contacts[key];
                            messages.push({
                                from: json.address,
                                to: key,
                                time: contact.time,
                                text: contact.message
                            })
                        }
                    }
                }
                return messages
            })
            .catch((err) => {
                console.log(err)
                return [];
            })
    }

    static blnScanChatMapV2(address: any): Promise<IBlnScanMessage[]> {
        return fetch.get(`${blnscanAPI}/api/chatmap/v2/${address}`)
            .then((res) => res.json())
            .then((json) => {
                const messages: IBlnScanMessage[] = []
                if (Object.keys(json.groups).length) {
                    for (const key in json.groups) {
                        if (Object.prototype.hasOwnProperty.call(json.groups, key)) {
                            let contact = json.groups[key] || {};
                            messages.push({
                                from: contact.from,
                                to: key,
                                time: contact.time,
                                group: true,
                                // groupInfo: group.data,
                                // text: contact.data ? contact.data.text : contact.text
                                text: contact.text
                            })
                        }
                    }
                }
                if (json.address && Object.keys(json.contacts).length) {
                    for (const key in json.contacts) {
                        if (Object.prototype.hasOwnProperty.call(json.contacts, key)) {
                            const contact = json.contacts[key];
                            messages.push({
                                from: json.address,
                                to: key,
                                time: contact.time,
                                text: contact.message
                            })
                        }
                    }
                }
                return messages
            })
            .catch((err) => {
                console.log(err)
                return [];
            })
    }

    static blnScanChatGroupMemebers(txid: any): Promise<string[]> {
        return fetch.get(`${blnscanAPI}/api/public/group/member/${txid}`)
            .then((res) => res.json())
            .catch((err) => {
                console.log(err)
                return [];
            })
    }

    static fetchAppInfo(): Promise<any> {
        if (IS_IOS) {
            return fetch.get(`https://itunes.apple.com/lookup?id=${appleID}`)
                .then((res) => res.json())
                .then((json) => {
                    if (json.resultCount) {
                        const app = json.results[0]
                        return {
                            "Code": 0, //0代表请求成功，非0代表失败
                            "Msg": "", //请求出错的信息
                            "UpdateStatus": 1, //0代表不更新，1代表有版本更新，不需要强制升级，2代表有版本更新，需要强制升级
                            "VersionCode": 1,
                            "VersionName": app.version,
                            "ModifyContent": app.releaseNotes,
                            "DownloadUrl": app.trackViewUrl,
                            "ApkSize": new BigNumber(app.fileSizeBytes).dividedBy(1024).toNumber(),
                            "ApkMd5": ""
                        }
                    }
                    return undefined
                }).catch((e) => {

                });
        } else {
            return fetch.get(`https://blnscan.loqunbai.com/api/v4/projects/${androidID}/releases`)
                .then((res) => res.json())
                .then((json) => {
                    if (json.length) {
                        const app = json[0]
                        const link = app.assets.links[0]
                        const md5reg = link.name.toString().match(/\[md5:\s?([0-9a-f]{32})\]/i)
                        if (md5reg && md5reg[1]) {
                            app.ApkMd5 = md5reg[1]
                        }
                        const sizereg = link.name.toString().match(/\[size:\s?(\d+)\]/i)
                        if (sizereg && sizereg[1]) {
                            app.ApkSize = sizereg[1]
                        }
                        return {
                            "Code": 0, //0代表请求成功，非0代表失败
                            "Msg": "", //请求出错的信息
                            "UpdateStatus": 1,
                            "VersionCode": 1,
                            "VersionName": app.tag_name,
                            "ModifyContent": app.description,
                            "DownloadUrl": link.url.replace('gitlab.com', 'blnscan.loqunbai.com'),
                            "ApkSize": new BigNumber(app.ApkSize).dividedBy(1024).toNumber(),
                            "ApkMd5": app.ApkMd5 ?? ""
                        }
                    }
                    return undefined
                }).catch((e) => {

                });
        }
    }

    static fetchGroupOption(type: GroupOpType, params?: IRequestParams): Promise<ITxResultPaginate<IBlnScanMessage[]>>{
        return fetch.get(`${blnscanAPI}/api/message/type/${type}`, params)
        .then((res)=>res.json())
        .then((json)=>{
            let page = 1
            if(params && params.page){
                page = parseInt(params.page.toString())
            }
            return {
                txns: json,
                pagination: {
                    current_page: page,
                    per_page: page - 1,
                    txns_len: json.length
                },
                params: params
            }
        })
    }

    static fetchGroupLists(params?: IRequestParams): Promise<ITxResultPaginate<IBlnGroup[]>>{
        return fetch.get(`${blnscanAPI}/api/message/type/createGroup`, params)
        .then((res)=>res.json())
        .then((json)=>{
            let page = 1
            if(params && params.page){
                page = parseInt(params.page.toString())
            }
            return {
                txns: json,
                pagination: {
                    current_page: page,
                    per_page: page - 1,
                    txns_len: json.length
                },
                params: params
            }
        })
    }
}   


export enum GroupOpType {
    GroupCreate = 'createGroup',
    GroupApply = 'applyGroup',
    GroupUpdate = 'updateGroup',
    GroupRemove = 'removeGroup',
    GroupAddPublic = 'addPublicGroup',
    GroupAgree = 'agreeGroup'
}

export enum GroupOpFee {
    GroupCreate = 5000,
    GroupCreatePublic = 1000,
    GroupApply = 10,
    GroupUpdate = 100,
    GroupRemove = 10,
    GroupAddPublic = 10,
    GroupAgree = 5
}
/**
 * Touchid/Faceid service
 * @file Touchid
 * @module app/services/touchid
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { LANGUAGE_KEYS } from '@app/constants/language';
import TouchID from 'react-native-touch-id';
import i18n from './i18n';

export const isSupported = (): Promise<boolean> => {
    const optionalConfigObject = {
        unifiedErrors: false, // use unified error messages (default false)
        passcodeFallback: false // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
    }
    // 返回true/false 表示支持与不支持
    return TouchID.isSupported(optionalConfigObject)
    .then(biometryType => {
        if (biometryType === 'FaceID') {
            console.log('FaceID is supported.');
        } else {
            console.log('TouchID is supported.');
        }
        return true
    })
    .catch(error => {
        // Failure code
        console.log(error);
        return false
    });
}
export const authenticate = (): Promise<any> => {
    const optionalConfigObject = {
        title: 'Authentication Required', // Android
        imageColor: '#e00606', // Android
        imageErrorColor: '#ff0000', // Android
        sensorDescription: 'Touch sensor', // Android
        sensorErrorDescription: 'Failed', // Android
        cancelText: 'Cancel', // Android
        // fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
        unifiedErrors: false, // use unified error messages (default false)
        passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
    };
    return TouchID.authenticate('Touch/Face ID', optionalConfigObject)
    .then((res:any) => {
        return true
    })
    .catch((error:any) => {
        return Promise.reject(i18n.t(LANGUAGE_KEYS.TOUCHID_FACEID_CANCEL))
    });
}

export default {
    isSupported,
    authenticate
}
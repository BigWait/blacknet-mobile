/**
 * Gravatar service
 * @file Gravatar
 * @module app/services/gravatar
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */
import gravatar from 'gravatar'
import { EMAIL } from '@app/constants/regexp'

export const getUrlByEmail = (email: string|undefined): string => {
  if (!EMAIL.test(email||"")) {
    email = email+"@blacknet.ninja"
  }
  return gravatar
    .url(email, { protocol: 'https' })
}

export default gravatar

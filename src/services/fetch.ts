/**
 * Fetch service
 * @file Fetch
 * @module app/services/fetch
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */
import { stringify } from 'query-string'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { TRequestUrlPath, TRequestData, IRequestParams } from '@app/types/http'
import i18n from '@app/services/i18n'
import { showToast } from './toast'

// 构造参数
export const formatURL = (url: TRequestUrlPath, params?: IRequestParams): TRequestUrlPath => {
  let query = ''
  if (params && Object.keys(params).length) {
    query = url.includes('?')
      ? `&${stringify(params)}`
      : `?${stringify(params)}`
  }
  return `${url}${query}`
}

// 请求服务
export const httpService = <T>(url: TRequestUrlPath, options: RequestInit = {}): Promise<any> => {
  const defaultOptions = {
    includeCredentials: true,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }
  const start = new Date().getTime()
  return Promise.race([
    fetch(url, Object.assign(defaultOptions, options))
    .catch(() => {
        Promise.reject(new Error(i18n.t(LANGUAGE_KEYS.NETWORK_ERROR)))
    }),
    delay(30000)
  ])
  .then((res)=>{
    console.log(url, new Date().getTime()-start+"ms")
    return res
  })
  .catch((err: Error) => {
    showToast(`${err.message}`)
    console.log(`url：${url}`, err.message)
    return Promise.reject(err)
  })
}

export const get = <T>(url: TRequestUrlPath, getParams?: IRequestParams): Promise<Response> => {
  return httpService<T>(formatURL(url, getParams), { method: 'GET' })
}

export const post = <T>(url: TRequestUrlPath, data?: TRequestData): Promise<Response> => {
  return httpService<T>(url, { method: 'POST', body: JSON.stringify(data) })
}

export const put = <T>(url: TRequestUrlPath, data?: TRequestData): Promise<Response> => {
  return httpService<T>(url, { method: 'PUT', body: JSON.stringify(data) })
}

export const patch = <T>(url: TRequestUrlPath, data?: TRequestData): Promise<Response> => {
  return httpService<T>(url, { method: 'PATCH', body: JSON.stringify(data) })
}

export const remove = <T>(url: TRequestUrlPath, data?: TRequestData): Promise<Response> => {
  return httpService<T>(url, { method: 'DELETE', body: JSON.stringify(data) })
}

export default {
  get,
  post,
  put,
  patch,
  remove,
  upload: fetch
}

const delay = (timeOut = 30*1000) =>{
  return new Promise((resolve,reject) =>{
      setTimeout(() =>{
          reject(new Error(i18n.t(LANGUAGE_KEYS.NETWORK_TIMEOUT)))
      }, timeOut);
  })
}
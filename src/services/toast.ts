/**
 * Toast service
 * @file 吐司服务
 * @module app/services/toast
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import Toast, { ToastOptions } from 'react-native-root-toast'
import fonts from '@app/style/fonts'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import Clipboard from '@react-native-community/clipboard'

export const showToast = (message: string, options?: ToastOptions): void => {
  Toast.show(
    message || "UNKNOW_ERROR",
    Object.assign({
      delay: 0,
      duration: 1000,
      position: Toast.positions.CENTER,
      shadow: false,
      animation: true,
      hideOnPress: true,
      textStyle: {
        fontSize: fonts.base.fontSize
      }
    }, options)
  )
}

export const copyToast = (str: string): void => {
    Clipboard.setString(str)
    showToast(i18n.t(LANGUAGE_KEYS.COPIED_ADDRESS))
}

export default showToast

/**
 * forum store
 * @file forum store
 * @module app/pages/forum/stores/forum
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { STORAGE } from '@app/constants/storage'
import { IBlnScanForumPost } from '@app/types/bln'
import API from '@app/services/api'

export class ForumStore {

    constructor() {
        this.resetStore();
    }

    @observable lists: IBlnScanForumPost[] = []

    @action.bound
    updatePostList(lists: Array<IBlnScanForumPost>) {
        this.lists = lists
        storage.set(STORAGE.LOCAL_FORUM_POST_LIST, lists)
    }
    @action.bound
    getPostList(): Promise<Array<IBlnScanForumPost>> {
        return storage.get<Array<IBlnScanForumPost>>(STORAGE.LOCAL_FORUM_POST_LIST)
            .then(lists => {
                if (!lists) {
                    return []
                }
                return lists
            })
    }
    @boundMethod
    resetStore() {
        this.initPostList()
    }
    @boundMethod
    private initPostList() {
        this.getPostList().then(lists => {
            if (lists) {
                console.log('Init forum last post:', lists.length)
                this.updatePostList(lists)
            }
        })
    }

    @boundMethod
    refresh(page: number = 1): Promise<any> {
        return API.fetchPosts({ page })
            .then((lists) => {
                if (page === 1) {
                    this.updatePostList(lists.txns)
                }
                return lists
            })
    }
}

export default new ForumStore()
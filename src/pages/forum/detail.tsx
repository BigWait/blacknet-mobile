/**
 * Post Detail
 * @file Post Detail
 * @module pages/forum/detail
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import fonts from '@app/style/fonts'
import mixins from '@app/style/mixins'
import API from '@app/services/api'
import { IBlnScanForumPost } from '@app/types/bln'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { NoResult } from '@app/components/ui/NoResult'
import { TouchableView } from '@app/components/common/touchable-view'
import { ChatRoutes, DiscoverRoutes, HomeRoutes } from '@app/routes'
import { IRequestParams, ITxPaginate, ITxResultPaginate } from '@app/types/http'
import { PostListDetail, PostListReply } from '@app/components/ui/forum'
import { TextInput } from 'react-native-gesture-handler'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Loading from '@app/components/common/loading'
import showToast from '@app/services/toast'
import BottomBar from '@app/components/ui/BottomBar'
import { MyFlatList, MyFlatListFooter_LOADING, MyFlatListFooter_LOADMORE, MyFlatListFooter_NO_MORE } from '@app/components/common/FlatList'
import { userStore } from '@app/stores/users'

type ITxResultPaginateLists = ITxResultPaginate<IBlnScanForumPost[]>
type TransactionListElement = RefObject<FlatList<IBlnScanForumPost>>

export interface IIndexProps extends IPageProps { }

@observer export class PostDetailScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
        // init data
        this.data = this.props.route.params.data
        this.txid = this.props.route.params.id
        formStore.changeReplyTheme(this.props.route.params.id)
        // fetch list
        this.fetchLists()
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.DETAIL)
        }
    }

    private listElement: TransactionListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }
    @observable private isLoading: boolean = false
    @observable.shallow private lists: IBlnScanForumPost[] = []
    @observable.ref private params: IRequestParams = {}
    @observable.ref private pagination: ITxPaginate | null = null
    @observable.ref private data: IBlnScanForumPost
    @observable.ref private txid: string

    @computed
    private get ListData(): IBlnScanForumPost[] {
        return this.lists.slice() || []
    }

    @computed
    private get isNoMoreData(): boolean {
        return (
            !!this.pagination &&
            this.pagination.txns_len < 100
        )
    }

    @action
    private updateLoadingState(loading: boolean) {
        this.isLoading = loading
    }

    @action
    private updateResultData(result: ITxResultPaginateLists) {
        const { txns, pagination } = result
        this.pagination = pagination
        if (pagination.current_page > 1) {
            this.lists.push(...txns)
        } else {
            this.lists = [...txns]
        }
    }

    @action
    private addResultData(post: IBlnScanForumPost) {
        this.lists.push(post)
    }

    @boundMethod
    private handleLoadmoreList() {
        if (!this.isNoMoreData && !this.isLoading && this.pagination) {
            this.fetchLists(this.pagination.current_page + 1)
        }
    }

    @boundMethod
    private fetchLists(page: number = 1): Promise<any> {
        this.updateLoadingState(true)
        return API.fetchPostDetail(this.props.route.params.id, { ...this.params, page })
            .then((json) => {
                this.updateResultData(json)
                return json
            })
            .finally(() => this.updateLoadingState(false))
    }

    private getIdKey(tx: IBlnScanForumPost, index?: number): string {
        return `index:${index}:sep:${tx.txid}`
    }

    private getItemLayout(_: any, index: number) {
        const height = 40
        return {
            index,
            length: height,
            offset: height * index
        }
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
            <Observer
                render={() => (
                    <NoResult />
                )}
            />
        )
    }

    // 渲染脚部的三种状态：空、加载中、无更多、上拉加载
    @boundMethod
    private renderListFooterView(): JSX.Element | null {
        if (!this.lists.length) {
            return null
        }
        if (this.lists.length < 100 || this.isNoMoreData) {
            return (
                <MyFlatListFooter_NO_MORE />
            )
        }
        if (this.isLoading) {
            return (
                <MyFlatListFooter_LOADING />
            )
        }
        return (
            <MyFlatListFooter_LOADMORE />
        )
    }

    @boundMethod
    private renderListHeaderView() {
        const { styles } = obStyles
        return (<View style={styles.seq}>
            <PostListDetail
                data={this.data}
                onReplyPress={() => {
                    formStore.changeReplyTheme(this.txid)
                }}
                onNamePress={() => {
                    this.onPressNavigation(HomeRoutes.ExplorerDetail, {
                        type: "account",
                        id: this.data.from
                    })
                }}
            />
        </View>)
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.listWarp}>
                <MyFlatList
                    style={obStyles.styles.listView}
                    data={this.ListData}
                    ref={this.listElement}
                    // reander
                    ListHeaderComponent={this.renderListHeaderView}
                    // 手动维护每一行的高度以优化性能
                    getItemLayout={this.getItemLayout}
                    // 列表为空时渲染
                    ListEmptyComponent={this.renderListEmptyView}
                    // 加载更多时渲染
                    ListFooterComponent={this.renderListFooterView}
                    // 当前列表 loading 状态
                    refreshing={this.isLoading}
                    // 刷新
                    onRefresh={this.fetchLists}
                    // 加载更多
                    onEndReached={this.handleLoadmoreList}
                    // 唯一 ID
                    keyExtractor={this.getIdKey}
                    // 单个主体
                    renderItem={({ item: post, index }) => {
                        return (
                            <Observer
                                render={() => (
                                    <PostListReply
                                        navigation={this.props.navigation}
                                        style={{ paddingBottom: 0 }}
                                        floor={index + 2}
                                        key={index}
                                        data={post}
                                        onReplyPress={() => {
                                            formStore.changeReplyFloor(post.txid, post.from, index + 2)
                                        }}
                                        onNamePress={() => {
                                            this.onPressNavigation(HomeRoutes.ExplorerDetail, {
                                                type: "account",
                                                id: post.from
                                            })
                                        }}
                                        onCommentReplyPress={(post: IBlnScanForumPost) => {
                                            formStore.changeReply(post.txid, post.data.quote || "", post.from, index + 2)
                                        }}
                                    />
                                )}
                            />
                        )
                    }}
                />
                {this.renderBottom()}
            </View>
        )
    }

    @boundMethod
    renderBottom() {
        const { styles } = obStyles
        return <BottomBar>
            <View style={styles.bootomBar}>
                <TextInput
                    style={styles.bootomBarInput}
                    onChangeText={text => formStore.changeContent(text)}
                    value={formStore.content}
                    onSubmitEditing={this.onSubmit}
                    underlineColorAndroid="transparent"
                    returnKeyType={"send"}
                    placeholder={formStore.placeholder}
                />
                <TouchableView
                    style={styles.bootomBarButton}
                    onPress={this.onSubmit}
                >
                    <FontAwesome name="send" size={16} color={colors.primary}></FontAwesome>
                </TouchableView>
            </View>
        </BottomBar>
    }

    @boundMethod
    private onSubmit() {
        if (!formStore.verify) {
            return
        }
        const ld = Loading.show()
        API.addReplyPost(
            accountStore.currentMnemonic,
            accountStore.currentAddress,
            accountStore.currentAddress,
            formStore.data
        ).then((res: any) => {
            showToast(i18n.t(LANGUAGE_KEYS.FORUM_ADD_SUCCESS));
            // goto detail or goto list
            this.addResultData(res)
        })
            .catch((err) => {
                showToast(`${err}`)
                console.log("Add Post failed.", err);
            })
            .finally(() => {
                formStore.resetContent()
                Loading.hide(ld)
                accountStore.refreshBalance()
            })
    }

    @boundMethod
    private onPressNavigation(routeName: ChatRoutes | DiscoverRoutes | HomeRoutes, params?: any) {
        this.props.navigation.push(routeName, params)
    }
}

class FormStore {
    @observable content: string | undefined
    @observable replyid: string | undefined
    @observable quotereplyid: string | undefined
    @observable quote: string | undefined
    @observable quoteName: string | undefined
    @observable quoteFloor: number | undefined
    @observable replyType: "theme" | "floor" | "reply" = "theme"

    @action.bound
    changeReplyTheme(replyid: string) {
        this.replyType = "theme"
        this.replyid = replyid
    }

    @action.bound
    changeReplyFloor(quote: string, quoteName?: string, quoteFloor?: number) {
        this.replyType = "floor"
        this.quote = quote
        this.quoteFloor = quoteFloor
        this.quoteName = userStore.getUserName(quoteName)
    }

    @action.bound
    changeReply(quotereplyid: string, quote: string, quoteName?: string, quoteFloor?: number) {
        this.replyType = "reply"
        this.quote = quote
        this.quoteFloor = quoteFloor
        this.quotereplyid = quotereplyid
        this.quoteName = userStore.getUserName(quoteName)
    }

    @action.bound
    resetContent() {
        this.content = undefined
    }

    @action.bound
    changeContent(content: string) {
        this.content = content
    }

    @computed
    get placeholder(): string {
        switch (this.replyType) {
            case "theme":
                return i18n.t(LANGUAGE_KEYS.FORUM_REPLY)
            case "floor":
                return `${i18n.t(LANGUAGE_KEYS.FORUM_REPLY)}: #${this.quoteFloor} ${this.quoteName}`
            case "reply":
                return `${i18n.t(LANGUAGE_KEYS.FORUM_REPLY)}: #${this.quoteFloor} ${this.quoteName}`
        }
    }

    @computed
    get verify(): boolean {
        switch (this.replyType) {
            case "theme":
                return this.content != "" && this.content != undefined && this.replyid != undefined
            case "floor":
                return this.content != "" && this.content != undefined && this.replyid != undefined && this.quote != undefined
            case "reply":
                return this.content != "" && this.content != undefined && this.replyid != undefined && this.quote != undefined && this.quotereplyid != undefined
        }
    }

    @computed
    get data(): any {
        switch (this.replyType) {
            case "theme":
                return {
                    content: this.content,
                    replyid: this.replyid
                }
            case "floor":
                return {
                    content: this.content,
                    replyid: this.replyid,
                    quote: this.quote
                }
            case "reply":
                return {
                    content: this.content,
                    replyid: this.replyid,
                    quote: this.quote,
                    quotereplyid: this.quotereplyid
                }
        }
    }
}
export const formStore = new FormStore()

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            bootomBarInput: {
                backgroundColor: colors.textInputBG,
                padding: 8,
                flex: 1
            },
            bootomBarButton: {
                paddingRight: 8,
                marginLeft: 10
            },
            bootomBar: {
                height: 50,
                paddingLeft: 10,
                paddingRight: 10,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: 'space-between',
                paddingTop: 10
            },
            seq: {
                marginBottom: 10
            },
            centerContainer: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: sizes.gap
            },
            loadmoreViewContainer: {
                ...mixins.rowCenter,
                padding: sizes.goldenRatioGap
            },
            normalTitle: {
                ...fonts.base,
                color: colors.textSecondary
            },
            smallTitle: {
                ...fonts.small,
                color: colors.textSecondary
            },
            listView: {
                width: sizes.screen.width
            },
            listWarp: {
                flex: 1,
                backgroundColor: colors.background
            },
            container: {
                flex: 1,
                backgroundColor: colors.background
            },
            headerCheckedIcon: {
                position: 'absolute',
                right: sizes.gap - 4,
                bottom: -1
            }
        })
    }
})

/**
 * addPost
 * @file addPost
 * @module pages/forum/addPost
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, ScrollView } from 'react-native'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import { accountStore } from '@app/stores/account'
import { LabelListTitle } from '@app/components/ui/list-title'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import API from '@app/services/api'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'
import { SafeAreaView } from 'react-native-safe-area-context'
import TextInput from '@app/components/common/text-input'
import { IPageProps } from '@app/types/props'

class FormStore {
    @observable title: string = ""
    @observable content: string = ""

    @action.bound
    changeTitle(title: string) {
        this.title = title
    }
    @action.bound
    changeContent(content: string) {
        this.content = content
    }
    @computed
    get verify(): boolean{
        return this.content != undefined && this.content != ''
    }
    @computed
    get data(): any{
        return {
            title: this.title,
            content: this.content
        }
    }
    @action.bound
    reset(){
        this.title = ""
        this.content = ""
    }
}
export const formStore = new FormStore()
export interface IIndexProps extends IPageProps { }
@observer export class AddPostScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          title: i18n.t(LANGUAGE_KEYS.FORUM_ADD_POST)
        }
    }

    componentDidMount(){
        formStore.reset()
    }

    @boundMethod
    private onPressSubmit(){
        if(!formStore.verify){
            return
        }
        const ld = Loading.show()
            API.addPost(
                accountStore.currentMnemonic, 
                accountStore.currentAddress, 
                accountStore.currentAddress, 
                formStore.data
            ).then((res:any)=>{
                showToast(i18n.t(LANGUAGE_KEYS.FORUM_ADD_SUCCESS));
                // goto detail or goto list
                setTimeout(()=>{
                    this.props.navigation.goBack()
                }, 500)
            })
            .catch((err)=>{
                showToast(`${err}`)
                console.log("Add Post failed.", err);
            })
            .finally(()=>{
                Loading.hide(ld)
                accountStore.refreshBalance()
            })
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.container}>
                <ScrollView style={styles.content}>
                   {/*<LabelListTitle
                        leading={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.FORUM_TITLE)}</Text>}
                    />
                    <TextInput
                        style={styles.inputTitle}
                        multiline={true}
                        onChangeText={text => formStore.changeTitle(text)}
                        value={formStore.title}
                        placeholder={LANGUAGE_KEYS.OPTION}
                    />
                   */} 
                    <LabelListTitle
                        leading={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.FORUM_CONTENT)}</Text>}
                    />
                    <TextInput
                        style={styles.inputContent}
                        multiline={true}
                        onChangeText={text => formStore.changeContent(text)}
                        value={formStore.content}
                        numberOfLines={10}
                    />
                </ScrollView>
                <SafeAreaView>
                    <View style={styles.bootomBar}>
                        <BottomButton text={i18n.t(LANGUAGE_KEYS.FORUM_PUBLISH)} onPress={this.onPressSubmit}/>
                    </View>
                </SafeAreaView>
            </View>
        )
    }
}

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        bootomBar:{
            height: 40,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 10
        },
        inputTitle: {
            padding: 4,
            color: colors.textDefault
        },
        inputContent: {
            height: 200,
            padding: 4,
            textAlignVertical: 'top',
            color: colors.textDefault
        },
        container: {
            flex: 1
        },
        content: {
            flex: 1,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 20
        }
      })
    }
})
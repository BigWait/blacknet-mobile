/**
 * Forum
 * @file Forum
 * @module pages/forum/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import fonts from '@app/style/fonts'
import mixins from '@app/style/mixins'
import API from '@app/services/api'
import { IBlnScanForumPost } from '@app/types/bln'
import { NoResult } from '@app/components/ui/NoResult'
import { ChatRoutes, DiscoverRoutes, HomeRoutes } from '@app/routes'
import { IRequestParams, ITxPaginate, ITxResultPaginate } from '@app/types/http'
import { PostList } from '@app/components/ui/forum'
import { AddButton } from '@app/components/ui/button'
import forumStore from '@app/pages/forum/stores/forum'
import usersService from '@app/services/users'
import { MyFlatList, MyFlatListFooter_LOADING, MyFlatListFooter_LOADMORE, MyFlatListFooter_NO_MORE } from '@app/components/common/FlatList'
import { isFunction } from 'lodash'

type ITxResultPaginateLists = ITxResultPaginate<IBlnScanForumPost[]>
type TransactionListElement = RefObject<FlatList<IBlnScanForumPost>>

export interface IIndexProps extends IPageProps { }

@observer export class ForumScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
        // users
        usersService.refresh()
        // init list
        this.initList()
    }

    @boundMethod
    private initList(): Promise<any> {
        return forumStore.getPostList().then((json) => {
            if (json.length > 0) {
                action(() => {
                    this.lists = json
                })()
            }
            return json
        })
    }

    private listElement: TransactionListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }
    @observable private isLoading: boolean = false
    @observable.shallow private lists: IBlnScanForumPost[] = []
    @observable.ref private params: IRequestParams = {}
    @observable.ref private pagination: ITxPaginate | null = null

    @computed
    private get ListData(): IBlnScanForumPost[] {
        return this.lists.slice() || []
    }

    @computed
    private get isNoMoreData(): boolean {
        return (
            !!this.pagination &&
            this.pagination.txns_len < 100
        )
    }

    private showLoading: boolean = false
    private unsubscribe: any;

    componentDidMount() {
        this.showLoading = true
        // Listen for screen focus event
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
    }

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }

    onScreenFocus = () => {
        this.showLoading = false
        this.fetchLists().finally(() => {
            this.showLoading = true
        })
    }

    @action
    private updateLoadingState(loading: boolean) {
        if (!this.showLoading) {
            return
        }
        this.isLoading = loading
    }

    @action
    private updateResultData(result: ITxResultPaginateLists) {
        const { txns, pagination } = result
        this.pagination = pagination
        if (pagination.current_page > 1) {
            this.lists.push(...txns)
        } else {
            this.lists = [...txns]
        }
    }

    @boundMethod
    private handleLoadmoreList() {
        if (!this.isNoMoreData && !this.isLoading && this.pagination) {
            this.fetchLists(this.pagination.current_page + 1)
        }
    }

    @boundMethod
    private fetchLists(page: number = 1): Promise<any> {
        this.updateLoadingState(true)
        let res: Promise<any>
        if (page === 1) {
            res = forumStore.refresh()
        } else {
            res = API.fetchPosts({ ...this.params, page })
        }
        return res.then((json) => this.updateResultData(json))
            .finally(() => this.updateLoadingState(false))
    }

    private getIdKey(tx: IBlnScanForumPost, index?: number): string {
        return `index:${index}:sep:${tx.txid}`
    }

    private getItemLayout(_: any, index: number) {
        const height = 40
        return {
            index,
            length: height,
            offset: height * index
        }
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
            <Observer
                render={() => (
                    <NoResult />
                )}
            />
        )
    }

    // 渲染脚部的三种状态：空、加载中、无更多、上拉加载
    @boundMethod
    private renderListFooterView(): JSX.Element | null {
        const { styles } = obStyles
        if (!this.lists.length) {
            return null
        }
        if (this.lists.length < 100 || this.isNoMoreData) {
            return (
                <MyFlatListFooter_NO_MORE />
            )
        }
        if (this.isLoading) {
            return (
                <MyFlatListFooter_LOADING />
            )
        }
        return (
            <MyFlatListFooter_LOADMORE />
        )
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.listWarp}>
                <MyFlatList
                    style={obStyles.styles.listView}
                    data={this.ListData}
                    ref={this.listElement}
                    // 列表为空时渲染
                    ListEmptyComponent={this.renderListEmptyView}
                    // 加载更多时渲染
                    ListFooterComponent={this.renderListFooterView}
                    // 当前列表 loading 状态
                    refreshing={this.isLoading}
                    // 刷新
                    onRefresh={this.fetchLists}
                    // 加载更多
                    onEndReached={this.handleLoadmoreList}
                    // 唯一 ID
                    keyExtractor={this.getIdKey}
                    // 单个主体
                    renderItem={({ item: post, index }) => {
                        return (
                            <Observer
                                render={() => (
                                    <View style={styles.seq}>
                                        <PostList
                                            style={{ borderRadius: 3 }}
                                            key={index}
                                            data={post}
                                            onPress={() => {
                                                this.onPressNavigation(DiscoverRoutes.Detail, {
                                                    id: post.txid,
                                                    data: post
                                                })
                                            }}
                                            onNamePress={() => {
                                                this.onPressNavigation(HomeRoutes.ExplorerDetail, {
                                                    type: "account",
                                                    id: post.from
                                                })
                                            }}
                                        />
                                    </View>
                                )}
                            />
                        )
                    }}
                />
                {/* add button */}
                <AddButton style={styles.addButton} size={30}
                    onPress={() => {
                        this.onPressNavigation(DiscoverRoutes.AddPost)
                    }}
                />
            </View>
        )
    }

    @boundMethod
    private onPressNavigation(routeName: ChatRoutes | DiscoverRoutes | HomeRoutes, params?: any) {
        this.props.navigation.navigate(routeName, params)
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            addButton: {
                position: "absolute",
                width: 40,
                height: 40,
                backgroundColor: colors.primary,
                borderRadius: 20,
                shadowColor: colors.secondary,
                shadowOpacity: 0.3,
                shadowOffset: { width: 0, height: 0 },
                bottom: 50,
                right: 30
            },
            seq: {
                marginBottom: 5,
                marginLeft: 5,
                marginRight: 5
            },
            centerContainer: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: sizes.gap
            },
            loadmoreViewContainer: {
                ...mixins.rowCenter,
                padding: sizes.goldenRatioGap
            },
            normalTitle: {
                ...fonts.base,
                color: colors.textSecondary
            },
            smallTitle: {
                ...fonts.small,
                color: colors.textSecondary
            },
            listView: {
                width: sizes.screen.width
            },
            listWarp: {
                flex: 1,
                paddingTop: 5,
                backgroundColor: colors.cardBackground
            },
            container: {
                flex: 1
            },
            headerCheckedIcon: {
                position: 'absolute',
                right: sizes.gap - 4,
                bottom: -1
            }
        })
    }
})

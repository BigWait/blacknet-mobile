import React, { Component, RefObject } from 'react'
import { StyleSheet, View, TextInput, ScrollView, Text } from 'react-native'
import { action, computed, observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { CopyButton, DeleteButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'
import { LabelListTitle } from '@app/components/ui/list-title'
import { HeaderTitle } from '@react-navigation/stack'
import { IBlnGroup, IBlnGroupData } from '@app/types/bln'
import { IIndexProps } from '@app/pages/home'
import storeapi from '@app/services/storeapi'

@observer export class ChatGroupApplyScreen extends Component<IIndexProps> {

    state = {
        message: ''
    }

    componentDidMount(){
        this.group = this.props.route.params.group
        this.renderTitle()
    }

    @boundMethod
    private renderTitle() {
        this.props.navigation.setOptions({
            headerTitle: <HeaderTitle>{`${i18n.t(LANGUAGE_KEYS.CHAT_APPLY)}`}</HeaderTitle>
        })
    }

    private group: IBlnGroup = {} as IBlnGroup

    @boundMethod
    private onPressSubmit(){
        const ld = Loading.show()
        const groupOwner = this.group.from
        const isPrivate = !!this.group.data.isPrivate
        const groupID = this.group.txid
        storeapi.applyGroup(accountStore.currentMnemonic, accountStore.currentAddress, groupOwner, isPrivate, groupID, this.state.message)
        .then((txid: string)=>{
            showToast(`${i18n.t(LANGUAGE_KEYS.CHAT_APPLY_SUCCESS)}`)
            this.setState({
                message: ''
            }, ()=>{
                setTimeout(()=>{
                    this.props.navigation.goBack()
                }, 1000)
            })
        })
        .catch((err: any)=>{
            showToast(`${err}`)
        })
        .finally(()=>{
            Loading.hide(ld)
        })
    }

    render() {
        const { styles } = chatGroupApplyStyles
        return (
            <ScrollView style={styles.container}>
                <LabelListTitle
                    leading={<Text style={{color:colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.CHAT_APPLY_MESSAGE)}</Text>}
                />
                <TextInput
                    style={styles.textarea}
                    multiline={true}
                    onChangeText={text => {
                        this.setState({
                            message: text
                        })
                    }}
                    value={this.state.message}
                    numberOfLines={5}
                />
                <CopyButton text={i18n.t(LANGUAGE_KEYS.CHAT_JOIN)} onPress={()=>{
                    this.onPressSubmit()
                }}/>
            </ScrollView>
        )
    }
}

const chatGroupApplyStyles = observable({
    get styles() {
      return StyleSheet.create({
        textarea: {
            height: 150,
            borderColor: colors.border, 
            borderWidth: 1,
            padding: 10,
            color: colors.textDefault,
            marginBottom: 20
        },
        container: {
            flex: 1,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 40
        }
      })
    }
})
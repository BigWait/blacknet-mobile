import React, { Component, RefObject } from 'react'
import { StyleSheet, View, TextInput, ScrollView, Text } from 'react-native'
import { action, computed, observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { CopyButton, DeleteButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'
import { LabelListTitle } from '@app/components/ui/list-title'
import { HeaderTitle } from '@react-navigation/stack'
import { IIndexProps } from '@app/pages/home'
import storeapi from '@app/services/storeapi'
import group from '@app/utils/group'
import { chatGroupStore } from '../stores/group'

@observer export class ChatGroupUpdateScreen extends Component<IIndexProps> {

    state = {
        name: "",
        description: ""
    }

    componentDidMount(){
        this.renderTitle()
        this.setState({
            name: chatGroupStore.getGroupName(this.props.route.params.groupInfo, accountStore.currentMnemonic),
            description: chatGroupStore.getGroupDesc(this.props.route.params.groupInfo, accountStore.currentMnemonic),
        })
    }

    @boundMethod
    private renderTitle() {
        this.props.navigation.setOptions({
            headerTitle: <HeaderTitle>{`${i18n.t(LANGUAGE_KEYS.MODIFY)}${i18n.t(LANGUAGE_KEYS.CHAT_INFO)}`}</HeaderTitle>
        })
    }

    @boundMethod
    private onPressSubmit(){
        const ld = Loading.show()
        const txid = this.props.route.params.address
        const isPrivate = !!this.props.route.params.isPrivate
        let groupSK = ''
        if(isPrivate){
            groupSK = group.decrypt(accountStore.currentMnemonic, accountStore.currentAddress, this.props.route.params.groupInfo.privateKey)
        }
        storeapi.updateGroup(accountStore.currentMnemonic, accountStore.currentAddress, isPrivate, txid, {
            name: this.state.name,
            description: this.state.description
        }, groupSK)
        .then((txid: string)=>{
            showToast(`${i18n.t(LANGUAGE_KEYS.SUCCESS)}`)
            this.setState({
                name: '',
                description: ''
            }, ()=>{
                setTimeout(()=>{
                    this.props.navigation.goBack()
                }, 1000)
            })
        })
        .catch((err: any)=>{
            showToast(`${err}`)
        })
        .finally(()=>{
            Loading.hide(ld)
        })
    }

    render() {
        const { styles } = chatGroupApplyStyles
        return (
            <ScrollView style={styles.container}>
                {this.props.route.params.type === 'name' ? 
                    <> 
                        <LabelListTitle
                            leading={<Text style={{color:colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_NAME)}</Text>}
                        />
                        <TextInput
                            style={styles.textarea}
                            multiline={true}
                            onChangeText={text => {
                                this.setState({
                                    name: text
                                })
                            }}
                            value={this.state.name}
                            numberOfLines={5}
                        />
                    </>
                : null}
                {this.props.route.params.type === 'description' ? 
                    <> 
                        <LabelListTitle
                            leading={<Text style={{color:colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_DESC)}</Text>}
                        />
                        <TextInput
                            style={styles.textarea}
                            multiline={true}
                            onChangeText={text => {
                                this.setState({
                                    description: text
                                })
                            }}
                            value={this.state.description}
                            numberOfLines={5}
                        />
                    </>
                : null}
                <CopyButton text={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={()=>{
                    this.onPressSubmit()
                }}/>
            </ScrollView>
        )
    }
}

const chatGroupApplyStyles = observable({
    get styles() {
      return StyleSheet.create({
        textarea: {
            height: 150,
            borderColor: colors.border, 
            borderWidth: 1,
            padding: 10,
            color: colors.textDefault,
            marginBottom: 20
        },
        container: {
            flex: 1,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 40
        }
      })
    }
})
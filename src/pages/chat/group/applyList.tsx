import React, { Component, RefObject } from 'react'
import { StyleSheet, View, TextInput, ScrollView, Text } from 'react-native'
import { action, computed, observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { CopyButton, DeleteButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import CheckBox from 'react-native-check-box'
import API from '@app/services/api'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'
import { LabelListTitle } from '@app/components/ui/list-title'
import { HeaderTitle } from '@react-navigation/stack'
import Video from '@app/components/common/video'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import ListTitle from '@app/components/common/list-title'
import Image from '@app/components/common/image'
import { FlatList } from 'react-native-gesture-handler'
import { defaultAvatar } from '@app/utils/bln'
import sizes from '@app/style/sizes'
import SearchInput from '@app/components/common/searchInput'
import { ChatRoutes } from '@app/routes'
import { MyFlatListFooter_LOADING, MyFlatListFooter_LOADMORE, MyFlatListFooter_NO_MORE } from '@app/components/common/FlatList'
import { NoResult } from '@app/components/ui/NoResult'
import { IBlnGroup, IBlnGroupData, IBlnScanMessage } from '@app/types/bln'
import { IRequestParams, ITxPaginate, ITxResultPaginate } from '@app/types/http'
import { isFunction } from 'lodash'
import { TouchableView } from '@app/components/common/touchable-view'
import { IIndexProps } from '@app/pages/home'
import { chatGroupStore } from '../stores/group'
import storeapi, { GroupOpType } from '@app/services/storeapi'
import { userStore } from '@app/stores/users'
import group from '@app/utils/group'

type ITxResultPaginateLists = ITxResultPaginate<IBlnScanMessage[]>
type GroupListsElement = RefObject<FlatList<IBlnScanMessage>>
// group apply ists
@observer export class ChatGroupApplyListsScreen extends Component<IIndexProps> {

    private showLoading: boolean = false
    private unsubscribe: any;

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }

    componentDidMount() {
        this.renderTitle()
        this.showLoading = true
        // Listen for screen focus event
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
    }

    onScreenFocus = () => {
        this.showLoading = false
        this.fetchLists().finally(() => {
            this.showLoading = true
        })
    }

    @boundMethod
    private renderTitle() {
        this.props.navigation.setOptions({
            headerTitle: <Observer render={()=>
                <HeaderTitle>{`${i18n.t(LANGUAGE_KEYS.CHAT_APPLY_LIST)}`}</HeaderTitle>
            }/>
        })
    }

    private listElement: GroupListsElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }
    @observable private isLoading: boolean = false
    @observable.shallow private lists: IBlnScanMessage[] = []
    @observable.ref private params: IRequestParams = {}
    @observable.ref private pagination: ITxPaginate | null = null

    @computed
    private get count(){
        return this.ListData.length
    }

    @computed
    private get ListData(): IBlnScanMessage[] {
        return this.lists.slice() || []
    }

    @computed
    private get isNoMoreData(): boolean {
        return (
            !!this.pagination &&
            this.pagination.txns_len < 100
        )
    }

    @action
    private updateLoadingState(loading: boolean) {
        if (!this.showLoading) {
            return
        }
        this.isLoading = loading
    }

    @action
    private updateResultData(result: ITxResultPaginateLists) {
        const { txns, pagination } = result
        this.pagination = pagination
        if (pagination.current_page > 1) {
            this.lists.push(...txns)
        } else {
            this.lists = [...txns]
        }
    }

    @boundMethod
    private handleLoadmoreList() {
        if (!this.isNoMoreData && !this.isLoading && this.pagination) {
            this.fetchLists(this.pagination.current_page + 1)
        }
    }

    @boundMethod
    private fetchLists(page: number = 1): Promise<any> {
        this.updateLoadingState(true)
        return storeapi.fetchGroupOption(this.props.route.params.groupInfo.isPrivate ? GroupOpType.GroupApply : GroupOpType.GroupAddPublic, {txid: this.props.route.params.address, page: page})
        .then((json) => this.updateResultData(json))
        .finally(() => this.updateLoadingState(false))
    }

    private getIdKey(tx: IBlnScanMessage, index?: number): string {
        return `index:${index}:sep:${tx.txid}`
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
            <Observer
                render={() => (
                    <NoResult />
                )}
            />
        )
    }

    // 渲染脚部的三种状态：空、加载中、无更多、上拉加载
    @boundMethod
    private renderListFooterView(): JSX.Element | null {
        if (!this.lists.length) {
            return null
        }
        if (this.lists.length < 100 || this.isNoMoreData) {
            return (
                <MyFlatListFooter_NO_MORE />
            )
        }
        if (this.isLoading) {
            return (
                <MyFlatListFooter_LOADING />
            )
        }
        return (
            <MyFlatListFooter_LOADMORE />
        )
    }

    render() {
        const { styles } = chatGroupListsStyles
        return (
            <FlatList 
                style={[styles.container]}
                data={this.ListData}
                ref={this.listElement}
                // 列表为空时渲染
                ListEmptyComponent={this.renderListEmptyView}
                // 加载更多时渲染
                ListFooterComponent={this.renderListFooterView}
                // 当前列表 loading 状态
                refreshing={this.isLoading}
                // 刷新
                onRefresh={this.fetchLists}
                // 加载更多
                onEndReached={this.handleLoadmoreList}
                // 唯一 ID
                keyExtractor={this.getIdKey}
                renderItem={({item}) => (
                    <View>
                        <ListTitle
                            leadingStyle={{
                                width: 'auto',
                                height: 'auto',
                                paddingLeft: 0,
                                paddingRight: 15
                            }}
                            title={<Text numberOfLines={1}>{userStore.getName(item.from)}</Text>}
                            subtitle={<Text numberOfLines={1}>{this.props.route.params.groupInfo.isPrivate ? group.decrypt(accountStore.currentMnemonic, accountStore.currentAddress, item.data?.text) : item.data?.text}</Text>}
                            trailing={<View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <TouchableView
                                    style={[{marginRight: 8}, styles.button]}
                                    onPress={()=>{
                                        this.onPressAgree(item)
                                    }}
                                >
                                    <Text>{i18n.t(LANGUAGE_KEYS.AGREE)}</Text>
                                </TouchableView>
                                <TouchableView
                                    style={styles.button}
                                    onPress={this.onPressReject}
                                >
                                    <Text>{i18n.t(LANGUAGE_KEYS.REJECT)}</Text>
                                </TouchableView>
                            </View>}
                        />
                    </View>
                )}
            />
        )
    }

    @boundMethod
    private onPressAgree(item: IBlnScanMessage){
        const ld = Loading.show()
        const txid = this.props.route.params.address
        const isPrivate = !!this.props.route.params.isPrivate
        let groupSK = ''
        if(isPrivate){
            groupSK = group.decrypt(accountStore.currentMnemonic, accountStore.currentAddress, this.props.route.params.groupInfo.privateKey)
        }
        storeapi.adminAgreeGroup(accountStore.currentMnemonic, accountStore.currentAddress, item.from, isPrivate, txid, groupSK)
        .then((txid: string)=>{
            console.log(txid)
            // this.props.navigation.goBack()
            // todo remove txlist
        })
        .catch((err: any)=>{
            showToast(`${err}`)
        })
        .finally(()=>{
            this.fetchLists()
            Loading.hide(ld)
        })
    }

    @boundMethod
    private onPressReject(){
        const ld = Loading.show()
        const txid = this.props.route.params.address
        const isPrivate = !!this.props.route.params.isPrivate
        storeapi.adminRejectGroup(accountStore.currentMnemonic, accountStore.currentAddress, txid, isPrivate, txid)
        .then((txid: string)=>{
            // this.props.navigation.goBack()
            // todo remove txlist
        })
        .catch((err: any)=>{
            showToast(`${err}`)
        })
        .finally(()=>{
            this.fetchLists()
            Loading.hide(ld)
        })
    }
}

const chatGroupListsStyles = observable({
    get styles() {
      return StyleSheet.create({
        container: {
            flex: 1
        },
        logo: {
            width: 40,
            height: 40,
            borderRadius: 40
        },
        button: {
            backgroundColor: colors.primary,
            paddingLeft:8,
            paddingRight: 8,
            paddingTop: 4,
            paddingBottom: 4,
            borderRadius: 4
        }
      })
    }
})
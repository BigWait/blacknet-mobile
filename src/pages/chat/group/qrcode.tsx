import React, { Component, RefObject } from 'react'
import { StyleSheet, View, TextInput, ScrollView, Text } from 'react-native'
import { action, computed, observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { CopyButton, DeleteButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import CheckBox from 'react-native-check-box'
import API from '@app/services/api'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'
import { LabelListTitle } from '@app/components/ui/list-title'
import { HeaderTitle } from '@react-navigation/stack'
import Video from '@app/components/common/video'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import ListTitle from '@app/components/common/list-title'
import Image from '@app/components/common/image'
import { FlatList } from 'react-native-gesture-handler'
import { defaultAvatar } from '@app/utils/bln'
import sizes from '@app/style/sizes'
import SearchInput from '@app/components/common/searchInput'
import { ChatRoutes } from '@app/routes'
import { MyFlatListFooter_LOADING, MyFlatListFooter_LOADMORE, MyFlatListFooter_NO_MORE } from '@app/components/common/FlatList'
import { NoResult } from '@app/components/ui/NoResult'
import { IBlnGroup, IBlnGroupData } from '@app/types/bln'
import { IRequestParams, ITxPaginate, ITxResultPaginate } from '@app/types/http'
import { isFunction } from 'lodash'
import { TouchableView } from '@app/components/common/touchable-view'
import { IIndexProps } from '@app/pages/home'
import storeapi from '@app/services/storeapi'
import QRCode from 'react-native-qrcode-svg'

@observer export class ChatGroupQrcodeScreen extends Component<IIndexProps> {

    componentDidMount(){
        this.renderTitle()
    }

    @boundMethod
    private renderTitle() {
        this.props.navigation.setOptions({
            headerTitle: <HeaderTitle>{`${i18n.t(LANGUAGE_KEYS.QRCODE)}`}</HeaderTitle>
        })
    }

    render() {
        const { styles } = chatGroupQrcodeStyles
        return (
            <View style={styles.container}>
                <View style={styles.qrImg}>
                    <QRCode
                        value={`groupid: ${this.props.route.params.txid}?isPrivate=${!!this.props.route.params.isPrivate}`}
                        // logo={require('@app/assets/images/1024.png')}
                        // logoSize={50}
                        size={300}
                        logoBackgroundColor="transparent"
                    />
                </View>
            </View>
        )
    }
}

const chatGroupQrcodeStyles = observable({
    get styles() {
      return StyleSheet.create({
        container: {
            flex: 1
        },
        qrImg: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        }
      })
    }
})

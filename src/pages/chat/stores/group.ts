/**
 * chat group store
 * @file chat group store
 * @module app/pages/explorer/stores/group
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { STORAGE } from '@app/constants/storage'
import { IBlnGroup, IBlnGroupData, IBlnScanMessage } from '@app/types/bln'
import { verifyAccount } from '@app/utils/bln'
import api from '@app/services/api'
import { IRequestParams, ITxResultPaginate } from '@app/types/http'
import storeapi, { GroupOpType } from '@app/services/storeapi'
import Blacknetjs from 'blacknetjs';

export class ChatGroupStore {

    @observable datas: Map<string, IBlnGroup> = new Map()
    @observable members: Map<string, string[]> = new Map()

    constructor() {
        this.init()
        this.refresh()
    }

    @action.bound
    private init() {
        // init cache
        storage.get<Array<IBlnGroup>>(STORAGE.LOCAL_CHAT_GROUPS)
        .then(lists => {
            if (lists && lists.length) {
                lists.forEach((group) => {
                    const v = this.datas.get(group.txid)
                    if (v) {
                        this.datas.set(group.txid, { ...v, ...group })
                    } else {
                        this.datas.set(group.txid, group)
                    }
                })
            }
            return lists
        })
        // member 
        storage.get<Array<any>>(STORAGE.LOCAL_CHAT_GROUP_MEMBERS)
        .then(lists => {
            if (lists && lists.length) {
                lists.forEach((group) => {
                    this.members.set(group[0], group[1])
                })
            }
            return lists
        })
        // storage.remove(STORAGE.LOCAL_CHAT_GROUP_MEMBERS)
    }

    @computed
    get lists(){
        return Array.from(this.datas, ([name, value]) => value)
    }

    @action.bound
    upsert(lists: Array<IBlnGroup>) {
        lists.forEach((group) => {
            const v = this.datas.get(group.txid)
            if (v) {
                this.datas.set(group.txid, { ...v, ...group })
            } else {
                this.datas.set(group.txid, group)
            }
        })
        return this.save()
    }

    @boundMethod
    get(txid: string){
        if(verifyAccount(txid)){
            let group
            this.datas.forEach((g)=>{
                if(g.to === txid){
                    group = g
                }
            })
            return group
        }
        return this.datas.get(txid)
    }

    @action.bound
    set(txid: string, group: IBlnGroup){
        const v = this.datas.get(txid)
        if (v) {
            return this.datas.set(txid, { ...v, ...group })
        } else {
            return this.datas.set(txid, group)
        }
    }

    @action.bound
    refresh(params?: IRequestParams | undefined): Promise<ITxResultPaginate<IBlnGroup[]>> {
        return storeapi.fetchGroupLists(params)
            .then((lists) => {
                if (lists && lists.txns && lists.txns.length) {
                    this.upsert(lists.txns)
                }
                return lists
            })
    }

    @boundMethod
    search(text: string): Array<IBlnGroup> {
        const reg = new RegExp(text, 'ig');
        const arr: Array<IBlnGroup> = [];
        this.datas.forEach((group) => {
            if (reg.test([group.data.name, group.data.description].join(' '))) {
                arr.push(group)
            }
        })
        return arr
    }

    private save(){
        return storage.set(STORAGE.LOCAL_CHAT_GROUPS, Array.from(this.datas, ([name, value]) => value))
    }


    @boundMethod
    getGroupName(group: IBlnGroupData, mnemonic?: string, address?: string) {
        if(!group){
            return null
        }
        if(mnemonic && group.isPrivate){
            address = address || Blacknetjs.Address(mnemonic)
            try {
                const groupSK = Blacknetjs.Decrypt(mnemonic, address, group.privateKey);
                const groupPK = Blacknetjs.Address(groupSK)
                return Blacknetjs.Decrypt(groupSK, groupPK, group.name);
            } catch (error) {
                return null   
            }
        }
        return group.name
    }

    @boundMethod
    getGroupLogo(group: IBlnGroupData, mnemonic?: string, address?: string) {
        if(!group){
            return null
        }
        if(mnemonic && group.isPrivate){
            address = address || Blacknetjs.Address(mnemonic)
            try {
                const groupSK = Blacknetjs.Decrypt(mnemonic, address, group.privateKey);
                const groupPK = Blacknetjs.Address(groupSK)
                return Blacknetjs.Decrypt(groupSK, groupPK, group.logo);
            } catch (error) {
                return null   
            }
        }
        return group.logo
    }
    @boundMethod
    getGroupDesc(group: IBlnGroupData, mnemonic?: string, address?: string) {
        if(!group){
            return null
        }
        if(mnemonic && group.isPrivate){
            address = address || Blacknetjs.Address(mnemonic)
            try {
                const groupSK = Blacknetjs.Decrypt(mnemonic, address, group.privateKey);
                const groupPK = Blacknetjs.Address(groupSK)
                return Blacknetjs.Decrypt(groupSK, groupPK, group.description);
            } catch (error) {
                return null   
            }
        }
        return group.description
    }




    // group members
    @action.bound
    refreshMembers(txid: string): Promise< string[]> {
        return storeapi.blnScanChatGroupMemebers(txid)
            .then((lists) => {
                if (lists) {
                    this.setMembers(txid, lists)
                }
                return lists
            })
    }
    @boundMethod
    getMember(txid: string){
        return this.members.get(txid) || []
    }

    @action.bound
    setMembers(txid: string, m: string[]){
        this.members.set(txid, m)
        return storage.set(STORAGE.LOCAL_CHAT_GROUP_MEMBERS, Array.from(this.members, ([k,v])=>[k, v]))
    }
}

export const chatGroupStore = new ChatGroupStore()
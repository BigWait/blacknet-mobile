/**
 * chat key store
 * @file chat key store
 * @module app/pages/explorer/stores/key
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { STORAGE } from '@app/constants/storage'
import { IBlnScanMessage } from '@app/types/bln'
import group from '@app/utils/group'
import storeapi, { GroupOpType } from '@app/services/storeapi'

// k: toaddress v: IBlnScanMessage
export class ChatKeyStore extends Map<String, IBlnScanMessage> {

    constructor() {
        super()
        // init cache
        storage.get<[String, IBlnScanMessage[]][]>(STORAGE.LOCAL_CHAT_KEY)
        .then(lists => {
            if (lists && lists.length) {
                lists.forEach(([k, v]) => {
                    this.keyMaps.set(k, v)
                })
            }
            return lists
        })
    }

    @observable private txid?: string
    @observable private keyMaps: Map<String, Array<IBlnScanMessage>> = new Map() //k: txid  v: IBlnScanMessage[]

    init(txid: string) {
        this.clear()
        this.txid = txid
        const keys = this.keyMaps.get(txid)
        if(keys && keys.length){
            keys.forEach((tx)=>{
                const key = this.get(tx.to)
                if(!key || (key && tx.time > key.time)){
                    this.set(tx.to, tx)
                }
            })
        }
        // refresh
        this.refresh()
    }

    refresh(txid?: string){
        txid = txid  || this.txid
        return storeapi.fetchGroupOption(GroupOpType.GroupAgree, {txid: txid})
            .then((lists) => {
                if (lists && lists.txns && lists.txns.length) {
                    this.upsert(lists.txns, txid)
                }
                return lists
            })
    }

    @computed
    get keyArr() {
        return Array.from(this.keyMaps)
    }

    @boundMethod
    upsert(keys: Array<IBlnScanMessage>, txid?: string) {
        if(txid && txid !== this.txid){
            this.keyMaps.set(txid, keys)
        }else{
            keys.forEach((tx)=>{
                const key = this.get(tx.to)
                if(!key || (key && tx.time > key.time)){
                    this.set(tx.to, tx)
                }
            })
            this.keyMaps.set(this.txid || "", keys)
        }
        storage.set(STORAGE.LOCAL_CHAT_KEY, this.keyArr)
    }

    @boundMethod
    remove(currentAddress: string, txid?: string) {
        txid = txid || this.txid || ""
        const keys = this.keyMaps.get(txid)
        if(keys){
            let index = -1;
            keys.slice().forEach((t, i)=>{
                if(t.to === currentAddress){
                    index = i
                }
            })
            if(index>-1){
                keys.splice(index, 1)
            }
            this.keyMaps.set(txid, keys)
        }
        if(this.txid === txid){
            this.delete(currentAddress)
        }
        storage.set(STORAGE.LOCAL_CHAT_KEY, this.keyArr)
    }

    getSK(currentMnemonic: string, currentAddress: string, txid?: string){
        let key: IBlnScanMessage | undefined
        if(txid){
            const keys = this.keyMaps.get(txid)
            if(keys){
                keys.forEach((tx)=>{
                    if(tx.to === currentAddress){
                        if(!key || (key && key.time < tx.time)){
                            key = tx
                        }    
                    }
                })  
            }
        } else {
            key = this.get(currentAddress)
        }
        if(key){
            return group.decrypt(currentMnemonic, currentAddress, key.data?.privateKey)
        }
        return null
    }
}

export const chatKeyStore = new ChatKeyStore()
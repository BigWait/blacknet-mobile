/**
 * chant store
 * @file chat store
 * @module app/pages/explorer/stores/chat
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { IBlnScanMessage } from '@app/types/bln'
import storeAPI from '@app/services/storeapi'
import { accountStore } from '@app/stores/account'
import { chatGroupStore } from './group'
import { join } from 'lodash'

export class ChatStore {

    @observable lists: Array<IBlnScanMessage> = []

    @boundMethod
    private listKey(address: string):string{
        return `chat:messages:${address}`;
    }

    @boundMethod
    getList(address: string): Promise<IBlnScanMessage[]>{
        return storage.get<Array<IBlnScanMessage>>(this.listKey(address)).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        })  
    }

    @action.bound
    setList(address: string, txns: Array<IBlnScanMessage>) {
        this.lists = txns.slice()
    }

    @action.bound
    setListWithStorage(address: string, txns: Array<IBlnScanMessage>) {
        this.lists = txns.slice()
        return storage.set(this.listKey(address), this.lists)
    }

    @boundMethod
    upsertsList(address: string, ntxns: IBlnScanMessage[]) {
        return this.getList(address).then((txns)=>{
            if(!txns){
                return this.setListWithStorage(address, ntxns) 
            }
            for (const tx of ntxns) {
                let index = -1;
                txns.slice().forEach((t, i)=>{
                    if(tx.group && t.to === tx.to){
                        index = i
                    } else if (t.from === tx.from && t.to === tx.to){
                        index = i
                    }
                })
                if(index>-1){
                    if(tx.group && !tx.time){
                        txns.splice(index, 1, {...txns[index], ...tx})
                    } else if (parseInt(tx.time) >= parseInt(txns[index].time)){
                        txns.splice(index, 1, {...txns[index], ...tx})
                    }
                }else{
                    txns.push(tx)
                }
            }
            const groups: IBlnScanMessage[]= []
            txns.slice().forEach((tx, i)=>{
                if(tx.group){
                    txns.splice(i, 1)
                    groups.push(tx)
                }
            })
            txns.sort((a, b)=>{
                return parseInt(b.time) - parseInt(a.time)      
            })
            return this.setListWithStorage(address, groups.concat(txns))
        })
    }

    @boundMethod
    private key(address: string, to: string):string{
        return `chat:messages:${address}:with:${to}`;
    }

    @boundMethod
    get(address: string, to: string): Promise<IBlnScanMessage[]>{
        return storage.get<Array<IBlnScanMessage>>(this.key(address, to)).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        })  
    }

    @boundMethod
    getOne(address: string, to: string, txid: string): Promise<IBlnScanMessage|undefined>{
        return this.get(address, to).then((txs)=>{
            let tx: IBlnScanMessage | undefined = undefined
            txs.forEach((t)=>{
                if(t.txid === txid){
                    tx = t
                }
            })
            return tx
        }).then((tx)=>{
            // 缓存没找到请求网络
            return tx
        })
    }

    @boundMethod
    set(address: string, to: string, txns: Array<IBlnScanMessage>) {
        return storage.set(this.key(address, to), txns)
    }

    @boundMethod
    upserts(address: string, to: string, ntxns: Array<IBlnScanMessage>) {
        return this.get(address, to).then((txns)=>{
            if(!txns){
                return this.set(address, to, ntxns) 
            }
            for (const tx of ntxns) {
                let index = -1;
                txns.slice().forEach((t, i)=>{
                    if(t.txid && t.txid === tx.txid){
                        index = i
                    }
                })
                if(index>-1){
                    if(parseInt(tx.time) >= parseInt(txns[index].time)){
                        txns.splice(index, 1, {...txns[index], ...tx})
                    }
                }else{
                    txns.push(tx)
                }
            }
            txns.sort((a, b)=>{
                return parseInt(b.time) - parseInt(a.time)
            })
            return this.set(address, to, txns)
        })
    }

    @boundMethod
    replace(address: string, to: string, txid: string, tx: IBlnScanMessage) {
        return this.get(address, to).then((txns)=>{
            if(!txns){
                return this.set(address, to, [tx]) 
            }
            let index = -1;
            txns.slice().forEach((t, i)=>{
                if(t.txid && t.txid === txid){
                    index = i
                }
            })
            if(index>-1){
                if(parseInt(tx.time) >= parseInt(txns[index].time)){
                    txns.splice(index, 1, {...txns[index], ...tx})
                }
            }else{
                txns.push(tx)
            }
            return this.set(address, to, txns)
        })
    }

    @boundMethod
    refreshChatMap(){
        return storeAPI.blnScanChatMapV2(accountStore.currentAddress)
        .then((json)=>{
            for (const key in json) {
                if (Object.prototype.hasOwnProperty.call(json, key)) {
                    const msg = json[key];
                    if(msg.group){
                        const group = chatGroupStore.get(msg.to)
                        if(group){
                            json[key] = {...msg, groupInfo: group.data}
                        }
                    }
                }
            }
            return json
        })
        .then((json) => {
            // return this.upsertsList(accountStore.currentAddress, json)
            // return this.set(accountStore.currentAddress, to, txns)
            this.setListWithStorage(accountStore.currentAddress, json)
            return json
        })
    }
    
}

export const chatStore = new ChatStore()
/**
 * Message
 * @file message
 * @module pages/chat/message
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { Dimensions, Platform, StyleSheet, View, TextInput, Keyboard, DeviceEventEmitter} from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import API from '@app/services/api'
import StoreAPI from '@app/services/storeapi'

import { IBlnScanMessage } from '@app/types/bln'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { MIME_VIDEO } from '@app/constants/regexp'
import showToast from '@app/services/toast';
import { formatChatContent, formatGroupChatContent, overFileSizeLImit, verifyAccount } from '@app/utils/bln'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { GiftedChat, IMessage, Send, MessageText, Bubble, InputToolbar, AvatarProps, User, Composer, MessageImageProps, MessageImage, RenderMessageTextProps } from 'react-native-gifted-chat'
import { ChatTitle } from '@app/components/layout/title'
import {chatStore} from '@app/pages/chat/stores/chat'
import contactStore from '@app/stores/contact'
import { showBalance } from '@app/utils/bln'
import { shortAddress } from '@app/utils/address'
import { optionStore } from '@app/stores/option'
import usersService from '@app/services/users'
import { TouchableView } from '@app/components/common/touchable-view'
import { ChatRoutes, HomeRoutes } from '@app/routes'
import { BLNText, Text } from '@app/components/common/text'
import Image from "@app/components/common/image";
import { SafeAreaView, SafeAreaInsetsContext } from 'react-native-safe-area-context'
import { isFunction, transform } from 'lodash'
import Animated, {Easing} from 'react-native-reanimated'
import EmojiPicker from '@app/components/ui/chat/emoji'
import { ChatPlusPicker } from '@app/components/ui/chat/picker'
import MyBubble from '@app/components/ui/chat/myBubble'
import MyComposer from '@app/components/ui/chat/myComposer'
import ImagePicker from 'react-native-image-picker';
import md5 from 'md5';
import MSG from '@app/utils/msg'
import { userStore } from '@app/stores/users'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { getHeaderButtonStyle } from '@app/style/mixins'
import { chatKeyStore } from './stores/key'

export interface IIndexProps extends IPageProps { }
@observer export class MessageScreen extends Component<IIndexProps> {
    state: {
        isLoadingEarlier: false,
        messages: []
    }
    constructor(props: IIndexProps) {
        super(props);
        this.state = {
            messages: [],
            isLoadingEarlier: false,
        };
        // init key
        chatKeyStore.init(this.props.route.params.address)
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {

        }
    }

    private keyboardDidShowListener: any;
    private keyboardDidHideListener: any;
    private unsubscribe: any;
    private subscription: any;

    onScreenFocus = () => {
        // messages
        this.fetchMessages()
    }
    
    componentDidMount(){
        this.subscription = DeviceEventEmitter.addListener('WebSocket_New_Messages',(message)=> {
            console.log('websocket update messages')
            this.fetchMessages()
        });
        // title
        this.renderTitle()
        // header right
        this.renderHeaderRight()
        // init messages
        this.initMessages()
    }
    //  渲染前加载消息
    UNSAFE_componentWillMount() {
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
        //监听键盘弹出事件
        this.keyboardDidShowListener = Keyboard.addListener( 'keyboardDidShow', ()=>chatOptionStore.updateKeyboardShown(true));
        //监听键盘隐藏事件
        this.keyboardDidHideListener = Keyboard.addListener( 'keyboardDidHide', ()=>chatOptionStore.updateKeyboardShown(false));

        chatOptionStore.reset()

        this.group = !!this.props.route.params.group
    }

    componentWillUnmount(){
        this.keyboardDidShowListener && this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener && this.keyboardDidHideListener.remove()
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
        // cleartimer
        this.subscription.remove();
    }

    @boundMethod
    private renderTitle() {
        if(this.props.route.params.group){
            this.props.navigation.setOptions({
                headerTitle: <ChatTitle
                    title={verifyAccount(this.props.route.params.name) ? shortAddress(this.props.route.params.name, 10): this.props.route.params.name}
                />
            })
        } else {
            StoreAPI.getBalance(this.props.route.params.address)
            .then((balance) => {
                contactStore.setBalance(this.props.route.params.address, balance.balance)
                this.props.navigation.setOptions({
                    headerTitle: <ChatTitle
                        title={verifyAccount(this.props.route.params.name) ? shortAddress(this.props.route.params.name, 10): this.props.route.params.name}
                        subtitle={`${showBalance(balance.balance)} ${accountStore.blnSymbol}`}
                    />
                })
            })
        }
    }
    @boundMethod
    private renderHeaderRight() {
        this.props.navigation.setOptions({
            headerRight: () => <Observer render={() => (
                <TouchableView
                    onPress={()=>{
                        this.props.navigation.navigate(ChatRoutes.Info, this.props.route.params)
                    }}
                >
                    <FontAwesome5
                        name="ellipsis-h"
                        {...getHeaderButtonStyle(18)}
                        color={colors.textTitle}
                    />
                </TouchableView>
            )} />
        })
    }

    @observable.shallow private messages: IMessage[] = []
    @observable private group: boolean = false
    @observable private isLoadingEarlier: boolean = false
    @observable private page: number = 1
    @observable private contentHeight: number = 0
    private GiftedChatRef: React.RefObject<GiftedChat<IMessage>> = React.createRef()

    @action
    private updateIsLoadingEarlierState(loading: boolean) {
        this.isLoadingEarlier = loading
    }

    @computed
    private get Data(): IMessage[] {
        return this.messages.slice() || []
    }

    @computed
    private get IDs(): string[] {
        const ids: string[] = [];
        this.messages.slice().forEach((msg) => {
            ids.push(`${msg._id}`)
        })
        return ids
    }

    @action
    private formatMessages(msgs: Array<IBlnScanMessage>, save?: boolean) {
        const messages: IMessage[] = []
        msgs.forEach((msg) => {
            const _id = `${msg.txid}`
            if (this.IDs.includes(_id)) {
                return
            }
            let text
            try {
                if(this.group){
                    const isPrivate = !!this.props.route.params.isPrivate
                    text = MSG.encodeGroupChatContent(msg.text, isPrivate, chatKeyStore.getSK(accountStore.currentMnemonic, accountStore.currentAddress))
                } else if (this.owner(msg.from)) {
                    text = MSG.encodeChatContent(accountStore.currentMnemonic, msg.to, msg.text)
                } else {
                    text = MSG.encodeChatContent(accountStore.currentMnemonic, msg.from, msg.text)
                }
            } catch (error) {
                // console.log(msg, error)
            }
            text = text || msg.text
            let obj: {[key: string]: any}= {
                text: text,
                sent: msg.sent === undefined ? true : msg.sent,
                received: msg.received === undefined ? true : msg.received,
                pending: msg.pending === undefined ? true : msg.pending
                // sent: true,
                // received: true,
                // pending: false
            }
            if(MSG.isImage(text, false)) {
                // obj['text'] = ''
                obj['image'] = MSG.isImage(text) ? MSG.getIPFSURL(text) : MSG.getCID(text)
            }
            if(MSG.isVideo(text, false)) {
                // obj['text'] = ''
                obj['video'] = MSG.isVideo(text) ? MSG.getIPFSURL(text) : MSG.getCID(text)
            }
            if(MSG.isAudio(text, false)) {
                // obj['text'] = ''
                obj['audio'] = MSG.isAudio(text) ? MSG.getIPFSURL(text) : MSG.getCID(text)
            }
            messages.push({
                _id: _id, //消息的ID，用于标示，必须唯一
                createdAt: new Date(parseInt(msg.time) * 1000),    //发送的时间
                user: {
                    _id: msg.from, //发送方的ID 如果是自己的ID 则在右边，否则在左边
                    name: userStore.getName(msg.from)
                },
                ...obj
                // text: text || msg.text,
                // sent: msg.sent,
                // received: msg.received,
                // pending: msg.pending
            } as IMessage)
        })
        this.messages = GiftedChat.append(this.messages, messages).sort((a, b)=>{
            return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
        })
    }

    @boundMethod
    imagePicker() {
        ImagePicker.launchImageLibrary({},
        async (image) => {
            this.appendMessage(image);
        })
    }

    @boundMethod
    launchCamera(){
        ImagePicker.launchCamera({}, (image) => {
            this.appendMessage(image);
        })
    }

    @boundMethod
    appendMessage(image: any){
        if(image.didCancel == true) return;
        if(image.error) return showToast(image.error);
        if(overFileSizeLImit(image.fileSize)) return showToast(i18n.t(LANGUAGE_KEYS.FILE_SIZE_LIMI));
        const address = this.props.route.params.address
        const isGroup = this.group
        const imgSrc = Platform.OS === "android" ? image.uri : image.uri.replace("file://", "")
        const createdAt = new Date()
        const isPrivate = !!this.props.route.params.isPrivate
        const id = `${md5(imgSrc)}:${image.type}:${isGroup}:${address}:${isPrivate}:${Math.floor(createdAt.getTime() / 1000).toString()}`
        let text = ''
        if(chatOptionStore.composerQuoteMessage){
            text = `[txid:${chatOptionStore.composerQuoteMessage._id}]`
        }
        let msg: IMessage= {
            _id: id,
            text: '',
            createdAt: createdAt,
            user: {
                _id: accountStore.currentAddress,
                name: userStore.getName(accountStore.currentAddress)
            },
            sent: false,
            received: false,
            pending: true
        }
        if(MIME_VIDEO.test(image.type) ){
            msg.video = imgSrc
        }else{
            msg.image = imgSrc
        }
        this.messages = GiftedChat.append(this.messages, [msg])
        chatOptionStore.hideQuote()
    }

    @boundMethod
    private fetchMessages(page: number = 1) {
        let promis;
        if(this.group){
            promis = API.blnScanGroupMessages(this.props.route.params.address, {
                page: page
            })
        } else {
            promis = API.blnScanMessages(accountStore.currentAddress, {
                with: this.props.route.params.address,
                page: page
            })
        }
        return promis.then((messages) => {
            if (messages.length && page === 1) {
                chatStore.upserts(accountStore.currentAddress, this.props.route.params.address, messages.slice())
            }
            this.formatMessages(messages.slice())
            return messages
        })
    }

    @boundMethod
    private initMessages() {
        return chatStore.get(accountStore.currentAddress, this.props.route.params.address)
            .then((messages) => {
                this.formatMessages(messages.slice())
                return messages
            })
    }

    @action
    private onSend(messages: IMessage[]) {
        const address = this.props.route.params.address
        const isGroup = this.group
        const createdAt = new Date()
        const sents: string[] = []
        const newMessages: IMessage[] = []
        const isPrivate = !!this.props.route.params.isPrivate
        messages.forEach((msg)=>{
            if(sents.includes(msg.text)){
                return 
            }
            let text = `[text:${msg.text}]`
            if(chatOptionStore.composerQuoteMessage){
                text = `[txid:${chatOptionStore.composerQuoteMessage._id}][text:${msg.text}]`
            }
            newMessages.push({
                _id: `${md5(msg.text)}:${isGroup}:${address}:${isPrivate}:${Math.floor(createdAt.getTime() / 1000).toString()}`,
                text: text,
                createdAt: createdAt,
                user: {
                    _id: accountStore.currentAddress,
                    name: userStore.getName(accountStore.currentAddress)
                },
                sent: false,
                received: false,
                pending: true
            } as IMessage)
        })
        this.messages = GiftedChat.append(this.messages, newMessages)
        chatOptionStore.hideQuote()
    }

    @boundMethod
    private owner(address: string) {
        return address === this.props.route.params.user
    }

    @boundMethod
    private renderFooter(){
        return null;
    }
    @boundMethod
    private renderDay(props: any){
        let cMsg = props.currentMessage;
        let pMsg = props.previousMessage;

        if(!props.previousMessage.createdAt){
            return null;
        }

        let cdate = cMsg.createdAt, pdate = pMsg.createdAt;
        let sub = +cdate - (+pdate);

        if(sub / (1000 * 60) < 15){
            return null
        }

        let now = Date.now(), prefix = '';

        if(now - (+cdate) > 1000 * 60 * 60 * 24 * 1 ){
            prefix = cdate.getFullYear() + '-' + cdate.getMonth() + '-' + cdate.getDate() + ' ';
        }

        return <BLNText style={{textAlign: 'center', marginVertical: 10}}>
            {`${prefix}${cdate.getHours()} : ${('0' + cdate.getMinutes()).slice(-2)}`}
        </BLNText>;
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.listWarp}>
                <GiftedChat
                    ref={this.GiftedChatRef}
                    messages={this.Data}
                    onSend={messages => this.onSend(messages)}
                    user={{
                        _id: this.props.route.params.user
                    }}
                    renderAvatar={this.renderAvatar}
                    onPressAvatar={this.onPressAvatar}
                    renderSend={this.renderSend}
                    renderInputToolbar={this.renderInputToolbar}
                    renderBubble={this.renderBubble}
                    onLoadEarlier={this.onLoadEarlier}
                    renderFooter={this.renderFooter}
                    renderDay={this.renderDay}
                    renderUsernameOnMessage={true}
                    // renderLoadEarlier
                    loadEarlier={this.isLoadingEarlier}
                    renderAvatarOnTop={true}
                    showUserAvatar={true}
                    isLoadingEarlier={this.isLoadingEarlier}
                    listViewProps={{
                        canLoad: true,
                        isLoadding: false,
                        ifShowRefresh: true,
                        scrollEnabled: chatOptionStore.scrollEnabled,
                        page: this.page,
                        onScroll: this.onScroll,
                        style: {
                            paddingTop: 10
                        }
                    }}
                    keyboardShouldPersistTaps={Platform.select({
                        ios: 'never',
                        android: 'never',
                        default: 'never',
                    })}
                    showAvatarForEveryMessage={true}
                    placeholder={i18n.t(LANGUAGE_KEYS.CHAT_TYPE_A_MESSAGE)}
                    renderAccessory={this.renderAccessory}
                    renderComposer={this.renderComposer}
                    renderActions={this.renderActions}
                    onInputTextChanged={chatOptionStore.onInputTextChanged}
                    minInputToolbarHeight={chatOptionStore.minInputToolbarHeight}
                    minComposerHeight={chatOptionStore.minComposerHeight}
                    text={chatOptionStore.inputText}
                />
            </View>
        )
    }

    @boundMethod
    private onPressAvatar(usr?: User){
        if(usr?._id){
            this.props.navigation.navigate(HomeRoutes.ExplorerDetail, {
                type: "account",
                id: usr?._id
            })
        }
    }

    @boundMethod
    private onScroll(event: any) {
        const y = event.nativeEvent.contentOffset.y;
        const height = event.nativeEvent.layoutMeasurement.height;
        const contentHeight = event.nativeEvent.contentSize.height;
        if(y > 0){
            chatOptionStore.isScroll = true
        } else {
            chatOptionStore.isScroll = false
        }
        // 关闭键盘 向上滚动一个屏幕高度时关闭键盘
        if(y > 0 && chatOptionStore.scrollEnabled){
            // 键盘按下去
            if(chatOptionStore.KeyboardShown){
                Keyboard.dismiss()
            }
            // 表情窗按下去
            if(chatOptionStore.accessory){
                chatOptionStore.hideAccessory()
            }
        }
        if (y + height >= contentHeight - 20 && y > 0 && this.contentHeight != contentHeight) {//上啦下一页
            this.contentHeight = contentHeight
            this.onLoadEarlier()
        }
        else if (y < 0 || y == 0) {//下拉上一页ios

        }
    }

    private onLoadEarlier() {
        this.updateIsLoadingEarlierState(true)
        const page = this.page + 1
        this.fetchMessages(page).then((messages) => {
            if (messages.length > 0) {
                this.page = page
            }
            return this.updateIsLoadingEarlierState(false)
        })
    }
    
    @boundMethod
    private renderAvatar(props: AvatarProps<IMessage>) {
        let source: any= require('@app/assets/images/1024.png')
        if(props.currentMessage?.user._id && userStore.getUserImage(props.currentMessage?.user._id.toString())){
            source = {uri: userStore.getUserImage(props.currentMessage?.user._id.toString())}
        }
        return <TouchableView
            onPress={()=>this.onPressAvatar(props.currentMessage?.user)}
        >
            <Image
                style={{
                    width: 35,
                    height: 35,
                    borderRadius: 40
                }}
                source={source}
            />
        </TouchableView>
    }

    // 输入框
    @boundMethod
    private renderComposer(props: any){
        const { onSend } = props
        return <MyComposer 
                {...props} 
                textInputStyle={[props.textInputStyle, {
                    backgroundColor: colors.border,
                    borderRadius: 4,
                    color: colors.textDefault,
                    marginLeft: 0,
                    marginTop: 0,
                    marginBottom: 0,
                    marginRight: 0,
                    paddingTop: Platform.select({
                        ios: 12,
                        android: 6
                    }),
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingBottom: 6
                }]}
                multiline={true}
                textInputProps = {{
                    ...props.textInputProps,
                    numberOfLines: 5,
                    onBlur: ()=> Keyboard.dismiss(), //失去焦点关闭键盘， 这里跟blurOnSubmit没有关联 
                    returnKeyType: 'send',
                    returnKeyLabel: 'send',
                    onSubmitEditing: (e)=>{
                        onSend({ text: e.nativeEvent.text.trim() }, true) // false 不重制inputbar 也就不会cleartext/dismiss keyboard
                    },
                    blurOnSubmit: true, // 这里会强制失去焦点 关闭键盘， 如果不要键盘也不会显示send和触发onSubmitEditing
                    onFocus: ()=>{
                        this.scrollToBottom(()=>{
                            if(chatOptionStore.accessory){
                                chatOptionStore.hideAccessory()
                            }
                        })
                    }
                }}
                minComposerQuoteHeight={chatOptionStore.minComposerQuoteHeight}
                quoteMessage={chatOptionStore.composerQuoteMessage}
                clearQuote={()=>{
                    chatOptionStore.hideQuote()
                }}
            />
    }
    // 发送按钮 取消
    @boundMethod
    private renderSend(props: any) {
        return null
    }

    // 表情等按钮
    @boundMethod
    private renderActions() {
        return <View style={{ marginLeft: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', height: chatOptionStore.inputToolbarHeight}}>
            <TouchableView style={{marginRight: 10}}
                onPress={()=>{
                    if(chatOptionStore.accessory){
                        if(chatOptionStore.isEmoji){
                            chatOptionStore.hideAccessory()
                            this.GiftedChatRef.current?.focusTextInput()
                        }else{
                            chatOptionStore.switchAccessory()
                        }
                    } else {
                        Keyboard.dismiss()
                        this.scrollToBottom(()=>{
                            chatOptionStore.showEmoji()
                        })
                    }
                    this.setState({})
                }}
            >
                <FontAwesome name={chatOptionStore.accessory ? "keyboard-o" : "smile-o"} size={Platform.select({
                    ios: 24,
                    android: 30
                })} color={colors.textDefault}></FontAwesome>
            </TouchableView>
            <TouchableView
                onPress={()=>{
                    if(chatOptionStore.accessory){
                        if(chatOptionStore.isMore){
                            chatOptionStore.hideAccessory()
                            this.GiftedChatRef.current?.focusTextInput()
                        }else{
                            chatOptionStore.switchAccessory()
                        }
                    } else {
                        Keyboard.dismiss()
                        this.scrollToBottom(()=>{
                            chatOptionStore.showMore()
                        })
                    }
                    this.setState({})
                }}
            >
                <AntDesign name="pluscircleo" size={Platform.select({
                    ios: 20,
                    android: 26
                })} color={colors.textDefault}></AntDesign>
            </TouchableView>
        </View>
    }

    // 输入框底部区域
    @boundMethod
    private renderAccessory(props: any){
        // return null
        if(!chatOptionStore.accessory){
            return null
        }
        const { onSend } = props
        return <View 
            style={{
                height: chatOptionStore.accessoryHeight,
                position: 'relative',
                overflow: 'hidden'
            }}
        >
            {chatOptionStore.accessory && chatOptionStore.accessoryType === 'more' ?
                <ChatPlusPicker 
                    onPress={(text)=>{
                        switch (text) {
                            case 'album':
                                this.imagePicker()
                                break;
                            default:
                                this.launchCamera()
                                break;
                        }
                    }}
                />
            : null}
            {chatOptionStore.accessory && chatOptionStore.accessoryType === 'emoji' ?
                <EmojiPicker 
                    onEmojiSelected={emoji => {
                        chatOptionStore.appendInputText(emoji)
                    }}
                    enableAction={!!chatOptionStore.inputText}
                    onDelPress={()=>{
                        chatOptionStore.popInputText()
                    }}
                    onSendPress={()=>{
                        onSend({ text: chatOptionStore.inputText.trim() }, true) // false 不重制inputbar 也就不会cleartext/dismiss keyboard
                    }}
                />
            : null}
        </View>
    }

    @boundMethod
    private renderInputToolbar(props: any) {
        return <SafeAreaInsetsContext.Consumer>
            {(insets) => (
                <InputToolbar
                    {...props}
                    primaryStyle={{
                        padding: 0,
                        margin: 0,
                        marginLeft:10,
                        marginRight:10,
                        flexDirection: 'row-reverse',
                        alignSelf: 'baseline',
                        paddingTop: 8,
                        paddingBottom: 10
                    }}
                    accessoryStyle={{
                        height: 'auto'
                    }}
                    containerStyle={{
                        backgroundColor: colors.background,
                        margin: 0,
                        padding: 0,
                        borderTopWidth: 1,
                        borderTopColor: colors.border
                    }}
                />
            )}
        </SafeAreaInsetsContext.Consumer>
    }

    @boundMethod
    private renderBubble(props: any) {
        return <MyBubble
            {...props}
            onQuotePress={(messages: IMessage)=>{
                chatOptionStore.showQuote(messages)
                this.scrollToBottom(()=>{
                    this.GiftedChatRef.current?.focusTextInput()
                })
            }}
            formatQuote={(text: string)=>{
                let quote = MSG.decodeContent(text)
                if(quote['txid']){
                    return chatStore.getOne(accountStore.currentAddress, this.props.route.params.address, quote['txid'])
                    .then((msg) => {
                        if(msg){
                            let text
                            try {
                                if(MSG.isGroup(msg.to)){
                                    const isPrivate = !!this.props.route.params.isPrivate
                                    text = MSG.encodeGroupChatContent(msg.text, isPrivate, chatKeyStore.getSK(accountStore.currentMnemonic, accountStore.currentAddress))
                                } else if (this.owner(msg.from)) {
                                    text = MSG.encodeChatContent(accountStore.currentMnemonic, msg.to, msg.text)
                                } else {
                                    text = MSG.encodeChatContent(accountStore.currentMnemonic, msg.from, msg.text)
                                }
                            } catch (error) {

                            }
                            text = MSG.formatText(text || msg.text)
                            quote['text'] = text
                            quote['name'] = userStore.getName(msg.from) || msg.from
                            quote['from'] = msg.from
                            if(MSG.isImage(text, false)) {
                                quote['text'] = '[image]'
                                quote['image'] = MSG.isImage(text) ? MSG.getIPFSURL(text) : MSG.getCID(text)
                            }
                            if(MSG.isVideo(text, false)) {
                                quote['text'] = '[video]'
                                quote['video'] = MSG.isVideo(text) ? MSG.getIPFSURL(text) : MSG.getCID(text)
                            }
                            if(MSG.isAudio(text, false)) {
                                quote['text'] = '[audio]'
                                quote['audio'] = MSG.isAudio(text) ? MSG.getIPFSURL(text) : MSG.getCID(text)
                            }
                        }
                        return quote
                    })
                }else{
                    return Promise.resolve(quote)
                }
            }}
            formatText={(text: string)=>{
                return MSG.hasText(text) ? MSG.formatText(text) : text
            }}
            extendObject={(txid: string)=>{
                return {
                    groupInfo: this.props.route.params.groupInfo
                }
            }}
        />
    }

    @boundMethod
    private scrollToBottom(func?: any, animated: boolean = true){
        chatOptionStore.updateScrollEnabled(false)
        this.GiftedChatRef.current?.scrollToBottom(animated)
        const hide = (n: number)=>{
            if(n===1 && isFunction(func)){
                func()
            }
            if(!chatOptionStore.isScroll){
                chatOptionStore.updateScrollEnabled()
            } else {
                if(animated){
                    requestAnimationFrame(hide)
                } else {
                    setTimeout(hide.call(this, n+1), 1);
                }
            }
        }
        return hide(1)
    }
}

const minComposerHeight = Platform.select({
    ios: 33,
    android: 33
})

class ChatOptionStore {
    // input
    @observable private inputTextArr: string = ''
    @computed
    get inputText(){
        return this.inputTextArr
    }
    @action.bound
    onInputTextChanged(text?: string){ 
        this.inputTextArr = text || ''
    }
    @action.bound
    appendInputText(text: string){
        this.inputTextArr = this.inputTextArr + text;
    }
    @action.bound
    popInputText(){
        this.inputTextArr = this.inputTextArr.slice(0, -1)
    }
    // scroll
    isScroll: boolean = false
    @observable KeyboardShown: boolean = false
    @observable scrollEnabled: boolean = true
    @action.bound
    updateScrollEnabled(emable: boolean = true) {
        this.scrollEnabled = emable
    }
    @action.bound
    updateKeyboardShown(emable: boolean = false) {
        this.KeyboardShown = emable
    }

    // inputbar height
    @observable inputToolbarHeight = Platform.select({
        ios: 41,
        android: 41
    })

    @observable composerHeight = minComposerHeight || 0

    @computed
    get minComposerHeight(){
        return minComposerHeight
    }
    // 计算底部chatgift底部高度 / 2
    @computed
    get minInputToolbarHeight(){
        return  (this.minComposerQuoteHeight + this.minAccessoryHeight + (this.inputToolbarHeight || 0)) / 2
    }
    @computed
    get minComposerQuoteHeight(){
        return  this.composerQuoteHeight
    }
    @computed
    get minAccessoryHeight(){
        return  this.accessoryHeight
    }
    // accessory
    @observable accessoryHeight = 0
    @observable composerQuoteHeight = 0
    @observable composerQuoteMessage?: IMessage = undefined
    @observable accessory: boolean = false
    @observable accessoryType: 'emoji' | 'more' = 'emoji'
    accessoryAnimated: Animated.Value<number> = new Animated.Value(0)
    @computed
    get isEmoji(){
        return this.accessoryType === 'emoji'
    }
    @computed
    get isMore(){
        return this.accessoryType === 'more'
    }

    @computed
    get accessoryHeightOutputRange(){
        if(this.accessoryType === 'more'){
            return [0, accessoryMoreHeight]
        }
        return [0, accessoryEmojiHeight]
    }

    @action.bound
    hideQuote() {
        this.composerQuoteHeight = 0
        this.composerQuoteMessage = undefined
    }

    @action.bound
    showQuote(message: IMessage) {
        this.composerQuoteHeight = 34.5
        this.composerQuoteMessage = message
    }

    @action.bound
    switchAccessory() {
        if(this.accessoryType === 'emoji'){
            this.accessoryType = 'more'
            this.accessoryHeight = accessoryMoreHeight
        } else {
            this.accessoryType = 'emoji'
            this.accessoryHeight = accessoryEmojiHeight
        }
    }

    @action.bound
    showEmoji() {
        this.accessoryType = 'emoji'
        this.accessoryHeight = accessoryEmojiHeight
        this.showAccessory()
    }

    @action.bound
    hideEmoji() {
        this.accessoryType = 'emoji'
        this.hideAccessory()
    }

    @action.bound
    showMore() {
        this.accessoryType = 'more'
        this.accessoryHeight = accessoryMoreHeight
        this.showAccessory()
    }

    @action.bound
    hideMore() {
        this.accessoryType = 'more'
        this.hideAccessory()
    }

    @action.bound
    showAccessory() {
        this.accessory = true
    }

    @action.bound
    hideAccessory() {
        this.accessoryHeight = 0
        this.accessory = false
    }

    @action.bound
    reset(){
        this.accessory = false
        this.accessoryType = 'emoji'
        this.accessoryHeight = 0
    }
}
export const chatOptionStore = new ChatOptionStore()

const accessoryEmojiHeight = Platform.select({
    ios: 326 - sizes.safeAreaViewBottom,
    android: 274
}) || 0

const accessoryMoreHeight = Platform.select({
    // ios: 54 + 200,
    // android: 62 + 200
    ios: 326 - sizes.safeAreaViewBottom,
    android: 274
}) || 0

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            listView: {
                width: sizes.screen.width
            },
            listWarp: {
                flex: 1,
                backgroundColor: colors.background
            },
            container: {
                flex: 1,
                backgroundColor: colors.background
            },
            headerCheckedIcon: {
                position: 'absolute',
                right: sizes.gap - 4,
                bottom: -1
            }
        })
    }
})

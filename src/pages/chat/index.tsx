/**
 * Chat
 * @file chat
 * @module pages/chat/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList, Text } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import fonts from '@app/style/fonts'
import mixins from '@app/style/mixins'
import { IBlnScanerURL, IBlnScanMessage } from '@app/types/bln'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { ButtonResult } from '@app/components/ui/NoResult'
import { verifyAccount } from '@app/utils/bln'
import { TouchableView } from '@app/components/common/touchable-view'
import { getHeaderButtonStyle } from '@app/style/mixins'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { ChatMessageListItem } from '@app/components/ui/chat/list'
import { ChatRoutes, HomeRoutes, SettingsRoutes } from '@app/routes'
import contactStore from '@app/stores/contact';
import { chatStore } from '@app/pages/chat/stores/chat'

export type ListElement = RefObject<FlatList<IBlnScanMessage>>
export interface IIndexProps extends IPageProps { }



import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuTrigger,
    renderers,
    MenuOption,
} from 'react-native-popup-menu';
import { RowList } from '@app/components/common/list-title'
const { Popover } = renderers

@observer export class ChatScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
        // init
        contactStore.init(accountStore.currentAddress)
        contactStore.refresh(accountStore.currentAddress)
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            headerTitle: () => (<AutoI18nTitle
                color={colors.textDefault}
                size={18}
                i18nKey={LANGUAGE_KEYS.CHAT}
            />),
            headerRight: () => <Observer render={() => (
                <Menu renderer={Popover} rendererProps={{ placement: 'bottom' }}>
                    <MenuTrigger>
                        <FontAwesome5
                            name="plus"
                            {...getHeaderButtonStyle(18)}
                            color={colors.textTitle}
                        />
                    </MenuTrigger>
                    <MenuOptions customStyles={{
                        optionsContainer: {
                            paddingLeft: 0,
                            paddingRight: 0,
                            paddingTop: 0,
                            paddingBottom: 0,
                            flexDirection: 'column'
                        }
                    }}>
                        <MenuOption
                            onSelect={() => navigation.navigate(ChatRoutes.CreateGroup)}
                            style={{
                                borderBottomWidth: 1,
                                borderBottomColor: colors.border
                            }}
                        >
                            <RowList
                                leading={<FontAwesome5
                                    name="comments"
                                    color={colors.textTitle}
                                    size={16}
                                />}
                                trailing={<Text>{i18n.t(LANGUAGE_KEYS.GROUP)}</Text>}
                            />
                        </MenuOption>
                        <MenuOption
                            onSelect={() => navigation.navigate(ChatRoutes.NewChat)}
                            style={{
                                borderBottomWidth: 1,
                                borderBottomColor: colors.border
                            }}
                        >
                            <RowList
                                leading={<FontAwesome5
                                    name="comment"
                                    color={colors.textTitle}
                                    size={16}
                                />}
                                trailing={<Text>{i18n.t(LANGUAGE_KEYS.CHAT)}</Text>}
                            />
                        </MenuOption>
                        <MenuOption
                            onSelect={() => navigation.navigate(HomeRoutes.QrScanner, {
                                callback: (data: any)=>{
                                    if(data.groupid){
                                        navigation.navigate(ChatRoutes.GroupApply, {
                                            group: {},
                                            groupid: data.groupid
                                        })
                                    }
                                }
                            })}
                            style={{
                                borderBottomWidth: 1,
                                borderBottomColor: colors.border
                            }}
                        >
                            <RowList
                                leading={<FontAwesome5
                                    name="qrcode"
                                    color={colors.textTitle}
                                    size={16}
                                />}
                                trailing={<Text>{i18n.t(LANGUAGE_KEYS.SCAN_QRCODE)}</Text>}
                            />
                        </MenuOption>
                        <MenuOption
                            onSelect={() => navigation.navigate(ChatRoutes.GroupLists)}
                        >
                            <RowList
                                leading={<FontAwesome5
                                    name="bullhorn"
                                    color={colors.textTitle}
                                    size={16}
                                />}
                                leadingStyle={{
                                    bottom: -5
                                }}
                                trailing={<Text>{'Groups'}</Text>}
                            />
                        </MenuOption>
                    </MenuOptions>
                </Menu>
            )} />
        }
    }

    private unsubscribe: any;

    componentDidMount() {
        this.showLoading = true
        // init chat list
        chatStore.getList(accountStore.currentAddress).then((lists) => this.updateResultData(lists))
        // Listen for screen focus event
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
    }

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }

    onScreenFocus = () => {
        this.showLoading = false
        this.fetchLists().finally(() => {
            this.showLoading = true
        })
    }

    private listElement: ListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }
    @observable private isLoading: boolean = false

    @computed
    private get ListData(): IBlnScanMessage[] {
        return chatStore.lists
    }

    private showLoading: boolean = false

    @action
    private updateLoadingState(loading: boolean) {
        if (!this.showLoading) {
            return
        }
        this.isLoading = loading
    }

    @action
    private updateResultData(result: Array<IBlnScanMessage>, save?: boolean) {
        if (save) {
            chatStore.upsertsList(accountStore.currentAddress, result)
            // chatStore.setListWithStorage(accountStore.currentAddress, result)
        } else {
            chatStore.setList(accountStore.currentAddress, result)
        }
    }

    @boundMethod
    private fetchLists(): Promise<any> {
        this.updateLoadingState(true)
        return chatStore.refreshChatMap().finally(() => this.updateLoadingState(false))
    }

    private getIdKey(tx: IBlnScanMessage, index?: number): string {
        return `index:${index}:sep:${tx.to}`
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        const { styles } = obStyles
        return (
            <Observer
                render={() => (
                    <View style={styles.buttonResult}>
                        <ButtonResult
                            style={styles.buttonResult}
                            title={i18n.t(LANGUAGE_KEYS.CHAT_NO_CONTACT)}
                            button={i18n.t(LANGUAGE_KEYS.CHAT_START_CONTACT)}
                            onPress={() => {
                                this.onPressNavigation(ChatRoutes.NewChat)
                            }}
                        />
                    </View>
                )}
            />
        )
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.listWarp}>
                <MyFlatList
                    style={obStyles.styles.listView}
                    data={this.ListData}
                    ref={this.listElement}
                    // // 列表为空时渲染
                    ListEmptyComponent={this.renderListEmptyView}
                    // // 当前列表 loading 状态
                    refreshing={this.isLoading}
                    // // 刷新
                    onRefresh={this.fetchLists}
                    // // 唯一 ID
                    keyExtractor={this.getIdKey}
                    // 单个主体
                    renderItem={({ item: account, index }) => {
                        let name = account.group ? chatGroupStore.getGroupName(account.groupInfo, accountStore.currentMnemonic) : userStore.getUserName(account.to)
                        let text = account.text;
                        let logo = account.group ? chatGroupStore.getGroupLogo(account.groupInfo, accountStore.currentMnemonic) : userStore.getUserImage(account.to)
                        if (text) {
                            if (account.group) {
                                try {
                                    text = MSG.decodeGroupChatContent(text, !!account.groupInfo.isPrivate, chatKeyStore.getSK(accountStore.currentMnemonic, accountStore.currentAddress, account.to))       
                                } catch (error) {

                                }
                            } else {
                                try {
                                    text = MSG.decodeChatContent(accountStore.currentMnemonic, account.to, account.text)
                                } catch (error) {
                                    try {
                                        text = MSG.decodeChatContent(accountStore.currentMnemonic, account.from, account.text)
                                    } catch (error) {

                                    }
                                }
                            }
                            try {
                                text = MSG.decodeChatContent(accountStore.currentMnemonic, account.to, account.text)
                            } catch (error) {
                                try {
                                    text = MSG.decodeChatContent(accountStore.currentMnemonic, account.from, account.text)
                                } catch (error) {

                                }
                            }
                        }
                        if (MSG.isImage(text)) {
                            text = '[image]'
                        }
                        if (MSG.isVideo(text)) {
                            text = '[video]'
                        }
                        if (MSG.isAudio(text)) {
                            text = '[audio]'
                        }
                        return (
                            <ChatMessageListItem
                                name={name}
                                last={text}
                                avatar={logo}
                                time={account.time}
                                onPress={() => {
                                    this.onPressNavigation(ChatRoutes.Message, { address: account.to, name: name, user: accountStore.currentAddress, group: account.group, groupInfo: account.groupInfo, isPrivate: !account.group ? 1 : account.groupInfo && account.groupInfo.isPrivate ? 1 : 0})
                                }}
                            />
                        )
                    }}
                />
            </View>
        )
    }

    @boundMethod
    private onPressNavigation(routeName: ChatRoutes | SettingsRoutes | HomeRoutes, params?: any) {
        this.props.navigation.navigate(routeName, params)
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            centerContainer: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: sizes.gap

            },
            loadmoreViewContainer: {
                ...mixins.rowCenter,
                padding: sizes.goldenRatioGap
            },
            normalTitle: {
                ...fonts.base,
                color: colors.textSecondary
            },
            smallTitle: {
                ...fonts.small,
                color: colors.textSecondary
            },
            listView: {
                width: sizes.screen.width
            },
            listWarp: {
                flex: 1,
                backgroundColor: colors.cardBackground
            },
            container: {
                flex: 1,
                backgroundColor: colors.background
            },
            headerCheckedIcon: {
                position: 'absolute',
                right: sizes.gap - 4,
                bottom: -1
            },
            buttonResult: {
                height: sizes.screen.height - 175,
                justifyContent: "center"
            },

            input: {
                paddingLeft: 5,
                height: 60,
                textAlignVertical: 'top',
            },
            inputBox: {
                marginLeft: 20,
                marginRight: 20,
                marginTop: 30
            },
            scanner: {
                paddingRight: 15
            },
            bootomBar: {
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 8,
                paddingTop: 8
            },
            lists: {
                flex: 1
            }
        })
    }
})

import { LabelListTitle } from '@app/components/ui/list-title'
import TextInput from '@app/components/common/text-input'
import BottomButton from '@app/components/ui/bottom-button'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { ScrollView } from 'react-native-gesture-handler'
import showToast from '@app/services/toast'
import BottomBar from '@app/components/ui/BottomBar'
import { userStore } from '@app/stores/users'
import { MyFlatList } from '@app/components/common/FlatList'
import { AutoI18nTitle } from '@app/components/layout/title'
import { isFunction } from 'lodash'
import MSG from '@app/utils/msg'
import { chatGroupStore } from './stores/group'
import { chatKeyStore } from './stores/key'

class FormStore {

    @observable name?: string
    @observable address?: string

    @action.bound
    changeName(v?: string) {
        this.name = v
    }

    @action.bound
    changeAddress(v?: string, show?: boolean) {
        this.address = v
    }

    @boundMethod
    reset() {
        this.address = undefined
        this.name = undefined
    }

    @computed
    get verify(): boolean {
        return this.address != undefined && this.address != ''
    }
}
export const formStore = new FormStore()


@observer export class NewChatScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
    }

    componentDidMount() {
        formStore.reset()
        if (this.props.route.params && this.props.route.params.address) {
            formStore.changeAddress(this.props.route.params.address, true)
        }
    }

    onChangeAddress(text: string) {
        if (verifyAccount(text)) {
            formStore.changeAddress(text)
        }
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={<Text>{i18n.t(LANGUAGE_KEYS.ADDRESS)}</Text>}
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={text => this.onChangeAddress(text)}
                            value={formStore.address}
                            numberOfLines={2}
                            multiline={true}
                            placeholder={i18n.t(LANGUAGE_KEYS.INPUT_ADDRESS)}
                            trailing={<TouchableView style={styles.scanner}
                                onPress={() => {
                                    this.onPressScaner()
                                }}
                            >
                                <Icon name={'qrcode'} size={20}></Icon>
                            </TouchableView>}
                        />
                    </View>
                </ScrollView>
                <BottomBar>
                    <View style={styles.bootomBar}>
                        <BottomButton text={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={this.onPressSubmit} />
                    </View>
                </BottomBar>
            </View>
        )
    }

    @boundMethod
    onSuccessScaner(uri: IBlnScanerURL) {
        if (uri.address) {
            formStore.changeAddress(uri.address)
        }
    }

    @boundMethod
    private onPressScaner() {
        this.props.navigation.navigate(HomeRoutes.QrScanner, {
            callback: this.onSuccessScaner
        })
    }

    @boundMethod
    private onPressSubmit() {
        if (!formStore.verify) {
            return showToast(i18n.t(LANGUAGE_KEYS.INPUT_ERROR))
        }
        this.props.navigation.replace(ChatRoutes.Message, { address: formStore.address, name: `${formStore.name ?? formStore.address}`, user: accountStore.currentAddress })
    }
}
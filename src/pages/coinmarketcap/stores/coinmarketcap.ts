/**
 * coinmarketcap store
 * @file coinmarketcap store
 * @module app/pages/coinmarketcap/stores/coinmarketcap
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { STORAGE } from '@app/constants/storage'
import { ICoinCap, ICoinCapTicker } from '@app/types/bln'
import API from '@app/services/api'
import Coins from './map.json';
import { optionStore } from '@app/stores/option'
import storeapi from '@app/services/storeapi'
import BigNumber from 'bignumber.js'

export class CoinmarketcapStore extends Map<String, ICoinCap> {

    constructor() {
        super()
        this.init()
    }

    private init() {
        // init
        Coins.forEach((coin) => {
            this.set(coin.name, coin as ICoinCap)
        })
        // init cache
        storage.get<Array<ICoinCap>>(STORAGE.LOCAL_COINMARKETCAP_LIST)
            .then(lists => {
                if (lists && lists.length) {
                    lists.forEach((coin) => {
                        const v = this.get(coin.name)
                        if (v) {
                            this.set(coin.name, { ...v, ...coin, quote: { ...v.quote, ...coin.quote } })
                        } else {
                            this.set(coin.name, coin)
                        }
                    })
                }
                return lists
            }).then(() => {
                // init bln
                this.set(this.bln.name, this.bln)
            })
    }

    @computed
    get bln() {
        return {
            ...{
                "id": 9999,
                "name": "Blacknet",
                "symbol": "BLN",
                "slug": "blacknet",
                "max_supply": 0,
                "url": "https://blacknet.ninja/images/logo.png",
                "circulating_supply": 1018860665,
                "total_supply": 1018860665,
                "quote": {}
            }, ...this.get('Blacknet'), url: "https://blacknet.ninja/images/logo.png"
        } as ICoinCap
    }

    @boundMethod
    updateBLNPrice(ticker: ICoinCapTicker) {
        const price = ticker.price
        const bln = this.get('Blacknet')
        const btc = this.get('Bitcoin')
        if (!bln || !btc) {
            return
        }
        let btcUSDTPrice, btcCNYPrice;
        if (btc.quote['CNY']) {
            btcCNYPrice = btc.quote['CNY'].price
        }
        if (btc.quote['USD']) {
            btcUSDTPrice = btc.quote['USD'].price
        }
        // cny
        bln.quote['CNY'] = { ...bln.quote['CNY'], ...ticker, price: price }
        // btc
        if (btcCNYPrice) {
            bln.quote['BTC'] = { ...bln.quote['BTC'], ...ticker, price: new BigNumber(price).dividedBy(btcCNYPrice).toNumber() }
        }
        // usdt
        if (btcUSDTPrice && btcCNYPrice) {
            bln.quote['USD'] = { ...bln.quote['USD'], ...ticker, price: new BigNumber(price).dividedBy(btcCNYPrice).multipliedBy(btcUSDTPrice).toNumber() }
        } else {
            bln.quote['USD'] = { ...bln.quote['USD'], ...ticker, price: new BigNumber(price).dividedBy(6.5).toNumber() }
        }
        this.set('Blacknet', bln)
    }

    @computed
    get coins() {
        return this.getCoinList()
    }

    @boundMethod
    update(coin: ICoinCap) {
        const v = this.get(coin.name)
        if (v) {
            this.set(coin.name, { ...v, ...coin, quote: { ...v.quote, ...coin.quote } })
        } else {
            this.set(coin.name, coin)
        }
    }

    @boundMethod
    getCoinList(convert: string = optionStore.currency): Array<ICoinCap> {
        return Array.from(this, ([key, value]) => {
            if (value.quote[convert]) {
                value.currentQuote = convert
                value.currentQuoteData = value.quote[convert]
            }
            return { ...value }
        })
    }

    @boundMethod
    upsert(lists: Array<ICoinCap>) {
        lists.forEach((coin) => {
            const v = this.get(coin.name)
            if (v) {
                this.set(coin.name, { ...v, ...coin, quote: { ...v.quote, ...coin.quote } })
            } else {
                this.set(coin.name, coin)
            }
        })
        storage.set(STORAGE.LOCAL_COINMARKETCAP_LIST, this.coins)
    }

    @boundMethod
    refresh(page: number = 1, convert: string = optionStore.currency): Promise<any> {
        return API.fetchCoinList(page, { convert, limit: 2000 })
            .then((lists) => {
                if (lists && lists.txns) {
                    this.upsert(lists.txns)
                    storeapi.getBlnPrice().then((price) => this.updateBLNPrice(price))
                }
                return lists
            })
    }

    @boundMethod
    refreshBLN(): Promise<any> {
        return storeapi.getBlnPrice().then((price) => this.updateBLNPrice(price))
    }

    @boundMethod
    search(text: string): Array<ICoinCap> {
        const reg = new RegExp(text, 'ig');
        const arr: Array<ICoinCap> = [];
        this.forEach((coin) => {
            if (reg.test([coin.name, coin.symbol, coin.slug].join(' '))) {
                if (coin.quote[optionStore.currency]) {
                    coin.currentQuote = optionStore.currency
                    coin.currentQuoteData = coin.quote[optionStore.currency]
                }
                arr.push({ ...coin })
            }
        })
        return arr
    }
}

export default new CoinmarketcapStore()
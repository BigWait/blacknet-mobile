/**
 * Coinmarketcap
 * @file Coinmarketcap
 * @module pages/coinmarketcap/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed, reaction } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import API from '@app/services/api'
import { ICoinCap } from '@app/types/bln'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { NoResult } from '@app/components/ui/NoResult'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { ChatRoutes, DiscoverRoutes, HomeRoutes } from '@app/routes'
import { Text } from '@app/components/common/text'
import { IRequestParams, ITxPaginate, ITxResultPaginate } from '@app/types/http'
import usersService from '@app/services/users'
import { MyFlatList, MyFlatListFooter_LOADING, MyFlatListFooter_LOADMORE, MyFlatListFooter_NO_MORE } from '@app/components/common/FlatList'
import { isFunction } from 'lodash'
import ListTitle from '@app/components/common/list-title'
import Image from '@app/components/common/image'
import coinmarketcapStore from './stores/coinmarketcap'
import { CURRENCY, optionStore } from '@app/stores/option'
import Numeral from '@app/utils/numeral'

type ITxResultPaginateLists = ITxResultPaginate<ICoinCap[]>
type CoinsListElement = RefObject<FlatList<ICoinCap>>

class FilterStore {
    constructor() {
        // 当过滤条件变化时进行重请求
        reaction(
            () => optionStore.currency,
            (currency: CURRENCY) => this.change(currency)
        )
    }

    @observable quote: string = optionStore.currency

    @action.bound
    change(type: CURRENCY) {
        this.quote = type
    }
}
export const filterStore = new FilterStore()

export interface IIndexProps extends IPageProps { }

@observer export class CoinmarketcapScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
        // users
        usersService.refresh()
        // init list
        this.initList()
    }

    @boundMethod
    private initList() {
        this.lists = coinmarketcapStore.getCoinList(filterStore.quote)
    }

    private listElement: CoinsListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }

    @observable private isLoading: boolean = false
    @observable.shallow private lists: ICoinCap[] = []
    @observable.ref private params: IRequestParams = {}
    @observable.ref private pagination: ITxPaginate | null = null

    @computed
    private get ListData(): ICoinCap[] {
        return this.lists.slice() || []
    }

    @computed
    private get isNoMoreData(): boolean {
        return (
            !!this.pagination &&
            this.pagination.txns_len < 100
        )
    }

    private showLoading: boolean = false
    private unsubscribe: any;

    componentDidMount() {
        this.showLoading = true
        // Listen for screen focus event
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
    }

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }

    onScreenFocus = () => {
        this.showLoading = false
        this.fetchLists().finally(() => {
            this.showLoading = true
        })
    }

    @action
    private updateLoadingState(loading: boolean) {
        if (!this.showLoading) {
            return
        }
        this.isLoading = loading
    }

    @action
    private updateResultData(result: ITxResultPaginateLists) {
        const { txns, pagination } = result
        this.pagination = pagination
        if (pagination.current_page > 1) {
            this.lists.push(...txns)
        } else {
            this.lists = [...txns]
        }
    }

    @boundMethod
    private handleLoadmoreList() {
        if (!this.isNoMoreData && !this.isLoading && this.pagination) {
            this.fetchLists(this.pagination.current_page + 1)
        }
    }

    @boundMethod
    private fetchLists(page: number = 1): Promise<any> {
        this.updateLoadingState(true)
        let res: Promise<any>
        if (page === 1) {
            res = coinmarketcapStore.refresh(page, filterStore.quote)
        } else {
            res = API.fetchCoinList(page, { ...this.params, convert: filterStore.quote })
        }
        return res.then((json) => this.updateResultData(json))
            .finally(() => this.updateLoadingState(false))
    }

    private getIdKey(tx: ICoinCap, index?: number): string {
        return `index:${index}:sep:${tx.slug}`
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
            <Observer
                render={() => (
                    <NoResult />
                )}
            />
        )
    }

    // 渲染脚部的三种状态：空、加载中、无更多、上拉加载
    @boundMethod
    private renderListFooterView(): JSX.Element | null {
        const { styles } = obStyles
        if (!this.lists.length) {
            return null
        }
        if (this.lists.length < 100 || this.isNoMoreData) {
            return (
                <MyFlatListFooter_NO_MORE />
            )
        }
        if (this.isLoading) {
            return (
                <MyFlatListFooter_LOADING />
            )
        }
        return (
            <MyFlatListFooter_LOADMORE />
        )
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.listWarp}>
                <MyFlatList
                    style={obStyles.styles.listView}
                    data={this.ListData}
                    ref={this.listElement}
                    // 列表为空时渲染
                    ListEmptyComponent={this.renderListEmptyView}
                    // 加载更多时渲染
                    ListFooterComponent={this.renderListFooterView}
                    // 当前列表 loading 状态
                    refreshing={this.isLoading}
                    // 刷新
                    onRefresh={this.fetchLists}
                    // 加载更多
                    onEndReached={this.handleLoadmoreList}
                    // 唯一 ID
                    keyExtractor={this.getIdKey}
                    // 单个主体
                    renderItem={({ item: coin, index }) => {
                        return (
                            <Observer
                                render={() => (
                                    <View style={styles.seq}>
                                        <CoinList data={{ ...coin, rank: index + 1 }} />
                                    </View>
                                )}
                            />
                        )
                    }}
                />
            </View>
        )
    }

    @boundMethod
    private onPressNavigation(routeName: ChatRoutes | DiscoverRoutes | HomeRoutes, params?: any) {
        this.props.navigation.navigate(routeName, params)
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            seq: {
                margin: 0
            },
            listView: {
                width: sizes.screen.width
            },
            listWarp: {
                flex: 1,
                paddingTop: 5,
                backgroundColor: colors.cardBackground
            }
        })
    }
})

export interface CoinListProps {
    data: ICoinCap
    onPress?(): void
}

@observer
export class CoinList extends Component<CoinListProps> {
    render() {
        const { styles } = obStyles
        return (
            <ListTitle
                contentsStyle={{ borderTopWidth: 0, paddingLeft: 15 }}
                leadingStyle={{ width: 'auto' }}
                leading={this.renderIcon()}
                title={this.renderTitle()}
                subtitle={this.renderSubtitle()}
                trailing={this.renderTrailing()}
                trailingStyle={{ borderTopWidth: 0, paddingLeft: 10, marginRight: 10 }}
                style={{ borderBottomColor: colors.border, borderBottomWidth: 1, marginLeft: 0, marginRight: 0 }}
            />
        );
    }
    get quote() {
        const { data } = this.props
        if (!data.quote[filterStore.quote]) {
            return null
        }
        return data.quote[filterStore.quote]
    }
    renderIcon() {
        return <View style={{ flexDirection: "row", alignItems: 'center' }}>
            <Text style={{ width: 30, fontSize: 18, textAlign: 'center' }}>{this.props.data.rank}</Text>
            <Image
                style={{ width: 40, height: 40 }}
                source={{ uri: this.props.data.url }}
            />
        </View>
    }
    renderTitle() {
        return <Text style={{ fontWeight: 'bold' }}>{this.props.data.name}</Text>
    }
    renderSubtitle() {
        let percent;
        if (this.quote) {
            percent = <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: 'center', alignSelf: "center" }}>
                <FontAwesome5
                    name={this.quote.percent_change_24h > 0 ? "sort-up" : "sort-down"}
                    color={this.quote.percent_change_24h > 0 ? colors.transferIn : colors.transferOut}
                    size={18}
                />
                <Text>{Numeral.percentage(this.quote.percent_change_24h)}</Text>
            </View>
        }
        return <View style={{ flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
            {/* <View style={{backgroundColor: colors.border, padding: 2, paddingLeft: 4, paddingRight: 4, borderRadius: 2, marginRight: 4}}><Text>{this.props.data.rank}</Text></View> */}
            <Text style={{ marginRight: 10 }}>{this.props.data.symbol}</Text>
            {percent}
        </View>
    }
    renderTrailing() {
        if (!this.quote) {
            return null
        }
        return <View style={{ flexDirection: 'column', justifyContent: "flex-start", alignItems: 'flex-end' }}>
            <Text style={{ fontWeight: 'bold' }}>{Numeral.localString(this.quote.price)}</Text>
            <Text>{i18n.t(LANGUAGE_KEYS.MARKET_VALUE)} {Numeral.value(this.quote.market_cap)}</Text>
        </View>
    }
}
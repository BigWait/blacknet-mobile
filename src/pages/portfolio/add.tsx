/**
 * portfolio add
 * @file portfolio add 
 * @module pages/portfolio/add
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, ScrollView } from 'react-native'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import { boundMethod } from 'autobind-decorator'
import { SafeAreaView } from 'react-native-safe-area-context'
import TextInput from '@app/components/common/text-input'
import { IPageProps } from '@app/types/props'
import Image from '@app/components/common/image'
import { ICoinCap } from '@app/types/bln'
import sizes from '@app/style/sizes'
import { IS_IOS } from '@app/config'
import BigNumber from 'bignumber.js'
import portfolioStore from './stores'
import { BLNText, TextLink } from '@app/components/common/text'
import Numeral from '@app/utils/numeral'

class FormStore {
    @observable amount: string = ""
    @observable coin?: ICoinCap
    @observable placeholder: string = "0.00"
    @action.bound
    changeCoin(coin: ICoinCap) {
        this.coin = coin
    }
    @action.bound
    onBlur() {
        this.placeholder = "0.00"
    }
    @action.bound
    onFocus() {
        this.placeholder = ""
    }
    @action.bound
    changeAmount(amount: string) {
        this.amount = amount
    }
    @computed
    get verify(): boolean {
        // && this.amount != undefined && this.amount != '' && new BigNumber(this.amount).toNumber() > 0
        return !!this.coin?.symbol;
    }
    @computed
    get totalValue(): string {
        return `${this.coin?.currentQuote} ` + Numeral.total(new BigNumber(this.amount || '0').multipliedBy(this.coin?.currentQuoteData?.price || '1').toString())
    }
    @computed
    get data(): any {
        return {
            amount: this.amount || 0,
            symbol: this.coin?.symbol,
            date: new Date().getTime(),
            name: this.coin?.name
        }
    }
    @action.bound
    reset() {
        this.amount = ""
        this.placeholder = ''
        this.coin = undefined
    }
}
export const formStore = new FormStore()
export interface IIndexProps extends IPageProps { }
@observer export class PortfolioAddScreen extends Component<IIndexProps> {

    @observable private data: ICoinCap;

    constructor(props: any) {
        super(props)
        this.data = this.props.route.params.data
    }

    componentDidMount() {
        formStore.reset()
        formStore.changeCoin(this.data)
        formStore.changeAmount(this.props.route.params.amount || '')
    }

    @boundMethod
    onPressSubmit() {
        if (!formStore.verify) {
            return showToast(i18n.t(LANGUAGE_KEYS.INPUT_ERROR))
        }
        portfolioStore.upsert(formStore.data)
        this.props.navigation.goBack()
    }
    @boundMethod
    onPressRemove() {
        portfolioStore.remove(this.data.name)
        this.props.navigation.goBack()
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.container}>
                <ScrollView style={styles.content}>
                    {this.renderTitle()}
                    {this.renderInput()}
                    {this.renderTotalValue()}
                    {this.renderRemove()}
                </ScrollView>
                {this.renderFooter()}
            </View>
        )
    }
    renderRemove() {
        if (!this.props.route.params.amount && this.props.route.params.amount != 0) {
            return null
        }
        return <TextLink
            onPress={this.onPressRemove}
            style={{ marginTop: 15, textAlign: 'center' }}
        >{i18n.t(LANGUAGE_KEYS.PORTFOLIO_REMOVE)}</TextLink>
    }
    renderTotalValue() {
        const { styles } = obStyles
        return <View style={styles.totalView}>
            <BLNText style={{ fontWeight: 'bold', fontSize: 16 }}>{i18n.t(LANGUAGE_KEYS.TOTAL)} {formStore.totalValue}</BLNText>

        </View>
    }
    renderInput() {
        const { styles } = obStyles
        return <View style={styles.inputView}>
            <BLNText style={{ fontSize: 18, color: colors.textMuted }}>{i18n.t(LANGUAGE_KEYS.INPUT_AMOUNT)}</BLNText>
            <View style={styles.inputWarp}>
                <TextInput
                    autoFocus={true}
                    style={{ textAlign: 'center', fontSize: 24, fontWeight: 'bold', color: colors.textDefault, backgroundColor: 'transparent', height: 'auto' }}
                    placeholder={formStore.placeholder}
                    onBlur={formStore.onBlur}
                    onFocus={formStore.onFocus}
                    keyboardType={'numeric'}
                    viewStyle={{ borderWidth: 0, borderBottomWidth: 1, borderBottomColor: colors.border, margin: 0, paddingBottom: IS_IOS ? 15 : 5, paddingTop: IS_IOS ? 10 : 5 }}
                    value={formStore.amount}
                    onChangeText={formStore.changeAmount}
                />
            </View>
        </View>
    }
    renderTitle() {
        const { styles } = obStyles
        return <View style={styles.titleView}>
            <Image style={styles.titleImage} source={{ uri: this.data.url }} />
            <BLNText>#{this.data.name}</BLNText>
            <Text style={{ marginLeft: 10, color: colors.textMuted }}>{this.data.symbol}</Text>
        </View>
    }
    renderFooter() {
        const { styles } = obStyles
        return <SafeAreaView>
            <View style={styles.bootomBar}>
                <BottomButton text={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={this.onPressSubmit} />
            </View>
        </SafeAreaView>
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            bootomBar: {
                height: 40,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 10
            },
            container: {
                flex: 1
            },
            content: {
                flex: 1
            },
            titleView: {
                flexDirection: "row",
                alignItems: 'center',
                justifyContent: "center",
                marginTop: 15
            },
            titleImage: {
                width: 25,
                height: 25,
                borderRadius: 15,
                marginRight: 10
            },
            inputView: {
                marginTop: 30,
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: "center"
            },
            inputWarp: {
                width: sizes.screen.width - 100,
                marginTop: 10
            },
            totalView: {
                marginTop: 30,
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: "center"
            }
        })
    }
})
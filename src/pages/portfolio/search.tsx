/**
 * Portfolio
 * @file Portfolio
 * @module pages/portfolio/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList, StatusBar } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import { ICoinCap, IPortfolioData } from '@app/types/bln'
import { NoResult } from '@app/components/ui/NoResult'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { Text, TextLink } from '@app/components/common/text'
import { MyFlatList } from '@app/components/common/FlatList'
import { isFunction } from 'lodash'
import ListTitle from '@app/components/common/list-title'
import Image from '@app/components/common/image'
import coinmarketcapStore from '@app/pages/coinmarketcap/stores/coinmarketcap'
import SearchInput from '@app/components/common/searchInput'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import portfolioStore from './stores'
import { IS_ANDROID } from '@app/config'

type CoinsListElement = RefObject<FlatList<ICoinCap>>

export interface IIndexProps {
    onPress?(coin: ICoinCap, p?: IPortfolioData): void
    onCancelPress?(): void
    showCancel?: boolean
}

@observer export class SearchPortfolio extends Component<IIndexProps> {

    componentDidMount() {
        this.lists = coinmarketcapStore.coins.slice()
        if (IS_ANDROID) StatusBar.setBackgroundColor(colors.cardBackground)
    }

    componentWillUnmount() {
        if (IS_ANDROID) StatusBar.setBackgroundColor('transparent')
    }

    private listElement: CoinsListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }

    @observable.shallow private lists: ICoinCap[] = []

    @computed
    private get ListData(): ICoinCap[] {
        return this.lists.slice()
    }

    @action
    private updateData(list: ICoinCap[]) {
        this.lists = list
    }

    private getIdKey(tx: ICoinCap, index?: number): string {
        return `index:${index}:sep:${tx.slug}`
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
            <Observer
                render={() => (
                    <NoResult />
                )}
            />
        )
    }

    // search change
    private timer: any
    @boundMethod
    onChangeText(text: string) {
        clearTimeout(this.timer)
        this.timer = setTimeout(() => {
            this.updateData(coinmarketcapStore.search(text))
        }, 206)
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.listWarp}>
                <View style={{ margin: 15, marginTop: 0, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                    <SearchInput onChangeText={this.onChangeText} />
                    {this.props.showCancel ?
                        <TextLink
                            onPress={this.props.onCancelPress}
                            style={{ marginLeft: 10 }}
                        >{i18n.t(LANGUAGE_KEYS.CANCEL)}</TextLink>
                        : null}
                </View>
                <MyFlatList
                    style={obStyles.styles.listView}
                    data={this.ListData}
                    ref={this.listElement}
                    // 列表为空时渲染
                    ListEmptyComponent={this.renderListEmptyView}
                    // 唯一 ID
                    keyExtractor={this.getIdKey}
                    // 单个主体
                    renderItem={({ item: data, index }) => {
                        data = { ...data, rank: index + 1 }
                        let coin = portfolioStore.get(data.name)
                        return (
                            <Observer
                                render={() => (
                                    <View style={styles.seq}>
                                        <CoinList
                                            data={data}
                                            coin={coin}
                                            onPress={() => {
                                                if (isFunction(this.props.onPress)) {
                                                    this.props.onPress(data, coin)
                                                }
                                            }}
                                        />
                                    </View>
                                )}
                            />
                        )
                    }}
                />
            </View>
        )
    }
}

export default SearchPortfolio

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            seq: {
                margin: 0
            },
            listView: {
                width: sizes.screen.width
            },
            listWarp: {
                flex: 1,
                paddingTop: 5,
                backgroundColor: colors.cardBackground
            }
        })
    }
})

export interface CoinListProps {
    data: ICoinCap
    coin?: IPortfolioData
    onPress?(): void
}

@observer
export class CoinList extends Component<CoinListProps> {
    render() {
        return (
            <ListTitle
                contentsStyle={{ borderTopWidth: 0, paddingLeft: 15 }}
                leadingStyle={{ width: 'auto', marginLeft: 15 }}
                leading={this.renderIcon()}
                title={this.renderTitle()}
                subtitle={this.renderSubtitle()}
                trailing={this.renderTrailing()}
                onPress={this.props.onPress}
                trailingStyle={{ borderTopWidth: 0, paddingLeft: 10, marginRight: 15 }}
                style={{ borderBottomColor: colors.border, borderBottomWidth: 1, marginLeft: 0, marginRight: 0 }}
            />
        );
    }
    renderIcon() {
        return <Image
            style={{ width: 30, height: 30 }}
            source={{ uri: this.props.data.url }}
        />
    }
    renderTitle() {
        return <Text style={{ fontWeight: 'bold' }}>{this.props.data.name}</Text>
    }
    renderSubtitle() {
        return <Text style={{ fontWeight: 'bold' }}>{this.props.data.symbol}</Text>
    }
    renderTrailing() {
        return <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            {this.props.coin ?
                <FontAwesome5 name="edit" size={20} color={colors.primary} />
                :
                <FontAwesome5 name="plus-circle" size={20} color={colors.primary} />
            }
        </View>
    }
}
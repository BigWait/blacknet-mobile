/**
 * Portfolio
 * @file Portfolio
 * @module pages/portfolio/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList, Modal } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import { ICoinCap, IPortfolioData } from '@app/types/bln'
import { ButtonResult } from '@app/components/ui/NoResult'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { ChatRoutes, DiscoverRoutes, HomeRoutes } from '@app/routes'
import { BLNText } from '@app/components/common/text'
import { MyFlatList } from '@app/components/common/FlatList'
import { isFunction } from 'lodash'
import ListTitle from '@app/components/common/list-title'
import Image from '@app/components/common/image'
import coinmarketcapStore from '@app/pages/coinmarketcap/stores/coinmarketcap'
import { optionStore } from '@app/stores/option'
import { AutoI18nTitle } from '@app/components/layout/title'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { TouchableView } from '@app/components/common/touchable-view'
import { getHeaderButtonStyle } from '@app/style/mixins'
import i18n from '@app/services/i18n'
import SearchPortfolio from './search'
import { SafeAreaView } from 'react-native-safe-area-context'
import portfolioStore from './stores'
import BigNumber from 'bignumber.js'
import Numeral from '@app/utils/numeral'
import { SortTouch } from '@app/components/ui/sort'

class FormStore {
    @observable isVisible: boolean = false

    @action.bound
    changeVisiable(isVisible: boolean = false) {
        this.isVisible = isVisible
    }
    @action.bound
    reset() {
        this.isVisible = false
        this.sortType = undefined
        this.sortFiled = undefined
    }

    @observable sortType?: 'up' | 'down' = undefined
    @observable sortFiled?: 'price' | 'hold' = undefined
    @action.bound
    changeSort(filed: 'price' | 'hold', sort?: 'up' | 'down') {
        this.sortFiled = filed
        this.sortType = sort
    }
}

export const formStore = new FormStore()

type CoinsListElement = RefObject<FlatList<IPortfolioData>>

export interface IIndexProps extends IPageProps { }

@observer export class PortfolioScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            headerTitle: () => (<AutoI18nTitle
                color={colors.textDefault}
                size={18}
                i18nKey={LANGUAGE_KEYS.PORTFOLIO}
            />),
            headerRight: () => <Observer render={() => (
                <TouchableView
                    accessibilityLabel=""
                    accessibilityHint=""
                    onPress={() => {
                        formStore.changeVisiable(true)
                    }}
                >
                    <FontAwesome5
                        name="plus"
                        {...getHeaderButtonStyle(18)}
                        color={colors.textTitle}
                    />
                </TouchableView>
            )} />
        }
    }

    constructor(props: IIndexProps) {
        super(props)
    }

    private listElement: CoinsListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }

    @observable private isLoading: boolean = false

    @computed
    get currency() {
        return optionStore.currency
    }

    @computed
    private get ListData(): IPortfolioData[] {
        const datas = portfolioStore.getData()
        if (formStore.sortFiled && formStore.sortType) {
            datas.sort((a, b) => {
                const tickerA = coinmarketcapStore.get(a.name)
                const tickerB = coinmarketcapStore.get(b.name)
                if (formStore.sortFiled === 'hold') {
                    if (formStore.sortType === 'up') {
                        return new BigNumber(a.amount).multipliedBy(tickerA?.currentQuoteData?.price || "0").toNumber() - new BigNumber(b.amount).multipliedBy(tickerB?.currentQuoteData?.price || "0").toNumber()
                    } else {
                        return new BigNumber(b.amount).multipliedBy(tickerB?.currentQuoteData?.price || "0").toNumber() - new BigNumber(a.amount).multipliedBy(tickerA?.currentQuoteData?.price || "0").toNumber()
                    }
                } else {
                    if (formStore.sortType === 'up') {
                        return new BigNumber(tickerA?.currentQuoteData?.percent_change_24h || '0').toNumber() - new BigNumber(tickerB?.currentQuoteData?.percent_change_24h || '0').toNumber()
                    } else {
                        return new BigNumber(tickerB?.currentQuoteData?.percent_change_24h || '0').toNumber() - new BigNumber(tickerA?.currentQuoteData?.percent_change_24h || '0').toNumber()
                    }
                }
            })
        }
        return datas;
    }

    @boundMethod
    totalValue(): string {
        let t = new BigNumber(0)
        this.ListData.forEach((p) => {
            const ticker = coinmarketcapStore.get(p.name)
            if (ticker?.quote[this.currency]) {
                t = t.plus(new BigNumber(p.amount).multipliedBy(ticker?.quote[this.currency].price || "0"))
            }
        })
        return Numeral.total(t)
    }

    private showLoading: boolean = false
    private unsubscribe: any;

    componentDidMount() {
        this.showLoading = true
        // Listen for screen focus event
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
    }

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }

    onScreenFocus = () => {
        this.showLoading = false
        coinmarketcapStore.refresh().finally(() => {
            this.showLoading = true
        })
    }

    @action
    private updateLoadingState(loading: boolean) {
        if (!this.showLoading) {
            return
        }
        this.isLoading = loading
    }

    @boundMethod
    private fetchLists(page: number = 1): Promise<any> {
        this.updateLoadingState(true)
        portfolioStore.initAccount();
        return coinmarketcapStore.refresh()
            .finally(() => this.updateLoadingState(false))
    }

    private getIdKey(tx: IPortfolioData, index?: number): string {
        return `index:${index}:sep:${tx.symbol}`
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        const { styles } = obStyles
        return (
            <Observer
                render={() => (
                    <View style={styles.buttonResult}>
                        <ButtonResult
                            style={styles.buttonResult}
                            title={i18n.t(LANGUAGE_KEYS.PORTFOLIO_NODATA)}
                            button={i18n.t(LANGUAGE_KEYS.PORTFOLIOADD)}
                            onPress={() => {
                                formStore.changeVisiable(true)
                            }}
                        />
                    </View>
                )}
            />
        )
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.listWarp}>
                {this.renderTotalHeader()}
                <MyFlatList
                    style={obStyles.styles.listView}
                    data={this.ListData}
                    ref={this.listElement}
                    // 列表为空时渲染
                    ListEmptyComponent={this.renderListEmptyView}
                    // 当前列表 loading 状态
                    refreshing={this.isLoading}
                    // 刷新
                    onRefresh={this.fetchLists}
                    // 唯一 ID
                    keyExtractor={this.getIdKey}
                    // 单个主体
                    renderItem={({ item: coin, index }) => {
                        return (
                            <Observer
                                render={() => (
                                    <View style={styles.seq}>
                                        <CoinList
                                            data={coin}
                                            onPress={() => {
                                                const ticker = coinmarketcapStore.get(coin.name)
                                                this.onPressNavigation(HomeRoutes.PortfolioAdd, { data: ticker, amount: coin.amount })
                                            }}
                                        />
                                    </View>
                                )}
                            />
                        )
                    }}
                    ListHeaderComponent={this.renderListHeaderComponent()}
                />
                <Modal
                    visible={formStore.isVisible}
                    hardwareAccelerated={true}
                    presentationStyle={"overFullScreen"}
                    animationType={'fade'}
                    transparent={true}
                >
                    <SafeAreaView style={{ flex: 1 }}>
                        <SearchPortfolio
                            onPress={(data: ICoinCap, coin: IPortfolioData) => {
                                formStore.changeVisiable(false)
                                this.onPressNavigation(HomeRoutes.PortfolioAdd, { data: data, amount: coin?.amount })
                            }}
                            onCancelPress={() => {
                                formStore.changeVisiable(false)
                            }}
                            showCancel={true}
                        />
                    </SafeAreaView>
                </Modal>
            </View>
        )
    }

    renderListHeaderComponent() {
        if (this.ListData.length < 1) {
            return null
        }
        return <ListTitle
            contentsStyle={{ borderTopWidth: 0, height: 'auto', alignItems: 'flex-end', flex: 2 }}
            leadingStyle={{ width: 'auto', height: 'auto', flex: 1 }}
            leading={<BLNText>{i18n.t(LANGUAGE_KEYS.ASSET)}</BLNText>}
            title={<SortTouch name={i18n.t(LANGUAGE_KEYS.PRICE)}
                direction={formStore.sortFiled === 'price' ? formStore.sortType : undefined}
                onChange={(d) => {
                    formStore.changeSort('price', d)
                }}
            />}
            trailing={<SortTouch name={i18n.t(LANGUAGE_KEYS.HOLD)}
                direction={formStore.sortFiled === 'hold' ? formStore.sortType : undefined}
                onChange={(d) => {
                    formStore.changeSort('hold', d)
                }}
            />}
            trailingStyle={{ borderTopWidth: 0, height: 'auto', alignItems: 'flex-end', flex: 2 }}
            style={{ borderBottomColor: colors.border, borderBottomWidth: 1, marginLeft: 0, marginRight: 0, padding: 10 }}
        />
    }

    renderTotalHeader() {
        if (this.ListData.length < 1) {
            return null
        }
        const { styles } = obStyles
        return (<View style={styles.totalHeader}>
            <BLNText style={{ color: colors.background, fontSize: 24, marginBottom: 10 }}>{this.totalValue()}</BLNText>
            <BLNText style={{ color: colors.background }}>{i18n.t(LANGUAGE_KEYS.TOTAL)} {this.currency}</BLNText>
        </View>)
    }

    @boundMethod
    private onPressNavigation(routeName: ChatRoutes | DiscoverRoutes | HomeRoutes, params?: any) {
        this.props.navigation.navigate(routeName, params)
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            totalHeader: {
                margin: 10,
                backgroundColor: colors.primary,
                shadowColor: colors.background,
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.4,
                borderRadius: 4,
                padding: 15
            },
            buttonResult: {
                height: sizes.screen.height - 175,
                justifyContent: "center"
            },
            seq: {
                margin: 0
            },
            listView: {
                width: sizes.screen.width
            },
            listWarp: {
                flex: 1,
                paddingTop: 5,
                backgroundColor: colors.cardBackground
            }
        })
    }
})

export interface CoinListProps {
    data: IPortfolioData
    onPress?(): void
}
@observer
export class CoinList extends Component<CoinListProps> {
    render() {
        return (
            <ListTitle
                onPress={this.props.onPress}
                contentsStyle={{ borderTopWidth: 0, height: 'auto', alignItems: 'flex-end', flex: 2 }}
                leadingStyle={{ width: 'auto', height: 'auto', flex: 1 }}
                leading={this.renderLeading()}
                title={this.renderTitle()}
                trailing={this.renderTrailing()}
                trailingStyle={{ borderTopWidth: 0, height: 'auto', alignItems: 'flex-end', flex: 2 }}
                style={{ borderBottomColor: colors.border, borderBottomWidth: 1, marginLeft: 0, marginRight: 0, padding: 10 }}
            />
        )
    }
    renderLeading() {
        const ticker = coinmarketcapStore.get(this.props.data.name)
        return <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
            <Image
                style={{ width: 25, height: 25 }}
                source={{ uri: ticker?.url }}
            />
            <BLNText numberOfLines={1} style={{ fontWeight: 'bold', marginLeft: 5 }}>{ticker?.symbol}</BLNText>
        </View>
    }
    renderTitle() {
        const ticker = coinmarketcapStore.get(this.props.data.name)
        return <View style={{ flex: 2, flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end' }}>
            <BLNText numberOfLines={1} style={{ fontWeight: 'bold' }}>{Numeral.localString(ticker?.currentQuoteData?.price)}</BLNText>
            <View style={{ flexDirection: "row", justifyContent: 'flex-end', alignItems: 'center' }}>
                <FontAwesome5
                    name={new BigNumber(ticker?.currentQuoteData?.percent_change_24h || '0').toNumber() > 0 ? "sort-up" : "sort-down"}
                    color={new BigNumber(ticker?.currentQuoteData?.percent_change_24h || '0').toNumber() > 0 ? colors.transferIn : colors.transferOut}
                    size={18}
                />
                <BLNText numberOfLines={1} style={{}}>{Numeral.percentage(ticker?.currentQuoteData?.percent_change_24h)}</BLNText>
            </View>
        </View>
    }
    renderTrailing() {
        const ticker = coinmarketcapStore.get(this.props.data.name)
        return <View style={{ flex: 2, flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end' }}>
            <BLNText numberOfLines={1} style={{ fontWeight: 'bold' }}>{Numeral.value(new BigNumber(this.props.data.amount).multipliedBy(ticker?.currentQuoteData?.price || "0").toNumber())}</BLNText>
            <BLNText>{Numeral.decimal(this.props.data.amount)} {this.props.data.symbol}</BLNText>
        </View>
    }
}
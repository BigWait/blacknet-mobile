/**
 * portfolio store
 * @file portfolio store
 * @module app/pages/portfolio/stores/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import { BlnTransaction, IPortfolioData } from '@app/types/bln'
import API from '@app/services/api'
import { accountStore } from '@app/stores/account'
import transactionStore from '@app/pages/wallet/stores/transaction'
import Blacknetjs from 'blacknetjs';
import coinmarketcap from '@app/pages/coinmarketcap/stores/coinmarketcap'
import storeapi from '@app/services/storeapi'
import { stashStore } from '@app/stores/stash'
import BigNumber from 'bignumber.js'
/**
 * 
 * 存储格式
 * {
 *      'Bitcoin': 2000,
 *      'Litecoin': 100
 * }
 * 
 * 
 */

export class PortfolioStore {

    timerId:any = null

    @observable coinMap:Map<string, string> = new Map();

    @action.bound
    initAccount(){

        let address = accountStore.currentAddress;
        let mncode = accountStore.currentMnemonic;
        API.getPortfolio(address).then((msg)=>{
            if(msg.text){
                
                let text = msg.text.replace('portfolio: ', '');
                let str = Blacknetjs.Decrypt(mncode, address, text);
                let data ;
                try {
                    data = JSON.parse(str);
                    console.log(data)
                    for(let name in data){
                        this.coinMap.set(name, data[name]);
                    }
                } catch (error) {
                    console.log('PortfolioStore initAccount method', error.stack)
                }

            }else{
                // 如果账户不存在portfolio
                this.coinMap.clear();
            }
        });
    }

    @computed
    get data(){
        return this.getData().slice()
    }

    @action.bound
    upsert(coin: IPortfolioData) {

        this.coinMap.set(coin.name ,coin.amount);

        this.syncToBlockChain();
        
    }

    @action.bound
    readySyncToBlockChain(){
        let data: {[key: string]: string} = {};
        for(let [name, amount] of this.coinMap){
            data[name] = amount;
        }
        let message = JSON.stringify(data);
        let mncode = accountStore.currentMnemonic, address = accountStore.currentAddress;

        console.log(` portfolio data is ${message}`)
        storeapi.sendPortfolio(mncode, address, message).then((txid)=>{
            const tx = BlnTransaction.fromTx(txid, address, address, new BigNumber(0.1).multipliedBy(1e8).toString(), message)
            stashStore.add(address, tx)
            transactionStore.upsertTx(address, tx)
        });
    }
    @action.bound
    syncToBlockChain(){
        
        if(this.timerId) clearTimeout(this.timerId);

        this.readySyncToBlockChain();
    }

    @action.bound
    remove(name: string) {
        this.coinMap.delete(name);
        this.syncToBlockChain();
    }

    @boundMethod
    exist(name: string) {
        return this.coinMap.has(name);
    }

    @boundMethod
    get(name: string) {
        return this.coinMap.get(name);
    }

    @boundMethod
    getData(): IPortfolioData[]{

        let array:IPortfolioData[] = [];
        for(let [name, amount] of this.coinMap){
            
            let data = coinmarketcap.get(name);
            let xData:IPortfolioData = {
                name: data?.name || '',
                symbol: data?.symbol|| '',
                amount: amount
            }
            array.push(xData);
        }

        return array
    }
}

export default new PortfolioStore()

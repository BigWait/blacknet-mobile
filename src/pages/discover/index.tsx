/**
 * Discover
 * @file Discover
 * @module pages/discover/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

import ListTitle from "@app/components/common/list-title";
import { LANGUAGE_KEYS } from "@app/constants/language";
import { DiscoverRoutes } from "@app/routes";
import i18n from "@app/services/i18n";
import colors from "@app/style/colors";
import { IPageProps } from "@app/types/props";
import { observable } from "mobx";
import { observer } from "mobx-react";
import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

export interface IIndexProps extends IPageProps { }
import * as RootNavigation from "@app/rootNavigation";

@observer
export class DiscoverScreen extends Component<IIndexProps> {
  private unsubscribe: any;

  componentDidMount() {
    // Listen for screen focus event
  }

  componentWillUnmount() {
    // unsubscribe
  }

  render() {
    const { styles } = obStyles;
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View style={styles.content}>
            {this._momentItem()}
            {this._explorerItem()}
            {this._coinmarketcap()}
            {this._portfolio()}
            {this._poollist()}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }

  _momentItem() {
    const { styles } = obStyles;
    return (
      <ListTitle
        style={[styles.list, { marginTop: -1 }]}
        leading={
          <View style={styles.listTitleLeading}>
            <FontAwesome5 name="comment-alt" style={styles.listTitleIcon} />
          </View>
        }
        title={
          <Text style={styles.listTitleText}>
            {i18n.t(LANGUAGE_KEYS.MOMENT)}
          </Text>
        }
        trailing={
          <FontAwesome5 color={colors.textDefault} name="chevron-right" />
        }
        onPress={() => {
          RootNavigation.navigate(DiscoverRoutes.Forum, {});
        }}
      />
    );
  }

  _explorerItem() {
    const { styles } = obStyles;
    return (
      <ListTitle
        style={styles.list}
        leading={
          <View style={styles.listTitleLeading}>
            <FontAwesome5 name="search" style={styles.listTitleIcon} />
          </View>
        }
        title={
          <Text style={styles.listTitleText}>
            {i18n.t(LANGUAGE_KEYS.EXPLORER)}
          </Text>
        }
        trailing={
          <FontAwesome5 color={colors.textDefault} name="chevron-right" />
        }
        onPress={() => {
          RootNavigation.navigate(DiscoverRoutes.Explorer, {});
        }}
      />
    );
  }
  _coinmarketcap() {
    const { styles } = obStyles;
    return (
      <ListTitle
        style={[styles.list, { marginTop: -1 }]}
        leading={
          <View style={styles.listTitleLeading}>
            <FontAwesome5 name="bitcoin" style={styles.listTitleIcon} />
          </View>
        }
        title={
          <Text style={styles.listTitleText}>
            {i18n.t(LANGUAGE_KEYS.COINMARKETCAP)}
          </Text>
        }
        trailing={
          <FontAwesome5 color={colors.textDefault} name="chevron-right" />
        }
        onPress={() => {
          RootNavigation.navigate(DiscoverRoutes.Coinmarketcap, {});
        }}
      />
    );
  }
  _portfolio() {
    const { styles } = obStyles;
    return (
      <ListTitle
        style={[styles.list, { marginTop: -1 }]}
        leading={
          <View style={styles.listTitleLeading}>
            <FontAwesome5 name="coins" style={styles.listTitleIcon} />
          </View>
        }
        title={
          <Text style={styles.listTitleText}>
            {i18n.t(LANGUAGE_KEYS.PORTFOLIO)}
          </Text>
        }
        trailing={
          <FontAwesome5 color={colors.textDefault} name="chevron-right" />
        }
        onPress={() => {
          RootNavigation.navigate(DiscoverRoutes.Portfolio, {});
        }}
      />
    );
  }
  _poollist() {
    const { styles } = obStyles;
    return (
      <ListTitle
        style={[styles.list, { marginTop: -1 }]}
        leading={
          <View style={styles.listTitleLeading}>
            <FontAwesome5 name="truck-monster" style={styles.listTitleIcon} />
          </View>
        }
        title={
          <Text style={styles.listTitleText}>
            {i18n.t(LANGUAGE_KEYS.POOLLIST)}
          </Text>
        }
        trailing={
          <FontAwesome5 color={colors.textDefault} name="chevron-right" />
        }
        onPress={() => {
          RootNavigation.navigate(DiscoverRoutes.Poollist, {});
        }}
      />
    );
  }

}

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      versionView: {
        position: "relative",
      },
      versionViewPoint: {
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: colors.transferOut,
        position: "absolute",
        right: 0,
      },
      container: {
        flex: 1,
      },
      content: {
        borderBottomColor: colors.border,
        borderBottomWidth: 1,
        flex: 1,
      },
      list: {
        // paddingLeft: 10,
        // paddingRight: 10,
        // paddingTop: 8,
        // paddingBottom: 8,
        // backgroundColor: colors.cardBackground
      },
      balance: {
        height: 200,
        alignItems: "center",
        justifyContent: "center",
      },
      balanceText: {
        color: colors.yellow,
        fontSize: 30,
      },
      listTitleLeading: {
        // width: 40,
        // height: 40,
        // borderRadius: 40,
        // backgroundColor: 'transparent',
        // alignItems: "center",
        // justifyContent: "center"
      },
      listTitleIcon: {
        color: colors.textDefault,
        fontSize: 16
      },
      listTitleText: {
        color: colors.textDefault,
        marginRight: 10,
      },
      listLanguage: {
        flexDirection: "row",
        alignItems: "center",
      },
    });
  },
});

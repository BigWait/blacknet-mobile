/**
 * Feedback
 * @file Feedback
 * @module pages/settings/feedback
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, TextInput, ScrollView } from 'react-native'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { CopyButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import CheckBox from 'react-native-check-box'
import API from '@app/services/api'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'

class FormStore {
    @observable isChecked: boolean = false
    @observable content: string = ""

    @action.bound
    changeChecked(checked?: boolean) {
        this.isChecked = !this.isChecked
    }
    @action.bound
    changeContent(content: string) {
        this.content = content
    }
    @action.bound
    reset() {
        this.content = ""
        this.isChecked = false
    }
    @computed
    get verify(): boolean{
        return this.content != undefined && this.content != ''
    }
    @computed
    get data(): any{
        return {
            text: this.content
        }
    }
}
export const formStore = new FormStore()

export interface IIndexProps extends IPageProps {}

@observer export class FeedbackScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          title: i18n.t(LANGUAGE_KEYS.FEEDBACK)
        }
    }

    @boundMethod
    private onPressSubmit(){
        if(!formStore.verify){
            return
        }
        const params:any = formStore.data
        if(!formStore.isChecked){
            params.account = accountStore.currentAddress
        }
        const ld = Loading.show()
        API.blnFeedback(params).then(()=>{
            showToast(i18n.t(LANGUAGE_KEYS.FEEDBACK_SUCCESS))
            formStore.reset()
        })
        .catch((err)=>{
            showToast(`${err}`)
            console.log("Feedback failed.", err);
        })
        .finally(()=>{
            Loading.hide(ld)
        })
    }

    render() {
        const { styles } = obStyles
        return (
            <ScrollView style={styles.container}>
                <TextInput
                    style={styles.input}
                    multiline={true}
                    onChangeText={text => formStore.changeContent(text)}
                    value={formStore.content}
                    numberOfLines={10}
                />
                <View style={styles.checkboxView}>
                <CheckBox
                    style={styles.checkbox}
                    onClick={()=>{
                        formStore.changeChecked()
                    }}
                    isChecked={formStore.isChecked}
                    rightText={i18n.t(LANGUAGE_KEYS.ANONYMOUS)}
                />
                </View>
                <CopyButton text={i18n.t(LANGUAGE_KEYS.SUBMIT)} onPress={()=>{
                    this.onPressSubmit()
                }}/>
            </ScrollView>
        )
    }
}

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        checkboxView:{
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 20,
            paddingBottom: 20
        },
        checkbox: {
            width: 110,
            color: colors.textDefault
        },
        input: {
            height: 150,
            borderColor: colors.border, 
            borderWidth: 1,
            padding: 10,
            color: colors.textDefault
        },
        container: {
            flex: 1,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 40
        }
      })
    }
})

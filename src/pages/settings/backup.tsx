/**
 * Backup
 * @file Backup
 * @module pages/settings/backup
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView } from 'react-native'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { CopyButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import { LabelListTitle } from '@app/components/ui/list-title'
import { TouchableView } from '@app/components/common/touchable-view'
import Clipboard from '@react-native-community/clipboard'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import { boundMethod } from 'autobind-decorator'
export interface IIndexProps extends IPageProps {}

@observer export class BackupScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          title: i18n.t(LANGUAGE_KEYS.BACKUP)
        }
    }

    @boundMethod
    private onPressCopy(){
      Clipboard.setString(accountStore.currentMnemonic)
      showToast(i18n.t(LANGUAGE_KEYS.COPIED_MNEMONIC))
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <LabelListTitle
                        leading={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.MNEMONIC)}</Text>}
                    />
                    <TouchableView style={styles.item}
                    >
                        <Text style={{color: colors.textDefault}}>{accountStore.currentMnemonic}</Text>
                    </TouchableView>
                    <CopyButton text={i18n.t(LANGUAGE_KEYS.COPY)} onPress={()=>{
                        this.onPressCopy()
                    }}/>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        input: {
            height: 40,
            borderColor: colors.border, 
            borderWidth: 1,
            color: colors.textDefault
        },
        item: {
            backgroundColor: colors.border,
            marginBottom: 20,
            borderRadius: 2,
            padding: 10
        },
        bootomBar:{
          height: 40,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 10
        },
        lists: {
          flex: 1,
          paddingLeft: 20,
          paddingRight: 20,
          paddingTop: 40
        },
        container: {
          flex: 1
        }
      })
    }
})

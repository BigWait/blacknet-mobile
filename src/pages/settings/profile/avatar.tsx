/**
 * Profile
 * @file Profile
 * @module pages/settings/profile
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Text,
    SafeAreaView,
    ScrollView,
    Platform,
} from "react-native";
import { action, computed, observable } from "mobx";
import { observer } from "mobx-react";
import { IPageProps } from "@app/types/props";
import colors from "@app/style/colors";
import BottomButton from "@app/components/ui/bottom-button";
import { accountStore } from "@app/stores/account";
import i18n from "@app/services/i18n";
import { LANGUAGE_KEYS } from "@app/constants/language";
import { showToast } from "@app/services/toast";
import Loading from "@app/components/common/loading";
import API from "@app/services/api";
import { boundMethod } from "autobind-decorator";
import { LabelListTitle } from "@app/components/ui/list-title";
import TextInput from "@app/components/common/text-input";
import { color } from "react-native-reanimated";
import Image from "@app/components/common/image";
import { URL } from "@app/constants/regexp";
export interface IIndexProps extends IPageProps { }
import ImagePicker, { ImageOrVideo } from "react-native-image-crop-picker";
class FormStore {
    @observable content: string | undefined = undefined;

    @action.bound
    changeContent(content: string | undefined) {
        this.content = content;
    }

    @action.bound
    reset() {
        this.content = "";
    }

    @computed
    get verify(): boolean {
        return (
            this.content != undefined && this.content != "" && URL.test(this.content)
        );
    }
}
export const formStore = new FormStore();
const createFormData = (image: ImageOrVideo) => {
    const data = new FormData();
    let path =
        Platform.OS === "android" ? image.path : image.path.replace("file://", "");

    let obj = {
        name: image.filename || path.split("/").pop(),
        type: image.mime,
        uri: path,
    };
    data.append("photo", obj);
    return data;
};
@observer
export class AvatarScreen extends Component<IIndexProps> {
    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.AVATAR),
        };
    };

    componentDidMount() {
        console.log(accountStore.currentAvatarUrl);
        formStore.changeContent(accountStore.currentAvatarUrl);
    }

    uploadImage() {
        const ld = Loading.show();
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
        }).then(async (image) => {

            let options = {
                method: "POST",
                body: createFormData(image),
                headers: { "Content-Type": "multipart/form-data" },
            };

            console.log(image)

            fetch("https://blnscan.loqunbai.com/api/upload", options)
                .then((response) => response.json())
                .then((response) => {
                    formStore.changeContent(
                        `https://ipfs.loqunbai.com/ipfs/${response.cid}`
                    );
                })
                .catch((error) => {
                    console.log("upload error", error);
                })
                .finally(() => {
                    Loading.hide(ld);
                });
        }).catch((e)=>{
            Loading.hide(ld);
        });
    }

    render() {
        const { styles } = obStyles;
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={
                                <Text style={{ color: colors.textDefault }}>
                                    {i18n.t(LANGUAGE_KEYS.INPUT_AVATAR)}
                                </Text>
                            }
                        />
                        <Text style={{ color: colors.textDefault , marginBottom: 10}}>{formStore.content}</Text>
                        <BottomButton text={"Select Photo"} onPress={this.uploadImage} />
                    </View>
                    <View style={styles.preview}>
                        {formStore.verify ? (
                            <Image
                                style={styles.image}
                                source={{
                                    uri: formStore.content,
                                }}
                            />
                        ) : null}
                    </View>
                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton
                        text={i18n.t(LANGUAGE_KEYS.SAVE)}
                        onPress={this.onPress}
                    />
                </View>
            </SafeAreaView>
        );
    }
    @boundMethod
    private onPress() {
        if (formStore.verify) {
            const ld = Loading.show();
            API.blnScanUpdateProfile(
                accountStore.currentMnemonic,
                accountStore.currentAddress,
                accountStore.currentAddress,
                { imageurl: formStore.content }
            )
                .then((res: any) => {
                    accountStore.updateImageURL(formStore.content);
                    showToast(i18n.t(LANGUAGE_KEYS.SUCCESS));
                    setTimeout(this.props.navigation.goBack, 1000);
                })
                .catch((err) => {
                    showToast(`${err}`);
                    console.log("Save Avatar failed.", err);
                })
                .finally(() => {
                    Loading.hide(ld);
                    accountStore.refreshBalance();
                });
        }
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            preview: {
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "row",
                marginTop: 40,
            },
            image: {
                
                width: 300,
                height: 300,
            },
            input: {
                color: colors.textDefault,
                height: 120,
                textAlignVertical: "top",
                padding: 5,
            },
            inputBox: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 30,
            },
            bootomBar: {
                height: 40,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 10,
            },
            lists: {
                flex: 1,
            },
            container: {
                flex: 1,
                backgroundColor: colors.background,
            },
        });
    },
});

/**
 * Profile
 * @file Profile
 * @module pages/settings/profile
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView } from 'react-native'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import Loading from '@app/components/common/loading'
import API from '@app/services/api'
import { boundMethod } from 'autobind-decorator'
import { LabelListTitle } from '@app/components/ui/list-title'
import TextInput from '@app/components/common/text-input'

export interface IIndexProps extends IPageProps { }

class FormStore {

    @observable content: string | undefined = undefined

    @action.bound
    changeContent(content: string | undefined) {
        this.content = content
    }

    @action.bound
    reset() {
        this.content = ""
    }

    @computed
    get verify(): boolean{
        return this.content != undefined && this.content != ''
    }
}
export const formStore = new FormStore()

@observer export class SignTextScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.SIGNTEXT)
        }
    }

    componentDidMount() {
        accountStore.fetchSignmessage()
        formStore.changeContent(accountStore.currentSignmessage)
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.INPUT_MESSAGE)}</Text>}
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={text => formStore.changeContent(text)}
                            value={formStore.content}
                            multiline={true}
                            numberOfLines={10}
                        />
                    </View>

                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton text={i18n.t(LANGUAGE_KEYS.SAVE)} onPress={() => {
                        this.onPressInputSignMessage()
                    }} />
                </View>
            </SafeAreaView>
        )
    }
    @boundMethod
    private onPressInputSignMessage() {
        if (formStore.verify) {
            const ld = Loading.show()
            API.blnScanUpdateProfile(
                accountStore.currentMnemonic,
                accountStore.currentAddress,
                accountStore.currentAddress,
                {signmessage: formStore.content}
            ).then((res: any) => {
                accountStore.updateSignmessage(formStore.content)
                showToast(i18n.t(LANGUAGE_KEYS.SUCCESS));
                setTimeout(this.props.navigation.goBack, 1000)
            })
            .catch((err) => {
                showToast(`${err}`)
                console.log("Save SIGN TEXT failed.", err);
            })
            .finally(() => {
                Loading.hide(ld)
                accountStore.refreshBalance()
            })
        }
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            input: {
                color: colors.textDefault,
                height: 200,
            },
            inputBox: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 30,
            },
            bootomBar: {
                height: 40,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 10
            },
            lists: {
                flex: 1
            },
            container: {
                flex: 1
            }
        })
    }
})

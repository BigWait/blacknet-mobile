/**
 * Profile
 * @file Profile
 * @module pages/settings/profile
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView } from 'react-native'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import Loading from '@app/components/common/loading'
import API from '@app/services/api'
import { boundMethod } from 'autobind-decorator'
import { LabelListTitle } from '@app/components/ui/list-title'
import TextInput from '@app/components/common/text-input'
import { color } from 'react-native-reanimated'
export interface IIndexProps extends IPageProps { }

class FormStore {

    @observable content: string | undefined = undefined

    @action.bound
    changeContent(content: string | undefined) {
        this.content = content
    }

    @action.bound
    reset() {
        this.content = ""
    }

    @computed
    get verify(): boolean{
        return this.content != undefined && this.content != ''
    }
}
export const formStore = new FormStore()

@observer export class NickNameScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.NICKNAME)
        }
    }

    componentDidMount() {
        accountStore.fetchNickname()
        formStore.changeContent(accountStore.currentNickname)
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.INPUT_NICKNAME)}</Text>}
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={text => formStore.changeContent(text)}
                            value={formStore.content}
                            multiline={false}
                            numberOfLines={1}
                        />
                    </View>

                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton text={i18n.t(LANGUAGE_KEYS.SAVE)} onPress={() => {
                        this.onPressInputNickName()
                    }} />
                </View>
            </SafeAreaView>
        )
    }
    @boundMethod
    private onPressInputNickName() {
        if (formStore.verify) {
            const ld = Loading.show()
            API.blnScanUpdateProfile(
                accountStore.currentMnemonic, 
                accountStore.currentAddress, 
                accountStore.currentAddress, 
                {nickname: formStore.content}
            ).then((res:any)=>{
                accountStore.updateNickname(formStore.content)
                showToast(i18n.t(LANGUAGE_KEYS.SUCCESS));
                setTimeout(this.props.navigation.goBack, 1000)
            })
            .catch((err)=>{
                showToast(`${err}`)
                console.log("Save Nickname failed.", err);
            })
            .finally(()=>{
                Loading.hide(ld)
                accountStore.refreshBalance()
            })
        }
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            input: {
                color: colors.textDefault
            },
            inputBox: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 30
            },
            bootomBar: {
                height: 40,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 10
            },
            lists: {
                flex: 1
            },
            container: {
                flex: 1,
                backgroundColor: colors.background
            }
        })
    }
})

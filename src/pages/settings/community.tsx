/**
 * Community
 * @file Community
 * @module pages/settings/community
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet,  Text, ScrollView, Linking } from 'react-native'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { LineTitle } from '@app/components/ui/line-title'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { boundMethod } from 'autobind-decorator'
export interface IIndexProps extends IPageProps { }

@observer export class CommunityScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.COMMUNITY)
        }
    }

    @boundMethod
    private openUrl(url: string): Promise<any> {
        return Linking
            .openURL(url)
            .catch(error => {
                console.warn('Open url failed:', error)
                return Promise.reject(error)
            })
    }

    private get socials() {
        return [
            {
                name: i18n.t(LANGUAGE_KEYS.COMMUNITY_OFFICE_WEBSITE),
                url: 'https://blacknet.ninja'
            },
            {
                name: i18n.t(LANGUAGE_KEYS.COMMUNITY_BLACKNET_SOURCE),
                url: 'https://gitlab.com/blacknet-ninja/blacknet'
            },
            {
                name: i18n.t(LANGUAGE_KEYS.COMMUNITY_BITCOINTALK),
                url: 'https://bitcointalk.org/index.php?topic=469640.0'
            },
            {
                name: i18n.t(LANGUAGE_KEYS.COMMUNITY_REDDIT),
                url: 'https://www.reddit.com/r/blacknet'
            },
            {
                name: i18n.t(LANGUAGE_KEYS.COMMUNITY_MATRIX),
                url: 'https://app.element.io/#/group/+blacknet:matrix.org'
            },
            {
                name: i18n.t(LANGUAGE_KEYS.COMMUNITY_BLACKNET_APP_SOURCE),
                url: 'https://gitlab.com/blacknet-ninja/blacknet-mobile'
            },
            {
                name: i18n.t(LANGUAGE_KEYS.COMMUNITY_QQGROUP),
                url: 'mqqopensdkapi://bizAgent/qm/qr?url=http%3A%2F%2Fqm.qq.com%2Fcgi-bin%2Fqm%2Fqr%3Ffrom%3Dapp%26p%3Dandroid%26jump_from%3Dwebapi%26k%3Dyv5nChbAruAcgkpHh9pOocsrlEr7Xfhw',
                trailing: "705602427",
            },
            {
                name: i18n.t(LANGUAGE_KEYS.COMMUNITY_TWITTER),
                url: 'https://twitter.com/BlacknetN',
                trailing: "@BlacknetN"
            }
        ]
    }

    render() {
        const { styles } = obStyles
        return (
            <ScrollView style={styles.container}>
                {this.socials.map((social) => {
                    return <LineTitle
                        key={social.name}
                        marginDirection={"left"}
                        marginSize={15}
                        title={
                            <Text style={styles.title}>{social.name}</Text>
                        }
                        trailing={<Text style={styles.trailing}>{social.trailing}</Text>}
                        onPress={social.url ? () => {
                            this.openUrl(social.url)
                        } : undefined}
                    />
                })}
            </ScrollView>
        )
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            container: {
                flex: 1
            },
            title: {
                fontSize: 16,
                color: colors.textDefault
            },
            trailing: {
                fontSize: 16,
                marginRight: 15,
                color: colors.textDefault
            }
        })
    }
})

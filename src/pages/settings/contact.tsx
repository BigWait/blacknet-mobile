/**
 * Contact
 * @file Contact
 * @module pages/settings/contact
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, Text, FlatList, SafeAreaView, ScrollView } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed, reaction } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import { IBlnScanerURL } from '@app/types/bln'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import fonts from '@app/style/fonts'
import mixins from '@app/style/mixins'
import API from '@app/services/api'
import { IBlnScanContact } from '@app/types/bln'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import ListTitle from '@app/components/common/list-title'
import Loading from '@app/components/common/loading'
import showToast from '@app/services/toast';
import contactStore from '@app/stores/contact';
import { showContactName, verifyAccount } from '@app/utils/bln'
import { TouchableView } from '@app/components/common/touchable-view'
import { getHeaderButtonStyle } from '@app/style/mixins'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { NoResult } from '@app/components/ui/NoResult'
import Modal from '@app/components/common/modal'
import QRCode from 'react-native-qrcode-svg';

export type LeaseListElement = RefObject<FlatList<IBlnScanContact>>
export interface IIndexProps extends IPageProps {}

@observer export class ContactScreen extends Component<IIndexProps> {

    state = {
        visible: false
    }

    constructor(props: IIndexProps) {
        super(props)
        // 当过滤条件变化时进行重请求
        reaction(
            () => [
                contactStore.list
            ],
            ([list]: any) => {
                this.lists=  list
            }
        )
        // fetch
        this.fetchLists()
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          title: i18n.t(LANGUAGE_KEYS.CONTACT),
          headerRight: () => <Observer render={() => (
            <TouchableView
              accessibilityLabel=""
              accessibilityHint=""
              onPress={() => {
                navigation.navigate(SettingsRoutes.AddContact)
              }}
            >
              <FontAwesome5
                name="plus"
                {...getHeaderButtonStyle(18)}
              />
            </TouchableView>
          )}/>
        }
    }

    UNSAFE_componentWillMount() {
        contactStore.get(accountStore.currentAddress).then((lists)=>{ this.updateResultData(lists) })
    }

    private listElement: LeaseListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }

    @observable private isLoading: boolean = false
    @observable.shallow private lists: IBlnScanContact[] = []

    @computed
    private get ListData(): IBlnScanContact[] {
        return this.lists.slice() || []
    }

    @action
    private updateLoadingState(loading: boolean) {
        this.isLoading = loading
    }

    @action
    private updateResultData(result: Array<IBlnScanContact>, save?: boolean) {
        this.lists = result
        // save store
        if(save){
            contactStore.upsert(accountStore.currentAddress, this.lists)
        }
    }

    @boundMethod
    private fetchLists(): Promise<any> {
        this.updateLoadingState(true)
        return API.blnScanContact(accountStore.currentAddress)
        .then((json)=>{
            this.updateResultData(json, true)
            return json
        })
        .catch(error => {
            console.warn('Fetch contact list error:', error)
            return Promise.reject(error)
        })
        .finally(()=>this.updateLoadingState(false))
    }

    private getIdKey(tx: IBlnScanContact, index?: number): string {
        return `index:${index}:sep:${tx.account}`
    }

    private getItemLayout(_: any, index: number) {
        const height = 40
        return {
            index,
            length: height,
            offset: height * index
        }
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
        <Observer
            render={() => (
                <NoResult />
            )}
        />
        )
    }

    render() {
        return (
        <View style={obStyles.styles.listViewContainer}>
            <MyFlatList
                style={obStyles.styles.listView}
                data={this.ListData}
                ref={this.listElement}
                // 列表为空时渲染
                ListEmptyComponent={this.renderListEmptyView}
                // 当前列表 loading 状态
                refreshing={this.isLoading}
                // 刷新
                onRefresh={this.fetchLists}
                // 唯一 ID
                keyExtractor={this.getIdKey}
                // 单个主体
                renderItem={({ item: account, index }) => {
                    const name = showContactName(accountStore.currentMnemonic, accountStore.currentAddress, account.name)
                return (
                    <Observer
                        render={() => (
                            <ListTitle
                                contentsStyle={{paddingLeft: 10, paddingRight: 10}}
                                key={index}
                                title={<Text>{name}</Text>}
                                subtitle={<Text>{account.account}</Text>}
                                onPress={()=>{
                                    this.props.navigation.navigate(HomeRoutes.ChatMessage, {address: account.account, name: `${name}`, user: accountStore.currentAddress})
                                }}
                            />
                        )}
                    />
                    )
                }}
            />
            <SafeAreaView>
            <View style={obStyles.styles.bootomBar}>
                <BottomButton text={i18n.t(LANGUAGE_KEYS.MY_QRCODE)} onPress={this.showModal}/>
            </View>
            </SafeAreaView>
            {this.renderMyqrcode()}
        </View>
        )
    }
    renderMyqrcode(){
        return (
            <Modal
                visible={this.state.visible}
                dismiss={this.hideModal}
                style={{width: 320}}
            >
                <View>
                    <QRCode 
                        value={`blacknet:${accountStore.currentAddress}`}
                        size={300}
                        logoBackgroundColor='transparent'
                    />
                </View>
            </Modal>
        )
    }
    showModal = () => this.setState({ visible: true });
    hideModal = () => this.setState({ visible: false });
}

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        listViewContainer: {
          position: 'relative',
          flex: 1
        },
        listView: {
          width: sizes.screen.width
        },
        centerContainer: {
          justifyContent: 'center',
          alignItems: 'center',
          padding: sizes.gap
        },
        loadmoreViewContainer: {
          ...mixins.rowCenter,
          padding: sizes.goldenRatioGap
        },
        normalTitle: {
          ...fonts.base,
          color: colors.textSecondary
        },
        smallTitle: {
          ...fonts.small,
          color: colors.textSecondary
        },
        input: {
            borderWidth: 1,
            paddingLeft: 5
        },
        inputBox: {
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 30
        },
        scanner:{
            paddingRight: 15
        },
        bootomBar:{
            height: 40,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            paddingBottom: 10
        },
        lists: {
          flex: 1
        },
        container: {
          flex: 1
        }
      })
    }
  })

import { LabelListTitle } from '@app/components/ui/list-title'
import TextInput from '@app/components/common/text-input'
import BottomButton from '@app/components/ui/bottom-button'
import { HomeRoutes, SettingsRoutes } from '@app/routes'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { MyFlatList } from '@app/components/common/FlatList'

class FormStore {

    @observable name?: string
    @observable address?: string
    @observable result?: string
    @observable showInput?: boolean
  
    @action.bound
    changeName(v?: string) {
        this.name = v
    }

    @action.bound
    changeAddress(v?: string, show?: boolean) {
        this.address = v
        this.showInput = show
    }

    @action.bound
    changeResult(v?: string) {
        this.name = undefined
        this.address = undefined
        this.showInput = false
        this.result = v
    }

    @action.bound
    changeShow(show?: boolean) {
        this.showInput = show
    }
  
    @computed
    get verify(): boolean{
        return this.name != undefined && this.name != ''
    }
  }
  export const formStore = new FormStore()

@observer export class AddContactScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
        if(this.props.route.params && this.props.route.params.address){
            formStore.changeAddress(this.props.route.params.address, true)
        }
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          title: i18n.t(LANGUAGE_KEYS.ADD_CONTACT)
        }
    }

    onChangeAddress(text: string){
        if(verifyAccount(text)){
            API.blnScanNickname(text).then((name)=>{
                if(!name){
                    formStore.changeShow(true)
                } else {
                    formStore.changeName(name)
                }
            })
        }
        formStore.changeAddress(text, false)
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        {formStore.showInput ? <>
                            <LabelListTitle
                                leading={<Text>{i18n.t(LANGUAGE_KEYS.NAME)}</Text>} 
                            />
                            <TextInput
                                style={styles.input}
                                onChangeText={text => formStore.changeName(text)}
                                value={formStore.name}
                                placeholder={i18n.t(LANGUAGE_KEYS.INPUT_NAME)}
                            />
                        </> : null}
                        <LabelListTitle
                            leading={<Text>{i18n.t(LANGUAGE_KEYS.ADDRESS)}</Text>}
                        />
                        <TextInput
                            viewStyle={styles.input}
                            onChangeText={text => this.onChangeAddress(text)}
                            value={formStore.address}
                            placeholder={i18n.t(LANGUAGE_KEYS.INPUT_ADDRESS)}
                            trailing={<TouchableView style={styles.scanner}
                                onPress={()=>{
                                    this.onPressScaner()
                                }}
                            >
                                <Icon name={'qrcode'} size={20}></Icon>
                            </TouchableView>}
                        />
                        <LabelListTitle
                            leading={<Text>{formStore.result}</Text>}
                        />
                    </View>
                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton text={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={()=>{
                        accountStore.authenticate().then(()=>{
                            this.onPressSubmit()
                        }).catch((error)=>{
                            showToast(`${error}`)
                        })
                    }}/>
                </View>
            </SafeAreaView>
        )
    }

    @boundMethod
    onSuccessScaner(uri: IBlnScanerURL){
        if(uri.address){
            formStore.changeAddress(uri.address)
        }
    }

    @boundMethod
    private onPressScaner(){
        this.props.navigation.navigate(HomeRoutes.QrScanner, {
            callback: this.onSuccessScaner
        })
    }

    @boundMethod
    private onPressSubmit(){
        if(!formStore.verify){
            return showToast(i18n.t(LANGUAGE_KEYS.INPUT_ERROR))
        }
        const ld = Loading.show()
        API.addContact(
            accountStore.currentMnemonic, 
            accountStore.currentAddress, 
            formStore.address || "",
            formStore.name || ""
        ).then((res:any)=>{
            showToast(i18n.t(LANGUAGE_KEYS.ADD_SUCCESS))
            contactStore.upsert(accountStore.currentAddress, [{
                name: res.message,
                account: formStore.address || ""
            }])
            formStore.changeResult(res.txid)
            setTimeout(()=>{
                this.props.navigation.goBack()
            }, 50)
        })
        .catch((err)=>{
            showToast(`${err}`)
            console.log("Add contact failed.", err);
        })
        .finally(()=>{
            Loading.hide(ld)
            accountStore.refreshBalance()
        })
    }
}
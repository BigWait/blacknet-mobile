/**
 * Profile
 * @file Profile
 * @module pages/settings/profile
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, Text, ScrollView, View, Image, Platform } from 'react-native'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { accountStore } from '@app/stores/account'
import { LineTitle } from '@app/components/ui/line-title'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { boundMethod } from 'autobind-decorator'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { SettingsRoutes } from '@app/routes'
import Loading from '@app/components/common/loading'
import ImagePicker, { ImageOrVideo } from "react-native-image-crop-picker";
import showToast from '@app/services/toast'
import API from "@app/services/api";
import { userStore } from '@app/stores/users'



export interface IIndexProps extends IPageProps { }
const createFormData = (image: ImageOrVideo) => {
    const data = new FormData();
    let path =
        Platform.OS === "android" ? image.path : image.path.replace("file://", "");

    let obj = {
        name: image.filename || path.split("/").pop(),
        type: image.mime,
        uri: path,
    };
    data.append("photo", obj);
    return data;
};

@observer export class ProfileScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.PROFILE)
        }
    }
    @observable imageUrl:string = '';
    componentDidMount() {
        accountStore.fetchProfile().then((profile)=>{
            this.imageUrl = profile.imageurl
        });
    }

    render() {
        const { styles } = obStyles
        return (
            <ScrollView style={styles.container}>
                {this._avatarItem()}
                {this._nicknameItem()}
                {this._signatureItem()}
            </ScrollView>
        )
    }
    _nicknameItem() {
        const { styles } = obStyles
        return <LineTitle
            title={
                <Text style={styles.title}>{i18n.t(LANGUAGE_KEYS.NICKNAME)}</Text>
            }
            subtitle={
                accountStore.currentNickname ? <Text style={styles.subtitle}>{accountStore.currentNickname}</Text> : null
            }
            trailing={
                <FontAwesome5 style={styles.trailing} name="chevron-right" color={colors.textDefault} />
            }
            onPress={this.onPressInputNickname}
        />
    }
    _signatureItem() {
        const { styles } = obStyles
        return <LineTitle
            title={
                <Text style={styles.title}>{i18n.t(LANGUAGE_KEYS.PERSONAL_SIGNATURE)}</Text>
            }
            subtitle={
                accountStore.currentSignmessage ? <Text style={styles.subtitle}>{accountStore.currentSignmessage}</Text> : null
            }
            trailing={

                <FontAwesome5 style={styles.trailing} name="chevron-right" color={colors.textDefault} />
            }
            onPress={this.onPressInputSignmessage}
        />
    }

    @boundMethod
    uploadImage() {
        const ld = Loading.show();
        let that = this;
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
        }).then(async (image) => {

            let options = {
                method: "POST",
                body: createFormData(image),
                headers: { "Content-Type": "multipart/form-data" },
            };

            fetch("https://blnscan.loqunbai.com/api/upload", options)
                .then((response) => response.json())
                .then((response) => {

                    let url = `https://ipfs.loqunbai.com/ipfs/${response.cid}`;
                    that.saveUrlToBlockchain(url);
                })
                .finally(() => {
                    Loading.hide(ld);
                });
        }).catch((e) => {
            Loading.hide(ld);
        });
    }
    @boundMethod
    private saveUrlToBlockchain(url: string) {
        const ld = Loading.show();
        API.blnScanUpdateProfile(
            accountStore.currentMnemonic,
            accountStore.currentAddress,
            accountStore.currentAddress,
            { imageurl: url }
        )
            .then((res: any) => {
                

                showToast(i18n.t(LANGUAGE_KEYS.SUCCESS));
                this.imageUrl = url;
            })
            .catch((err: any) => {
                showToast(`${err}`);
                console.log("Save Avatar failed.", err);

            })
            .finally(() => {
                Loading.hide(ld);
            });
    }

    _avatarItem() {
        const { styles } = obStyles
        return <LineTitle
            title={
                <View>
                    <Image
                        //添加个头像居中
                        style={styles.imgUserTitle}
                        source={{ uri: this.imageUrl  || userStore.getUserImage(accountStore.currentAddress)}}
                    />
                    <Text style={styles.imgText}>{i18n.t(LANGUAGE_KEYS.AVATAR)}</Text>
                </View>
            }
            onPress={this.uploadImage}
        />
    }
    
    @boundMethod
    private onPressInputNickname() {
        this.props.navigation.navigate(SettingsRoutes.NickName)
    }
    @boundMethod
    private onPressInputSignmessage() {
        this.props.navigation.navigate(SettingsRoutes.SignText)
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            container: {
                flex: 1
            },
            title: {
                fontSize: 16,
                marginLeft: 15,
                marginBottom: 10,
                color: colors.textDefault
            },
            subtitle: {
                marginLeft: 15,
                fontSize: 15,
                color: colors.textSecondary
            },
            trailing: {
                fontSize: 16,
                marginRight: 15
            }, imgUserTitle: {
                marginTop: 40,
                height: 80,
                width: 80,
                borderRadius: 40,
                alignSelf: 'center',
            }, imgText: {
                alignSelf: 'center',
                fontSize: 16,
                marginBottom: 10,
                color: colors.textDefault
            }
        })
    }
})

/**
 * Settings
 * @file Settings
 * @module pages/settings/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Alert, Switch, Image } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import ListTitle from '@app/components/common/list-title'
import { DeleteButton } from '@app/components/ui/button'
import { StackActions } from '@react-navigation/native'
import { HomeRoutes, SettingsRoutes } from '@app/routes'
import { accountStore } from '@app/stores/account'
import { CURRENCY, optionStore } from '@app/stores/option'
import { languageMaps } from '@app/services/i18n'
import BottomSheet from '@app/components/common/bottom-sheet'
import { LANGUAGES } from '@app/constants/language'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import TouchID from '@app/services/touchid'
import showToast from '@app/services/toast'
import { stashStore } from '@app/stores/stash'
import * as RootNavigation from '../../rootNavigation';
import Upgrade from '@app/components/common/upgrade'
import { isFunction } from 'lodash'
import { userStore } from '@app/stores/users'
import { TouchableView } from '@app/components/common/touchable-view'
import { PostListName } from '@app/components/ui/forum'

export interface IIndexProps extends IPageProps { }

@observer export class Settings extends Component<IIndexProps> {

    private unsubscribe: any;

    componentDidMount() {
        // Listen for screen focus event
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
    }

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }

    onScreenFocus = () => {
        optionStore.refreshVersion()
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>
                    <View style={styles.content}>
                        {this._nicknameItem()}
                        {/*this._profileItem()*/}
                        {this._localauthItem()}
                        {this._themeDarkItem()}
                        {this._backupItem()}
                        {this._switchLanguageItem()}
                        {this._switchCurrencyItem()}
                        {this._communityItem()}
                        {this._stashItem()}
                        {this._versionItem()}
                        {this._logoutItem()}
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }

    _nicknameItem() {
        const { styles } = obStyles
        return (<TouchableView
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Profile)
            }}>
            <View style={styles.szHeader}>
                <View style={styles.szLeft}>
                    <Image style={styles.avatar} source={{ uri: userStore.getUserImage(accountStore.currentAddress) }} />
                </View>

                <View style={styles.szName}>
                    <PostListName>{userStore.getUserName(accountStore.currentAddress)}</PostListName>
                </View>
                <View style={styles.rightlogo}>
                    <FontAwesome5
                        name="chevron-right"
                        style={styles.listTitleIcon}
                    />
                </View>
            </View>

        </TouchableView>
        )
    }

    @boundMethod
    private onPressNavigation(routeName: SettingsRoutes, params?: any) {
        this.props.navigation.navigate(routeName)
    }

    @boundMethod
    private onPressLogout() {
        accountStore.switchAccount(undefined)
        stashStore.reset();
        this.props.navigation.dispatch(StackActions.replace(HomeRoutes.Home))
    }

    @boundMethod
    private onPressDelete() {
        const resetAction = StackActions.replace(HomeRoutes.Home)
        Alert.alert(i18n.t(LANGUAGE_KEYS.WARNING), i18n.t(LANGUAGE_KEYS.DELETE_CURRENT_ACCOUNT),
            [
                {
                    text: i18n.t(LANGUAGE_KEYS.CONFIRM), onPress: () => {
                        accountStore.deleteAccount(accountStore.current)
                        accountStore.switchAccount(undefined)
                        this.props.navigation.dispatch(resetAction)
                    }
                },
                { text: i18n.t(LANGUAGE_KEYS.CANCEL) }
            ]
        );
    }
    @boundMethod
    private toggleSwitch(toggle: boolean) {
        TouchID.isSupported()
            .then((enable: boolean) => {
                if (enable) {
                    TouchID.authenticate().then(() => {
                        accountStore.updateTouchid(toggle)
                    })
                } else {
                    showToast(i18n.t(LANGUAGE_KEYS.TOUCHID_NOT_SUPPORTED))
                }
            })
    }
    @boundMethod
    private toggleThemeSwitch(toggle: boolean) {
        optionStore.updateDarkTheme(toggle)
    }

    _localauthItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="fingerprint"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.TOUCHID_FACEID)}</Text>
            }
            trailing={
                <Switch
                    trackColor={{ false: colors.grey, true: colors.primary }}
                    ios_backgroundColor={colors.background}
                    onValueChange={this.toggleSwitch}
                    value={accountStore.currentTouchidEnable}
                />
            }
        />)
    }

    _themeDarkItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="adjust"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.THEME_DARK)}</Text>
            }
            trailing={
                <Switch
                    trackColor={{ false: colors.grey, true: colors.primary }}
                    // thumbColor={accountStore.currentTouchidEnable ? "#ffffff" : "#ffffff"}
                    ios_backgroundColor={colors.background}
                    onValueChange={this.toggleThemeSwitch}
                    value={optionStore.darkTheme}
                />
            }
        />)
    }

    _profileItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={[styles.list, { marginTop: -1 }]}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="id-card"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.PROFILE)}</Text>
            }
            trailing={
                <FontAwesome5
                    color={colors.textDefault}
                    name="chevron-right"
                />
            }
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Profile)
            }}
        />)
    }

    _backupItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="shield-alt"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.BACKUP)}</Text>
            }
            onPress={() => {
                accountStore.authenticate().then(() => {
                    this.onPressNavigation(SettingsRoutes.Backup)
                }).catch((error) => {
                    showToast(`${error}`)
                })
            }}
        />)
    }

    _contactItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="address-book"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.CONTACT)}</Text>
            }
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Contact)
            }}
        />)
    }
    _switchLanguageItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="language"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.SWTICH_LANGUAGE)}</Text>
            }
            trailing={
                <View style={styles.listLanguage}>
                    <Text style={styles.listTitleText}>{languageMaps[optionStore.language].name}</Text>
                    <FontAwesome5
                        color={colors.textDefault}
                        name="chevron-right"
                    />
                </View>
            }
            onPress={() => {
                BottomSheet.selectLanguage((lang: LANGUAGES) => {
                    optionStore.updateLanguage(lang)
                }, optionStore.language)
            }}
        />)
    }
    _switchCurrencyItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="dollar-sign"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.SWTICH_CURRENCY)}</Text>
            }
            trailing={
                <View style={styles.listLanguage}>
                    <Text style={styles.listTitleText}>{optionStore.currency}</Text>
                    <FontAwesome5
                        color={colors.textDefault}
                        name="chevron-right"
                    />
                </View>
            }
            onPress={() => {
                BottomSheet.selectCurrency((currency: CURRENCY) => {
                    optionStore.updateCurrency(currency)
                }, optionStore.currency)
            }}
        />)
    }
    _communityItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="home"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.COMMUNITY)}</Text>
            }
            trailing={
                <FontAwesome5
                    color={colors.textDefault}
                    name="chevron-right"
                />
            }
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Community)
            }}
        />)
    }
    _versionItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="code"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.VERSION)}</Text>
            }
            trailing={
                <View style={styles.versionView}>
                    <Text style={styles.listTitleText}>{optionStore.currentVersion}</Text>
                    {optionStore.versionShouldUpdate ?
                        <View style={styles.versionViewPoint}></View>
                        : null}
                </View>
            }
            onPress={this.checkVersionPress}
        />)
    }
    _feedbackItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="pen-square"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            trailing={
                <FontAwesome5
                    color={colors.textDefault}
                    name="chevron-right"
                />
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.FEEDBACK)}</Text>
            }
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Feedback)
            }}
        />)
    }

    _stashItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="history"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.HISTORY)}</Text>
            }
            trailing={
                <View>
                    <FontAwesome5
                        color={colors.textDefault}
                        name="chevron-right"
                    />
                    {stashStore.pendingLength > 0 ?
                        <View style={styles.stashPoint}>
                            <Text style={{ color: '#fff', textAlign: "center" }}>{stashStore.pendingLength}</Text>
                        </View>
                        : null}
                </View>
            }
            onPress={() => {
                RootNavigation.navigate(HomeRoutes.Stash, {});
            }}
        />)
    }
    _logoutItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={styles.list}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="chevron-right"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.QUIT)}</Text>
            }
            onPress={() => {
                this.onPressLogout()
            }}
        />)
    }

    _DeleteItem() {
        return (<DeleteButton
            text={i18n.t(LANGUAGE_KEYS.DELETE)}
            onPress={() => {
                this.onPressDelete()
            }}
        />)
    }

    @boundMethod
    private checkVersionPress() {
        if (!optionStore.versionShouldUpdate) {
            return showToast(i18n.t(LANGUAGE_KEYS.ISLASTESTVERSION))
        }
        return Upgrade.update(optionStore.version)
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            versionView: {
                position: 'relative'
            },
            versionViewPoint: {
                width: 8,
                height: 8,
                borderRadius: 8,
                backgroundColor: colors.transferOut,
                position: 'absolute',
                right: 0
            },
            stashPoint: {
                width: 18,
                height: 18,
                borderRadius: 18,
                backgroundColor: colors.transferOut,
                position: 'absolute',
                right: 14,
                justifyContent: 'center',
                top: -3
            },
            container: {
                flex: 1
            },
            content: {
                borderBottomColor: colors.border,
                borderBottomWidth: 1,
                flex: 1
            },
            list: {
                // paddingLeft: 10,
                // paddingRight: 10,
                // paddingTop: 8,
                // paddingBottom: 8,
                // backgroundColor: colors.cardBackground
            },
            balance: {
                height: 200,
                alignItems: "center",
                justifyContent: "center"
            },
            balanceText: {
                color: colors.yellow,
                fontSize: 30
            },
            listTitleLeading: {
            },
            listTitleIcon: {
                color: colors.textDefault,
                fontSize: 16
            },
            listTitleText: {
                color: colors.textDefault,
                marginRight: 10
            },
            listLanguage: {
                flexDirection: 'row',
                alignItems: 'center'
            }, avatar: {
                flex: 1,
                marginTop: 10,
                height: 80,
                width: 80,
                borderRadius: 10,
                marginLeft: 20
            },
            szName: {
                marginLeft: -5,
                flex: 2,
                marginTop: 20
            },
            szHeader: {
                paddingBottom: 5,
                flexDirection: "row",
                //borderBottomColor: colors.border,
                //borderBottomWidth: 1,
                paddingTop: 20,
                paddingBottom: 20,
            },
            szLeft: {
                flex: 1,
            },
            rightlogo: {
                color: colors.textDefault,
                marginRight: 10,
                paddingTop: 10,
                flexDirection: 'column',
                justifyContent: 'center',
            }
        })
    }
})

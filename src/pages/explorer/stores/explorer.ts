/**
 * explorer store
 * @file explorer store
 * @module app/pages/explorer/stores/explorer
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { STORAGE } from '@app/constants/storage'
import { IBlnTransaction, IBlnScanStatistics, IBlnBlock, IBlnScanAccount } from '@app/types/bln'
import API from '@app/services/api'
import BigNumber from 'bignumber.js'

export class ExplorerStore {
    
    constructor() {
        this.resetStore();
    }
      
    @observable blocks: Array<IBlnBlock> = []
    @observable topAccounts: Array<IBlnScanAccount> = []
    @observable transactions: Array<IBlnTransaction> = []
    @observable statistics: IBlnScanStatistics | undefined = undefined
    
    @computed get supply() {
        return this.statistics?.supply || "1000000000"
    }
    @computed get blockCount() {
        return this.statistics?.blocks || "0"
    }
    @computed get transactionCount() {
        return this.statistics?.transactions || "0"
    }
    @computed get accountCount() {
        return this.statistics?.accounts || "0"
    }
    @action.bound
    updateBlocks(blocks: Array<IBlnBlock>) {
        this.blocks = blocks
        storage.set(STORAGE.LOCAL_EXPLORER_RECENT_BLOCKS, blocks)
    }
    @action.bound
    updateTransactions(txs: Array<IBlnTransaction>) {
        this.transactions = txs
        storage.set(STORAGE.LOCAL_EXPLORER_RECENT_TRANSACTIONS, txs)
    }
    @action.bound
    updateTopAccounts(top: Array<IBlnScanAccount>) {
        this.topAccounts = top
        storage.set(STORAGE.LOCAL_EXPLORER_TOP_ACCOUNTS, top)
    }
    @action.bound
    updateStatistics(s: IBlnScanStatistics) {
        this.statistics = s
        storage.set(STORAGE.LOCAL_EXPLORER_STATISTICS, s)
    }
    @boundMethod
    showPercent(balance: string){
        return new BigNumber(balance || "0").div(1e8).div(new BigNumber(this.statistics?.supply || "1000000000")).multipliedBy(100).toFixed(2) + "%"
    }
    @boundMethod
    refreshStatistics(){
        return API.blnScanStatistics().then((statistics)=>{
            return this.updateStatistics(Object.assign({}, this.statistics, statistics))
        })
    }
    @boundMethod
    refreshRecentTransactions(){
        return API.blnScanRecentTransactions().then((txs)=>{
            if(txs && txs.length){
                return this.updateTransactions(txs)
            }
        })
    }
    @boundMethod
    refreshRecentBlocks(){
        return API.blnScanRecentBlocks().then((blocks)=>{
            if(blocks && blocks.length){
                return this.updateBlocks(blocks)
            }
        })
    }
    @boundMethod
    refreshTopAccounts(){
        return API.blnScanTopAccounts().then((accounts)=>{
            if(accounts && accounts.length){
                return this.updateTopAccounts(accounts)
            }
        })
    }
    @boundMethod
    resetStore() {
        this.initStatistics()
        this.initBlocks()
        this.initTransactions()
        this.initTopAccounts()
    }
    @boundMethod
    private initStatistics() {
        storage.get<IBlnScanStatistics>(STORAGE.LOCAL_EXPLORER_STATISTICS)
        .then(s => {
            if (s) {
                this.updateStatistics(s)
            }
        })
    }
    @boundMethod
    private initBlocks() {
        storage.get<Array<IBlnBlock>>(STORAGE.LOCAL_EXPLORER_RECENT_BLOCKS)
        .then(blocks => {
            if(blocks){
                this.updateBlocks(blocks)
            }
        })
    }
    @boundMethod
    private initTransactions() {
        storage.get<Array<IBlnTransaction>>(STORAGE.LOCAL_EXPLORER_RECENT_TRANSACTIONS)
        .then(txs => {
            if(txs){
                this.updateTransactions(txs)
            }
        })
    }
    @boundMethod
    private initTopAccounts() {
        storage.get<Array<IBlnScanAccount>>(STORAGE.LOCAL_EXPLORER_TOP_ACCOUNTS)
        .then(as => {
            if(as){
                console.log('Init explorer top accounts:', as.length)
                this.updateTopAccounts(as)
            }
        })
    }

}

export default new ExplorerStore()
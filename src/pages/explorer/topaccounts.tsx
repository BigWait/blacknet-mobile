/**
 * Explorer Top Accounts
 * @file Top Accounts
 * @module pages/explorer/topaccounts
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, ReactNode } from 'react'
import { StyleSheet, View, Text, ScrollView, RefreshControl} from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import colors from '@app/style/colors'
import explorerStore from './stores/explorer'
import { TouchableView } from '@app/components/common/touchable-view'
import { DiscoverRoutes } from '@app/routes'
import BLNAddress from '@app/components/ui/BLNAddress'
import BLNBalance from '@app/components/ui/BLNBalance'
import { userStore } from '@app/stores/users'

export interface IIndexProps extends IPageProps {}
@observer export class ExplorerTopAccountsScreen extends Component<IIndexProps> {

  static getPageScreenOptions = ({ navigation }: any) => {
    return {
        title: i18n.t(LANGUAGE_KEYS.EXPLORER_TOP_ACCOUNTS)
    }
  }

  @observable private isRefreshing: boolean = false
  private timer: any = undefined

  constructor(props: IIndexProps) {
    super(props)
    this.onRefresh()
}

  @action
  private updateLoadingState(loading: boolean) {
      this.isRefreshing = loading
  }

  @boundMethod
  onRefresh(){
    this.updateLoadingState(true)
    explorerStore.refreshTopAccounts().finally(()=>this.updateLoadingState(false))
  }

  @boundMethod
  private onPressNavigation(routeName: DiscoverRoutes, params: any){
    this.props.navigation.navigate(routeName, params)
  }

  render() {
    const { styles } = obStyles
    return (
      <ScrollView style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
      >
        <View style={styles.content}>
          <View style={styles.last}>
            <View style={styles.lastHeader}>
                <View style={styles.lastSeq}>
                    <ListItem
                        key={"-1"}
                        leading={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.RANK)}</Text>}
                        title={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.ACCOUNT)}</Text>}
                        subtitle={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.ACCOUNT_BALANCE)}</Text>}
                        trailing={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.PERCENT)}</Text>}
                    />
                </View>
            </View>
            <View style={styles.lastList}>
              {explorerStore.topAccounts.map((account, i)=>{
                return <ListItem
                  key={i}
                  leading={<Text style={{color: colors.textDefault}}>{i+1}</Text>}
                  title={<BLNAddress>{userStore.getUserName(account.address)}</BLNAddress>}
                  subtitle={<BLNBalance short={true}>{account.realBalance}</BLNBalance>}
                  trailing={<Text style={{color: colors.textDefault}}>{explorerStore.showPercent(account.realBalance)}</Text>}
                  onPress={()=>{
                    this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                      type: "account",
                      id: account.address,
                      data: account
                    })
                  }}
                />
              })}
            </View>
          </View> 
        </View>
      </ScrollView>
    )
  }
}

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      lastList: {

      },
      last:{
      },
      lastHeader: {
        marginLeft: 20,
        marginRight: 20,
        // borderBottomWidth: 1,
        // borderBottomColor: colors.border,
      },
      lastSeq:{
        marginLeft: -20,
        marginRight: -20,
        paddingBottom: 5
      },
      container: {
        flex: 1,
        backgroundColor: colors.background
      },
      content: {

      },
      search: {
        padding: 30
      },
      searchInput: {
        borderRadius: 1,
        borderWidth: 1,
        borderColor: colors.border,
        padding: 5
      },
      listItem: {
        alignItems: 'center',
        paddingTop: 10,
        marginLeft: 10,
        marginRight: 10,
        justifyContent: 'center',
        flexDirection: 'row',
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
      },
      listItemTrailing: {
        // flex: 1,
        paddingLeft: 10
      },
      listItemLeading: {
        // flex: 1
      },
      listItemTitle: {
        flex: 1,
        paddingLeft: 10
      },
      listItemSubTitle: {
        flex: 1,
        paddingLeft: 10
      }
    })
  }
})

export interface ListItemProps {
  trailing?: ReactNode
  leading?: ReactNode
  title?: ReactNode
  subtitle?: ReactNode
  onPress?(): void
}

@observer
export class ListItem extends Component<ListItemProps> {
  constructor(props: ListItemProps) {
      super(props)
  }
  render() {
      const styles = obStyles.styles
      return (
          <TouchableView
          style={[styles.listItem]}
          onPress={this.props.onPress}
          >
              <View style={styles.listItemLeading}>
               {this.props.leading ?? this.props.leading}
              </View>
              <View style={styles.listItemTitle}>
               {this.props.title ?? this.props.title}
              </View>
              <View style={styles.listItemSubTitle}>
               {this.props.subtitle ?? this.props.subtitle}
              </View>
              <View style={styles.listItemTrailing}>
                {this.props.trailing ?? this.props.trailing}
              </View>
          </TouchableView>
      );
  }
}
/**
 * Explorer Detail
 * @file Detail
 * @module pages/explorer/detail
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, Text, FlatList, Platform, ScrollView } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed, reaction } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import { RowListItem } from '@app/components/ui/list-title'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { accountStore } from '@app/stores/account'
import { getTypeName, showTime } from '@app/utils/bln'
import { IBlnTransaction, IBlnBlock, IBlnScanAccount } from '@app/types/bln'
import API from '@app/services/api'
import BLNAddress from '@app/components/ui/BLNAddress'
import BLNBalance from '@app/components/ui/BLNBalance'
import BLNBlock from '@app/components/ui/BLNBlock'
import { DiscoverRoutes, HomeRoutes, WalletRoutes } from '@app/routes'
import colors from '@app/style/colors'
import { HeaderBlnBalance } from '@app/components/ui/BLNBalance'
import BottomSheet from '@app/components/common/bottom-sheet'
import { FilterType } from '@app/components/ui/filter'
import BottomButton from '@app/components/ui/bottom-button'
import sizes from '@app/style/sizes'
import { IRequestParams, ITxPaginate, ITxResultPaginate } from '@app/types/http'
import { NoResult } from '@app/components/ui/NoResult'
import mixins from '@app/style/mixins'
import fonts from '@app/style/fonts'
import { TransactionItem } from '@app/components/ui/list-title'
import { userStore } from '@app/stores/users'
import { MyFlatList, MyFlatListFooter_LOADING, MyFlatListFooter_LOADMORE, MyFlatListFooter_NO_MORE } from '@app/components/common/FlatList'
import BottomBar from '@app/components/ui/BottomBar'
import Image from '@app/components/common/image'
import { BLNText } from '@app/components/common/text'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { StashListItem } from '@app/components/ui/stash'
import { optionStore } from '@app/stores/option'
import showToast from '@app/services/toast'
import Loading from '@app/components/common/loading'

//const { width, height } = Dimensions.get('window');

class FilterStore {
    @observable type: number = -1
    @boundMethod changeType(type: number) {
        this.type = type
    }
}
const filterStore = new FilterStore()
type ITxResultPaginateLists = ITxResultPaginate<IBlnTransaction[]>
type TransactionListElement = RefObject<FlatList<IBlnTransaction>>

export interface IIndexProps extends IPageProps { }
@observer export class ExplorerDetailScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {

        }
    }

    private listElement: TransactionListElement = React.createRef()

    constructor(props: IIndexProps) {
        super(props)
        // 当过滤条件变化时进行重请求
        reaction(
            () => [
                filterStore.type
            ],
            ([type]: any) => this.handleFilterChanged(type)
        )
    }

    componentDidMount() {
        this.data = this.props.route.params.data
        this.type = this.props.route.params.type
        this.id = this.props.route.params.id
        // init params
        const params: IRequestParams = {}
        if (filterStore.type === -1) {
            params.type = "all"
        } else {
            params.type = filterStore.type
        }
        this.params = params
        // init title
        this.initTitle()
        // init
        this.onRefresh(true)
    }

    @observable private data: IBlnTransaction | IBlnBlock | IBlnScanAccount | undefined;
    @observable private type: 'account' | 'transaction' | 'block' | undefined;
    @observable private id: string | number | undefined;
    @observable private isLoading: boolean = false
    @observable.shallow private lists: IBlnTransaction[] = []
    @observable.ref private params: IRequestParams = {}
    @observable.ref private pagination: ITxPaginate | null = null

    @computed
    private get ListData(): IBlnTransaction[] {
        return this.lists.slice() || []
    }

    @computed
    private get isNoMoreData(): boolean {
        return (
            !!this.pagination &&
            this.pagination.txns_len < 100
        )
    }

    @boundMethod
    private handleFilterChanged(type: number) {
        // 归顶
        if (this.pagination && this.lists.length > 0) {
            this.scrollToListTop()
        }
        // 修正参数，更新数据
        setTimeout(() => {
            action(() => {
                const params: IRequestParams = {}
                if (type === -1) {
                    params.type = "all"
                } else {
                    params.type = type
                }
                this.params = params
                this.fetchLists()
            })()
        }, 266)
    }

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }

    @boundMethod
    private initTitle() {
        switch (this.type) {
            case "account":
                this.props.navigation.setOptions({
                    title: i18n.t(LANGUAGE_KEYS.ACCOUNT_INFO)
                })
                break;
            case "block":
                this.props.navigation.setOptions({
                    title: i18n.t(LANGUAGE_KEYS.BLOCK_INFO)
                })
                break;
            case "transaction":
                this.props.navigation.setOptions({
                    title: i18n.t(LANGUAGE_KEYS.TRANSACTION_INFO)
                })
                break;
            default:
                break;
        }
    }

    @action
    private updateLoadingState(loading: boolean) {
        this.isLoading = loading
    }
    @action
    private updateDataState(data: IBlnTransaction | IBlnBlock | IBlnScanAccount | undefined) {
        this.data = data
    }
    @action
    private updateResultData(result: ITxResultPaginateLists) {
        const { txns, pagination } = result
        this.pagination = pagination
        if (pagination.current_page > 1) {
            this.lists.push(...txns)
        } else {
            this.lists = txns
        }
    }
    @action
    private updateListData(txns: Array<IBlnTransaction>) {
        this.lists = txns
    }
    @boundMethod
    onRefresh(init?: boolean) {
        if (!init)
            this.updateLoadingState(true)
        switch (this.type) {
            case "account":
                API.blnScanAccount(this.id?.toString() || "").then((data) => this.updateDataState(data)).finally(() => this.updateLoadingState(false))
                this.fetchLists()
                break;
            case "block":
                API.blnScanBlock(this.id || "").then((data) => {
                    this.updateDataState(data)
                    this.updateListData(data.transactions)
                }).finally(() => this.updateLoadingState(false))
                break;
            case "transaction":
                API.blnScanTransaction(this.id?.toString() || "").then((data) => this.updateDataState(data)).finally(() => this.updateLoadingState(false))
                break;
            default:
                this.updateLoadingState(false)
                break;
        }
    }

    render() {
        const { styles } = obStyles
        const address = this.id?.toString() || ""
        return (
            <View style={styles.container}>
                <MyFlatList
                    ListHeaderComponent={
                        <View style={styles.rows}>
                            {this.data && this.type === 'account' ? this.renderListHeaderAccount() : null}
                            {this.data && this.type === 'transaction' ? this.renderListHeaderTransaction() : null}
                            {this.data && this.type === 'block' ? this.renderListHeaderBlock() : null}
                        </View>
                    }
                    style={{
                        width: sizes.screen.width
                    }}
                    data={this.ListData}
                    ref={this.listElement}
                    // 列表为空时渲染
                    ListEmptyComponent={this.renderListEmptyView}
                    // 加载更多时渲染
                    ListFooterComponent={this.type === 'account' ? this.renderListFooterView : <View style={styles.listFooterComponent}></View>}
                    // 当前列表 loading 状态
                    refreshing={this.isLoading}
                    // 刷新
                    onRefresh={this.onRefresh}
                    // 加载更多
                    onEndReached={this.handleLoadmoreList}
                    // 唯一 ID
                    keyExtractor={this.getIdKey}
                    // 单个主体
                    renderItem={({ item: tx, index }) => {
                        let view: {} | null | undefined;
                        let onpress = () => {
                            this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                                type: tx.type === 254 ? "block" : "transaction",
                                id: tx.type === 254 ? tx.blockHeight : tx.txid,
                                data: tx.type === 254 ? undefined : tx,
                            })
                        }
                        if (tx.type != 254) {
                            view = <StashListItem
                                key={index}
                                data={tx}
                                status={false}
                                onPress={onpress}
                            />
                        } else {
                            view = <TransactionItem
                                tx={tx}
                                small={true}
                                address={this.type === 'block' ? (this.data as IBlnBlock).generator : address}
                                onPress={onpress}
                            />
                        }

                        return (
                            <Observer
                                render={() => (
                                    <View style={{
                                        paddingHorizontal: Platform.OS == 'android' ? 10 : 0
                                    }}>
                                        {view}
                                    </View>
                                )}
                            />
                        )
                    }}
                />
                {/* {this.renderBottom()} */}
            </View>
        )
    }
    @boundMethod
    renderBottom() {
        const { styles } = obStyles
        const data = this.data as IBlnScanAccount
        if (this.data && this.type === 'account') {
            return <BottomBar><View style={styles.bootomBar}>
                <BottomButton text={i18n.t(LANGUAGE_KEYS.CLICK_TRANSFER)} onPress={() => {
                    this.onPressNavigation(HomeRoutes.Transfer, {
                        address: data.address
                    })
                }} />
                <BottomButton text={i18n.t(LANGUAGE_KEYS.EXPLORER_CLICK_CHAT)} onPress={() => {
                    const name = data.displayName || userStore.getName(data.address)
                    this.onPressNavigation(HomeRoutes.ChatMessage, { address: data.address, name: `${name}`, user: accountStore.currentAddress })
                }} />
            </View></BottomBar>
        }
        return null
    }
    @boundMethod
    private fetchLists(page: number = 1): Promise<any> {
        const address = this.id?.toString() || ""
        return API.fetchTransactions(address, { ...this.params, page })
            .then((json) => {
                this.updateResultData(json)
                return json
            })
            .catch(error => {
                console.warn('Fetch tx list error:', error)
                return Promise.reject(error)
            })
    }

    @boundMethod
    private handleLoadmoreList() {
        if (!this.isNoMoreData && !this.isLoading && this.pagination) {
            this.fetchLists(this.pagination.current_page + 1)
        }
    }
    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
            <Observer
                render={() => (
                    this.type === 'account' ? <NoResult /> : null
                )}
            />
        )
    }
    // 渲染脚部的三种状态：空、加载中、无更多、上拉加载
    @boundMethod
    private renderListFooterView(): JSX.Element | null {
        const { styles } = obStyles
        if (!this.lists.length) {
            return null
        }
        if (this.lists.length < 100 || this.isNoMoreData) {
            return (
                <MyFlatListFooter_NO_MORE />
            )
        }
        if (this.isLoading) {
            return (
                <MyFlatListFooter_LOADING />
            )
        }
        return (
            <MyFlatListFooter_LOADMORE />
        )
    }
    @boundMethod
    private getIdKey(tx: IBlnTransaction, index?: number): string {
        return `index:${index}:sep:${tx.hash}:${tx.seq}`
    }
    @boundMethod
    private getItemLayout(_: any, index: number) {
        const height = 30
        return {
            index,
            length: height,
            offset: height * index
        }
    }
    @boundMethod
    private onPressNavigation(routeName: DiscoverRoutes | WalletRoutes | HomeRoutes, params?: any) {
        if (this.props.route.name === HomeRoutes.ExplorerDetail && routeName === DiscoverRoutes.ExplorerDetail) {
            this.props.navigation.push(HomeRoutes.ExplorerDetail, params)
        } else {
            this.props.navigation.push(routeName, params)
        }
    }
    _ownerRow(displayName?: string | null, account?: boolean) {
        if (!displayName) {
            return null
        }
        if (account) {
            return <Text style={obStyles.styles.displayName}>{displayName}</Text>
        }
        return <RowListItem
            leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_OWNER)}</ListItemLeading>}
            trailing={<ListItemTrailing>{displayName}</ListItemTrailing>}
        />
    }
    @boundMethod
    renderListHeaderBlock() {
        const data = this.data as IBlnBlock
        return (<>
            <RowListItem
                leading={<Text style={obStyles.styles.leadingTitle}>{i18n.t(LANGUAGE_KEYS.BLOCK_INFO)}</Text>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.BLOCK_HEIGHT)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.height}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.VERSION)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.version}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>Hash</ListItemLeading>}
                trailing={<ListItemTrailing>{data.blockHash}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TIME)}</ListItemLeading>}
                trailing={<ListItemTrailing>{showTime(data.time)}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TX_SIZE)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.size} bytes</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.BLOCK_REWARD)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.reward} {accountStore.symbol}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.BLOCK_GENERATOR)}</ListItemLeading>}
                trailing={<BLNAddress style={obStyles.styles.trailing} onPress={() => {
                    this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                        type: "account",
                        id: data.generator
                    })
                }}>{userStore.getUserName(data.generator)}</BLNAddress>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.BLOCK_PREV_BLOCK)}</ListItemLeading>}
                trailing={<BLNBlock onPress={() => {
                    this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                        type: "block",
                        id: data.height - 1
                    })
                }}>{data.height - 1}</BLNBlock>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.SIGNATURE)}</ListItemLeading>}
                trailing={<ListItemMiniTrailing>{data.signature.toLowerCase()}</ListItemMiniTrailing>}
            />
            <View style={obStyles.styles.transactionsView}>
                <View style={obStyles.styles.transactionsViewSeq}><Text style={obStyles.styles.leadingTitle}>{i18n.t(LANGUAGE_KEYS.TX_TRANSACTIONS)}</Text></View>
            </View>
        </>)
    }
    @boundMethod
    renderListHeaderTransaction() {
        const data = this.data as IBlnTransaction
        return (<>
            <RowListItem
                leading={<Text style={obStyles.styles.leadingTitle}>{i18n.t(LANGUAGE_KEYS.TRANSACTION_INFO)}</Text>}
            />

            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TX_AMOUNT)}</ListItemLeading>}
                trailing={<BLNBalance>{data.amount}</BLNBalance>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TX_FEE)}</ListItemLeading>}
                trailing={<BLNBalance>{data.fee}</BLNBalance>}
            />
            {data.from ?
                <RowListItem
                    leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TX_FROM)}</ListItemLeading>}
                    trailing={<BLNAddress style={obStyles.styles.trailing} onPress={() => {
                        this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                            type: "account",
                            id: data.from
                        })
                    }}>{userStore.getUserName(data.from)}</BLNAddress>}
                />
                : null}
            {data.to ?
                <RowListItem
                    leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TX_TO)}</ListItemLeading>}
                    trailing={<BLNAddress style={obStyles.styles.trailing} onPress={() => {
                        this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                            type: "account",
                            id: data.to
                        })
                    }}>{userStore.getUserName(data.to)}</BLNAddress>}
                />
                : null}
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TIME)}</ListItemLeading>}
                trailing={<ListItemTrailing>{showTime(data.time)}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TX_SIZE)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.size} bytes</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_SEQ)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.seq}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TX_TYPE)}</ListItemLeading>}
                trailing={<ListItemTrailing>{getTypeName(data.type)}</ListItemTrailing>}
            />

            {data.blockHeight ?
                <RowListItem
                    leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.BLOCK_HEIGHT)}</ListItemLeading>}
                    trailing={<BLNBlock onPress={() => {
                        this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                            type: "block",
                            id: data.blockHeight
                        })
                    }}>{data.blockHeight}</BLNBlock>}
                />
                : null}
            {data.signature ?
                <RowListItem
                    leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.SIGNATURE)}</ListItemLeading>}
                    trailing={<ListItemMiniTrailing>{data.signature.toLowerCase()}</ListItemMiniTrailing>}
                />
                : null}
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TX_ID)}</ListItemLeading>}
                trailing={<ListItemMiniTrailing>{data.txid}</ListItemMiniTrailing>}
            />
            {data.message ?
                <RowListItem
                    leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.TX_MESSAGE)}</ListItemLeading>}
                    trailing={<ListItemMessageTrailing>{data.message}</ListItemMessageTrailing>}
                />
                : null}
        </>)
    }
    @boundMethod
    private rewardPress(reward: number) {
        const data = this.data as IBlnScanAccount
        accountStore.authenticate().then((verify) => {
            const ld = Loading.show()
            API.transfer(
                accountStore.currentMnemonic || "",
                reward.toString(),
                accountStore.currentAddress,
                data.address,
                `tip: ${reward}BLN`
            ).then((tx: IBlnTransaction) => {
                Loading.hide(ld)
                showToast(i18n.t(LANGUAGE_KEYS.TRANSFER_SUCCESS))
            })
                .finally(() => {

                    accountStore.refreshBalance()
                })
                .catch((err) => {
                    Loading.hide(ld)
                    showToast(`${err}`)

                })
        }).catch((error) => {
            showToast(`${error}`)
        })
    }
    @boundMethod
    renderListHeaderAccount() {
        const data = this.data as IBlnScanAccount
        const styles = obStyles.styles;
        return (<>
            <View style={styles.address}>
                <View style={styles.viewUserTop}>
                    <Image style={styles.avatar} source={{ uri: userStore.getUserImage(data.address) }} />
                </View>
                <View style={styles.addresswai}>
                    <Text style={styles.addressEdit}>{userStore.getName(data.address)}</Text>
                </View>
                <HeaderBlnBalance
                    balance={data.balance}
                />
                <View style={{ paddingLeft: 10, flex: 6, marginTop: 10 }}>
                    <BLNText style={{ fontSize: 12, lineHeight: 14 }}>{userStore.getDescription(data.address)}</BLNText>
                </View>
            </View>
            <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'space-between', marginLeft: 30, marginRight: 30 }}>
                <TouchableOpacity onPress={() => {
                    this.onPressNavigation(HomeRoutes.Transfer, {
                        address: data.address
                    })
                }} >
                    <Text style={styles.button}>{i18n.t(LANGUAGE_KEYS.CLICK_TRANSFER)}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    BottomSheet.selectReward(this.rewardPress)
                }} >
                    <Text style={styles.button}>{i18n.t(LANGUAGE_KEYS.BLOCK_REWARD)}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    const name = data.displayName || userStore.getName(data.address)
                    this.onPressNavigation(HomeRoutes.ChatMessage, { address: data.address, name: `${name}`, user: accountStore.currentAddress })
                }} >
                    <Text style={styles.button}>{i18n.t(LANGUAGE_KEYS.EXPLORER_CLICK_CHAT)}</Text>
                </TouchableOpacity>
            </View>

            {/* <BLNAddress style={styles.displayAddress} onPress={() => {
                    copyToast(data.address)
                }}>{data.address}</BLNAddress> */}
            {/* {this._ownerRow(data.displayName, true)} */}
            <ScrollView style={styles.xcontainer}>
                {this._walletinfo()}
            </ScrollView>

            {/*
            <ScrollableTabView
                renderTabBar={() => <ScrollableTabBar />}
            >
                <View tabLabel='详情' style={{ marginBottom: 40, paddingLeft: 5, paddingRight: 5 }}>
                </View>
                <View tabLabel='圈子' style={{ marginBottom: 40, paddingLeft: 5, paddingRight: 5 }}>
                </View>
                <View tabLabel='交易' style={{ marginBottom: 40, paddingLeft: 5, paddingRight: 5 }}>
                </View>
            </ScrollableTabView>

            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_REAL_BALANCE)}</ListItemLeading>}
                trailing={<BLNBalance>{data.realBalance}</BLNBalance>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_STAKING_BALANCE)}</ListItemLeading>}
                trailing={<ListItemTrailing><BLNBalance>{data.stakingBalance}</BLNBalance></ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_CONFIRMED_BALANCE)}</ListItemLeading>}
                trailing={<ListItemTrailing><BLNBalance>{data.confirmedBalance}</BLNBalance></ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_IN_LEASES_BALANCE)}</ListItemLeading>}
                trailing={<ListItemTrailing><BLNBalance>{data.inLeasesBalance}</BLNBalance></ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_OUT_LEASES_BALANCE)}</ListItemLeading>}
                trailing={<ListItemTrailing><BLNBalance>{data.outLeasesBalance}</BLNBalance></ListItemTrailing>}
            />

            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_TX_AMOUNT)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.txamount || 0}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_POS_BLOCKS)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.blocks || 0}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_SEQ)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.seq}</ListItemTrailing>}
            />
            {/* <View style={obStyles.styles.blanaceRow}>
                <BalanceItem
                    name={i18n.t(LANGUAGE_KEYS.ACCOUNT_STAKING_BALANCE)}
                    value={data.stakingBalance}
                />
                <BalanceItem
                    name={i18n.t(LANGUAGE_KEYS.ACCOUNT_CONFIRMED_BALANCE)}
                    value={data.confirmedBalance}
                />
            </View>
            <View style={obStyles.styles.blanaceRow}>
                <BalanceItem
                    name={i18n.t(LANGUAGE_KEYS.ACCOUNT_IN_LEASES_BALANCE)}
                    value={data.inLeasesBalance}
                />
                <BalanceItem
                    name={i18n.t(LANGUAGE_KEYS.ACCOUNT_OUT_LEASES_BALANCE)}
                    value={data.outLeasesBalance}
                />
            </View> */}
            <View style={obStyles.styles.transactionsView}>
                <Text style={obStyles.styles.transactionH1}>{i18n.t(LANGUAGE_KEYS.TX_TRANSACTIONS)}</Text>
                <View style={obStyles.styles.rightFilter}>
                    <FilterType onPress={this.onPressSelectTxType} type={filterStore.type} all={true} />
                </View>
            </View>
        </>)
    }

    @boundMethod
    private onPressSelectTxType() {
        return BottomSheet.selectTxType((type: number,) => filterStore.changeType(type), filterStore.type)
    }

    _walletinfo() {
        const { styles } = obStyles
        const data = this.data as IBlnScanAccount
        return (<>
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_REAL_BALANCE)}</ListItemLeading>}
                trailing={<BLNBalance>{data.realBalance}</BLNBalance>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_STAKING_BALANCE)}</ListItemLeading>}
                trailing={<ListItemTrailing><BLNBalance>{data.stakingBalance}</BLNBalance></ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_CONFIRMED_BALANCE)}</ListItemLeading>}
                trailing={<ListItemTrailing><BLNBalance>{data.confirmedBalance}</BLNBalance></ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_IN_LEASES_BALANCE)}</ListItemLeading>}
                trailing={<ListItemTrailing><BLNBalance>{data.inLeasesBalance}</BLNBalance></ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_OUT_LEASES_BALANCE)}</ListItemLeading>}
                trailing={<ListItemTrailing><BLNBalance>{data.outLeasesBalance}</BLNBalance></ListItemTrailing>}
            />

            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_TX_AMOUNT)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.txamount || 0}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_POS_BLOCKS)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.blocks || 0}</ListItemTrailing>}
            />
            <RowListItem
                leading={<ListItemLeading>{i18n.t(LANGUAGE_KEYS.ACCOUNT_SEQ)}</ListItemLeading>}
                trailing={<ListItemTrailing>{data.seq}</ListItemTrailing>}
            />
        </>)
    }

}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            avatar: {
                flex: 1,
                marginTop: 20,
                height: 80,
                width: 80,
                borderRadius: 40,
                alignSelf: 'center',
            },
            centerContainer: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: sizes.gap,
                backgroundColor: colors.background
            },
            loadmoreViewContainer: {
                ...mixins.rowCenter,
                padding: sizes.goldenRatioGap
            },
            normalTitle: {
                ...fonts.base,
                color: colors.textSecondary
            },
            smallTitle: {
                ...fonts.small,
                color: colors.textSecondary
            },
            transactionH1: {
                fontSize: 18,
                lineHeight: 40,
                color: colors.textDefault
            },
            rightFilter: {
                position: 'absolute',
                right: 0,
                top: 0
            },
            leading: {
                paddingRight: 15,
                color: colors.textDefault
            },
            leadingTitle: {
                paddingRight: 15,
                fontSize: 25,
                color: colors.textDefault
            },
            trailing: {
                textAlign: 'right',
                color: colors.textDefault
            },
            minitrailing: {
                fontSize: 12,
                color: colors.textSecondary
            },
            message: {
                color: colors.primary
            },

            container: {
                flex: 1,
                backgroundColor: colors.background
            },
            xcontainer: {
                flex: 1,
                marginTop: 15,
                borderTopWidth: 1,
                borderTopColor: colors.border,
                backgroundColor: colors.background
            },
            rows: {
                paddingLeft: 15,
                paddingRight: 15,
                paddingTop: 15
            },
            statusView: {
                height: 60,
                marginTop: 30,
                marginBottom: 10,
                justifyContent: 'space-between',
                alignItems: 'center',
            },
            addressStyle: {
                borderBottomColor: colors.border,
                borderBottomWidth: 1
            },
            RowListItem: {
                paddingBottom: 10,
                borderBottomWidth: 1,
                borderBottomColor: colors.border
            },
            displayName: {
                textAlign: 'center',
                marginBottom: 10
            },
            displayAddress: {

            },
            address: {
                borderBottomWidth: 1,
                borderBottomColor: colors.border,
                paddingBottom: 14,
                marginHorizontal: -20,
                paddingHorizontal: 15,
                marginBottom: 15,
                alignItems: 'center',
            },
            button: {
                color: optionStore.darkTheme ? colors.primary : colors.textDefault,
                backgroundColor: colors.deep,
                textAlign: 'center',
                lineHeight: 30,
                height: 30,
                width: 80,
                borderRadius: 10,
                //borderWidth: 1,
            },
            blanaceRow: {
                flex: 1,
                marginTop: 20,
                flexDirection: 'row',
                justifyContent: 'space-between'
            },
            transactionsView: {
                marginTop: 40,
                position: 'relative',
                borderBottomWidth: 1,
                borderColor: colors.border,
                height: 40,
                lineHeight: 40,

            },
            transactionsViewSeq: {
                paddingBottom: 10,
                // borderBottomWidth: 1,
                borderBottomColor: colors.border,
            },
            bootomBar: {
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingTop: 8,
                paddingBottom: 8
            },
            listFooterComponent: {
                height: 20
            },
            balanceItemHeader: {
                backgroundColor: colors.pure,
                borderBottomWidth: 1,
                borderColor: colors.border,
                width: '100%',
                paddingVertical: 5
            },
            viewUser: {
                height: 250,
                backgroundColor: colors.background
            },
            viewUserTop: {
                height: 100,
                alignItems: 'center',
                justifyContent: 'center'
            },
            imgUserTitle: {
                marginTop: 20,
                height: 80,
                width: 80,
                borderRadius: 40,
                alignSelf: 'center',
            },
            imgText: {
                alignSelf: 'center',
                fontSize: 16,
                marginBottom: 0,
                color: colors.background
            },
            messagewai: {
                width: 300,
                height: 80,
            },
            messageEdit: {
                marginLeft: 20,
            },
            addresswai: {
                marginTop: 10,
                marginBottom: 10,
            },
            addressEdit: {
                marginLeft: 5,
                alignSelf: 'center',
                color: '#7997C7',
                fontSize: 20
            }
        })
    }
})

export const BalanceItem = observer((props: {
    value: string,
    name: string
}): JSX.Element => {
    const styles = obStyles.styles
    return (
        <View style={[{
            flex: (1 / 2.1),
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: colors.border,
            borderWidth: 1,
            paddingBottom: 5
        }]}>
            <View style={styles.balanceItemHeader}>
                <Text style={{ textAlign: 'center', color: colors.textDefault }}>{props.name}</Text>
            </View>
            <BLNBalance style={{
                textAlign: "center",
                marginTop: 5
            }}>{props.value}</BLNBalance>
        </View>
    )
})

@observer
class ListItemLeading extends Component {
    render() {
        return <Text style={obStyles.styles.leading}>{this.props.children}</Text>;
    }
}

@observer
class ListItemTrailing extends Component {
    render() {
        return <Text style={obStyles.styles.trailing}>{this.props.children}</Text>;
    }
}

@observer
class ListItemMessageTrailing extends Component {
    render() {
        return <Text style={[obStyles.styles.trailing, obStyles.styles.message]}>{this.props.children}</Text>;
    }
}

@observer
class ListItemMiniTrailing extends Component {
    render() {
        return <Text style={[obStyles.styles.trailing, obStyles.styles.minitrailing]}>{this.props.children}</Text>;
    }
}
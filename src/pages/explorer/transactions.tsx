/**
 * Explorer Transactions
 * @file Transactions
 * @module pages/explorer/transactions
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, ReactNode } from 'react'
import { StyleSheet, View, Text, ScrollView, RefreshControl } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showTimeFromNow } from '@app/utils/bln'
import colors from '@app/style/colors'
import explorerStore from './stores/explorer'
import { TouchableView } from '@app/components/common/touchable-view'
import { getTypeName } from '@app/utils/bln'
import { DiscoverRoutes } from '@app/routes'
import BLNBalance from '@app/components/ui/BLNBalance'

export interface IIndexProps extends IPageProps { }
@observer export class ExplorerTransactionsScreen extends Component<IIndexProps> {

  static getPageScreenOptions = ({ navigation }: any) => {
    return {
      title: i18n.t(LANGUAGE_KEYS.EXPLORER_RECENT_TRANSACTIONS)
    }
  }

  @observable private isRefreshing: boolean = false
  private timer: any = undefined

  constructor(props: IIndexProps) {
    super(props)
    this.onRefresh()
  }

  @action
  private updateLoadingState(loading: boolean) {
    this.isRefreshing = loading
  }

  @boundMethod
  onRefresh() {
    this.updateLoadingState(true)
    explorerStore.refreshRecentTransactions().finally(() => this.updateLoadingState(false))
  }

  @boundMethod
  private onPressNavigation(routeName: DiscoverRoutes, params: any) {
    this.props.navigation.navigate(routeName, params)
  }

  render() {
    const { styles } = obStyles
    return (
      <ScrollView style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
      >
        <View style={styles.content}>
          <View style={styles.last}>
            <View style={styles.lastHeader}>
              <View style={styles.lastSeq}>
                <ListItem
                  key={"-1"}
                  leading={<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TIME)}</Text>}
                  title={<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TX_TYPE)}</Text>}
                  trailing={<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TX_AMOUNT)}</Text>}
                />
              </View>
            </View>
            <View style={styles.lastList}>
              {explorerStore.transactions.map((tx, i) => {
                return <ListItem
                  key={i}
                  leading={<Text style={{ color: colors.textDefault }}>{showTimeFromNow(tx.time)}</Text>}
                  title={<Text style={{ color: colors.textDefault }}>{getTypeName(tx.type)}</Text>}
                  trailing={<BLNBalance>{tx.amount}</BLNBalance>}
                  onPress={() => {
                    this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                      type: "transaction",
                      id: tx.txid,
                      data: tx
                    })
                  }}
                />
              })}
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      lastList: {

      },
      last: {
      },
      lastHeader: {
        marginLeft: 20,
        marginRight: 20
      },
      lastSeq: {
        marginLeft: -20,
        marginRight: -20,
        paddingBottom: 5
      },
      container: {
        flex: 1
      },
      content: {

      },
      search: {
        padding: 30
      },
      searchInput: {
        borderRadius: 1,
        borderWidth: 1,
        borderColor: colors.border,
        padding: 5
      },
      listItem: {
        alignItems: 'center',
        paddingTop: 10,
        marginLeft: 20,
        marginRight: 20,
        justifyContent: 'center',
        flexDirection: 'row',
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
      },
      listItemTrailing: {
        flex: 1,
        paddingLeft: 10
      },
      listItemLeading: {
        flex: 1
      },
      listItemTitle: {
        flex: 1,
        paddingLeft: 10
      }
    })
  }
})

export interface ListItemProps {
  trailing?: ReactNode
  leading?: ReactNode
  title?: ReactNode
  onPress?(): void
}

@observer
export class ListItem extends Component<ListItemProps> {
  constructor(props: ListItemProps) {
    super(props)
  }
  render() {
    const styles = obStyles.styles
    return (
      <TouchableView
        style={styles.listItem}
        onPress={this.props.onPress}
      >
        <View style={styles.listItemLeading}>
          {this.props.leading ?? this.props.leading}
        </View>
        <View style={styles.listItemTitle}>
          {this.props.title ?? this.props.title}
        </View>
        <View style={styles.listItemTrailing}>
          {this.props.trailing ?? this.props.trailing}
        </View>
      </TouchableView>
    );
  }
}
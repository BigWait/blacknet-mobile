/**
 * Poollist
 * @file Poollist
 * @module pages/explorer/poollist
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, Text, ScrollView, View, TouchableOpacity } from 'react-native'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { ChatRoutes, DiscoverRoutes, HomeRoutes, WalletRoutes } from '@app/routes'
import { boundMethod } from 'autobind-decorator'
import { optionStore } from '@app/stores/option'
import StoreAPI from '@app/services/storeapi'
import BLNBalance from '@app/components/ui/BLNBalance'
import { LineTitle } from '@app/components/ui/line-title'
import { Linking } from 'react-native'
import { accountStore } from '@app/stores/account'
import { userStore } from '@app/stores/users'

export interface IIndexProps extends IPageProps { }
  
@observer export class Poollist extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.POOLLIST)
        }
    }

    @boundMethod
    private openUrl(url: string): Promise<any> {
        return Linking
            .openURL(url)
            .catch(error => {
                console.warn('Open url failed:', error)
                return Promise.reject(error)
            })
    }

    @observable socials = [
        {
            address: "blacknet126avvjlevw9xqnmlgscytxtuf57ndwzmz569vcaatceevhcjxjfq29ywqc",
            name: "Blnpool.io",
            fee: "4%  Fee",
            balance: 0
        },
        {
            address: "blacknet1mpc0w5a7pnukgw8yvwv0funszzl8sx7jt7x9u4vl4nm0a0k3cxgqknr777",
            name: "七神矿池",
            fee: "5.7%  Fee",
            balance: 0
        },
    ]
    componentDidMount() {
        this.socials.map((social, index) => this.getStakingBalance(social))
    }

    render() {
        const { styles } = obStyles
        return (
            <ScrollView style={styles.container}>
                <LineTitle
                    key={"LineTitle"}
                    //marginDirection={"right"}
                    marginSize={15}
                    title={
                        <Text style={styles.add}>{i18n.t(LANGUAGE_KEYS.ADD_NEWPOOL)}</Text>
                    }
                    onPress={() => {
                        //this.openUrl("https://gitlab.com/blacknet-ninja/blacknet-mobile/-/issues/new?issue")
                        const name = userStore.getName('blacknet1rj0029lrcdk8x7tmu0neusflece97ve8d93heanaq7unvkz95cjq8jul46')
                        this.onPressNavigation(HomeRoutes.ChatMessage, { address: 'blacknet1rj0029lrcdk8x7tmu0neusflece97ve8d93heanaq7unvkz95cjq8jul46', name: `${name}`, user: accountStore.currentAddress })
                    }}
                />

                {this.socials.map((social, index) => {
                    return (
                        <TouchableOpacity key={social.name} onPress={() => {
                            this.onPressNavigation(HomeRoutes.Transfer, {
                                address: social.address,
                                type: 2
                            })
                        }}>
                            <View style={styles.listtittle}>
                                <View style={[styles.kuangchi]}>
                                    <Text style={[styles.nametext]}>{social.name}</Text>
                                    <Text style={[styles.fees,]}>{social.fee}</Text>
                                </View>
                                <View style={[styles.kuangchi]}>
                                    <Text style={[styles.nametext]}>{i18n.t(LANGUAGE_KEYS.STAKING_BALANCE) + "  :  "}</Text>
                                    <BLNBalance>{social.balance}</BLNBalance>
                                </View>
                                <Text style={[styles.addresstext]}>{social.address}</Text>
                            </View>

                        </TouchableOpacity>
                    )
                })}
            </ScrollView >
        )
    }

    @boundMethod
    private onPressNavigation(routeName: WalletRoutes | DiscoverRoutes | HomeRoutes, params: { address: string; type: any }, type?: any) {
        this.props.navigation.navigate(routeName, params, type)
    }

    @boundMethod
    private getStakingBalance(data?: any) {
        StoreAPI.getBalance(data.address).then((balance) => {
            data.balance = balance.stakingBalance;
        })
    }


}


const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            container: {
                flex: 1,
                backgroundColor: colors.background,
                marginTop: 10,
            },
            listtittle: {
                borderBottomWidth: 1,
                borderBottomColor: colors.border,
                textAlign: 'left',
                paddingBottom: 10,
                paddingHorizontal: 20,
            },
            balance: {
                textAlign: 'left',
                color: colors.primary,
                lineHeight: 15,
            },
            balancetext: {
                fontSize: 12,
                lineHeight: 15,
                textAlign: 'left'
            },
            kuangchi: {
                flexDirection: 'row',
                marginTop: 20,
            },
            addresstext: {
                fontSize: 12,
                lineHeight: 15,
                marginTop: 20,
                textAlign: 'left',
                color: colors.textDefault,
            },
            nametext: {
                flex: 1,
                color: optionStore.darkTheme ? colors.primary : colors.textDefault,
                fontSize: 15,
                textAlign: 'left',
            },
            fees: {
                color: optionStore.darkTheme ? colors.primary : colors.textDefault,
                marginRight: 10
            },
            add: {
                color: optionStore.darkTheme ? colors.primary : colors.textDefault,
                backgroundColor: colors.deep,
                textAlign: 'center',
                lineHeight: 30,
                height: 30,
                width: 80,
                borderRadius: 10,
                alignSelf: 'center',
                fontSize: 12,
            }
        })
    }
})

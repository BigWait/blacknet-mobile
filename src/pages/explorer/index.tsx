/**
 * Explorer
 * @file Explorer
 * @module pages/explorer/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, ScrollView, RefreshControl } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import { TextLink } from '@app/components/common/text'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { accountStore } from '@app/stores/account'
import colors from '@app/style/colors'
import explorerStore from './stores/explorer'
import { TouchableView } from '@app/components/common/touchable-view'
import { DiscoverRoutes } from '@app/routes'
import SearchTextInput from './components/searchTextInput'
import BigNumber from 'bignumber.js'
import BLNBalance from '@app/components/ui/BLNBalance'
import { isFunction } from 'lodash'

export interface IIndexProps extends IPageProps { }
@observer export class Explorer extends Component<IIndexProps> {

  static getPageScreenOptions = ({ navigation }: any) => {
    return {
      title: i18n.t(LANGUAGE_KEYS.EXPLORER)
    }
  }

  @observable private isRefreshing: boolean = false
  private showLoading: boolean = false
  private unsubscribe: any;

  componentDidMount() {
    this.showLoading = true
    // Listen for screen focus event
    this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
  }

  componentWillUnmount() {
    // unsubscribe
    isFunction(this.unsubscribe) && this.unsubscribe()
  }

  onScreenFocus = () => {
    this.showLoading = false
    this.onRefresh().finally(() => {
      this.showLoading = true
    })
  }

  @action
  private updateLoadingState(loading: boolean) {
    if (!this.showLoading) {
      return
    }
    this.isRefreshing = loading
  }

  @boundMethod
  private getMarketValue() {
    return parseFloat(new BigNumber(accountStore.blnPrice).multipliedBy(new BigNumber(explorerStore.supply)).toFixed(8)).toLocaleString() + " RMB"
  }

  @boundMethod
  private onRefresh() {
    this.updateLoadingState(true)
    explorerStore.refreshStatistics().finally(() => this.updateLoadingState(false))
    return explorerStore.refreshRecentTransactions()
  }

  render() {
    const { styles } = obStyles
    return (
      <ScrollView style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
      >
        <View style={styles.content}>
          <View style={styles.search}>
            <SearchTextInput
              navigation={this.props.navigation}
            />
          </View>
          <View style={styles.statisticsRow}>
            <StatisticsRowItem onPress={() => {
              this.props.navigation.navigate(DiscoverRoutes.Blocks)
            }} name={i18n.t(LANGUAGE_KEYS.BLOCK_AMOUNT)} value={`${explorerStore.blockCount}`} />
            <StatisticsRowItem onPress={() => {
              this.props.navigation.navigate(DiscoverRoutes.Accounts)
            }} name={i18n.t(LANGUAGE_KEYS.ACCOUNT_AMOUNT)} value={`${explorerStore.accountCount}`} />
            <StatisticsRowItem onPress={() => {
              this.props.navigation.navigate(DiscoverRoutes.Transactions)
            }} name={i18n.t(LANGUAGE_KEYS.TRANSACTION_AMOUNT)} value={`${explorerStore.transactionCount}`} />
          </View>
          <View style={styles.statistics}>
            <StatisticsItem bln={true} name={i18n.t(LANGUAGE_KEYS.SUPPLY)} value={`${explorerStore.supply}`} />
            <StatisticsItem name={i18n.t(LANGUAGE_KEYS.MARKET_VALUE)} value={`${this.getMarketValue()}`} />
          </View>
          <View style={styles.last}>
            <View style={styles.lastSeq}>
              <Text style={{ color: colors.textDefault, fontSize: 16 }}>{i18n.t(LANGUAGE_KEYS.EXPLORER_LAST_TRANSACTIONS)}</Text>
            </View>
            <View style={styles.lastList}>
              {explorerStore.transactions.map((tx, i) => {
                return <ListItem
                  key={i}
                  trailing={tx.txid}
                  leading={tx.blockHeight}
                  onLeftPress={() => {
                    this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                      type: "block",
                      id: tx.blockHeight
                    })
                  }}
                  onRightPress={() => {
                    this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                      type: "transaction",
                      id: tx.txid,
                      data: tx
                    })
                  }}
                />
              })}
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }

  @boundMethod
  private onPressNavigation(routeName: DiscoverRoutes, params: any) {
    this.props.navigation.navigate(routeName, params)
  }
}

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      lastList: {

      },
      last: {
        marginTop: 15
      },
      lastSeq: {
        marginLeft: 20,
        marginRight: 20,
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
        height: 25
      },
      container: {
        flex: 1
      },
      content: {
        backgroundColor: colors.cardBackground
      },
      search: {
        padding: 30
      },
      searchInput: {
        borderRadius: 1,
        borderWidth: 1,
        borderColor: colors.border,
        padding: 5
      },
      statistics: {
        paddingLeft: 30,
        paddingRight: 30,
        flex: 1
      },
      statisticsRow: {
        paddingLeft: 30,
        paddingRight: 30,
        flex: 1,
        flexDirection: 'row'
      }
    })
  }
})

export const StatisticsRowItem = observer((props: {
  name: string,
  value: string,
  onPress(): void
}): JSX.Element => {
  const styles = obStyles.styles
  return (
    <TouchableView
      style={[{
        flex: (1 / 3),
        alignItems: 'center'
      }]}
      onPress={props.onPress}
    >
      <TextLink>{new BigNumber(props.value).toNumber().toLocaleString()}</TextLink>
      <TextLink>{props.name}</TextLink>
    </TouchableView>
  )
})

export const StatisticsItem = observer((props: {
  value: string,
  name: string,
  bln?: boolean
}): JSX.Element => {
  const styles = obStyles.styles
  return (
    <View style={[{
      flex: 1,
      alignItems: 'center',
      paddingTop: 15,
      justifyContent: 'center',
      flexDirection: 'row'
    }]}>
      <Text style={{ color: colors.textDefault }}>{props.name}: </Text>
      {props.bln ? <BLNBalance origin={true}>{props.value}</BLNBalance> : <Text style={{ color: colors.textDefault }}>{props.value}</Text>}
    </View>
  )
})

export interface ListItemProps {
  trailing: string
  leading: string | number
  onRightPress?(): void
  onLeftPress?(): void
}

@observer
export class ListItem extends Component<ListItemProps> {
  constructor(props: ListItemProps) {
    super(props)
  }
  render() {
    const styles = obStyles.styles
    return (
      <View
        style={[{
          alignItems: 'center',
          paddingTop: 10,
          paddingBottom: 10,
          marginLeft: 20,
          marginRight: 20,
          justifyContent: 'space-around',
          flexDirection: 'row',
          borderBottomWidth: 1,
          borderBottomColor: colors.border,
        }]}
      >
        <View style={{ flex: 1 }}>
          <TouchableView
            onPress={this.props.onLeftPress}
          >
            <TextLink>{this.props.leading}</TextLink>
          </TouchableView>
        </View>
        <View style={{ flex: 3 }}>
          <TouchableView
            onPress={this.props.onRightPress}
          >
            <TextLink style={{ textAlign: 'left' }} numberOfLines={1}>{this.props.trailing}</TextLink>
          </TouchableView>
        </View>
      </View>
    );
  }
}

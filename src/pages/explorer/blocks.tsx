/**
 * Explorer Blocks
 * @file Blocks
 * @module pages/explorer/blocks
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, ReactNode } from 'react'
import { StyleSheet, View, Text, ScrollView, RefreshControl } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import { TextLink } from '@app/components/common/text'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import colors from '@app/style/colors'
import explorerStore from './stores/explorer'
import { TouchableView } from '@app/components/common/touchable-view'
import { unix_to_local_time } from '@app/utils/bln'
import { DiscoverRoutes } from '@app/routes'

export interface IIndexProps extends IPageProps { }
@observer export class ExplorerBlocksScreen extends Component<IIndexProps> {

  static getPageScreenOptions = ({ navigation }: any) => {
    return {
      title: i18n.t(LANGUAGE_KEYS.EXPLORER_RECENT_BLOCKS)
    }
  }

  @observable private isRefreshing: boolean = false
  private timer: any = undefined

  constructor(props: IIndexProps) {
    super(props)
    this.onRefresh()
  }

  @action
  private updateLoadingState(loading: boolean) {
    this.isRefreshing = loading
  }

  @boundMethod
  onRefresh() {
    this.updateLoadingState(true)
    explorerStore.refreshRecentBlocks().finally(() => this.updateLoadingState(false))
  }

  @boundMethod
  private onPressNavigation(routeName: DiscoverRoutes, params: any) {
    this.props.navigation.navigate(routeName, params)
  }

  render() {
    const { styles } = obStyles
    return (
      <ScrollView style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
      >
        <View style={styles.content}>
          <View style={styles.last}>
            <View style={styles.lastHeader}>
              <View style={styles.lastSeq}>
                <ListItem
                  key={"-1"}
                  leading={<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.BLOCK_HEIGHT)}</Text>}
                  title={<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TIME)}</Text>}
                  trailing={<Text style={{ textAlign: "right", color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.ACCOUNT_TX_AMOUNT)}</Text>}
                />
              </View>
            </View>
            <View style={styles.lastList}>
              {explorerStore.blocks.map((block, i) => {
                return <ListItem
                  key={i}
                  leading={<TextLink>{block.height}</TextLink>}
                  title={<Text style={{ color: colors.textDefault }}>{unix_to_local_time(block.time)}</Text>}
                  trailing={<Text style={{ textAlign: "right", color: colors.textDefault }}>{block.transactions.length}</Text>}
                  onPress={() => {
                    this.onPressNavigation(DiscoverRoutes.ExplorerDetail, {
                      type: "block",
                      id: block.height,
                      data: block
                    })
                  }}
                />
              })}
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      lastList: {

      },
      last: {
      },
      lastHeader: {
        marginLeft: 20,
        marginRight: 20
      },
      lastSeq: {
        marginLeft: -20,
        marginRight: -20,
        paddingBottom: 5
      },
      container: {
        flex: 1
      },
      content: {

      },
      search: {
        padding: 30
      },
      searchInput: {
        borderRadius: 1,
        borderWidth: 1,
        borderColor: colors.border,
        padding: 5
      },
      listItem: {
        alignItems: 'center',
        paddingTop: 10,
        marginLeft: 20,
        marginRight: 20,
        justifyContent: 'center',
        flexDirection: 'row',
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
      },
      listItemTrailing: {
        flex: 1,
        paddingLeft: 10
      },
      listItemLeading: {
        flex: 1
      },
      listItemTitle: {
        flex: 1,
        paddingLeft: 10
      }
    })
  }
})

export interface ListItemProps {
  trailing?: ReactNode
  leading?: ReactNode
  title?: ReactNode
  onPress?(): void
}

@observer
export class ListItem extends Component<ListItemProps> {
  constructor(props: ListItemProps) {
    super(props)
  }
  render() {
    const styles = obStyles.styles
    return (
      <TouchableView
        style={styles.listItem}
        onPress={this.props.onPress}
      >
        <View style={styles.listItemLeading}>
          {this.props.leading ?? this.props.leading}
        </View>
        <View style={styles.listItemTitle}>
          {this.props.title ?? this.props.title}
        </View>
        <View style={styles.listItemTrailing}>
          {this.props.trailing ?? this.props.trailing}
        </View>
      </TouchableView>
    );
  }
}
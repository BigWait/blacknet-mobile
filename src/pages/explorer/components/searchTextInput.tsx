/**
 * searchTextInput
 * @file searchTextInput
 * @module app/pages/explorer/components/searchTextInput
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React from 'react'
import { StyleSheet, TextInputProps } from 'react-native'
import { observable } from 'mobx';
import { observer } from 'mobx-react'
// import sizes from '@app/style/sizes'
import TextInput from '@app/components/common/text-input'
import colors from '@app/style/colors';
import { Navigation } from '@app/types/props'
import { DiscoverRoutes } from '@app/routes'
import { verifyAccount, verifyBlockNumber, verifyHash } from '@app/utils/bln'
import i18n from '@app/services/i18n';
import { LANGUAGE_KEYS } from '@app/constants/language';

export interface SearchTextInputProps {
    navigation: Navigation
}

export const SearchTextInput = observer((props: TextInputProps & SearchTextInputProps): JSX.Element => {
    const styles = obStyles.styles
    const onChangeSearch = (text: string)=>{
        if (verifyAccount(text)) {
          return props.navigation.navigate(DiscoverRoutes.ExplorerDetail, {
            type: "account",
            id: text
          })
        }
        if (verifyBlockNumber(text)) {
          return props.navigation.navigate(DiscoverRoutes.ExplorerDetail, {
            type: "block",
            id: text
          })
        }
        if (verifyHash(text)) {
          return props.navigation.navigate(DiscoverRoutes.ExplorerDetail, {
            type: "transaction",
            id: text
          })
        }
    }
    return (
      <TextInput
          style={styles.searchInput}
          onEndEditing={(e)=>onChangeSearch(e.nativeEvent.text)}
          unless-editing={true}
          placeholder={i18n.t(LANGUAGE_KEYS.EXPLORER_SEARCH_PLACEHOLDER)}
      />
  )
})

export default SearchTextInput;

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        searchInput: {
            padding: 5,
            height: 30,
            color: colors.textDefault
        }
      })
    }
  })

/**
 * Send
 * @file Send
 * @module pages/wallet/send
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import { LabelListTitle } from '@app/components/ui/list-title'
import API from '@app/services/api'
import { accountStore } from '@app/stores/account'
import Loading from '@app/components/common/loading'
import showToast from '@app/services/toast';
import { IBlnScanerURL, IBlnTransaction } from '@app/types/bln';
import { HomeRoutes, WalletRoutes } from '@app/routes'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import TextInput from '@app/components/common/text-input'
import { TouchableView } from '@app/components/common/touchable-view'
import Icon from 'react-native-vector-icons/FontAwesome5'
import BottomSheet from '@app/components/common/bottom-sheet'
import { FilterType } from '@app/components/ui/filter'
import BLNBalance from '@app/components/ui/BLNBalance'
import * as RootNavigation from '@app/rootNavigation';

class FilterStore {
    @observable type: number = 0
    @boundMethod changeType(type: number) {
        this.type = type
    }
}
export const filterStore = new FilterStore()

class FormStore {
    @observable amount?: string
    @observable address?: string
    @observable message?: string
    @observable result?: string

    @action.bound
    changeAmount(v: string) {
        this.amount = v
    }
    @action.bound
    changeAddress(v: string) {
        this.address = v
    }
    @action.bound
    changeMessage(v: string) {
        this.message = v
    }
    @action.bound
    changeResult(v?: string) {
        this.amount = undefined
        this.address = undefined
        this.message = undefined
        this.result = v
    }
    @action.bound
    reset(){
        this.amount = undefined
        this.address = undefined
        this.message = undefined
        this.result = undefined
    }
    @computed
    get verify(): boolean {
        return this.address != undefined && this.address != '' && this.amount != undefined && this.amount != ''
    }
}
export const formStore = new FormStore()

export interface IIndexProps extends IPageProps { }
@observer export class SendScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
    }

    componentDidMount(){
        formStore.reset()
        let params = this.props.route.params;

        if(params){
            if (params.address) {
                formStore.changeAddress(params.address)
            }
            
            if (params.type || params.type === 0) {
                filterStore.changeType(params.type)
            }
            if (params.amount) {
                formStore.changeAmount(params.amount)
            }
            if (params.message) {
                formStore.changeMessage(params.message)
            }
        }else{
            filterStore.changeType(0)
        }
        
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.CREATE_TRANSACTION),
            lazy : false
        }
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <FilterType onPress={this.onPressSelectTxType} type={filterStore.type} />
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={<Text style={styles.subtitle}>{i18n.t(LANGUAGE_KEYS.TX_AMOUNT)}</Text>}
                            trailing={<BLNBalance>{accountStore.currentShowBalance}</BLNBalance>}
                        />
                        <TextInput
                            onChangeText={text => formStore.changeAmount(text)}
                            value={formStore.amount}
                            placeholder={i18n.t(LANGUAGE_KEYS.INPUT_AMOUNT)}
                            style={[styles.input]}
                        />
                        <LabelListTitle
                            leading={<Text style={styles.subtitle}>{i18n.t(LANGUAGE_KEYS.ADDRESS)}</Text>}
                        />
                        <TextInput
                            style={[styles.input, { height: 60 ,
                                color: colors.textDefault}]}
                            onChangeText={text => formStore.changeAddress(text)}
                            value={formStore.address}
                            placeholder={i18n.t(LANGUAGE_KEYS.INPUT_ADDRESS)}
                            multiline={true}
                            numberOfLines={2}
                            trailing={<TouchableView style={styles.scanner}
                                onPress={this.onPressScaner}
                            >
                                <Icon name={'qrcode'} size={20}></Icon>
                            </TouchableView>}
                        />
                        {filterStore.type === 0 ? <><LabelListTitle
                            leading={<Text style={styles.subtitle}>{i18n.t(LANGUAGE_KEYS.TX_MESSAGE)}</Text>}
                        />
                            <TextInput
                                style={[styles.input, { height: 100 }]}
                                onChangeText={text => formStore.changeMessage(text)}
                                value={formStore.message}
                                multiline={true}
                                numberOfLines={4}
                                placeholder={`(${i18n.t(LANGUAGE_KEYS.OPTION)})`}
                            /></> : null}
                        <LabelListTitle
                            leading={<Text>{formStore.result}</Text>}
                        />
                    </View>
                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton text={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={() => { this.onPressBootom() }} />
                </View>
            </SafeAreaView>
        )
    }

    @boundMethod
    private onPressBootom() {
        if (!formStore.verify) {
            return showToast(i18n.t(LANGUAGE_KEYS.INPUT_ERROR))
        }
        accountStore.authenticate().then(() => {
            if (filterStore.type === 0) {
                this.pressTransfer()
            }
            if (filterStore.type === 2) {
                this.pressLease()
            }
        }).catch((error) => {
            showToast(`${error}`)
        })
    }

    @boundMethod
    private onSuccessScaner(uri: IBlnScanerURL) {
        if (uri.address) {
            formStore.changeAddress(uri.address)
        }
        if (uri.amount) {
            formStore.changeAmount(uri.amount)
        }
    }

    @boundMethod
    private onPressSelectTxType() {
        return BottomSheet.selectReceivedType((type: number,) => filterStore.changeType(type), filterStore.type)
    }

    @boundMethod
    private onPressScaner() {
        this.props.navigation.navigate(HomeRoutes.QrScanner, {
            callback: this.onSuccessScaner
        })
    }

    @boundMethod
    private pressLease() {
        const ld = Loading.show()
        API.lease(
            accountStore.currentMnemonic,
            formStore.amount || "",
            accountStore.currentAddress,
            formStore.address || ""
        ).then((tx: IBlnTransaction) => {
            showToast(i18n.t(LANGUAGE_KEYS.LEASE_SUCCESS))
            // formStore.changeResult(tx.txid)
            formStore.reset()
            // go to detail page
            RootNavigation.navigate(WalletRoutes.WalletDetail, {
                tx: tx
            })
        })
        .catch((err) => {
            showToast(`${err}`)
            console.log("Lease failed.", err);
        })
        .finally(() => {
            Loading.hide(ld)
            accountStore.refreshBalance()
        })
    }
    @boundMethod
    private pressTransfer() {
        const ld = Loading.show()
        API.transfer(
            accountStore.currentMnemonic || "",
            formStore.amount || "",
            accountStore.currentAddress,
            formStore.address || "",
            formStore.message
        ).then((tx: IBlnTransaction) => {
            showToast(i18n.t(LANGUAGE_KEYS.TRANSFER_SUCCESS))
            // formStore.changeResult(tx.txid)
            formStore.reset()
            // go to detail page
            RootNavigation.navigate(WalletRoutes.WalletDetail, {
                tx: tx
            })
        })
        .catch((err) => {
            showToast(`${err}`)
            console.log("Transfer failed.", err);
        })
        .finally(() => {
            Loading.hide(ld)
            accountStore.refreshBalance()
        })
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            input: {
                textAlignVertical: 'top',
                color: colors.textDefault
            },
            subtitle: {
                color: colors.textDefault
            },
            inputBox: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 30
            },
            scanner: {
                paddingRight: 15
            },
            bootomBar: {
                height: 40,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 20
            },
            lists: {
                flex: 1
            },
            container: {
                flex: 1,
                backgroundColor: colors.background
            }
        })
    }
})

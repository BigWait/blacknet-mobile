/**
 * Transactions
 * @file Transactions
 * @module pages/wallet/transactions
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed, reaction } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import fonts from '@app/style/fonts'
import mixins from '@app/style/mixins'
import API from '@app/services/api'
import { IRequestParams, ITxResultPaginate, ITxPaginate } from '@app/types/http'
import { IBlnTransaction } from '@app/types/bln'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { TransactionItem } from '@app/components/ui/list-title'
import BottomSheet from '@app/components/common/bottom-sheet'
import transactionStore from './stores/transaction'
import { WalletRoutes } from '@app/routes'
import { FilterType } from '@app/components/ui/filter'
import { NoResult } from '@app/components/ui/NoResult'
import { MyFlatList, MyFlatListFooter_LOADING, MyFlatListFooter_LOADMORE, MyFlatListFooter_NO_MORE } from '@app/components/common/FlatList'

class FilterStore {
    @observable type: number = -1
    @boundMethod changeType(type: number) {
        this.type = type
    }
}
export const filterStore = new FilterStore()

type ITxResultPaginateLists = ITxResultPaginate<IBlnTransaction[]>
export type TransactionListElement = RefObject<FlatList<IBlnTransaction>>
export interface IIndexProps extends IPageProps { }

@observer export class TransactionsScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
        // init params
        const params: IRequestParams = {}
        if (filterStore.type === -1) {
            params.type = "all"
        } else {
            params.type = filterStore.type
        }
        this.params = params
        this.initList()
        this.fetchLists()
        // 当过滤条件变化时进行重请求
        reaction(
            () => [
                filterStore.type
            ],
            ([type]: any) => this.handleFilterChanged(type)
        )
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.TX_TRANSACTIONS)
        }
    }

    private listElement: TransactionListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }

    @boundMethod
    private handleFilterChanged(type: number) {
        // 归顶
        if (this.pagination && this.lists.length > 0) {
            this.scrollToListTop()
        }
        // 修正参数，更新数据
        setTimeout(() => {
            action(() => {
                const params: IRequestParams = {}
                if (type === -1) {
                    params.type = "all"
                } else {
                    params.type = type
                }
                this.params = params
                this.initList()
                this.fetchLists()
            })()
        }, 266)
    }

    @observable private isLoading: boolean = false
    @observable.shallow private lists: IBlnTransaction[] = []
    @observable.ref private params: IRequestParams = {}
    @observable.ref private pagination: ITxPaginate | null = null

    @computed
    private get ListData(): IBlnTransaction[] {
        return this.lists.slice() || []
    }

    @computed
    private get isNoMoreData(): boolean {
        return (
            !!this.pagination &&
            this.pagination.txns_len < 100
        )
    }

    @action
    private updateLoadingState(loading: boolean) {
        this.isLoading = loading
    }

    @action
    private updateResultData(result: ITxResultPaginateLists) {
        const { txns, pagination } = result
        this.pagination = pagination
        if (pagination.current_page > 1) {
            this.lists.push(...txns)
        } else {
            this.lists = txns
        }
    }

    @boundMethod
    private initList(): Promise<any> {
        return transactionStore.get(accountStore.currentAddress, filterStore.type).then((json) => {
            if (json.length > 0) {
                action(() => {
                    this.lists = json
                })()
            }
            return json
        })
    }

    @boundMethod
    private fetchLists(page: number = 1): Promise<any> {
        this.updateLoadingState(true)
        const address = accountStore.currentAddress
        // if(page === 1 && !this.pagination){
        //     this.initList()
        // }  
        return API.fetchTransactions(address, { ...this.params, page })
            .then((json) => {
                this.updateResultData(json)
                if (page === 1) {
                    transactionStore.upserts(address, filterStore.type, json.txns)
                }
                return json
            })
            .catch(error => {
                console.warn('Fetch tx list error:', error)
                return Promise.reject(error)
            })
            .finally(() => this.updateLoadingState(false))
    }

    @boundMethod
    private handleLoadmoreList() {
        if (!this.isNoMoreData && !this.isLoading && this.pagination) {
            this.fetchLists(this.pagination.current_page + 1)
        }
    }

    private getArticleIdKey(tx: IBlnTransaction, index?: number): string {
        return `index:${index}:sep:${tx.hash}:${tx.seq}`
    }

    private getAricleItemLayout(_: any, index: number) {
        const height = 40
        return {
            index,
            length: height,
            offset: height * index
        }
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
            <Observer
                render={() => (
                    <NoResult />
                )}
            />
        )
    }

    // 渲染脚部的三种状态：空、加载中、无更多、上拉加载
    @boundMethod
    private renderListFooterView(): JSX.Element | null {
        const { styles } = obStyles
        if (!this.lists.length) {
            return null
        }
        if (this.lists.length < 100 || this.isNoMoreData) {
            return (
                <MyFlatListFooter_NO_MORE />
            )
        }
        if (this.isLoading) {
            return (
                <MyFlatListFooter_LOADING />
            )
        }
        return (
            <MyFlatListFooter_LOADMORE />
        )
    }

    @boundMethod
    private onPressSelectTxType() {
        return BottomSheet.selectTxType((type: number,) => filterStore.changeType(type), filterStore.type)
    }

    @boundMethod
    private onPressNavigation(routeName: WalletRoutes, tx: IBlnTransaction) {
        this.props.navigation.navigate(routeName, {
            txid: tx.txid,
            tx: tx
        })
    }

    render() {
        return (
            <View style={obStyles.styles.listViewContainer}>
                <FilterType onPress={this.onPressSelectTxType} type={filterStore.type} all={true} />
                <MyFlatList
                    style={obStyles.styles.listView}
                    data={this.ListData}
                    ref={this.listElement}
                    // 列表为空时渲染
                    ListEmptyComponent={this.renderListEmptyView}
                    // 加载更多时渲染
                    ListFooterComponent={this.renderListFooterView}
                    // 当前列表 loading 状态
                    refreshing={this.isLoading}
                    // 刷新
                    onRefresh={this.fetchLists}
                    // 加载更多
                    onEndReached={this.handleLoadmoreList}
                    // 唯一 ID
                    keyExtractor={this.getArticleIdKey}
                    // 单个主体
                    renderItem={({ item: tx }) => {
                        return (
                            <Observer
                                render={() => (
                                    <View style={obStyles.styles.listViewItem}>
                                        <TransactionItem
                                            tx={tx}
                                            small={true}
                                            symbol={accountStore.symbol}
                                            address={accountStore.currentAddress}
                                            onPress={() => {
                                                this.onPressNavigation(WalletRoutes.WalletDetail, tx)
                                            }}
                                        />
                                    </View>
                                )}
                            />
                        )
                    }}
                />
            </View>
        )
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            listViewContainer: {
                position: 'relative',
                flex: 1,
                height: "100%"
            },
            listView: {
                width: sizes.screen.width
            },
            listViewItem: {
                paddingLeft: 10,
                paddingRight: 10
            },
            centerContainer: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: sizes.gap
            },
            loadmoreViewContainer: {
                ...mixins.rowCenter,
                padding: sizes.goldenRatioGap
            },
            normalTitle: {
                ...fonts.base,
                color: colors.textSecondary
            },
            smallTitle: {
                ...fonts.small,
                color: colors.textSecondary
            },
            headerCheckedIcon: {
                position: 'absolute',
                right: sizes.gap - 4,
                bottom: -1
            }
        })
    }
})

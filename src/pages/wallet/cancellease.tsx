/**
 * CancelLease
 * @file CancelLease
 * @module pages/wallet/CancelLease
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList,  SafeAreaView, Text } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed, reaction, IReactionDisposer, autorun } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import fonts from '@app/style/fonts'
import mixins from '@app/style/mixins'
import API from '@app/services/api'
import { IRequestParams, ITxResultPaginate, ITxPaginate } from '@app/types/http'
import { IBlnLease } from '@app/types/bln'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { LeaseItem } from '@app/components/ui/list-title'
import Loading from '@app/components/common/loading'
import showToast from '@app/services/toast';
import leaseStore from './stores/lease'
import { NoResult } from '@app/components/ui/NoResult'
import { WalletRoutes } from '@app/routes'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
import BottomButton from '@app/components/ui/bottom-button'
import { TouchableView } from '@app/components/common/touchable-view'
import { formatBalance, showBalance } from '@app/utils/bln'
import { TextLink } from '@app/components/common/text'
import { stashStore } from '@app/stores/stash'
import { MyFlatList } from '@app/components/common/FlatList'

type ITxResultPaginateLists = ITxResultPaginate<IBlnLease[]>
export type LeaseListElement = RefObject<FlatList<IBlnLease>>
export interface IIndexProps extends IPageProps {}

@observer export class CancelLeaseScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
        // init lease
        this.initLists()
        // fetch lease
        this.fetchLists()
    }

    dispose?: IReactionDisposer;
    componentDidMount(){
        this.dispose = reaction(
            () => [
                stashStore.cancelLeaseLength
            ],
            ([type]: any) => {
                this.initLists()
            }
        )
    }
    componentWillUnmount(){
        if(this.dispose) this.dispose()
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          title: i18n.t(LANGUAGE_KEYS.TX_CANCEL_LEASE)
        }
    }

    private listElement: LeaseListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }

    @observable private isLoading: boolean = false
    @observable.shallow private lists: IBlnLease[] = []
    @observable.ref private params: IRequestParams = {}
    @observable.ref private pagination: ITxPaginate | null = null

    @computed
    private get ListData(): IBlnLease[] {
        return this.lists.slice() || []
    }

    @action
    private updateLoadingState(loading: boolean) {
        this.isLoading = loading
    }

    @boundMethod
    private updateResultData(result: ITxResultPaginateLists) {
        const { txns, pagination } = result
        this.pagination = pagination
        let lists = this.lists.slice()
        if (pagination.current_page > 1) {
            lists.push(...txns)
        } else {
            lists = txns
        }
        this.updateData(lists)
    }

    @action
    private updateData(lists: IBlnLease[]) {
        lists.slice().forEach((tx, i)=>{
            if(stashStore.leaseIsSuccess(accountStore.currentAddress, tx.publicKey, tx.height)){
                lists.splice(i, 1)
            }
        })
        this.lists = lists
        // save store
        leaseStore.set(accountStore.currentAddress, this.lists)
    }

    @boundMethod
    private initLists(){
        return leaseStore.get(accountStore.currentAddress).then((json)=>{
            this.updateData(json)
            return json
        })
    }

    @boundMethod
    private fetchLists(page: number = 1): Promise<any> {
        this.updateLoadingState(true)
        return API.fetchLeaseList(accountStore.currentAddress, { ...this.params, page})
        .then((json)=>{
            this.updateResultData(json)
            return json
        })
        .catch(error => {
            console.warn('Fetch tx list error:', error)
            return Promise.reject(error)
        })
        .finally(()=>this.updateLoadingState(false))
    }

    private getArticleIdKey(tx: IBlnLease, index?: number): string {
        return `index:${index}:sep:${tx.publicKey}:${tx.height}`
    }

    private getAricleItemLayout(_: any, index: number) {
        const height = 40
        return {
            index,
            length: height,
            offset: height * index
        }
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
        <Observer
            render={() => (
                <NoResult />
            )}
        />
        )
    }

    render() {
        return (
        <View style={obStyles.styles.listViewContainer}>
            <MyFlatList
                style={obStyles.styles.listView}
                data={this.ListData}
                ref={this.listElement}
                // 列表为空时渲染
                ListEmptyComponent={this.renderListEmptyView}
                // 当前列表 loading 状态
                refreshing={this.isLoading}
                // 刷新
                onRefresh={this.fetchLists}
                // 唯一 ID
                keyExtractor={this.getArticleIdKey}
                // 单个主体
                renderItem={({ item: ls, index }) => {
                return (
                    <Observer
                        render={() => (
                            <LeaseItem 
                                tx={ls}
                                onPress={()=>{
                                    this.props.navigation.navigate(WalletRoutes.WithdrawLease, {data: ls})
                                }}
                            />
                        )}
                    />
                    )
                }}
            />
        </View>
        )
    }
}

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        listViewContainer: {
          position: 'relative',
          flex: 1,
          paddingLeft: 10,
          paddingRight: 10
        },
        listView: {
          width: sizes.screen.width - 20
        },
        centerContainer: {
          justifyContent: 'center',
          alignItems: 'center',
          padding: sizes.gap
        },
        loadmoreViewContainer: {
          ...mixins.rowCenter,
          padding: sizes.goldenRatioGap
        },
        normalTitle: {
          ...fonts.base,
          color: colors.textSecondary
        },
        smallTitle: {
          ...fonts.small,
          color: colors.textSecondary
        },
        container: {
            flex: 1
        },
        bootomBar:{
            height: 40,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            paddingBottom: 10
        },
        content: {
            flex: 1,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 10
        },
        withdrawTotal: {
            borderBottomColor: colors.border,
            borderBottomWidth: 1
        },
        withdrawViewText: {
            fontSize: 16, 
            fontWeight: 'bold',
            textAlign: 'center',
            marginTop: 15,
            marginBottom: 15
        },
        withdrawViewInput: {
            flexDirection: "row",
            alignItems: "center",
            paddingTop: 15
        },
        withdrawInput: {
            flex: 1,
            borderColor: colors.border,
            borderWidth: 1,
            padding: 8
        },
        withdrawMaxButton: {
            marginLeft: 10
        },
        withdrawMaxButtonText: {
            color: colors.textMuted
        }
      })
    }
  })



class FormStore {
    @observable amount: string = ""

    @action.bound
    changeAmount(amount: string) {
        this.amount = amount
    }

    @computed
    get verify(): boolean{
        return this.amount != undefined && this.amount != ''
    }
    @computed
    get data(): any{
        return {
            amount: this.amount
        }
    }
}
export const formStore = new FormStore()
@observer export class WithdrawLeaseScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps){
        super(props)
        this.data = this.props.route.params.data
    }

    componentDidMount(){
        this.onPressMax()
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          title: i18n.t(LANGUAGE_KEYS.TX_WITHDRAW_FROM_LEASE)
        }
    }

    @observable private data: IBlnLease

    @computed
    private get LeaseData(): IBlnLease {
        return this.data
    }

    @boundMethod
    private onPressSubmit() {
        if(!formStore.verify){
            return
        }
        const tx = this.LeaseData
        accountStore.authenticate().then(()=>{
            const ld = Loading.show()
            API.cancelLease(
                accountStore.currentMnemonic, 
                formStore.amount,
                accountStore.currentAddress, 
                tx.publicKey,
                tx.height
            ).then((res:any)=>{
                showToast(i18n.t(LANGUAGE_KEYS.CANCEL_LEASE_SUCCESS))
                setTimeout(() => {
                    this.props.navigation.goBack()
                }, 1000);
            })
            .catch((err)=>{
                showToast(`${err}`)
                console.log("CancelLease failed.", err);
            })
            .finally(()=>{
                Loading.hide(ld)
                accountStore.refreshBalance()
            })
        }).catch((error)=>{
            showToast(`${error}`)
        })
    }

    @boundMethod
    private onPressMax() {
        formStore.changeAmount(formatBalance(this.LeaseData.amount))
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.container}>
                <ScrollView style={styles.content}>
                    <View style={styles.withdrawTotal}>
                        <Text style={styles.withdrawViewText}>{i18n.t(LANGUAGE_KEYS.TOTAL)}: {showBalance(this.LeaseData.amount)} {accountStore.blnSymbol}</Text>
                    </View>
                    <View style={styles.withdrawViewInput}>
                        <Text style={{marginRight: 10}}>{i18n.t(LANGUAGE_KEYS.TX_WITHDRAW)}</Text>
                        <TextInput 
                            editable={false}
                            style={styles.withdrawInput}
                            placeholder={i18n.t(LANGUAGE_KEYS.TX_AMOUNT)}
                            onChangeText={text => formStore.changeAmount(text)}
                            value={formStore.amount}
                        />
                        <Text style={{marginLeft: 10}}>{accountStore.blnSymbol}</Text>
                        <TouchableView
                            disabled={true}
                            style={styles.withdrawMaxButton}
                            onPress={this.onPressMax}
                        >
                            <TextLink style={styles.withdrawMaxButtonText}>{i18n.t(LANGUAGE_KEYS.MAX)}</TextLink>
                        </TouchableView>
                    </View>
                    <View>
                        <Text style={styles.withdrawViewText}>{i18n.t(LANGUAGE_KEYS.TX_FROM)}</Text>
                        <Text>{this.LeaseData.publicKey}</Text>
                    </View>
                </ScrollView>
                <SafeAreaView>
                    <View style={styles.bootomBar}>
                        <BottomButton text={i18n.t(LANGUAGE_KEYS.SUBMIT)} onPress={this.onPressSubmit}/>
                    </View>
                </SafeAreaView>
            </View>
        )
    }
}
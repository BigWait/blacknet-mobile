/**
 * Lease
 * @file Lease
 * @module pages/wallet/lease
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, TextInput, ScrollView } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import { LabelListTitle } from '@app/components/ui/list-title'
import API from '@app/services/api'
import { accountStore } from '@app/stores/account'
import Loading from '@app/components/common/loading'
import showToast from '@app/services/toast';
import { IBlnTransaction } from '@app/types/bln';
import { WalletRoutes } from '@app/routes'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'

export interface IIndexProps extends IPageProps { }

@observer export class LeaseScreen extends Component<IIndexProps> {

    state = {
        amount: "",
        address: "",
        result: ""
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.TX_LEASE)
        }
    }

    onChangeAmount(text: string) {
        this.setState({
            amount: text
        })
    }

    onChangeAddress(text: string) {
        this.setState({
            address: text
        })
    }

    @boundMethod
    private onPressBottom() {
        const ld = Loading.show()
        API.lease(
            accountStore.currentMnemonic,
            this.state.amount,
            accountStore.currentAddress,
            this.state.address
        ).then((tx: IBlnTransaction) => {
            showToast(i18n.t(LANGUAGE_KEYS.LEASE_SUCCESS))
            this.setState({
                amount: "",
                address: "",
                result: tx.txid
            })
            // go to detail page
            this.props.navigation.navigate(WalletRoutes.WalletDetail, {
                tx: tx
            })
        })
            .catch((err) => {
                showToast(`${err}`)
                console.log("Lease failed.", err);
            })
            .finally(() => {
                Loading.hide(ld)
                accountStore.refreshBalance()
            })
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={<Text>{i18n.t(LANGUAGE_KEYS.TX_AMOUNT)}</Text>}
                            trailing={<Text>{accountStore.currentShowBalance} {accountStore.symbol}</Text>}
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={text => this.onChangeAmount(text)}
                            value={this.state.amount}
                            placeholder={i18n.t(LANGUAGE_KEYS.INPUT_AMOUNT)}
                        />
                        <LabelListTitle
                            leading={<Text>{i18n.t(LANGUAGE_KEYS.ADDRESS)}</Text>}
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={text => this.onChangeAddress(text)}
                            value={this.state.address}
                            placeholder={i18n.t(LANGUAGE_KEYS.INPUT_ADDRESS)}
                        />
                        <LabelListTitle
                            leading={<Text>{this.state.result}</Text>}
                        />
                    </View>
                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton text={i18n.t(LANGUAGE_KEYS.TX_LEASE)} onPress={() => { this.onPressBottom() }} />
                </View>
            </SafeAreaView>
        )
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            input: {
                height: 40,
                borderColor: colors.border,
                borderWidth: 1,
                paddingLeft: 5
            },
            inputBox: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 30
            },
            bootomBar: {
                height: 40,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 10
            },
            lists: {
                flex: 1
            },
            container: {
                flex: 1
            }
        })
    }
})

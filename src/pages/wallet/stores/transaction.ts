/**
 * transactions store
 * @file transactions store
 * @module app/pages/wallet/stores/transaction
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import {  IBlnTransaction } from '@app/types/bln'

export class TransactionStore {
    
    @boundMethod
    private key(address: string, type: number):string{
        return `txns:${type}:${address}`;
    }

    @boundMethod
    get(address: string, type: number): Promise<IBlnTransaction[]>{
        return storage.get<Array<IBlnTransaction>>(this.key(address, type)).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        })  
    }

    @boundMethod
    set(address: string, type: number, txns: Array<IBlnTransaction>) {
        return storage.set(this.key(address, type), txns)
    }

    @boundMethod
    upsert(address: string, type: number, tx: IBlnTransaction) {
        return this.get(address, type).then((txns)=>{
            if(!txns){
                return this.set(address, type, [tx]) 
            }
            let index = -1;
            txns.forEach((t, i)=>{
                if(t.type === tx.type && t.txid === tx.txid){
                    index = i
                }
            })
            if(index>-1){
                txns.splice(index, 1, tx)
            }else{
                txns.unshift(tx)
            }
            txns.sort((a, b)=>{
                return parseInt(b.time) - parseInt(a.time)
            })
            return this.set(address, type, txns.slice(0, 100))
        })
    }

    @boundMethod
    upserts(address: string, type: number, ntxns: IBlnTransaction[]) {
        return this.get(address, type).then((txns)=>{
            if(!txns){
                return this.set(address, type, ntxns.slice(0, 100)) 
            }
            for (const tx of ntxns) {
                let index = -1;
                txns.forEach((t, i)=>{
                    if(t.type === tx.type && t.txid === tx.txid){
                        index = i
                    }
                })
                if(index>-1){
                    txns.splice(index, 1, tx)
                }else{
                    txns.push(tx)
                }
            }
            txns.sort((a, b)=>{
                return parseInt(b.time) - parseInt(a.time)
            })
            return this.set(address, type, txns.slice(0, 100))
        })
    }

    @boundMethod
    upsertTx(address: string, tx: IBlnTransaction){
        return Promise.all([
            this.upsert(address, -1, tx),
            this.upsert(address, tx.type, tx)
        ])
    }
}

export default new TransactionStore()
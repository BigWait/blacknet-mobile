/**
 * lease store
 * @file lease store
 * @module app/pages/wallet/stores/lease
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { IBlnLease } from '@app/types/bln'

export class LeaseStore {
    
    @boundMethod
    private key(address: string):string{
        return `lease:${address}`;
    }

    @boundMethod
    get(address: string): Promise<IBlnLease[]>{
        return storage.get<Array<IBlnLease>>(this.key(address)).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        })  
    }

    @boundMethod
    set(address: string, txns: Array<IBlnLease>) {
        return storage.set(this.key(address), txns)
    }

}

export default new LeaseStore()
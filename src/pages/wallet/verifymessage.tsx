/**
 * VerifyMessage
 * @file VerifyMessage
 * @module pages/wallet/verifymessage
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, TextInput, ScrollView } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { LabelListTitle } from '@app/components/ui/list-title'
import { accountStore } from '@app/stores/account'
import blacknetjs from 'blacknetjs';
import showToast from '@app/services/toast';
import { verifyAccount } from '@app/utils/bln'

class FormStore {
    @observable content?: string
    @observable sign?: string
    @observable address?: string

    @action.bound
    changeAddress(v: string) {
        this.address = v
    }
    @action.bound
    changeContent(v: string) {
        this.content = v
    }
    @action.bound
    changeSign(v: string) {
        this.sign = v
    }
    @action.bound
    reset() {
        this.content = undefined
        this.sign = undefined
        this.address = undefined
    }
    @computed
    get verify(): boolean {
        return this.content != undefined && this.content != '' && this.sign != undefined && this.sign != '' && verifyAccount(this.address)
    }
}
export const formStore = new FormStore()

export interface IIndexProps extends IPageProps { }
@observer export class VerifyMessageScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.TX_VERIFY_MESSAGE)
        }
    }

    componentDidMount() {
        formStore.reset()
        formStore.changeAddress(accountStore.currentAddress)
    }

    @boundMethod
    private onPressBottom() {
        if (!formStore.verify) {
            return showToast(i18n.t(LANGUAGE_KEYS.INPUT_SIGNATURE_ERROR))
        }
        try {
            if (blacknetjs.VerifyMessage(formStore.address, formStore.sign, formStore.content)) {
                showToast(i18n.t(LANGUAGE_KEYS.SUCCESS))
            } else {
                showToast(i18n.t(LANGUAGE_KEYS.FAIL))
            }
        } catch (error) {
            showToast(i18n.t(LANGUAGE_KEYS.INPUT_SIGNATURE_ERROR))
        }
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TX_MESSAGE)}</Text>}
                        />
                        <TextInput
                            style={styles.input}
                            multiline={true}
                            onChangeText={text => formStore.changeContent(text)}
                            value={formStore.content}
                            numberOfLines={5}
                        />
                        <LabelListTitle
                            leading={<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.SIGNATURE)}</Text>}
                        />
                        <TextInput
                            style={styles.input}
                            multiline={true}
                            onChangeText={text => formStore.changeSign(text)}
                            value={formStore.sign}
                            numberOfLines={5}
                        />
                        <LabelListTitle
                            leading={<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.ADDRESS)}</Text>}
                        />
                        <TextInput
                            style={[styles.input, { height: 80 }]}
                            // editable={false}
                            onChangeText={text => formStore.changeAddress(text)}
                            multiline={true}
                            numberOfLines={5}
                            defaultValue={accountStore.currentAddress}
                        />
                    </View>
                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton text={i18n.t(LANGUAGE_KEYS.TX_VERIFY_MESSAGE)} onPress={() => {
                        this.onPressBottom()
                    }} />
                </View>
            </SafeAreaView>
        )
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            input: {
                height: 120,
                borderColor: colors.border,
                borderWidth: 1,
                padding: 5,
                textAlignVertical: 'top',
                color: colors.textDefault
            },
            inputBox: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 30
            },
            bootomBar: {
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 10
            },
            lists: {
                flex: 1
            },
            container: {
                flex: 1
            }
        })
    }
})

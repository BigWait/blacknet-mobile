/**
 * Wallet
 * @file Wallet
 * @module pages/wallet/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView, RefreshControl } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import ListTitle from '@app/components/common/list-title'
import { HeaderBlnBalance } from '@app/components/ui/BLNBalance'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { DiscoverRoutes, HomeRoutes, WalletRoutes } from '@app/routes'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { accountStore } from '@app/stores/account'
import { IBlnScanerURL } from '@app/types/bln'
import { userStore } from '@app/stores/users'
import { isFunction } from 'lodash'
import { optionStore } from '@app/stores/option'
import coinmarketcap from '../coinmarketcap/stores/coinmarketcap'
import BigNumber from 'bignumber.js'
import Numeral from '@app/utils/numeral'


export interface IIndexProps extends IPageProps { }
@observer export class Wallet extends Component<IIndexProps> {

  @observable private isRefreshing: boolean = false

  private unsubscribe: any;
  private showLoading: boolean = false

  @computed
  get currency(){
    return optionStore.currency
  }

  @computed
  get balance(){
    let t = new BigNumber(0)
    const ticker = coinmarketcap.get(optionStore.blnName)
    if(ticker?.quote[this.currency]){
        t = t.plus(new BigNumber(accountStore.currencybalance).multipliedBy(ticker?.quote[this.currency].price || "0"))
    }
    return Numeral.total(t)
  }

  componentDidMount() {
    this.showLoading = true
    // Listen for screen focus event
    this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
  }

  componentWillUnmount(){
      // unsubscribe
      isFunction(this.unsubscribe) && this.unsubscribe()
  }

  onScreenFocus = () => {
      this.showLoading = false
      this.onRefresh().finally(()=>{
          this.showLoading = true
      })
  }

  @action
  private updateLoadingState(loading: boolean) {
    if (!this.showLoading) {
        return
    }
    this.isRefreshing = loading
  }

  @boundMethod
  private onRefresh() {
    this.updateLoadingState(true)
    accountStore.refreshBalance().finally(() => this.updateLoadingState(false))
    return coinmarketcap.refreshBLN()
  }

  @boundMethod
  private onPressNavigation(routeName: WalletRoutes | HomeRoutes | DiscoverRoutes, params?: any) {
    this.props.navigation.navigate(routeName, params)
  }

  @boundMethod
  private onPressScaner() {
    this.props.navigation.navigate(HomeRoutes.QrScanner, {
      callback: this.onSuccessScaner
    })
  }

  @boundMethod
  private onSuccessScaner(uri: IBlnScanerURL) {
    switch (uri.screen) {
      case HomeRoutes.AddContact:
        this.props.navigation.navigate(HomeRoutes.AddContact, uri)
        break;
      default: //HomeRoutes.Transfer
        this.props.navigation.navigate(HomeRoutes.Transfer, uri)
        break;
    }
  }

  render() {
    const { styles } = obStyles
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.content}
          refreshControl={
            <RefreshControl
              refreshing={this.isRefreshing}
              onRefresh={this.onRefresh}
            />
          }
        >
          <HeaderBlnBalance
            style={styles.balance}
            onPress={() => {
              this.onPressNavigation(HomeRoutes.ExplorerDetail, {
                type: "account",
                id: accountStore.currentAddress
              })
            }}
            balance={accountStore.currentShowBalance}
            currency={this.balance}
            nickname={userStore.getName(accountStore.currentAddress)}
          />
          <View style={styles.menus}>
            {this._sendItem()}
            {this._receivedItem()}
            {this._TransactionHistoryItem()}
            {/* {this._leaseItem()} */}
            {this._cancelLeaseItem()}
            {this._signMessageItem()}
            {this._verifyMessageItem()}
            {this._scannerItem()}
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }

  _sendItem() {
    const { styles } = obStyles
    return (<ListTitle
      style={[styles.listItem, {marginTop: -1}]}
      leading={
        <View style={[styles.listTitleLeading]}>
          <AntDesign
            name="swap"
            style={styles.listTitleIcon}
          />
        </View>
      }
      title={
        <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.CREATE_TRANSACTION)}</Text>
      }
      trailing={
        <FontAwesome5
          name="chevron-right"
          style={styles.listTitleIcon}
        />
      }
      onPress={() => this.onPressNavigation(WalletRoutes.Send)}
    />)
  }
  _receivedItem() {
    const { styles } = obStyles
    return (<ListTitle
      style={styles.listItem}
      leading={
        <View style={styles.listTitleLeading}>
          <FontAwesome5
            name="qrcode"
            style={styles.listTitleIcon}
          />
        </View>
      }
      title={
        <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.TX_RECEIVED)}</Text>
      }
      trailing={
        <FontAwesome5
          name="chevron-right"
          style={styles.listTitleIcon}
        />
      }
      onPress={() => this.onPressNavigation(WalletRoutes.Received)}
    />)
  }
  _TransactionHistoryItem() {
    const { styles } = obStyles
    return (<ListTitle
      style={styles.listItem}
      leading={
        <View style={styles.listTitleLeading}>
          <FontAwesome5
            name="list"
            style={styles.listTitleIcon}
          />
        </View>
      }
      title={
        <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.TX_TRANSACTIONS)}</Text>
      }
      trailing={
        <FontAwesome5
          name="chevron-right"
          style={styles.listTitleIcon}
        />
      }
      onPress={() => this.onPressNavigation(WalletRoutes.Transactions)}
    />)
  }
  _leaseItem() {
    const { styles } = obStyles
    return (<ListTitle
      style={styles.listItem}
      leading={
        <View style={styles.listTitleLeading}>
          <FontAwesome5
            name="chevron-right"
            style={styles.listTitleIcon}
          />
        </View>
      }
      title={
        <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.TX_LEASE)}</Text>
      }
      trailing={
        <FontAwesome5
          name="chevron-right"
          style={styles.listTitleIcon}
        />
      }
      onPress={() => this.onPressNavigation(WalletRoutes.Lease)}
    />)
  }
  _cancelLeaseItem() {
    const { styles } = obStyles
    return (<ListTitle
      style={styles.listItem}
      leading={
        <View style={styles.listTitleLeading}>
          <AntDesign
            name="swap"
            style={styles.listTitleIcon}
          />
        </View>
      }
      title={
        <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.TX_CANCEL_LEASE)}</Text>
      }
      trailing={
        <FontAwesome5
          name="chevron-right"
          style={styles.listTitleIcon}
        />
      }
      onPress={() => this.onPressNavigation(WalletRoutes.CancelLease)}
    />)
  }
  _signMessageItem() {
    const { styles } = obStyles
    return (<ListTitle
      style={styles.listItem}
      leading={
        <View style={styles.listTitleLeading}>
          <FontAwesome5
            name="user"
            style={styles.listTitleIcon}
          />
        </View>
      }
      title={
        <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.TX_SIGN_MESSAGE)}</Text>
      }
      trailing={
        <FontAwesome5
          name="chevron-right"
          style={styles.listTitleIcon}
        />
      }
      onPress={() => this.onPressNavigation(WalletRoutes.SignMessage)}
    />)
  }
  _verifyMessageItem() {
    const { styles } = obStyles
    return (<ListTitle
      style={styles.listItem}
      leading={
        <View style={styles.listTitleLeading}>
          <FontAwesome5
            name="comment"
            style={styles.listTitleIcon}
          />
        </View>
      }
      title={
        <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.TX_VERIFY_MESSAGE)}</Text>
      }
      trailing={
        <FontAwesome5
          name="chevron-right"
          style={styles.listTitleIcon}
        />
      }
      onPress={() => this.onPressNavigation(WalletRoutes.VerifyMessage)}
    />)
  }
  _scannerItem() {
    const { styles } = obStyles
    return (<ListTitle
      style={styles.listItem}
      leading={
        <View style={styles.listTitleLeading}>
          <MaterialIcons
            name="qr-code-scanner"
            style={styles.listTitleIcon}
          />
        </View>
      }
      title={
        <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.SCAN_QRCODE)}</Text>
      }
      trailing={
        <FontAwesome5
          name="chevron-right"
          style={styles.listTitleIcon}
        />
      }
      onPress={this.onPressScaner}
    />)
  }
  _verifyMessageItem2() {
    const { styles } = obStyles
    return (<ListTitle
      style={styles.listItem}
      leading={
        <View style={styles.listTitleLeading}>
          <FontAwesome5
            name="comment"
            style={styles.listTitleIcon}
          />
        </View>
      }
      title={
        <Text style={styles.listTitleText}>{"Stash"}</Text>
      }
      trailing={
        <FontAwesome5
          name="chevron-right"
          style={styles.listTitleIcon}
        />
      }
      onPress={() => this.onPressNavigation(HomeRoutes.Stash)}
    />)
  }
}

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: colors.cardBackground
      },
      content: {
        flex: 1
      },
      listItem: {
        // paddingLeft: 10,
        // paddingRight: 10,
        // paddingTop: 8,
        // paddingBottom: 8,
        // backgroundColor: colors.cardBackground
      },
      menus: {
        borderTopWidth: 1,
        borderTopColor: colors.border,
        borderBottomColor: colors.border,
        borderBottomWidth: 1
      },
      balance: {
        height: 150
      },
      balanceText: {
        color: colors.yellow,
        fontSize: 30
      },
      balanceCNY: {
        fontSize: 14
      },
      listTitleLeading: {
        
      },
      listTitleIcon: {
        color: colors.textDefault,
        fontSize: 16
      },
      listTitleText: {
        color: colors.textDefault
      },
    })
  }
})

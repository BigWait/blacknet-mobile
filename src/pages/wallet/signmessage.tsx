/**
 * SignMessage
 * @file SignMessage
 * @module pages/wallet/signmessage
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, TextInput, ScrollView } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import { LabelListTitle } from '@app/components/ui/list-title'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { accountStore } from '@app/stores/account'
import blacknetjs from 'blacknetjs';
import showToast from '@app/services/toast';
import { TouchableView } from '@app/components/common/touchable-view'
import Clipboard from '@react-native-community/clipboard'

class FormStore {
    @observable content?: string
    @observable sign?: string

    @action.bound
    changeContent(v: string) {
        this.content = v
    }
    @action.bound
    changeSign(v: string) {
        this.sign = v
    }
    @computed
    get verify(): boolean {
        return this.content != undefined && this.content != ''
    }
}
export const formStore = new FormStore()

export interface IIndexProps extends IPageProps { }
@observer export class SignMessageScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.TX_SIGN_MESSAGE)
        }
    }

    @boundMethod
    private onPressBottom() {
        if (!formStore.verify) {
            return showToast(i18n.t(LANGUAGE_KEYS.INPUT_MESSAGE))
        }
        accountStore.authenticate().then(() => {
            const sign = blacknetjs.SignMessage(accountStore.currentMnemonic, formStore.content)
            formStore.changeSign(sign)
        }).catch((error) => {
            showToast(`${error}`)
        })
    }
    @boundMethod
    private onPressCopy() {
        Clipboard.setString(formStore.sign || "")
        showToast(i18n.t(LANGUAGE_KEYS.COPIED_SIGNATURE))
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.INPUT_MESSAGE)}</Text>}
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={text => formStore.changeContent(text)}
                            value={formStore.content}
                            multiline={true}
                            numberOfLines={10}

                        />
                        {formStore.sign ?
                            <TouchableView style={styles.item} onPress={() => {
                                this.onPressCopy()
                            }}>
                                <Text style={{ color: colors.textDefault }}>{formStore.sign}</Text>
                            </TouchableView>
                            : null}
                    </View>

                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton text={i18n.t(LANGUAGE_KEYS.TX_SIGN_MESSAGE)} onPress={() => {
                        this.onPressBottom()
                    }} />
                </View>
            </SafeAreaView>
        )
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            input: {
                height: 150,
                borderColor: colors.border,
                borderWidth: 1,
                padding: 10,
                textAlignVertical: 'top',
                color: colors.textDefault
            },
            inputBox: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 30
            },
            item: {
                backgroundColor: colors.border,
                marginBottom: 20,
                borderRadius: 2,
                padding: 10,
                marginTop: 10
            },
            bootomBar: {
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: 10
            },
            lists: {
                flex: 1
            },
            container: {
                flex: 1
            }
        })
    }
})

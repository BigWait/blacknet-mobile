/**
 * Detail
 * @file Detail
 * @module pages/wallet/detil
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, ScrollView, RefreshControl } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import { LabelListTitle } from '@app/components/ui/list-title'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { accountStore } from '@app/stores/account'
import transactionStore from './stores/transaction'
import { showTime, showBalance, showAmountPrefix } from '@app/utils/bln'
import { IBlnTransaction } from '@app/types/bln'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import colors from '@app/style/colors'
import BottomBar from '@app/components/ui/BottomBar'
import BottomButton from '@app/components/ui/bottom-button'
import { HomeRoutes } from '@app/routes'
import BLNBalance from '@app/components/ui/BLNBalance'
import { TextLink } from '@app/components/common/text'
import { TouchableView } from '@app/components/common/touchable-view'
import Clipboard from '@react-native-community/clipboard'
import showToast from '@app/services/toast'
import * as RootNavigation from '../../rootNavigation';
export interface IIndexProps extends IPageProps {}
import StoreAPI from '@app/services/storeapi'

@observer export class DetailScreen extends Component<IIndexProps> {

    @observable private isRefreshing: boolean = false
    @observable private tx: IBlnTransaction;
    private timer:any = null
    constructor(props: IIndexProps) {
        super(props)
        this.tx = this.props.route.params.tx
    }

    componentDidMount(){
        if(this.isPending){
            this.timer = setInterval(()=>{
                StoreAPI.fetchTX(this.tx.txid)
                .then((tx)=>{
                    if(tx){
                        clearInterval(this.timer)
                        transactionStore.upsertTx(accountStore.currentAddress, tx)
                        this.updateTxState(tx)
                    }
                })
                .finally(()=>accountStore.refreshBalance())
            }, 3000)
        }
    }

    componentWillUnmount(){
        clearInterval(this.timer)
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          title: i18n.t(LANGUAGE_KEYS.DETAIL),
          lazy : false
        }
    }

    @action
    private updateLoadingState(loading: boolean) {
        this.isRefreshing = loading
    }
    
    @action
    private updateTxState(tx: IBlnTransaction) {
        this.tx = {...this.tx, ...tx}
    }

    @computed private get isPending() {
        return this.tx.blockHash === undefined 
    }

    @boundMethod
    private onRefresh(){
        this.updateLoadingState(true)
        StoreAPI.fetchTX(this.tx.txid)
        .then((tx)=>{
            if(tx){
                transactionStore.upsertTx(accountStore.currentAddress, tx)
                this.updateTxState(tx)
            }
            return tx
        })
        .finally(()=>{
            accountStore.refreshBalance()
            this.updateLoadingState(false)
        })
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.container}>
                <ScrollView
                    refreshControl={
                    <RefreshControl
                        refreshing={this.isRefreshing}
                        onRefresh={this.onRefresh}
                    />
                    }
                >
                    <View style={styles.rows}>
                        {this._statusRow()}
                        {this._balanceRow()}
                        {this._feeRow()}
                        {this._toRow()}
                        {this._fromRow()}
                        {this._messageRow()}
                        {this._txidRow()}
                        {this._blockRow()}
                    </View>
                </ScrollView>
                {this.renderBottom()}
            </View>
        )
    }

    @boundMethod
    renderBottom(){
        if(this.isPending){
            return null
        }
        const { styles } = obStyles
        return <BottomBar>
            <View style={styles.bootomBar}>
                <BottomButton text={i18n.t(LANGUAGE_KEYS.STASH_VIEW_EXPLORER)} onPress={this.onPressView} />
            </View>
        </BottomBar>
    }

    @boundMethod
    onPressView(){
        RootNavigation.navigate(HomeRoutes.ExplorerDetail, {
            type: "transaction",
            id: this.tx.txid,
            data: this.tx
        })
    }

    @boundMethod
    copyPress(text?: string){
        if(text) {
            Clipboard.setString(text)
            showToast(i18n.t(LANGUAGE_KEYS.COPIED_ADDRESS))
        }
    }

    _statusRow(){
        const { styles } = obStyles
        return <View style={styles.statusView}>
            {this.isPending ? <Ionicons name="ellipsis-horizontal-circle-sharp" size={36}/> : <Ionicons name="checkmark-circle" size={36} color={colors.transferIn}/>}
            <Text style={{color: colors.textDefault}}>{showTime(this.tx.time)}</Text>
        </View>
    }

    _balanceRow(){
        return <LabelListTitle 
            leading={<Text style={obStyles.styles.leading}>{i18n.t(LANGUAGE_KEYS.TX_AMOUNT)}</Text>}
            trailing={<BLNBalance origin={true} prefix={showAmountPrefix(accountStore.currentAddress, this.tx)}>{showBalance(this.tx.amount)}</BLNBalance>}
        />
    }

    _feeRow(){
        return <LabelListTitle 
            leading={<Text style={obStyles.styles.leading}>{i18n.t(LANGUAGE_KEYS.TX_FEE)}</Text>}
            trailing={<BLNBalance>{this.tx.fee}</BLNBalance>}
        />
    }

    _toRow(){
        return <LabelListTitle 
            leading={<Text style={obStyles.styles.leading}>{i18n.t(LANGUAGE_KEYS.TX_TO)}</Text>}
            trailing={<TouchableView onPress={()=>this.copyPress(this.tx.to)}><TextLink>{this.tx.to}</TextLink></TouchableView>}
        />
    }

    _fromRow(){
        return <LabelListTitle 
            leading={<Text style={obStyles.styles.leading}>{i18n.t(LANGUAGE_KEYS.TX_FROM)}</Text>}
            trailing={<TouchableView onPress={()=>this.copyPress(this.tx.from)}><TextLink>{this.tx.from}</TextLink></TouchableView>}
        />
    }

    _messageRow(){
        if(!this.tx.message){
            return null
        }
        return <LabelListTitle 
            leading={<Text style={obStyles.styles.leading}>{i18n.t(LANGUAGE_KEYS.TX_MESSAGE)}</Text>}
            trailing={<Text style={{color: colors.textDefault}}>{ this.tx.message }</Text>}
        />
    }
    _txidRow(){
        if(!this.tx.txid){
            return null
        }
        return <LabelListTitle 
            leading={<Text style={obStyles.styles.leading}>{i18n.t(LANGUAGE_KEYS.TX_ID)}</Text>}
            trailing={<Text style={{color: colors.textDefault}}>{ this.tx.txid }</Text>}
        />
    }
    _blockRow(){
        if(!this.tx.blockHeight){
            return null
        }
        return <LabelListTitle 
            leading={<Text style={obStyles.styles.leading}>{i18n.t(LANGUAGE_KEYS.TX_BLOCK_HEIGHT)}</Text>}
            trailing={<Text style={{color: colors.textDefault}}>{this.tx.blockHeight}</Text>}
        />
    }

}

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
        leading:{
            paddingRight: 15,
            color: colors.textDefault
        },
        trailing:{
            paddingRight: 10,
            color: colors.textDefault
        },
        container: {
            flex: 1
        },
        rows:{
            paddingLeft: 20,
            paddingRight: 20
        },
        statusView:{
            height: 60,
            marginTop: 30,
            marginBottom: 10,
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        bootomBar: {
            height: 40,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            paddingTop: 10
        }
    })
  }
})

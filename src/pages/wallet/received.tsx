/**
 * Received
 * @file Received
 * @module pages/wallet/received
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from "react";
import { StyleSheet, View, Text, ScrollView, Alert } from "react-native";
import { boundMethod } from "autobind-decorator";
import { observable, action, computed } from "mobx";
import { observer } from "mobx-react";
import { IPageProps } from "@app/types/props";
import colors from "@app/style/colors";
import { accountStore } from "@app/stores/account";
import QRCode from "react-native-qrcode-svg";
import i18n from "@app/services/i18n";
import { LANGUAGE_KEYS } from "@app/constants/language";
import { TouchableView } from "@app/components/common/touchable-view";
import { copyToast } from "@app/services/toast";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import AntDesign from "react-native-vector-icons/AntDesign";
import BottomSheet from "@app/components/common/bottom-sheet";
import { stringify } from "query-string";
import Dialog from "react-native-dialog";

class StateStore {
	@observable type: number | undefined = undefined;
	@observable amount: string | undefined = undefined;
	@boundMethod
	updateTypeState(type: number | undefined) {
		this.type = type;
	}
	@boundMethod
	updateAmountState(amount: string | undefined) {
		this.amount = amount;
	}
	@computed get qrcodeUrl() {
		let params = stringify({
			amount: this.amount,
			type: this.type,
			message: undefined,
		});
		return `blacknet:${accountStore.currentAddress}${params.includes("=") ? `?${params}` : params
			}`;
	}
}
export const stateStore = new StateStore();
export interface IIndexProps extends IPageProps { }

@observer
export class ReceivedScreen extends Component<IIndexProps> {
	static getPageScreenOptions = ({ navigation }: any) => {
		return {
			title: i18n.t(LANGUAGE_KEYS.MY_QRCODE),
		};
	};

	@boundMethod
	@action
	private onPressInputAmount(visible: boolean) {
		Alert.prompt(
			i18n.t(LANGUAGE_KEYS.INPUT_AMOUNT),
			"",
			[
				{
					text: i18n.t(LANGUAGE_KEYS.CONFIRM),
					onPress: (amount: string | undefined) => {
						stateStore.updateAmountState(amount);
					},
				},
				{ text: i18n.t(LANGUAGE_KEYS.CANCEL) },
			],
			"plain-text"
		);
	}

	state = {
		amount: "",
	};

	@observable private visible: boolean = false;

	@boundMethod
	@action
	private updateVisible() {
		this.visible = !this.visible;
	}

	@boundMethod
	private onPressCancel() {
		this.updateVisible();
	}

	@boundMethod
	private onPressConfirm() {
		this.updateVisible();
		if (this.state.amount) {
			stateStore.updateAmountState(this.state.amount);
		}
	}

	@boundMethod
	private onPressChangeType() {
		BottomSheet.selectReceivedType(
			(type: number) => {
				stateStore.updateTypeState(type);
			},
			stateStore.type === undefined ? -1 : stateStore.type
		);
	}

	render() {
		const showTypeName = (type?: number) => {
			switch (stateStore.type) {
				case 0:
					return i18n.t(LANGUAGE_KEYS.TX_TRANSFER);
				case 2:
					return i18n.t(LANGUAGE_KEYS.TX_LEASE);
				default:
					if (type === 0) {
						return i18n.t(LANGUAGE_KEYS.TX_TRANSFER);
					}
					return i18n.t(LANGUAGE_KEYS.SELECT_TYPE);
			}
		};
		return (
			<ScrollView style={styles.container}>
				<View style={styles.content}>
					<View style={styles.qr}>
						<View style={styles.qrContent}>
							<View style={styles.qrTitle}>
								<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.SCAN_QRCODE)} </Text>
								<Text style={{ color: colors.textDefault }}>{showTypeName(0)} </Text>
								{stateStore.amount ? (
									<Text style={styles.qrTitleStrong}>{stateStore.amount}</Text>
								) : null}
								<Text style={{ color: colors.textDefault }}>{accountStore.symbol}</Text>
							</View>
							<View style={styles.qrImg}>
								<QRCode
									value={stateStore.qrcodeUrl}
									// logo={require('@app/assets/images/1024.png')}
									// logoSize={50}
									size={300}
									logoBackgroundColor="transparent"
								/>
							</View>
							<View style={styles.qrAddress}>
								<Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.ADDRESS)}</Text>
								<TouchableView
									style={styles.qrAddressText}
									onPress={() => {
										copyToast(accountStore.currentAddress);
									}}
								>
									<Text style={{ color: colors.textDefault }}>{accountStore.currentAddress}</Text>
								</TouchableView>
							</View>
						</View>
						<View style={styles.qrBottom}>
							<TouchableView
								style={styles.inputButton}
								onPress={this.updateVisible}
							>
								<Text style={styles.inputButtonText} >
									{i18n.t(LANGUAGE_KEYS.INPUT_AMOUNT)}
								</Text>
								<FontAwesome5 style={{ color: colors.textDefault }} name="dollar-sign" />
							</TouchableView>
							<View style={styles.qrBottomSeq}></View>
							<TouchableView
								style={styles.inputButton}
								onPress={() => {
									this.onPressChangeType();
								}}
							>
								<Text style={styles.inputButtonText}>{showTypeName()}</Text>
								<AntDesign name="caretdown" style={{ color: colors.textDefault }} />
							</TouchableView>
						</View>
					</View>
				</View>
				{this.renderInputAmount()}
			</ScrollView>
		);
	}
	renderInputAmount() {
		return (
			<Dialog.Container visible={this.visible}>
				<Dialog.Title>{i18n.t(LANGUAGE_KEYS.INPUT_AMOUNT)}</Dialog.Title>
				<Dialog.Input
					style={styles.dialogInput}
					onChangeText={(amount: string) => {
						this.setState({ amount: amount });
					}}
					autoFocus={true}
				></Dialog.Input>
				<Dialog.Button
					label={i18n.t(LANGUAGE_KEYS.CONFIRM)}
					onPress={this.onPressConfirm}
				/>
				<Dialog.Button
					label={i18n.t(LANGUAGE_KEYS.CANCEL)}
					onPress={this.onPressCancel}
				/>
			</Dialog.Container>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	content: {
		paddingLeft: 20,
		paddingRight: 20,
		paddingTop: 20,
	},
	qr: {},
	qrTitle: {
		paddingTop: 20,
		paddingBottom: 20,
		fontSize: 20,
		width: 300,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
	},
	qrContent: {
		backgroundColor: colors.cardBackground,
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		borderColor: colors.border,
		borderWidth: 1,
	},
	qrBottom: {
		height: 40,
		flex: 1,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: colors.background,
		borderWidth: 1,
		borderColor: colors.border,
		borderTopWidth: 0
	},
	qrTitleStrong: {
		paddingLeft: 2,
		paddingRight: 2,
		fontWeight: "bold",
	},
	qrImg: {},
	qrBottomSeq: {
		height: 20,
		width: 1,
	},
	qrAddress: {
		paddingTop: 20,
		paddingBottom: 20,
		justifyContent: "center",
		alignItems: "center",
	},
	qrAddressText: {
		width: 300,
		marginTop: 10,
	},
	inputButton: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
	},
	inputButtonText: {
		paddingRight: 5,
		color: colors.textDefault
	},
	dialogInput: {
		borderWidth: 1,
		borderColor: colors.border,
	},
});

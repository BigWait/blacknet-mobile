/**
 * Stash Detail
 * @file Stash Detail
 * @module pages/home/stash_detail
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, ScrollView, RefreshControl, SafeAreaView } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { accountStore } from '@app/stores/account'
import { showBalance } from '@app/utils/bln'
import { BlnTxType, IBlnTransaction } from '@app/types/bln'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import API from '@app/services/api'
import StoreAPI from '@app/services/storeapi'
import { stashStore } from '@app/stores/stash'
import { HomeRoutes } from '@app/routes'
import Loading from '@app/components/common/loading'
import showToast from '@app/services/toast'

export interface IIndexProps extends IPageProps { }

@observer export class StashDetailScreen extends Component<IIndexProps> {

    @observable private isRefreshing: boolean = false
    @observable private tx: IBlnTransaction;
    private timer: any = null

    constructor(props: IIndexProps) {
        super(props)
        this.tx = this.props.route.params.tx
        this.onRefresh()
    }

    componentDidMount() {
        if (this.isPending) {
            this.timer = setInterval(() => {
                StoreAPI.fetchTX(this.tx.txid)
                    .then((tx) => this.updateTxState(tx))
            }, 3000)
        }
    }

    componentWillUnmount() {
        this.timer && clearInterval(this.timer)
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.DETAIL)
        }
    }

    @action
    private updateLoadingState(loading: boolean) {
        this.isRefreshing = loading
    }

    @action
    private updateTxState(tx: IBlnTransaction) {

        if (tx == undefined) return;
        this.tx = tx
        stashStore.add(accountStore.currentAddress, tx)
    }

    @computed private get isPending() {

        if (this.tx == undefined) return true;
        return this.tx.blockHash === undefined
    }

    @boundMethod
    private onRefresh() {
        this.updateLoadingState(true)
        StoreAPI.fetchTX(this.tx.txid)
            .then((tx) => this.updateTxState(tx))
            .finally(() => {
                this.updateLoadingState(false)
            })
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.container}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.isRefreshing}
                            onRefresh={this.onRefresh}
                        />
                    }
                >
                    <View style={styles.rows}>
                        {this.renderStatus()}
                        {this.renderTransfer()}
                        {this.renderMessage()}
                    </View>
                </ScrollView>
                <View style={styles.bootomBar}>
                    {this.renderBottom()}
                </View>
            </SafeAreaView>
        )
    }

    renderStatus() {
        const { styles } = obStyles
        return <View style={styles.statusView}>
            {this.isPending ? <Ionicons name="ellipsis-horizontal-circle-sharp" size={36} /> : <Ionicons name="checkmark-circle" size={36} color={colors.transferIn} />}
        </View>
    }
    renderTransfer() {
        const { styles } = obStyles
        return <View style={styles.transferView}>
            <Text style={{ color: colors.textDefault }}>{this.tx.from}</Text>
            <Text style={styles.viewBoldText}>{i18n.t(LANGUAGE_KEYS.TX_TRANSFER)} {showBalance(this.tx.amount)} {accountStore.blnSymbol}</Text>
            <Text style={{ color: colors.textDefault }}>{this.tx.to}</Text>
        </View>
    }
    renderMessage() {
        const { styles } = obStyles
        if (!this.tx.message) {
            return null
        }
        return <View style={styles.messageView}>
            <Text style={styles.viewBoldText}>{i18n.t(LANGUAGE_KEYS.TX_WITH_MESSAGE)}</Text>
            <Text style={{ color: colors.textDefault }}>{this.tx.message}</Text>
        </View>
    }
    renderBottom() {
        const { styles } = obStyles
        if (this.isPending) {
            <BottomButton style={styles.bootomButton} text={i18n.t(LANGUAGE_KEYS.STASH_BROADCAST_AGAIN)} onPress={this.onPressBroadcast} />
        }
        return <BottomButton style={styles.bootomButton} text={i18n.t(LANGUAGE_KEYS.STASH_VIEW_EXPLORER)} onPress={this.onPressView} />
    }

    @boundMethod
    onPressBroadcast() {
        const ld = Loading.show()
        const tx = this.tx
        if (this.tx.type === BlnTxType.Lease) {
            return API.lease(
                accountStore.currentMnemonic,
                tx.amount,
                accountStore.currentAddress,
                tx.to || ""
            ).then((tx: IBlnTransaction) => {
                showToast(i18n.t(LANGUAGE_KEYS.LEASE_SUCCESS))
            })
                .catch((err) => {
                    showToast(`${err}`)
                    console.log("Lease again failed.", err);
                })
                .finally(() => {
                    Loading.hide(ld)
                    accountStore.refreshBalance()
                })
        }
        if (this.tx.type === BlnTxType.Transfer) {
            API.transfer(
                accountStore.currentMnemonic || "",
                tx.amount,
                accountStore.currentAddress,
                tx.to || "",
                tx.message
            ).then((txid: any) => {
                showToast(i18n.t(LANGUAGE_KEYS.TRANSFER_SUCCESS))
            })
                .catch((err) => {
                    showToast(`${err}`)
                    console.log("Transfer again failed.", err);
                })
                .finally(() => {
                    Loading.hide(ld)
                    accountStore.refreshBalance()
                })
        }
        if (this.tx.type === BlnTxType.CancelLease) {
            API.cancelLease(
                accountStore.currentMnemonic,
                tx.amount,
                accountStore.currentAddress,
                tx.to || "",
                tx.leaseHeight || 0
            ).then((res: any) => {
                showToast(i18n.t(LANGUAGE_KEYS.CANCEL_LEASE_SUCCESS))
            })
                .catch((err) => {
                    showToast(`${err}`)
                    console.log("CancelLease failed.", err);
                })
                .finally(() => {
                    Loading.hide(ld)
                    accountStore.refreshBalance()
                })
        }
        return
    }

    @boundMethod
    onPressView() {
        this.props.navigation.navigate(HomeRoutes.ExplorerDetail, {
            type: "transaction",
            id: this.tx.txid,
            data: this.tx
        })
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            container: {
                flex: 1,
                backgroundColor: colors.background
            },
            rows: {
                paddingLeft: 20,
                paddingRight: 20
            },
            statusView: {
                marginTop: 20,
                paddingBottom: 15,
                justifyContent: 'space-between',
                alignItems: 'center',
                borderBottomColor: colors.border,
                borderBottomWidth: 1
            },
            transferView: {
                marginTop: 15,
                paddingBottom: 15,
                borderBottomColor: colors.border,
                borderBottomWidth: 1
            },
            viewBoldText: {
                marginTop: 10,
                marginBottom: 10,
                textAlign: "center",
                fontWeight: 'bold',
                fontSize: 16,
                color: colors.textDefault
            },
            messageView: {
                marginTop: 5,
                marginBottom: 10
            },
            bootomBar: {
                height: 50,
                borderStyle: "solid",

                flexDirection: "column",
                alignItems: "center",
                justifyContent: 'center'
            },
            bootomButton: {
                backgroundColor: colors.deep,
                flex: undefined,
                height: 40,
                maxWidth: 180,
                paddingLeft: 15,
                paddingRight: 15,
                borderRadius: 5
            }
        })
    }
})

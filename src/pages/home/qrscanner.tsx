/**
 * QrScanner
 * @file QrScanner
 * @module pages/home/qrscanner
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { Camera } from '@app/components/common/camera'
import { WithActivityIndicator } from '@app/components/common/activity-indicator'
import sizes from '@app/style/sizes'
import { parseQrscanerURL } from '@app/utils/bln'
import { isFunction } from 'lodash'

export interface IIndexProps extends IPageProps { }

@observer
export class QrScanner extends Component<IIndexProps> {

  static getPageScreenOptions = ({ navigation }: any) => {
    return {
      title: i18n.t(LANGUAGE_KEYS.SCAN_QRCODE)
    }
  }

  state = {
    scanBarcode: true
  }

  @boundMethod
  onSuccess(codeStringValue: string) {
    if (!this.state.scanBarcode) {
      return
    }
    if (codeStringValue) {
      const { onReadCode, callback } = this.props.route.params
      this.setState({ scanBarcode: false }, () => {
        this.props.navigation.goBack()
        if (isFunction(callback)) {
          callback(parseQrscanerURL(codeStringValue))
        }
        if (isFunction(onReadCode)) {
          onReadCode(codeStringValue)
        }
      })
    } else {
      //   if(isFunction(callback)){
      //     callback(parseQrscanerURL('', -1, i18n.t(LANGUAGE_KEYS.SCAN_QRCODE_ERROR)))
      //   }
      //   if(isFunction(onReadCode)){
      //     onReadCode(event)
      //   }
    }
  }

  render() {
    return <View style={{ flex: 1, backgroundColor: 'black' }}>
      <WithActivityIndicator>
        <Camera
          onSuccess={this.onSuccess}
        />
      </WithActivityIndicator>
      {this.renderFinder()}
    </View>
  }
  renderFinder() {
    const { styles } = obStyles
    const size = Math.min(sizes.screen.width, sizes.screen.height) / 3 * 2
    return <View style={styles.finderPort}>
      <View style={{ width: size, height: size, justifyContent: "space-between" }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={[styles.border, { borderTopWidth: 5, borderLeftWidth: 5, borderTopLeftRadius: 16 }]}></View>
          <View style={[styles.border, { borderTopWidth: 5, borderRightWidth: 5, borderTopRightRadius: 16 }]}></View>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={[styles.border, { borderBottomWidth: 5, borderLeftWidth: 5, borderBottomLeftRadius: 16 }]}></View>
          <View style={[styles.border, { borderBottomWidth: 5, borderRightWidth: 5, borderBottomRightRadius: 16 }]}></View>
        </View>
      </View>
    </View>
  }
}

export default QrScanner

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      finderPort: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
      border: {
        borderColor: colors.primary,
        width: 40,
        height: 40
      }
    })
  }
})
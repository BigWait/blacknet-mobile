/**
 * Stash
 * @file Stash
 * @module pages/home/stash
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { action, computed, observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { IBlnTransaction } from '@app/types/bln'
import { stashStore } from '@app/stores/stash'
import { FlatList } from 'react-native-gesture-handler'
import sizes from '@app/style/sizes'
import mixins from '@app/style/mixins'
import fonts from '@app/style/fonts'
import colors from '@app/style/colors'
import { StashListItem } from '@app/components/ui/stash'
import { HomeRoutes } from '@app/routes'
import { MyFlatList } from '@app/components/common/FlatList'

export interface IIndexProps extends IPageProps { }

type TransactionListElement = RefObject<FlatList<IBlnTransaction>>

@observer export class StashScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.STASH)
        }
    }

    constructor(props: IIndexProps) {
        super(props)
    }

    private listElement: TransactionListElement = React.createRef()
    @observable private isLoading: boolean = false
    private showLoading: boolean = false

    componentDidMount() {
        this.onRefresh()
        this.showLoading = true
    }

    @action
    private updateLoadingState(loading: boolean) {
        if (!this.showLoading) {
            return
        }
        this.isLoading = loading
    }

    @computed
    private get ListData(): IBlnTransaction[] {
        return stashStore.lists.slice()
    }

    private getIdKey(tx: IBlnTransaction, index?: number): string {
        return `index:${index}:sep:${tx.txid}`
    }

    private getItemLayout(_: any, index: number) {
        const height = 40
        return {
            index,
            length: height,
            offset: height * index
        }
    }

    @boundMethod
    private onRefresh() {
        this.updateLoadingState(true)
        stashStore.refresh(accountStore.currentAddress).finally(() => this.updateLoadingState(false))
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.container}>
                <MyFlatList
                    style={styles.listView}
                    data={this.ListData}
                    ref={this.listElement}
                    // 唯一 ID
                    keyExtractor={this.getIdKey}
                    // 当前列表 loading 状态
                    refreshing={this.isLoading}
                    // 刷新
                    onRefresh={this.onRefresh}
                    // 单个主体
                    renderItem={({ item: post, index }) => {
                        return (
                            <Observer
                                render={() => (
                                    <StashListItem
                                        key={index}
                                        data={post}
                                        onPress={() => {
                                            this.props.navigation.push(HomeRoutes.StashDetail, { tx: post })
                                        }}
                                    />
                                )}
                            />
                        )
                    }}
                />
            </View>
        )
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            container: {
                flex: 1,
                backgroundColor: colors.background
            },
            listView: {
                width: sizes.screen.width
            },
            centerContainer: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: sizes.gap
            },
            loadmoreViewContainer: {
                ...mixins.rowCenter,
                padding: sizes.goldenRatioGap
            },
            normalTitle: {
                ...fonts.base,
                color: colors.textSecondary
            },
            smallTitle: {
                ...fonts.small,
                color: colors.textSecondary
            }
        })
    }
})

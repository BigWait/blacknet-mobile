/**
 * Regexp constant
 * @file Regexp
 * @module app/constants/regexp
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */
export const EMAIL = /\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/


export const URL = /^https?:\/\//i


export const MIME_VIDEO = /^video\//i

export const MIME_AUDIO = /^audio\//i

export const MIME_IMAGE = /^image\//i

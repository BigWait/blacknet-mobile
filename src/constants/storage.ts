/**
 * Storage constant
 * @file 通用存储键
 * @module app/constants/storage
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

export enum STORAGE {
  LOCAL_LANGUAGE = 'localLanguage',
  LOCAL_DARK_THEME = 'localDarkTheme',
  LOCAL_ACCOUNTS = 'localAccounts',
  LOCAL_CURRENT_ACCOUNT = 'localCurrentAccount',
  LOCAL_PRICE_BLN = 'localPriceBln',
  LOCAL_EXPLORER_RECENT_BLOCKS = 'localExplorerRecentBlocks',
  LOCAL_EXPLORER_RECENT_TRANSACTIONS = 'localExplorerRecentTransactions',
  LOCAL_EXPLORER_TOP_ACCOUNTS = 'localExplorerTopAccounts',
  LOCAL_EXPLORER_STATISTICS = 'localExplorerStatistics',
  LOCAL_CONTACT_BALANCES = 'localContactBalances',
  LOCAL_COINMARKETCAP_LIST = 'localCoinmarketcapList',
  LOCAL_FORUM_POST_LIST = 'localForumPostList',
  LOCAL_FORUM_USERS = 'localForumUsers',
  LOCAL_CHAT_DATA = 'localChatData',
  LOCAL_CHAT_LIST = 'localChatList',
  LOCAL_CHAT_KEY = 'localChatKey',
  LOCAL_CHAT_GROUPS = 'localChatGroups',
  LOCAL_CHAT_GROUP_MEMBERS = 'localChatGroupsMembers',
  LOCAL_PORTFOLIO_LISTS = 'localPortfolioList',
  LOCAL_PORTFOLIO_CURRENCY = 'localPortfolioCurrency'
}

export default STORAGE

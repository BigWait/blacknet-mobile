/**
 * App entry.
 * @file App 入口文件
 * @module app/entry
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import "react-native-gesture-handler";
import React, { Component, useEffect, useRef, useState } from "react";
import SplashScreen from "react-native-splash-screen";
import { boundMethod } from "autobind-decorator";
import { computed, observable, action } from "mobx";
import { Observer, observer } from "mobx-react";
import {
    getFocusedRouteNameFromRoute,
    NavigationContainer,
    NavigationState,
} from "@react-navigation/native";
import {
    createStackNavigator,
    HeaderBackButton,
    TransitionPresets,
} from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { AppearanceProvider } from "react-native-appearance";
import { optionStore } from "@app/stores/option";
import { Home } from "@app/pages/home";
import { DiscoverScreen } from "@app/pages/discover";
import { Explorer } from "@app/pages/explorer";
import { Wallet } from "@app/pages/wallet";
import { Settings } from "@app/pages/settings";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Ionicons from "react-native-vector-icons/Ionicons";
import { AutoI18nTitle, headerStyles } from "@app/components/layout/title";
import {
    HomeRoutes,
    WalletRoutes,
    SettingsRoutes,
    ChatRoutes,
    DiscoverRoutes,
} from "@app/routes";
import colors from "@app/style/colors";
import { Import as ImportPage } from "@app/pages/home/import";
import { QrScanner as QrScannerScreen } from "@app/pages/home/qrscanner";
import { SendScreen } from "@app/pages/wallet/send";
import { DetailScreen } from "@app/pages/wallet/detail";
import { BalanceScreen } from "@app/pages/wallet/balance";
import { TransactionsScreen } from "@app/pages/wallet/transactions";
import { LeaseScreen } from "@app/pages/wallet/lease";
import {
    CancelLeaseScreen,
    WithdrawLeaseScreen,
} from "@app/pages/wallet/cancellease";
import { SignMessageScreen } from "@app/pages/wallet/signmessage";
import { VerifyMessageScreen } from "@app/pages/wallet/verifymessage";
import { BackupScreen } from "@app/pages/settings/backup";
import { FeedbackScreen } from "@app/pages/settings/feedback";
import { CommunityScreen } from "@app/pages/settings/community";
import { ProfileScreen } from "@app/pages/settings/profile";
import { SignTextScreen } from "@app/pages/settings/profile/sign";
import { NickNameScreen } from "@app/pages/settings/profile/nickname";
import { ContactScreen, AddContactScreen } from "@app/pages/settings/contact";
import { ReceivedScreen } from "@app/pages/wallet/received";
import { ExplorerBlocksScreen } from "@app/pages/explorer/blocks";
import { ExplorerTransactionsScreen } from "@app/pages/explorer/transactions";
import { ExplorerTopAccountsScreen } from "@app/pages/explorer/topaccounts";
import { ExplorerDetailScreen } from "@app/pages/explorer/detail";
import { ChatInfoScreen, CreateChatGroupScreen } from "@app/pages/chat/group";
import { ChatGroupApplyScreen } from "@app/pages/chat/group/apply";
import { ChatGroupListsScreen } from "@app/pages/chat/group/lists";
import { ChatGroupQrcodeScreen } from "@app/pages/chat/group/qrcode";
import { WebviewScreen } from "@app/pages/home/webview";
import i18n from "@app/services/i18n";
import { LANGUAGE_KEYS } from "@app/constants/language";
import { Poollist } from "@app/pages/explorer/poollist"

import { LogBox } from 'react-native';
LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state',
  'Warning: AsyncStorage has been extracted from react-native core and will be removed in a future release'
]);

const commonStackOptions = computed(() => ({
    headerTitleAlign: "center",
    headerTitleStyle: headerStyles.styles.title,
    headerTintColor: headerStyles.styles.title.color,
    headerStyle: {
        backgroundColor: colors.headerBGColor,
        borderBottomWidth: 0,
    },
    ...TransitionPresets.SlideFromRightIOS, // This is where the transition happens
}));

const modalStackOptions = computed(() => ({
    headerShown: true,
    gestureEnabled: true,
    cardOverlayEnabled: true,
    ...TransitionPresets.ModalPresentationIOS,
    headerStatusBarHeight: 0,
}));

const customModalHeader = ({ navigation }: any) => {
    return {
        ...modalStackOptions.get(),
        headerShown: true,
        headerStyle: {
            backgroundColor: colors.background,
        },
        cardStyle: {
            backgroundColor: colors.background,
        },
        title: i18n.t(LANGUAGE_KEYS.PORTFOLIOADD),
        headerLeft: (props: any) => (
            <Observer
                render={() => {
                    return (
                        <HeaderButtons HeaderButtonComponent={HeaderButton}>
                            <Item
                                {...props}
                                iconName={"ios-close"}
                                iconSize={23}
                                style={{ color: colors.textDefault }}
                                IconComponent={Ionicons}
                                onPress={() => {
                                    navigation.goBack();
                                }}
                            />
                        </HeaderButtons>
                    );
                }}
            />
        ),
    };
};

const DiscoverStackComponent = observer(() => {
    const DiscoverStack = createStackNavigator();
    return (
        <DiscoverStack.Navigator
            initialRouteName={DiscoverRoutes.Index}
            screenOptions={commonStackOptions.get()}
        >
            <DiscoverStack.Screen
                name={DiscoverRoutes.Index}
                component={DiscoverScreen}
                options={{
                    title: i18n.t(LANGUAGE_KEYS.DISCOVER),
                }}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.Forum}
                component={ForumScreen}
                options={{
                    title: i18n.t(LANGUAGE_KEYS.MOMENT),
                }}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.Coinmarketcap}
                component={CoinmarketcapScreen}
                options={{
                    title: i18n.t(LANGUAGE_KEYS.COINMARKETCAP),
                }}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.Portfolio}
                component={PortfolioScreen}
                options={PortfolioScreen.getPageScreenOptions}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.AddPost}
                component={AddPostScreen}
                options={AddPostScreen.getPageScreenOptions}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.Detail}
                component={PostDetailScreen}
                options={PostDetailScreen.getPageScreenOptions}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.Explorer}
                component={Explorer}
                options={Explorer.getPageScreenOptions}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.Transactions}
                component={ExplorerTransactionsScreen}
                options={ExplorerTransactionsScreen.getPageScreenOptions}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.Accounts}
                component={ExplorerTopAccountsScreen}
                options={ExplorerTopAccountsScreen.getPageScreenOptions}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.Blocks}
                component={ExplorerBlocksScreen}
                options={ExplorerBlocksScreen.getPageScreenOptions}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.ExplorerDetail}
                component={ExplorerDetailScreen}
                options={ExplorerDetailScreen.getPageScreenOptions}
            />
            <DiscoverStack.Screen
                name={DiscoverRoutes.Poollist}
                component={Poollist}
                options={Poollist.getPageScreenOptions}
            />
        </DiscoverStack.Navigator>
    );
});

const ChatStackComponent = observer(() => {
    const ChatStack = createStackNavigator();
    return (
        <ChatStack.Navigator
            initialRouteName={ChatRoutes.Chat}
            screenOptions={commonStackOptions.get()}
        >
            <ChatStack.Screen
                name={ChatRoutes.Chat}
                component={ChatScreen}
                options={ChatScreen.getPageScreenOptions}
            />
            <ChatStack.Screen
                name={ChatRoutes.NewChat}
                component={NewChatScreen}
                options={{
                    title: i18n.t(LANGUAGE_KEYS.NEW_CHAT),
                }}
            />
            <ChatStack.Screen
                name={ChatRoutes.CreateGroup}
                component={CreateChatGroupScreen}
                options={{
                    title: i18n.t(LANGUAGE_KEYS.CREATE_GROUP),
                }}
            />
            <ChatStack.Screen
                name={ChatRoutes.Message}
                component={MessageScreen}
                options={{
                    headerShown: true,
                }}
            />
            <ChatStack.Screen
                name={ChatRoutes.Info}
                component={ChatInfoScreen}
                options={(options: any) => {
                    return getCustomOption(options, ChatInfoScreen);
                }}
            />
            <ChatStack.Screen
                name={ChatRoutes.GroupQrcode}
                component={ChatGroupQrcodeScreen}
                options={(options: any) => {
                    return getCustomOption(options, ChatGroupQrcodeScreen);
                }}
            />
            <ChatStack.Screen
                name={ChatRoutes.GroupLists}
                component={ChatGroupListsScreen}
                options={(options: any) => {
                    return getCustomOption(options, ChatGroupListsScreen);
                }}
            />
            <ChatStack.Screen
                name={ChatRoutes.GroupApply}
                component={ChatGroupApplyScreen}
                options={(options: any) => {
                    return getCustomOption(options, ChatGroupApplyScreen);
                }}
            />
            <ChatStack.Screen
                name={ChatRoutes.GroupUpdate}
                component={ChatGroupUpdateScreen}
                options={(options: any) => {
                    return getCustomOption(options, ChatGroupUpdateScreen);
                }}
            />
            <ChatStack.Screen
                name={ChatRoutes.GroupApplyList}
                component={ChatGroupApplyListsScreen}
                options={(options: any) => {
                    return getCustomOption(options, ChatGroupApplyListsScreen);
                }}
            />
            <ChatStack.Screen
                name={ChatRoutes.GroupMember}
                component={ChatGroupMembersScreen}
                options={(options: any) => {
                    return getCustomOption(options, ChatGroupMembersScreen);
                }}
            />
        </ChatStack.Navigator>
    );
});

const WalletStackComponent = observer(() => {
    const WalletStack = createStackNavigator();
    return (
        <WalletStack.Navigator
            initialRouteName={WalletRoutes.Wallet}
            screenOptions={{
                ...commonStackOptions.get(),
            }}
        >
            <WalletStack.Screen
                name={WalletRoutes.Wallet}
                component={Wallet}
                options={{
                    title: i18n.t(LANGUAGE_KEYS.WALLET),
                }}
            />
            <WalletStack.Screen
                name={WalletRoutes.Balance}
                component={BalanceScreen}
                options={BalanceScreen.getPageScreenOptions}
            />
            <WalletStack.Screen
                name={WalletRoutes.Send}
                component={SendScreen}
                options={SendScreen.getPageScreenOptions}
            />
            <WalletStack.Screen
                name={WalletRoutes.Lease}
                component={LeaseScreen}
                options={LeaseScreen.getPageScreenOptions}
            />
            <WalletStack.Screen
                name={WalletRoutes.CancelLease}
                component={CancelLeaseScreen}
                options={CancelLeaseScreen.getPageScreenOptions}
            />
            <WalletStack.Screen
                name={WalletRoutes.SignMessage}
                component={SignMessageScreen}
                options={SignMessageScreen.getPageScreenOptions}
            />
            <WalletStack.Screen
                name={WalletRoutes.VerifyMessage}
                component={VerifyMessageScreen}
                options={VerifyMessageScreen.getPageScreenOptions}
            />
            <WalletStack.Screen
                name={WalletRoutes.Transactions}
                component={TransactionsScreen}
                options={TransactionsScreen.getPageScreenOptions}
            />
            <WalletStack.Screen
                name={WalletRoutes.WalletDetail}
                component={DetailScreen}
                options={DetailScreen.getPageScreenOptions}
            />
            <WalletStack.Screen
                name={WalletRoutes.Received}
                component={ReceivedScreen}
                options={ReceivedScreen.getPageScreenOptions}
            />
            <WalletStack.Screen
                name={WalletRoutes.WithdrawLease}
                component={WithdrawLeaseScreen}
                options={WithdrawLeaseScreen.getPageScreenOptions}
            />
        </WalletStack.Navigator>
    );
});

const SettingsStackComponent = observer(() => {
    const SettingsStack = createStackNavigator();
    return (
        <SettingsStack.Navigator
            initialRouteName={SettingsRoutes.Settings}
            screenOptions={commonStackOptions.get()}
        >
            <SettingsStack.Screen
                name={SettingsRoutes.Settings}
                component={Settings}
                options={{
                    title: i18n.t(LANGUAGE_KEYS.SETTINGS),
                }}
            />
            <SettingsStack.Screen
                name={SettingsRoutes.Backup}
                component={BackupScreen}
                options={BackupScreen.getPageScreenOptions}
            />
            <SettingsStack.Screen
                name={SettingsRoutes.Feedback}
                component={FeedbackScreen}
                options={FeedbackScreen.getPageScreenOptions}
            />
            <SettingsStack.Screen
                name={SettingsRoutes.Contact}
                component={ContactScreen}
                options={ContactScreen.getPageScreenOptions}
            />
            <SettingsStack.Screen
                name={SettingsRoutes.AddContact}
                component={AddContactScreen}
                options={AddContactScreen.getPageScreenOptions}
            />
            <SettingsStack.Screen
                name={SettingsRoutes.Community}
                component={CommunityScreen}
                options={CommunityScreen.getPageScreenOptions}
            />
            <SettingsStack.Screen
                name={SettingsRoutes.Profile}
                component={ProfileScreen}
                options={ProfileScreen.getPageScreenOptions}
            />
            <SettingsStack.Screen
                name={SettingsRoutes.SignText}
                component={SignTextScreen}
                options={SignTextScreen.getPageScreenOptions}
            />
            <SettingsStack.Screen
                name={SettingsRoutes.NickName}
                component={NickNameScreen}
                options={NickNameScreen.getPageScreenOptions}
            />
            <SettingsStack.Screen
                name={SettingsRoutes.Avatar}
                component={AvatarScreen}
                options={AvatarScreen.getPageScreenOptions}
            />
        </SettingsStack.Navigator>
    );
});

const TabComponent = observer(() => {
    let listeners = {
        tabPress: () => {
            // userStore.fetchUsers();
        },
    };

    const Tab = createBottomTabNavigator();
    const labelStyle = StyleSheet.create({
        text: {
            marginTop: IS_ANDROID ? -2 : 0,
            marginBottom: IS_ANDROID ? 5 : 0,
        },
    });

    const getTabOptions = (route: any, navigation: any, name: any, i18nKey: any, iconText: any) => {
        const isFocused = navigation.isFocused();
        const routeName = getFocusedRouteNameFromRoute(route);
        const isHomeRoute = !routeName || routeName === name;

        let options: any = {
            // 非根屏隐藏 Tabbar（Search、Detail）
            tabBarVisible: isFocused && isHomeRoute,
            tabBarLabel: ({ color }) => (
                <AutoI18nTitle
                    i18nKey={i18nKey}
                    size={12}
                    color={color}
                    style={labelStyle.text}
                />
            ),
            tabBarIcon: ({ color }) => (
                <FontAwesome5 name={iconText} size={19} color={color} />
            )
        };

        if (iconText == 'cog') {
            if (stashStore.pendingLength) {
                options.tabBarBadge = stashStore.pendingLength;
            }
        }
        return options

    }
    return (
        <Tab.Navigator
            tabBarOptions={{
                activeBackgroundColor: colors.cardBackground,
                inactiveBackgroundColor: colors.cardBackground,
                style: {
                    backgroundColor: colors.cardBackground,
                    borderBottomWidth: 0,
                    shadowColor: colors.cardBackground,
                    shadowOpacity: 0.2,
                    shadowOffset: { width: 0, height: 2 },
                },
            }}
        >
            <Tab.Screen
                name={ChatRoutes.Chat}
                component={ChatStackComponent}
                listeners={listeners}
                options={({ route, navigation }) => getTabOptions(route, navigation, ChatRoutes.Chat, LANGUAGE_KEYS.CHAT, 'comment')}
            />
            <Tab.Screen
                name={WalletRoutes.Wallet}
                component={WalletStackComponent}
                listeners={listeners}
                options={({ route, navigation }) => getTabOptions(route, navigation, WalletRoutes.Wallet, LANGUAGE_KEYS.WALLET, 'wallet')}
            />
            <Tab.Screen
                name={DiscoverRoutes.Index}
                component={DiscoverStackComponent}
                listeners={listeners}
                options={({ route, navigation }) => getTabOptions(route, navigation, DiscoverRoutes.Index, LANGUAGE_KEYS.DISCOVER, 'compass')}
            />
            <Tab.Screen
                name={SettingsRoutes.Settings}
                listeners={listeners}
                component={SettingsStackComponent}
                options={({ route, navigation }) => getTabOptions(route, navigation, SettingsRoutes.Settings, LANGUAGE_KEYS.SETTINGS, 'cog')}
            />
        </Tab.Navigator>
    );
});

const HomeStackComponent = observer(() => {
    const HomeStack = createStackNavigator();
    return (
        <HomeStack.Navigator
            initialRouteName={HomeRoutes.Home}
            screenOptions={commonStackOptions.get()}
        >
            <HomeStack.Screen
                name={HomeRoutes.Home}
                component={Home}
                options={{
                    title: i18n.t(LANGUAGE_KEYS.YOUR_ACCOUNTS),
                }}
            />
            <HomeStack.Screen
                name={HomeRoutes.Import}
                component={ImportPage}
                options={{
                    title: i18n.t(LANGUAGE_KEYS.IMPORT),
                }}
            />
        </HomeStack.Navigator>
    );
});

const Stack = createStackNavigator();

import { RootSiblingParent } from "react-native-root-siblings";
import { ChatScreen, NewChatScreen } from "./pages/chat";
import { MessageScreen } from "./pages/chat/message";
import { ForumScreen } from "./pages/forum";
import { AddPostScreen } from "./pages/forum/addPost";
import { PostDetailScreen } from "./pages/forum/detail";
import { StashScreen } from "./pages/home/stash";
import { StashDetailScreen } from "./pages/home/stash_detail";
import TopStash from "./topstash";
import { navigationRef } from "./rootNavigation";
import { userStore } from "./stores/users";
import { IS_ANDROID, IS_IOS } from "./config";
import { AppState, Button, StyleSheet, View } from "react-native";
import { AvatarScreen } from "./pages/settings/profile/avatar";
import { StatusBar } from "react-native";
import { CoinmarketcapScreen } from "./pages/coinmarketcap";
import { PortfolioScreen } from "./pages/portfolio";
import { PortfolioAddScreen } from "./pages/portfolio/add";
import {
    HeaderButtons,
    HeaderButton,
    Item,
} from "react-navigation-header-buttons";
import { stashStore } from "./stores/stash";
import { accountStore } from "./stores/account";
import { chatStore, ChatStore } from "./pages/chat/stores/chat";
import initWesocket from "./initWebsocket";
import {
    MenuProvider
  } from 'react-native-popup-menu';
import { ChatGroupUpdateScreen } from "./pages/chat/group/update";
import { ChatGroupApplyListsScreen } from "./pages/chat/group/applyList";
import { ChatGroupMembersScreen } from "./pages/chat/group/members";
const AppStateHooks = () => {
    const appState = useRef(AppState.currentState);
    const [appStateVisible, setAppStateVisible] = useState(appState.current);

    useEffect(() => {
        AppState.addEventListener("change", _handleAppStateChange);

        return () => {
            AppState.removeEventListener("change", _handleAppStateChange);
        };
    }, []);

    const _handleAppStateChange = (nextAppState: any) => {
        if (
            appState.current.match(/inactive|background/) &&
            nextAppState === "active"
        ) {
            accountStore.refreshBalance();
            stashStore.refresh();
            userStore.fetchUsers();
            chatStore.refreshChatMap();
        }

        appState.current = nextAppState;
        setAppStateVisible(appState.current);
        console.log("AppState", appState.current);
    };

    initWesocket();

    return null;
};
@observer
export default class App extends Component {
    @observable.ref
    private navigationState: NavigationState | undefined;

    @boundMethod
    @action
    private updateNavigationState(state: NavigationState | undefined) {
        this.navigationState = state;
    }

    componentDidMount() {
        SplashScreen.hide();
        userStore.fetchUsers();
    }

    componentWillUnmount() {
        // stop timer
        // BackgroundTimer.stopBackgroundTimer();
    }

    navigator() {
        let Navigator = Stack.Navigator;

        return (
            <Navigator
                initialRouteName={HomeRoutes.Home}
                screenOptions={{
                    ...commonStackOptions.get(),
                    cardStyle: {
                        backgroundColor: colors.pure,
                    },
                }}
                headerMode={"screen"}
            >
                <Stack.Screen
                    name={HomeRoutes.PortfolioAdd}
                    component={PortfolioAddScreen}
                    options={customModalHeader}
                />
                <Stack.Screen
                    name={HomeRoutes.Home}
                    options={{ headerShown: false }}
                    component={HomeStackComponent}
                />
                <Stack.Screen
                    name={HomeRoutes.Main}
                    options={{ headerShown: false }}
                    component={TabComponent}
                />
                <Stack.Screen
                    name={HomeRoutes.QrScanner}
                    component={QrScannerScreen}
                    options={(options: any) => {
                        return {
                            ...getCustomOption(options, QrScannerScreen),
                            ...TransitionPresets.FadeFromBottomAndroid
                        }
                    }}
                />
                <Stack.Screen
                    name={HomeRoutes.Transfer}
                    component={SendScreen}
                    options={(options: any) => {
                        return getCustomOption(options, SendScreen);
                    }}
                />
                <Stack.Screen
                    name={HomeRoutes.AddContact}
                    component={AddContactScreen}
                    options={(options: any) => {
                        return getCustomOption(options, AddContactScreen);
                    }}
                />
                <Stack.Screen
                    name={HomeRoutes.ChatMessage}
                    component={MessageScreen}
                    options={(options: any) => {
                        return getCustomOption(options, MessageScreen);
                    }}
                />
                <Stack.Screen
                    name={HomeRoutes.Stash}
                    component={StashScreen}
                    options={(options: any) => {
                        return getCustomOption(options, StashScreen);
                    }}
                />
                <Stack.Screen
                    name={HomeRoutes.StashDetail}
                    component={StashDetailScreen}
                    options={(options: any) => {
                        return getCustomOption(options, StashDetailScreen);
                    }}
                />
                <Stack.Screen
                    name={HomeRoutes.ExplorerDetail}
                    component={ExplorerDetailScreen}
                    options={(options: any) => {
                        return getCustomOption(options, ExplorerDetailScreen);
                    }}
                />
                <Stack.Screen
                    name={HomeRoutes.Webview}
                    component={WebviewScreen}
                    options={(options: any) => {
                        return getCustomOption(options, WebviewScreen);
                    }}
                />
            </Navigator>
        )
    }

    render() {
        return (
            <RootSiblingParent>
                <StatusBar translucent backgroundColor="transparent" />
                <AppStateHooks />
                <AppearanceProvider>
                    <MenuProvider>
                    <NavigationContainer
                        ref={navigationRef}
                        onStateChange={this.updateNavigationState}
                        theme={{
                            dark: optionStore.darkTheme,
                            colors: {
                                primary: colors.primary,
                                background: colors.background,
                                card: colors.cardBackground,
                                text: colors.textDefault,
                                border: colors.border,
                                notification: colors.border,
                            },
                        }}
                    >
                        {/* <TopStash></TopStash> */}
                        {this.navigator()}
                    </NavigationContainer>
                    </MenuProvider>
                </AppearanceProvider>
            </RootSiblingParent>
        );
    }
}

const getCustomOption = (options: any, screen: any) => {
    const { navigation } = options;
    const option = screen.getPageScreenOptions ? screen.getPageScreenOptions(options) : {}
    return {
        headerLeft: (props: any) => (
            <Observer render={()=>
                <HeaderBackButton
                    {...props}
                    label={i18n.t(LANGUAGE_KEYS.BACK)}
                    onPress={() => {
                        navigation.goBack();
                    }}
                />
            } />
        ),
        ...option
    };
};

/**
 * App routes
 * @file App 路由
 * @module app/routes
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

export enum HomeRoutes {
  Home = 'Home',
  Main = 'TabScreen',
  Import = 'Import',
  QrScanner = 'QrScanner',
  Transfer = 'Transfer',
  AddContact = 'AddContact',
  ChatMessage = 'ChatMessage',
  Stash = 'Stash',
  StashDetail = 'StashDetail',
  ExplorerDetail = 'ExplorerDetail',
  PortfolioAdd = 'PortfolioAdd',
  Webview = 'Webview'
}

export enum ModalRoutes {
  Index = 'ModalIndex',
  PortfolioAdd = 'PortfolioAdd'
}


export enum DiscoverRoutes {
  Index = 'DiscoverIndex',
  Forum = 'Forum',
  Detail = 'Detail',
  AddPost = 'AddPost',
  Explorer = 'Explorer',
  Blocks = 'Blocks',
  ExplorerDetail = 'ExplorerDetail',
  Transactions = 'Transactions',
  Accounts = 'Accounts',
  Coinmarketcap = 'Coinmarketcap',
  Portfolio = 'Portfolio',
  Poollist='Poollist'
}


export enum ChatRoutes {
  Chat = 'Chat',
  Message = 'Message',
  NewChat = 'NewChat',
  CreateGroup = 'CreateGroup',
  Info = 'ChatInfo',
  GroupQrcode = 'GroupQrcode',
  GroupLists = 'GroupLists',
  GroupMember = 'GroupMember',
  GroupApply = 'GroupApply',
  GroupApplyList = 'GroupApplyList',
  GroupUpdate = 'GroupUpdate'
}

export enum WalletRoutes {
  Wallet = 'Wallet',
  Send = 'Send',
  Lease = 'Lease',
  CancelLease = 'CancelLease',
  SignMessage = 'SignMessage',
  VerifyMessage = 'VerifyMessage',
  Transactions = 'Transactions',
  WalletDetail = 'WalletDetail',
  Received = 'Received',
  Balance = 'Balance',
  WithdrawLease = 'WithdrawLease'
}


export enum SettingsRoutes {
    Settings = 'Settings',
    Backup = 'Backup',
    Feedback = 'Feedback',
    Contact = 'Contact',
    AddContact = 'AddContact',
    Community = 'Community',
    Profile = 'Profile',
    SignText = 'SignText',
    NickName = 'nickname',
    Avatar = 'avatar'
}

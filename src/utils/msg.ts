import { getIPFSURL } from "@app/services/api"
import Blacknetjs from 'blacknetjs';
import { forEach, isObject, isString } from "lodash";

class MSG {
    static isGroup = (to: string)=>{
        return 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e' === to
    }
    static hasQuote = (message:string):boolean => {
        return /\[(txid:\w+)\]/i.test(message)
    }
    static hasText = (message:string):boolean => {
        return /\[text:([\s\S]*)\]/i.test(message)
    }
    static isImage(text: string, s: boolean = true){
        if(!s){
            return /\[image:\s+.*\]/.test(text)
        }
        return /\[image:\s+([A-Za-z0-9]{46})\]/.test(text)
    }
    static isVideo(text: string, s: boolean = true){
        if(!s){
            return /\[video:\s+.*\]/.test(text)
        }
        return /\[video:\s+([A-Za-z0-9]{46})\]/.test(text)
    }
    static isAudio(text: string, s: boolean = true){
        if(!s){
            return /^\[audio:\s+.*\]/.test(text)
        }
        return /\[audio:\s+([A-Za-z0-9]{46})\]/.test(text)
    }
    static isText(text: string){
        return !MSG.isImage(text) && !MSG.isVideo(text) && !MSG.isAudio(text)
    }

    static formatText = (message: string) => {
        const res = message.match(/\[text:([\s\S]*)\]/i)
        if(res){
            return res[1].trim()
        }
        return message
    }

    static getCID(text: string){
        // return text.replace(']', '').split(':')[1].trim()

        // // const res = text.match(/\[text:([^\]]*)\]/i)
        // const res = text.match(/\[text:([\s\S]*)\]$/i)
        // if(res){
        //     return res[1].trim()
        // }
        // return text.trim()

        const res = text.match(/\[(image|video|audio):\s?(\w+)\]/i)
        if(res){
            return res[2]
        }else{
            return text.trim()
        }
    }
    static getIPFSURL(text: string){
        return getIPFSURL(MSG.getCID(text))
    }

    static getQuote(text: string){
        const res = text.match(/\[txid:\s?(\w+)\]/i)
        if(res){
            return res[1]
        }
        return null
    }

    static decodeChatContent = (mnemonic: string, myAccount: string, message:string): string=>{
        return Blacknetjs.Decrypt(mnemonic, myAccount, message.replace(/(\[[\s\S]*\])/i, '').replace('chat: ', ''));
    }
    
    static decodeGroupChatContent = (message:string, isPrivate?: boolean, groupSK?: string): string=>{
        const text = message.replace('groupMessage: ', '')
        let data
        try {
            data = JSON.parse(text)
        } catch (error) {
            return ''
        }
        if(isPrivate && groupSK && data.text){
            const groupPK = Blacknetjs.Address(groupSK)
            return Blacknetjs.Decrypt(groupSK, groupPK, data.text.replace(/(\[[\s\S]*\])/i, '').trim())
        }
        return Buffer.from(data.text.replace(/(\[[\s\S]*\])/i, ''), 'base64').toString()
    }

    static formatChatContent = (mnemonic: string, myAccount: string, message:string) =>{
        const res = message.match(/\[(\w+:\w+)\]/ig)
        const obj: {[key: string]: any} = {
            text: Blacknetjs.Decrypt(mnemonic, myAccount, message.replace(/(\[[\s\S]*\])/i, '').replace('chat: ', ''))
        }
        if(res && res.length){
            res.forEach((data)=>{
                const [key, value] = data.replace('[', '').replace(']', '').split(":")
                obj[key] = value
            })
        }
        return obj
    }
    
    static formatGroupChatContent = (message:string) =>{
        const res = message.match(/\[(\w+:\w+)\]/ig)
        const obj: {[key: string]: any} = {
            text: Buffer.from(message.replace(/(\[[\s\S]*\])/i, '').replace('chat: ', ''), 'base64').toString()
        }
        if(res && res.length){
            res.forEach((data)=>{
                const [key, value] = data.replace('[', '').replace(']', '').split(":")
                obj[key] = value
            })
        }
        return obj
    }

    static decodeContent = (message:string) =>{
        const obj: {[key: string]: any} = {}
        const reg = message.match(/\[text:([\s\S]*)\]$/i)
        if(reg){
            obj['text'] = reg[1]
        }
        const res = message.replace(/\[text:([\s\S]*)\]$/i, '').match(/\[(\w+:\w+)\]/ig)
        if(res && res.length){
            res.forEach((data)=>{
                const [key, value] = data.replace('[', '').replace(']', '').split(":")
                obj[key] = value
            })
        }
        return obj
    }

    static encodeChatContent = (mnemonic: string, myAccount: string, message:string) =>{
        const text = Blacknetjs.Decrypt(mnemonic, myAccount, message.replace(/(\[[\s\S]*\])/i, '').replace('chat: ', ''))
        const reg = message.match(/(\[[\s\S]*\])/i)
        if(MSG.isText(text)){
            return `${reg ? reg[0]: ''}[text:${text}]`
        }else{
            return `${text}${reg ? reg[0]: ''}`
        }
    }

    static encodeGroupChatContent = (message:string, isPrivate?: boolean, groupSK?: string) =>{
        const text = message.replace('groupMessage: ', '')
        let data
        try {
            data = JSON.parse(text)
        } catch (error) {
            return `[text:${text}]`
        }
        if(!isObject(data)){
            return `[text:${text}]`
        }
        data = data as {[key: string]: any}
        if(isPrivate && groupSK && data.text){
            const groupPK = Blacknetjs.Address(groupSK)
            let text = Blacknetjs.Decrypt(groupSK, groupPK, data.text.replace(/(\[[\s\S]*\])/i, '').trim())
            const reg = data.text.match(/(\[[\s\S]*\])/i)
            if(reg && reg[0]){
                data.text = `${Buffer.from(text, 'utf8').toString('base64')}${reg[0]}`
            } else {
                data.text = Buffer.from(text, 'utf8').toString('base64')
            }
        }
        return formatData(data)
    }
    
}

export default MSG

function formatData(data: {[key: string]: any}): string{
    let text = ''
    if(data.text){
        text = formatText(data.text)
    }
    if(data){
        for (const k in data) {
            if (Object.prototype.hasOwnProperty.call(data, k)) {
                if(k !== 'text' ){
                    if(k === 'txid'){
                        text = `[groupid:${data[k]}]` + text
                    } else if (k === 'quote'){
                        text = `[txid:${data[k]}]` + text
                    } else {
                        text = `[${k}:${data[k]}]` + text
                    }
                }
            }
        }
    }
    return text
}

function formatText(message:string): string{
    const text = Buffer.from(message.replace(/(\[[\s\S]*\])/i, '').replace('groupMessage: ', ''), 'base64').toString()
    const reg = message.match(/(\[[\s\S]*\])/i)
    if(MSG.isText(text)){
        return `${reg ? reg[0]: ''}[text:${text}]`
    }else{
        return `${reg ? reg[0]: ''}${text}`
    }
}
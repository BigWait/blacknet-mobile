/**
 * util bln tools
 * @file util bln tools
 * @module utils/bln
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import BigNumber from 'bignumber.js'
import moment from 'moment'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import Blacknetjs from 'blacknetjs';
import { BlnTxType, IBlnTransaction, IBlnScanerURL, Bln, IBlnScanForumPostData } from '@app/types/bln'
import { parseUrl } from 'query-string'
import Identicon from 'identicon.js'
import md5 from 'md5';

export const overFileSizeLImit = (filesize: any): boolean=>{
    return new BigNumber(filesize).comparedTo(1024 * 1024 * 50) === 1
}

export const defaultAvatar = (address?: string, size: number = 60):string=>{
    return "data:image/svg+xml;base64,"+ new Identicon(md5(address || "d3b07384d113edec49eaa6238ad5ff00").toString(), size).toString() +""
}

export const showBalance = (balance: any, len?: number)=>{
    return parseFloat(new BigNumber(balance || "0").div(1e8).toFixed(8)).toString();
}

export const formatBalance = (balance: any)=>{
    return new BigNumber(balance || "0").div(1e8).toString();
}

export const showTime = (date: any)=>{

    if(date.length == 13) return moment(+date).format('MM/DD/YYYY HH:mm');

    return moment(date * 1000).format('MM/DD/YYYY HH:mm')
}

export const showTimeFromNow = (date: any)=>{
    return moment(date * 1000).fromNow()
}

export const showRelativeTime = (date: any)=>{
    return moment(date * 1000).startOf('hour').fromNow()
}

export const showTypeBalance = (address: string, tx: IBlnTransaction):string=>{
    const amount = parseFloat(new BigNumber(tx.amount || "0").div(1e8).toFixed(8)).toString()
    if (isReceive(address, tx) || tx.type === BlnTxType.Genesis || tx.type === BlnTxType.Generated) {
        return "+"+ amount
    }
    return "-"+ amount
}
export const showAmountPrefix = (address: string, tx: IBlnTransaction):string=>{
    if (isReceive(address, tx) || tx.type === BlnTxType.Genesis || tx.type === BlnTxType.Generated) {
        return "+"
    }
    return "-"
}
export const showTypeIcon = (address: string, tx: IBlnTransaction):string=>{
    if (isReceive(address, tx)) {
        return "chevron-down"
    }
    return "chevron-up"
}
export const showType = (address: string, tx: IBlnTransaction):string=>{
    if (tx.type === BlnTxType.Transfer) {
        return isReceive(address, tx) ? i18n.t(LANGUAGE_KEYS.TX_RECEIVED): i18n.t(LANGUAGE_KEYS.TX_SEND);
    }   
    if (tx.type === BlnTxType.Lease) {
        return isReceive(address, tx) ? i18n.t(LANGUAGE_KEYS.TX_LEASE_IN): i18n.t(LANGUAGE_KEYS.TX_LEASE_OUT);
    }
    return getTypeName(tx.type);
}

export const getTypeName = (type: number):string=>{
    switch(type){
        case BlnTxType.Generated: 
            return i18n.t(LANGUAGE_KEYS.TX_POS_GENERATED);
        case BlnTxType.CancelLease: 
            return i18n.t(LANGUAGE_KEYS.TX_CANCEL_LEASE);
        case BlnTxType.Transfer:  
            return i18n.t(LANGUAGE_KEYS.TX_TRANSFER);
        case BlnTxType.Lease: 
            return i18n.t(LANGUAGE_KEYS.TX_LEASE);
        case BlnTxType.Genesis:
            return 'Genesis';
    }
    return i18n.t(LANGUAGE_KEYS.TX_ALL);
}

export const isReceive = (address: string, tx: IBlnTransaction): boolean=>{
    switch(tx.type){
        case BlnTxType.Generated: 
        case BlnTxType.CancelLease: return address == tx.from;
        case BlnTxType.Transfer:  
        case BlnTxType.Lease: return address == tx.to;
    }
    return false;
}

export const unix_to_local_time = (unix_timestamp: string, real: boolean = true, isMobile?: boolean): string=>{
    if(!unix_timestamp) return unix_timestamp;
    if(unix_timestamp && unix_timestamp.length == 10){
        unix_timestamp += "000";
    }
    let date = new Date(+unix_timestamp), now = Date.now();
    let subSeconds= (now - (+date))/1000;
    if(subSeconds <  60 && real){
        return parseInt(subSeconds.toString()) + ' seconds ago';
    }else if(subSeconds < 60 * 60 && real){
        return Math.ceil(subSeconds / 60) + ' minutes ago';
    }
    let hours = "0" + date.getHours();
    let minutes = "0" + date.getMinutes();
    let seconds = "0" + date.getSeconds();
    let day = date.getDate();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let prefix = '';
    
    if(!isMobile) prefix = year + "-" ;

    return prefix + ('0' + month).substr(-2) + "-" +
        ('0' + day).substr(-2) + " " + hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
}


export const verifyAccount = (account?:string)=>{
    account = String(account)
    if(account.length > 21 && /^blacknet[a-z0-9]{59}$/i.test(account)){
        return true
    }
    return false
}

export const verifySign = (sign?:string): boolean=>{
    if(Object.prototype.toString.call(sign) === "[object String]" && sign && sign.length === 128){
        return true
    }
    return false
}

export const verifyHash = (hash?:string): boolean=>{
    if(/(\w){64}/i.test(String(hash))){
        return true
    }
    return false
}

export const verifyAlias = (alias?:string): boolean=>{
    if(Object.prototype.toString.call(alias) === "[object String]"){
        return true
    }
    return false
}

export const verifyBlockNumber = (height?:number|string): boolean=>{
    if(/^\d+$/i.test(height?.toString() || "")){
        return true
    }
    return false
}

export const showContactName = (mnemonic: string, myAccount: string, message:string): string=>{
    return Blacknetjs.Decrypt(mnemonic, myAccount, message.replace('new contact: ', ''));
}

export const formatChatContent = (mnemonic: string, myAccount: string, message:string): string=>{
    return Blacknetjs.Decrypt(mnemonic, myAccount, message.replace(/(\[.*\])/i, '').replace('chat: ', ''));
}

export const formatGroupChatContent = (message:string): string=>{
    return Buffer.from(message.replace(/(\[.*\])/i, '').replace('chat: ', ''), 'base64').toString()
}

export const formatPostTx = (message:string): IBlnScanForumPostData => {
    const jstr = Buffer.from(message.replace('post: ', ''), 'base64').toString()
    try {
        return JSON.parse(jstr)
    } catch (error) {
        console.log("formatPostTx", message, error);
    }
    return {} as IBlnScanForumPostData
}

export const parseQrscanerURL = (uri: string, errcode: number = 0, errmsg?: string): IBlnScanerURL=>{
    let res : IBlnScanerURL = {
        errcode: errcode,
        errmsg: errmsg
    }
    if(verifyAccount(uri)){
        return {
            ...res,
            address: uri
        }
    }
    const matchs = uri.match(/^blacknet:(blacknet[a-z0-9]{59})\??/i)
    if(matchs && matchs[1]){
        const urls = parseUrl(uri)
        return {
            ...res,
            address: matchs[1],
            ...urls.query
        }
    }
    const match = uri.match(/^(\w+):\s.([a-z0-9_-]+)\??/i)
    if(match && match[1]){
        const urls = parseUrl(uri)
        res[match[1]] = match[2]
        return {
            ...res,
            ...urls.query
        }
    }
    return {
        ...res
    }
}

export const versionStringCompare = (preVersion='', lastVersion='') => {
    var sources = preVersion.split('.');
    var dests = lastVersion.split('.');
    var maxL = Math.max(sources.length, dests.length);
    var result = 0;
    for (let i = 0; i < maxL; i++) {  
        let preValue = sources.length>i ? sources[i]: "0";
        let preNum = isNaN(Number(preValue)) ? preValue.charCodeAt(0) : Number(preValue);
        let lastValue = dests.length>i ? dests[i]: "0";
        let lastNum =  isNaN(Number(lastValue)) ? lastValue.charCodeAt(0) : Number(lastValue);
        if (preNum < lastNum) {
            result = -1;
            break;
        } else if (preNum > lastNum) { 
            result = 1;
            break;
        }
    }
    return result;
}

// class TxType {
//     static const Transfer = 0;
//     static const Burn = 1;
//     static const Lease = 2;
//     static const CancelLease = 3;
//     static const Bundle = 4;
//     static const CreateHTLC = 5;
//     static const UnlockHTLC = 6;
//     static const RefundHTLC = 7;
//     static const SpendHTLC = 8;
//     static const CreateMultisig = 9;
//     static const SpendMultisig = 10;
//     static const WithdrawFromLease = 11;
//     static const ClaimHTLC = 12;
//     static const MultiData = 16;
//     static const Generated = 254;
//   }
  

// const Map<int, String> BlnTxData = {
//     -1: "all",
//     0: "transfer",
//     1: "burn",
//     2: "lease",
//     3: "cancelLease",
//     4: "bundle",
//     5: "createHTLC",
//     6: "unlockHTLC",
//     7: "refund HTLC",
//     8: "spend HTLC",
//     9: "createMultisig",
//     10: "spendMultisig",
//     254: "posGenerated"
//   };


//   String _blnTypeName(){
//     if(BlnTxData[data.type] == "transfer"){
//       return _isReceive() ? S.of(context).transferReceived: S.of(context).transferSend;
//     }
//     // lease
//     if(BlnTxData[data.type] == "lease"){
//       return _isReceive() ? S.of(context).leaseIn: S.of(context).leaseOut;
//     }
//     return S.of(context).blnType(BlnTxData[data.type]);
//   }


export const shortAddress = (address: string, len?: number)=>{
    if(len === undefined){
        len = 16
    }
    return address.substring(0, len) +"..."+ address.substring(address.length - len, address.length);
}
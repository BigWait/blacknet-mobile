export const shortAddress = (address: string, len?: number)=>{
    if(len === undefined){
        len = 16
    }
    return address.substring(0, len) +"..."+ address.substring(address.length - len, address.length);
}
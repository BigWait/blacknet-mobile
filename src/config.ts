/**
 * App config
 * @file App 配置
 * @module app/config
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { Platform } from 'react-native'
import packageJSON from '../package.json'

export const appName = 'Blacknet'
export const projectName = packageJSON.name
export const projectUrl = packageJSON.homepage
export const version = packageJSON.version
export const license = packageJSON.license
export const dependencies = packageJSON.dependencies

export const IS_DEV = __DEV__
export const IS_IOS = Object.is(Platform.OS, 'ios')
export const IS_ANDROID = !IS_IOS
export const blnscanAPI = 'https://blnscan.loqunbai.com'
export const blnnodeAPI = 'https://blnapi.loqunbai.com'
// export const blnscanAPI = 'https://blnscan.io'
// export const blnnodeAPI = 'https://bln.nodes.bililab.com'
export const appleID = '1489451592'
export const androidID = '15558138'

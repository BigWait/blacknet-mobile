/**
 * Common Bln types
 * @file 公共 Bln 模型
 * @module types/bln
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { HomeRoutes } from '@app/routes';
import BigNumber from 'bignumber.js';

export interface IBln {
    address: string,
    mnemonic: string,
    balance: IBlnBalance,
    nickname?: string,
    signmessage?: string,
    imageurl?: string,
    touchid?: boolean
}

// Bln
export class Bln{
    
    static createIBln(mnemonic: string, address: string): IBln{
        return {
            mnemonic: mnemonic,
            address: address,
            balance: { 
                seq: 0,
                balance: "",
                confirmedBalance: "",
                stakingBalance: "",
                outLeasesBalance: "",
                inLeasesBalance: ""
            }
        }
    }
    static createEmptyIBln(): IBln{
        return {
            mnemonic: "",
            address: "",
            balance: { 
                seq: 0,
                balance: "",
                confirmedBalance: "",
                stakingBalance: "",
                outLeasesBalance: "",
                inLeasesBalance: ""
            }
        }
    }
}

export function createIBln(mnemonic: string, address: string): IBln{
        return {
            mnemonic: mnemonic,
            address: address,
            balance: { 
                seq: 0,
                balance: "",
                confirmedBalance: "",
                stakingBalance: "",
                outLeasesBalance: "",
                inLeasesBalance: ""
            }
        }
    }


export function createEmptyIBln(): IBln{
        return {
            mnemonic: "",
            address: "",
            balance: { 
                seq: 0,
                balance: "",
                confirmedBalance: "",
                stakingBalance: "",
                outLeasesBalance: "",
                inLeasesBalance: ""
            }
        }
    }

export interface IBlnBalance { 
    seq: number,
    balance: string,
    confirmedBalance: string,
    stakingBalance: string,
    outLeasesBalance: string,
    inLeasesBalance: string,
    txamount?: number,
    blocks?: number,
    realBalance?: string,
    displayName?: string
}

export interface IBlnLease {
    amount: string,
    height: number,
    publicKey: string
}

export interface IBlnScanerURL {
    address?: string,
    amount?: string,
    type?: string,
    message?: string,
    screen?: HomeRoutes,
    errmsg?: string,
    errcode: number
    [key: string]: any
}

export interface IBlnTransaction { 
    blockHash: string,
    fee: number,
    from: string,
    seq: number,
    signature: string,
    size: number,
    time: string,
    type: number,
    txid: string,
    hash: string,

    to?: string,
    amount: string,
    blockHeight: number,
    message?: string,
    reject?: string,
    leaseHeight?: number
}

// BlnTx
export class BlnTransaction {
    
    static fromTx(txid: string, from: string, to: string, amount: string, message?: string): IBlnTransaction{
        return {
            txid: txid,
            from: from,
            to: to,
            amount: new BigNumber(amount).toString(),
            type: 0,
            message: message,
            time: (new Date().getTime() / 1000).toString()
        } as IBlnTransaction
    }
    static fromLease(txid: string, from: string, to: string, amount: string): IBlnTransaction{
        return {
            txid: txid,
            from: from,
            to: to,
            amount: new BigNumber(amount).toString(),
            type: 2,
            time: (new Date().getTime() / 1000).toString()
        } as IBlnTransaction
    }
    static fromCancelLease(txid: string, from: string, to: string, amount: string, height: number): IBlnTransaction{
        return {
            txid: txid,
            from: from,
            to: to,
            leaseHeight: height,
            amount: new BigNumber(amount).toString(),
            type: 3,
            time: (new Date().getTime() / 1000).toString()
        } as IBlnTransaction
    }
}

export enum BlnTxType {
    All = -1,
    Transfer = 0,
    Burn = 1,
    Lease = 2,
    CancelLease = 3,
    Bundle = 4,
    CreateHTLC = 5,
    UnlockHTLC = 6,
    RefundHTLC = 7,
    SpendHTLC = 8,
    CreateMultisig = 9,
    SpendMultisig = 10,
    WithdrawFromLease = 11,
    ClaimHTLC = 12,
    MultiData = 13,
    Generated = 254,
    Genesis = 999
}

export const BlnTxTypeMap = {
    0: "Transfer",
    // "-1": "All",
    1: "Burn",
    2: 'Lease',
    3: 'CancelLease',
    4: 'Bundle',
    5: 'CreateHTLC',
    6: 'UnlockHTLC',
    7: 'RefundHTLC',
    8: 'SpendHTLC',
    9: 'CreateMultisig',
    10: 'SpendMultisig',
    254: 'PosGenerated',
    999: 'Genesis'
}

export interface IBlnScanStatistics { 
    accounts: number
    bapp: number
    cancelleases: number
    leases: number
    payments: number
    transactions: number
    blocks: number
    supply: string
}

export interface IBlnBlock { 
    blockHash: string
    contentHash: string
    generator: string
    height: number
    previous: string
    reward: string
    signature: string
    size: number
    time: string
    transactions: Array<any>
    version: string
}

export interface IBlnScanAccount { 
    address: string,
    seq: number,
    balance: string,
    confirmedBalance: string,
    stakingBalance: string,
    outLeasesBalance: string,
    inLeasesBalance: string,
    txamount: number,
    blocks: number,
    realBalance: string,
    displayName: string,
    profile?: any
}

export interface IBlnScanContact { 
    account: string,
    name: string,
    balance?: string,
    message?: IBlnScanMessage
}

export interface IBlnScanMessage { 
    from: string,
    to: string,
    text: string,
    time: string,
    txid?: string,
    group?: boolean,
    groupInfo?: IBlnGroupData,
    data?: {
        [key: string]: any
    },
    sent?: boolean,
    // Mark the message as received, using two tick
    received?: boolean,
    // Mark the message as pending with a clock loader
    pending?: boolean
}


export interface IBlnScanForumPost { 
    from: string,
    to: string,
    text: string,
    time: string,
    txid: string,
    amount: number,
    data: IBlnScanForumPostData
}

export interface IBlnScanForumPostData { 
    title?: string,
    content: string,
    replyid?: string,
    hasComment?: boolean,
    quotereplyid?: string,
    quote?: string,
    lastreply?: string
}

export interface  IBlnVersion{
    Code: number, //0代表请求成功，非0代表失败
    Msg: string, //请求出错的信息
    UpdateStatus: number, //0代表不更新，1代表有版本更新，不需要强制升级，2代表有版本更新，需要强制升级
    VersionCode: number, //编译版本号(唯一)
    VersionName: string, //版本名(用于展示)
    ModifyContent: string, //更新内容
    DownloadUrl: string,// 文件下载地址
    ApkSize: number, //文件的大小(单位:kb)
    ApkMd5: string  //md5值没有的话，就无法保证apk是否完整，每次都会重新下载。框架默认使用的是md5加密。
}

export interface  ICoinCapTicker{
    price: number,
    volume_24h: number,
    percent_change_1h: number,
    percent_change_24h: number,
    percent_change_7d: number,
    market_cap: number,
    last_updated: string
}

export interface  ICoinCap{
    id: number,
    rank?: number,
    name: string,
    symbol: string,
    slug: string,
    max_supply: number,
    url?: string,
    circulating_supply: number,
    total_supply: number,
    last_updated?: string,
    quote: { [key: string]: ICoinCapTicker; }
    currentQuote?: string
    currentQuoteData?: ICoinCapTicker
}

export interface  IPortfolioData{
    symbol: string,
    amount: string,
    name: string,
    date?: number,
    slug?: string
}

export interface  ICoin{
    url: string,
    name: string,
    symbol: string
}


export interface IBlnGroup { 
    from: string,
    to: string,
    text: string,
    time: string,
    txid: string,
    amount: number,
    data: IBlnGroupData
}

export interface IBlnGroupData { 
    name: string,
    description: string,
    logo: string,
    isPrivate: number,
    privateKey?: string
}
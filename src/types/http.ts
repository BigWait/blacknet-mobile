/**
 * Global HTTP types
 * @file Global HTTP types
 * @module types/http
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */
export type TRequestUrlPath = string
export type TRequestData = object

// request params
export interface IRequestParams {
  // [key: string]: string | number
  [key: string]: any
}

// HTTP 状态返回
export interface IHttpResponseBase {

}

// HTTP error
export type THttpErrorResponse = IHttpResponseBase & {

}

// HTTP success
export type THttpSuccessResponse<T> = IHttpResponseBase & T

// HTTP Response
export type THttpResponse<T> = THttpErrorResponse | THttpSuccessResponse<T>


export interface ITxPaginate {
  total?: number
  current_page: number
  txns_len: number
  total_page?: number
  per_page?: number
}

// 翻页数据
export interface ITxResultPaginate<T> {
  page?: string
  filter_pos_generated?: number
  page_number?: number
  pager?: Array<number>
  type?: string
  txns: T
  params?: any
  pagination: ITxPaginate
}
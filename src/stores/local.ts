/**
 * chant store
 * @file chat store
 * @module app/pages/explorer/stores/chat
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { IBlnScanMessage } from '@app/types/bln'
import storeAPI from '@app/services/storeapi'
import { accountStore } from '@app/stores/account'

// 存储分为： index [string]
// 实际存储： Map(<index, obj>)


export class LocalStore {


    @observable indexArray: Array<string> = [];
    @observable dataMap: Map<string, any> = new Map();
    @observable arraylist: Array<any> = []
    storgeKey:string = ""
    injector:Function = (v:any)=> v
    constructor(mainkey: string, injector?: Function){
        this.storgeKey = mainkey;
        if(injector) this.injector = injector
    }

    get list(){

        return this.arraylist
    }

    getData(key: string): any{
        
        return this.dataMap.get(key);
    }

    setData(key: string, data: any): any{
        
        return this.dataMap.set(key, data);
    }

    @boundMethod
    updateList(){

        let list = [];
        for(let key of this.indexArray){

            let obj = this.dataMap.get(key);
            obj = this.injector(obj);
            if(obj && obj._id){
                list.push(obj);
            }
        }
        this.arraylist = list.slice();
    }

    @boundMethod
    updateDataList(list: Array<any>){

        while(list.length){

            let tx = list.shift();
            let key = tx.txid;
            if(this.dataMap.has(key) == false){
                this.appendData(tx);
            }
        }
        this.asyncToLocalStorge();
    }
    @boundMethod
    addData(data: any){

        this.appendData(data);
        this.asyncToLocalStorge();
    }

    @boundMethod
    appendData(data: any){
        console.log(data)

        let key = data.txid;
        if(!key) return;
        console.log(this.arraylist.length)
        if(this.dataMap.has(key) == false){
            this.indexArray.push(key);
            this.dataMap.set(key, data);
            this.arraylist.push(this.injector(data))
        }
        console.log(this.arraylist.length)
    }

    @boundMethod
    deleteData(key: string){

        let index = this.indexArray.indexOf(key);

        this.dataMap.delete(key);
        this.indexArray.splice(index, 1);
    }

    @boundMethod
    asyncToLocalStorge(){
        
        storage.set(this.storgeKey + ':indexArray' , this.indexArray);
        storage.set(this.storgeKey + ':dataMap', this.dataMap);
    }

    @boundMethod
    asynFromLocalStorage(){
        storage.get<Array<string>>(this.storgeKey + ':indexArray').then((array)=>{
            this.indexArray = array || [];
        });
        return storage.get<Map<string, any>>(this.storgeKey + ':dataMap').then((map)=>{
            if(map){
                this.dataMap = new Map(Object.entries(map));
                this.updateList();
            }
        });
    }

}

export default LocalStore
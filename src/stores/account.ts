/**
 * App global account store
 * @file App 全局账户
 * @module app/stores/account
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import { IBln, IBlnBalance } from '@app/types/bln'
import { STORAGE } from '@app/constants/storage'
import storage from '@app/services/storage'
import StoreAPI from '@app/services/storeapi'
import BigNumber from 'bignumber.js'
import TouchID from '@app/services/touchid'
import { stashStore } from './stash'
import { userStore } from './users'
import { defaultAvatar } from '@app/utils/bln';
import { chatStore } from '@app/pages/chat/stores/chat'

export interface IAccountStore {
    current: IBln | undefined,
    lists: Array<IBln>
}

class AccountStore {

    constructor() {
        this.resetStore();
    }

    @observable lists: Array<IBln> = []
    @observable count: number = 0
    @observable current: IBln | undefined = undefined
    @observable blnPrice: string = "0"
    @observable blnSymbol: string = "BLN"

    @computed get symbol() {
        return this.blnSymbol
    }

    @computed get currentAddress() {
        return this.current?.address || ""
    }
    @computed get currentMnemonic() {
        return this.current?.mnemonic || ""
    }
    @computed get currentNickname() {
        return this.current?.nickname
    }
    @computed get currentAvatar() {
        return this.current?.imageurl || defaultAvatar(this.currentAddress)
    }

    @computed get currentAvatarUrl() {
        return this.current?.imageurl
    }
    @computed get currentTouchidEnable() {
        return this.current?.touchid || false
    }
    @computed get currentSignmessage() {
        return this.current?.signmessage
    }
    @computed get currentShowBalance() {
        return String(this.current?.balance.balance || 0)
    }
    @computed get currentBalance() {
        return this.current?.balance || {} as IBlnBalance
    }
    @computed get currencybalance() {
        return new BigNumber(this.current?.balance.balance || "0").div(1e8).toString()
    }
    @computed get addresses() {
        return this.lists.map((u)=>{
            return u.address
        })
    }
    // @computed get currentCurrency() {
    //     return parseFloat(new BigNumber(this.blnPrice).multipliedBy(new BigNumber(this.current?.balance.balance || "0").div(1e8)).toFixed(8)).toString()
    // }
    // @computed get currentCurrencySymbol() {
    //     return "RMB"
    // }
    @action.bound
    authenticate(account?: IBln): Promise<boolean> {
        let enable = false
        if (account) {
            enable = account.touchid || false
        } else {
            enable = this.currentTouchidEnable
        }
        if (enable) {
            return TouchID.authenticate()
        } else {
            return Promise.resolve(true)
        }
    }

    @action.bound
    updatePrice(price: string) {
        this.blnPrice = price
        storage.set(STORAGE.LOCAL_PRICE_BLN, price)
    }

    @action.bound
    updatePriceWithOutStorage(price: string) {
        this.blnPrice = price
    }

    @action.bound
    updateCurrentWithOutStorage(current: IBln | undefined) {
        this.current = current
    }

    @action.bound
    updateAccountsWithOutStorage(lists: Array<IBln>) {
        this.lists = lists
    }

    @action.bound
    updateCurrent(current: IBln | undefined) {
        this.current = current
        storage.set(STORAGE.LOCAL_CURRENT_ACCOUNT, current)
    }

    @action.bound
    updateAccounts(lists: Array<IBln>) {
        this.lists = lists
        const iblns: Array<IBln> = [];
        this.lists.forEach((bln) => {
            iblns.push(bln)
        })
        storage.set(STORAGE.LOCAL_ACCOUNTS, iblns)
    }

    @boundMethod
    addAccount(bln: IBln) {
        let exist = false
        const lists = this.lists.slice()
        lists.forEach((b) => {
            if (bln.address === b.address) {
                exist = true
            }
        })
        if (!exist) {
            lists.push(bln)
            this.updateAccounts(lists)
        }
    }

    @boundMethod
    getAccount(address: string){
        let account: IBln|undefined
        this.lists.slice().forEach((b) => {
            if (address === b.address) {
                account = b
            }
        })
        return account
    }

    @boundMethod
    switchAccount(bln: IBln | undefined) {
        let exist = false
        this.lists.forEach((b) => {
            if (bln === undefined || bln.address === b.address) {
                exist = true
            }
        })
        if (exist) {

            if (bln?.address) {

                
                stashStore.initStash(bln.address);
            }


            this.updateCurrentWithOutStorage(bln)
        }
    }

    @boundMethod
    updateAccount(bln: IBln) {
        const blns: Array<IBln> = []
        this.lists.forEach((b) => {
            if (bln.address === b.address) {
                blns.push(bln)
                if (this.current && this.current.address === bln.address) {
                    this.updateCurrent(bln)
                }
            } else {
                blns.push(b)
            }
        })
        this.updateAccounts(blns)
    }

    @boundMethod
    deleteAccount(bln: IBln | undefined) {
        if (!bln) {
            return
        }
        const blns: Array<IBln> = []
        this.lists.forEach((b) => {
            if (bln.address !== b.address) {
                blns.push(b)
            }
        })
        this.updateAccounts(blns)
    }

    @boundMethod
    resetStore() {
        // this.initCurrent()
        this.initAccounts()
        this.initPrice()
    }

    @boundMethod
    refreshBalance(ibalance?: string) {
        return StoreAPI.getBalance(this.currentAddress).then((balance) => {
            const ibln = this.current;
            if (ibln) {
                ibln.balance = Object.assign(ibln.balance, balance);
                if (ibalance) {
                    ibln.balance.balance = ibalance
                }
                this.updateAccount(ibln)
            }
            return balance
        })
    }

    @boundMethod
    refreshMoreBalance(ibalance?: string) {
        return StoreAPI.getMoreBalance(this.currentAddress).then((balance) => {
            const ibln = this.current;
            if (ibln) {
                ibln.balance = Object.assign(ibln.balance, balance);
                if (ibalance) {
                    ibln.balance.balance = ibalance
                }
                this.updateAccount(ibln)
            }
            return balance
        })
    }

    @boundMethod
    updateTouchid(enable?: boolean, address?: string) {
        let ibln;
        if(address){
            ibln  = this.getAccount(address)
        }
        if(!ibln){
            ibln = this.current
        }
        if (ibln) {
            ibln.touchid = enable
            this.updateAccount(ibln)
        }
    }

    @boundMethod
    updateNickname(nickname?: string) {
        const ibln = this.current;
        if (ibln) {
            ibln.nickname = nickname
            this.updateAccount(ibln)
        }
    }

    @boundMethod
    fetchNickname() {
        return StoreAPI.blnScanProfile(this.currentAddress).then((profile) => {
            const ibln = this.current;
            if (ibln) {
                if (profile.nickname) {
                    ibln.nickname = profile.nickname
                }
                this.updateAccount(ibln)
            }
            return profile
        })
    }

    @boundMethod
    updateImageURL(imageurl?: string) {
        const ibln = this.current;
        if (ibln) {
            ibln.imageurl = imageurl
            this.updateAccount(ibln)
        }
    }

    @boundMethod
    fetchImageURL() {
        return StoreAPI.blnScanProfile(this.currentAddress).then((profile) => {
            const ibln = this.current;
            if (ibln) {
                if (profile.imageurl) {
                    ibln.imageurl = profile.imageurl
                }
                this.updateAccount(ibln)
            }
            return profile
        })
    }

    @boundMethod
    updateSignmessage(signmessage?: string) {
        const ibln = this.current;
        if (ibln) {
            ibln.signmessage = signmessage
            this.updateAccount(ibln)
        }
    }

    @boundMethod
    fetchSignmessage() {
        return StoreAPI.blnScanProfile(this.currentAddress).then((profile) => {
            const ibln = this.current;
            if (ibln) {
                if (profile.signmessage) {
                    ibln.signmessage = profile.signmessage
                }
                this.updateAccount(ibln)
            }
            return profile
        })
    }

    @boundMethod
    fetchProfile(){
        return StoreAPI.blnScanProfile(this.currentAddress).then((profile) => {
            const ibln = this.current;
            if (ibln) {
                if (profile.signmessage) {
                    ibln.signmessage = profile.signmessage
                }
                if (profile.nickname) {
                    ibln.nickname = profile.nickname
                }
                if (profile.imageurl) {
                    ibln.imageurl = profile.imageurl
                }
                this.updateAccount(ibln)
            }
            return profile
        })
    }

    @boundMethod
    refreshBlnPrice() {
        return StoreAPI.getBlnPrice().then((price) => this.updatePrice(price.price.toString()))
    }

    private initCurrent() {
        storage.get<IBln>(STORAGE.LOCAL_CURRENT_ACCOUNT)
            .then(ibln => {
                console.log('Init app current account:', ibln)
                this.updateCurrentWithOutStorage(ibln)
            })
    }

    private initAccounts() {
        storage.get<Array<IBln>>(STORAGE.LOCAL_ACCOUNTS).then(iblns => {
            if (iblns && iblns.length) {
                console.log('Init app accounts:', iblns.length)
                this.updateAccountsWithOutStorage(iblns)
            }
        })
    }

    private initPrice() {
        storage.get<string>(STORAGE.LOCAL_PRICE_BLN).then(price => {
            if (price) {
                console.log('Init bln price:', price)
                this.updatePriceWithOutStorage(price)
            }
        })
    }

}

export const accountStore = new AccountStore()


/**
 * App global account store
 * @file App 全局账户
 * @module app/stores/account
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from "mobx";
import { boundMethod } from "autobind-decorator";
import { IBln, IBlnBalance, IBlnGroupData, IBlnScanAccount } from "@app/types/bln";
import { blnnodeAPI, blnscanAPI } from "@app/config";
import fetch from "../services/fetch";
import { defaultAvatar } from "@app/utils/bln";

class UserStore {
    @observable users: Array<IBlnScanAccount> = [];
    @observable User: Map<String, IBlnScanAccount> = new Map();

    constructor() { }

    @boundMethod
    getUsers(): Promise<Array<IBlnScanAccount>> {
        return fetch
            .get(`${blnscanAPI}/api/users`)
            .then((res) => res.json())
            .catch((err) => {
                console.log(err);
                return [];
            });
    }

    @boundMethod
    updateUsers() {

    }

    @boundMethod
    fetchUsers() {
        this.getUsers().then((json) => {
            if (json.length == 0) return;
            this.users = json;
            json.forEach((user) => {
                this.User.set(user.address, user);
            });
        });
    }

    @boundMethod
    getUserName(address: any) {
        if (address === 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e') return 'Blacknet Community'
        if (this.User.has(address)) return this.User.get(address)?.profile.nickname;
        return address;
    }

    @boundMethod
    getDescription(address?: string){

        return this.getProfile(address, 'signmessage');
    }

    getName(address: any) {
        if (address === 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e') return 'Blacknet Community'

        let name = this.getProfile(address, 'nickname');
        if (name) return name;
        return address;
    }

    getUserImage(address: any) {

        let imageurl = this.getProfile(address, 'imageurl');

        return imageurl.length > 0 ? imageurl : defaultAvatar(address);
    }

    @boundMethod
    getProfile(address: any, key: string) {

        if (this.User.has(address)) {
            let user = this.User.get(address), str = user?.profile[key];
            if (str) {
                return str;
            }
        }
        return '';
    }
}

export const userStore = new UserStore();

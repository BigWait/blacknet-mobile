/**
 * contact store
 * @file contact store
 * @module app/stores/contact
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { IBlnScanContact} from '@app/types/bln'
import { STORAGE } from '@app/constants/storage'
import API from '@app/services/api'

export class ContactStore {
    @observable list: IBlnScanContact[] = []
    @observable balances: IBlnScanContact[] = []

    @boundMethod
    init(address: string) {
        return this.get(address)
        .then((contacts)=>{
            this.list = contacts;
            return contacts
        })
    }

    @action
    refresh(address: string){
        return API.blnScanContact(address).then((contacts)=>this.set(address, contacts))
    }

    @boundMethod
    private key(address: string):string{
        return `contact:${address}`;
    }

    @boundMethod
    contact(address: string): IBlnScanContact | null{
        for (let index = 0; index < this.list.length; index++) {
            const contact = this.list[index];
            if(contact.account === address){
                return contact
            }
        }
        return null
    }

    @boundMethod
    get(address: string): Promise<IBlnScanContact[]>{
        return storage.get<Array<IBlnScanContact>>(this.key(address)).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        })  
    }

    @boundMethod
    set(address: string, txns: Array<IBlnScanContact>) {
        this.list = txns
        return storage.set(this.key(address), txns)
    }

    @boundMethod
    upsert(address: string, txns: Array<IBlnScanContact>) {
        return this.get(address).then((contacts)=>{
            if(contacts.length < 1){
                return this.set(address, txns) 
            }
            for (const ac of txns) {
                let index = -1
                contacts.forEach((t, i)=>{
                    if(t.account === ac.account && t.name === ac.name){
                        index = i
                    }
                })
                if(index>-1){
                    contacts.splice(index, 1, ac)
                }else{
                    contacts.push(ac)
                }
            }
            return this.set(address, contacts)
        })
    }

    @boundMethod
    getBalance(address: string): Promise<string>{
        return storage.get<Array<IBlnScanContact>>(STORAGE.LOCAL_CONTACT_BALANCES).then((bs)=>{
            if(!bs){
                return "0"
            }
            for (let index = 0; index < bs.length; index++) {
                const balance = bs[index];
                if(balance.account === address){
                    return balance.balance || "0"
                }
            }
            return "0"
        })
    }

    @boundMethod
    setBalance(address: string, balance: string): any{
        return storage.get<Array<IBlnScanContact>>(STORAGE.LOCAL_CONTACT_BALANCES).then((bs)=>{
            if(!bs){
                return [{
                    account: address,
                    balance: balance
                }]
            }
            for (let index = 0; index < bs.length; index++) {
                const b = bs[index];
                if(b.account === address){
                    b.balance = balance
                    bs[index] = b
                }
            }
            return bs
        }).then((bs)=>{
            return storage.set(STORAGE.LOCAL_CONTACT_BALANCES, bs)
        })
    }

}

export default new ContactStore()
/**
 * stash store
 * @file stash store
 * @module app/stores/stash
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, computed, action } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { IBlnTransaction } from '@app/types/bln'
import lodash from 'lodash';
import StoreAPI from '@app/services/storeapi'
import { txIsCancelLease, txIsPost, txIsReplyPost } from '@app/components/common/asset-type';
import { verifyAccount } from '@app/utils/bln';

class StashStore {
    
    @observable.shallow lists: Array<IBlnTransaction> = []
    @observable address: string = ""
    private timer: any
    private display: boolean = true

    @computed get length() {
        return this.lists.length
    }

    setDisplay(hide: boolean): void {
        this.display = hide;
    }

    @computed get isDisplay() {
        return this.display;
    }

    @computed get pendingLength() {
        let len = 0;
        this.lists.forEach((tx)=>{
            if(this.status(tx) === "pending"){
                len++
            }
        })
        return len
    }

    @computed get list() {
        return this.lists.slice()
    }

   
    @computed get postLength() {
        let len = 0;
        this.lists.forEach((tx)=>{
            if(this.status(tx) === "pending"){
                if(txIsPost(tx) || txIsReplyPost(tx)){
                    len++
                }
            }
        })
        return len
    }

    @computed get cancelLeaseLength() {
        let len = 0;
        this.lists.forEach((tx)=>{
            if(this.status(tx) === "pending" && txIsCancelLease(tx)){
                len++
            }
        })
        return len
    }

    @computed get pendingList(): Array<IBlnTransaction>{
        const list: Array<IBlnTransaction> = [];
        this.lists.forEach((tx)=>{
            if(this.status(tx) === "pending"){
                list.push(tx)
            }
        })
        return list
    }

    

    @boundMethod
    leaseIsPending(from: string, to: string, height: number): boolean{
        let pending = false;
        this.lists.forEach((tx)=>{
            const leaseHeight = tx.blockHeight || tx.leaseHeight
            if(this.status(tx) === "pending" && tx.from === from && tx.to === to && txIsCancelLease(tx) && leaseHeight === height){
                pending = true
            }
        })
        return pending
    }

    @boundMethod
    leaseIsSuccess(from: string, to: string, height: number): boolean{
        let pending = false;
        this.lists.forEach((tx)=>{
            if(txIsCancelLease(tx) && this.status(tx) === "success" && tx.from === from && tx.to === to && tx.leaseHeight === height){
                pending = true
            }
        })
        return pending
    }

    @boundMethod
    private key(address: string):string{
        return `stash:${address}`;
    }

    @boundMethod
    status(tx: IBlnTransaction): string{
        if(tx.blockHeight){
            return "success"
        }
        if(tx.reject){
            return "fail"
        }
        if(tx.time){

            let sub = Date.now() - (+tx.time) * 1000;
            sub = sub / 1000;

            let minutes = sub/60;

            if(minutes > 10) return 'fail';
        }
        return "pending"
    }


    @boundMethod
    get(address: string): Promise<IBlnTransaction[]>{
        return storage.get<Array<IBlnTransaction>>(this.key(address)).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        }).then((txns)=>{
            this.lists = txns;
            return txns
        }) 
    }

    @action
    set(address: string, txns: Array<IBlnTransaction>) {
        this.lists = txns
        return storage.set(this.key(address), txns)
    }

    @boundMethod
    add(address: string, tx: IBlnTransaction) {
        return this.get(address).then((txns)=>{
            if(!txns){
                return this.set(address, [tx]) 
            }
            let index = -1;
            txns.forEach((t, i)=>{
                if(t.txid === tx.txid){
                    index = i
                }
            })
            if(index>-1){
                txns.splice(index, 1, {...txns[index], ...tx})
            }else{
                txns.unshift(tx)
            }
            txns.sort((a, b)=>{
                return parseInt(b.time) - parseInt(a.time)
            })
            return this.set(address, txns)
        })
    }

    @boundMethod
    del(address: string, txid: string) {
        return this.get(address).then((txns)=>{
            if(txns && txns.length){
                let index = -1;
                txns.forEach((t, i)=>{
                    if(t.txid === txid){
                        index = i
                    }
                })
                if(index>-1){
                    txns.splice(index, 1)
                    this.set(address, txns)
                }
            }
        })
    }

    @boundMethod
    initStash(address: string) {
        // storage.remove(this.key(address))
        this.address = address
        return this.get(address)
        .then((txns)=>{
            this.lists = txns;
            return txns
        })
    }

    @boundMethod
    reset() {
        this.lists = [];
        this.address = ''
    }

    @boundMethod
    refresh(address?: string){
        address = address ?? this.address
        if(this.pendingLength && verifyAccount(address)){
            let txs = this.lists.filter((tx)=> this.status(tx) != 'success');

            return Promise.all(txs.map((tx)=>{
                return StoreAPI.fetchTX(tx.txid)
                .then((ntx)=>{
                    if(ntx){
                        return this.add(address??this.address, ntx)
                    }
                })
            }))
        }
        return Promise.resolve()
    }
}

export const stashStore = new StashStore()

import { accountStore } from "@app/stores/account";
import { stashStore } from "@app/stores/stash";
import { userStore } from "@app/stores/users";
import { chatStore } from "./pages/chat/stores/chat";
import StoreAPI from '@app/services/storeapi'
import { DeviceEventEmitter } from "react-native";
import * as RootNavigation from './rootNavigation';
import { IPageProps } from "./types/props";

let websocketInstance: WebSocket | null = null;

let timerId: NodeJS.Timeout ;

function initWesocket() {

    const url = 'ws://101.201.146.224/api/v2/websocket';
    console.log(`connect websocket`)
    websocketInstance = new WebSocket(url);
    if(timerId) clearInterval(timerId);
    websocketInstance.onopen = () => {
        

        if(websocketInstance?.readyState == WebSocket.CONNECTING){
           
            timerId = setInterval(()=>{
                if(websocketInstance?.readyState == WebSocket.OPEN){
                    sendMessage();
                    clearInterval(timerId);
                }
            }, 1000)

        }else if(websocketInstance?.readyState == WebSocket.OPEN){
            sendMessage();
        }
    };

    function sendMessage(){

        let blocks = {
            command: "subscribe",
            route: "block"
        };
        websocketInstance?.send(JSON.stringify(blocks));
    }


    websocketInstance.onmessage = (e) => {
        // a message was received
        try{
            let data = JSON.parse(e.data);
            let hash = data.message.hash;

            if (data.message && data.message.transactions > 0) {
                
                setTimeout(()=>getBlockInfo(hash), 1000)
                
            }

        }catch(e){
           console.log(e)
        }
    };

    websocketInstance.onerror = (e) => {
        console.log(e.message);
    };

    websocketInstance.onclose = (e) => {
        // connection closed
        console.log(e.code, e.reason);

        if(e.code == 1001) setTimeout(initWesocket, 500);
    };
}

let fundAddress = 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e';


function getBlockInfo(hash: string){
    StoreAPI.blnAPIBlock(hash).then((block)=>{

        if(block && block.transactions){

            for(let tx of block.transactions){
                processMessage(tx);
            }
        }
    })
}

function processMessage(tx: any){

    let data = tx.message, text;
    
    if(typeof tx.message == 'string') {
        text = tx.message;
    }else if(tx.data){
        text = tx.data?.message;
    }else if(typeof tx.message == 'object'){
        text = tx.message?.message;
    }

    
    let address = accountStore.currentAddress;
    
    let route = RootNavigation.getCurrentRoute();

    // 更新余额
    if(tx.from == address || tx.to == address){
        accountStore.refreshBalance();
    }

    // 更新stash状态
    if(tx.from == address){
        // 更新stash
        setTimeout(()=> stashStore.refresh(), 5000);
    }

    if(!text) return;

    if(text.indexOf('chat: ') == 0 && route?.name == 'Message'){
        // let params = route?.params;
        // if(tx.to === fundAddress && params.group == true){ // group message
        //     console.log('更新群聊')
        // }else{ // 私聊
        //     console.log('更新私聊')
        // }
        setTimeout(()=> {
            chatStore.refreshChatMap();
            DeviceEventEmitter.emit('WebSocket_New_Messages');
        }, 5000);
    }

    if(text.indexOf('post: ') == 0){
        console.log('更新论坛')
    }

    // 如果有人更新自己的Profile
    if(text.indexOf('profile: ') === 0){

        setTimeout(()=> userStore.fetchUsers(), 5000);
    }


}

export default initWesocket


/*
console.log('tx', txIsChat(tx), tx.to === fundAddress, tx)
    if(txIsChat(tx)){
        console.log('txIsChat(tx)', tx)
        console.log('accountStore.addresses', accountStore.addresses)
        if(accountStore.addresses.includes(tx.to) || tx.to === fundAddress){
            const msg = {
                from: tx.from,
                to: tx.to,
                text: tx.message,
                time: tx.time,
                txid: tx.hash,
                group: true,
                received: true,
                sent: true,
                pending: false
            }
            console.log('msg', msg)
            // chatStore.upserts(accountStore.currentAddress, msg.to, [msg])
            // chatStore.upsertsList(accountStore.currentAddress, [msg])
        }
    }
*/
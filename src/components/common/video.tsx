/**
 * Video
 * @file 公共video
 * @module app/components/common/video
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { observer } from 'mobx-react'
import _RNVideo, { VideoProperties } from 'react-native-video';
// import convertToProxyURL from 'react-native-video-cache'
import { isObject } from 'lodash';
// import RNThumbnail from 'react-native-thumbnail';

export const Video = observer(
    React.forwardRef((props: VideoProperties, ref: any): JSX.Element => {
        let source = props.source
        if(isObject(source) && source.uri){
            try {
                // source.uri = convertToProxyURL(source.uri)
            } catch (error) {
                
            }
        }
        return (
            <_RNVideo
                {...props}
                source={source}
                ref={ref}
            />
        )
    })
)
export type RNVideo = _RNVideo
export default Video
/**
 * BottomSheet
 * @file BottomSheet
 * @module app/components/common/bottom-sheet
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */


import React, { Component } from "react";
import { StyleSheet, View, Text, Button, TouchableOpacity, StatusBar } from 'react-native'
import RBSheet from "react-native-raw-bottom-sheet";
import colors from '@app/style/colors'
import { IChildrenProps } from '@app/types/props'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import RootSiblings from 'react-native-root-siblings';
import ListTitle from '@app/components/common/list-title'
import { Remind } from './remind'
import { languageMaps, TLanguage } from '@app/services/i18n';
import { LANGUAGES, LANGUAGE_KEYS } from '@app/constants/language';
import i18n from '@app/services/i18n'
import { observer } from "mobx-react";
import { observable } from "mobx";
import { IS_ANDROID } from "@app/config";
import { CURRENCY } from "@app/stores/option";

export interface BottomSheetProps {
    visible?: boolean
    onClose?(): void
    onOpen?(): void
    height?: number
}

@observer
export default class BottomSheet extends Component<BottomSheetProps&IChildrenProps> {

    private RBSheet: RBSheet | undefined;

    constructor(props:BottomSheetProps&IChildrenProps) {
        super(props);
    }

    componentDidMount(){
        if(this.props.visible){
            this.RBSheet?.open()
        } else {
            this.RBSheet?.close()
        }
    }

    render() {
        return (
            <RBSheet
                animationType={"fade"}
                ref={(ref:any) => this.RBSheet = ref}
                closeOnDragDown
                onOpen={()=>{
                    if(IS_ANDROID){
                        StatusBar.setBackgroundColor('rgba(0,0,0, 0.5)')
                    }
                    this.props.onOpen && this.props.onOpen()
                }}
                onClose={()=>{
                    if(IS_ANDROID){
                        StatusBar.setBackgroundColor('transparent')
                    }
                    this.props.onClose && this.props.onClose()
                }}
                customStyles={{
                    container: {
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        backgroundColor: colors.background
                    }
                }}
                height={this.props.height}
            >
                {this.props.children}
            </RBSheet>
        );
    }

    static importMnemonic(){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            visible={true}>
            <View style={styles.importMnemonicContainer}>
                <ListTitle 
                    leading={
                        <View style={styles.importMnemonicLeading}>
                            <FontAwesome5
                            name="user"
                            style={styles.importMnemonicIcon}
                            />
                        </View>
                    }
                    title={
                    <Text style={styles.importMnemonicText}>{i18n.t(LANGUAGE_KEYS.IMPORT)}</Text>
                    }
                />
            </View>
          </BottomSheet>);
        return rsb
    }

    static createAccount(press:Function){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            visible={true}>
            <View style={styles.importMnemonicContainer}>
                <ListTitle 
                    contentsStyle={{borderTopWidth: 0}}
                    trailingStyle={{borderTopWidth: 0}}
                    leadingStyle={{borderTopWidth: 0}}
                    onPress={()=>{
                        rsb.destroy()
                        press(rsb)
                    }}
                    leading={
                        <View style={styles.importMnemonicLeading}>
                            <FontAwesome5
                            name="user"
                            style={styles.importMnemonicIcon}
                            />
                        </View>
                    }
                    title={
                    <Text style={styles.importMnemonicText}>{i18n.t(LANGUAGE_KEYS.CREATE)}</Text>
                    }
                />
            </View>
          </BottomSheet>);
        return rsb
    }

    static selectReward(press:Function){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={250}
            visible={true}>
            <View style={styles.importMnemonicContainer}
                // onLayout={(e)=>{
                //     console.log('e.nativeEvent.layout.height', e.nativeEvent.layout.height)
                // }}
            >
                {[5, 10, 50 ,100].map((reward, i)=>{
                    return <ListTitle 
                        key={i}
                        contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                        trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        onPress={()=>{
                            rsb.destroy()
                            press(reward)
                        }}
                        title={
                            <Text style={styles.selectTypeTitle}>{reward} BLN</Text>
                        }
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }

    static selectTxType(press:Function, selected?: number){
        const txTypes = [-1, 0, 2, 3, 254];
        const txTypeNames = [
            i18n.t(LANGUAGE_KEYS.TX_ALL), 
            i18n.t(LANGUAGE_KEYS.TX_TRANSFER), 
            i18n.t(LANGUAGE_KEYS.TX_LEASE), 
            i18n.t(LANGUAGE_KEYS.TX_CANCEL_LEASE), 
            i18n.t(LANGUAGE_KEYS.TX_POS_GENERATED)
        ];
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={300}
            visible={true}>
            <View style={styles.importMnemonicContainer}>
                {txTypes.map((type, i)=>{
                    return <ListTitle 
                        key={i}
                        contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                        trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        onPress={()=>{
                            rsb.destroy()
                            press(type)
                        }}
                        trailing={
                            (selected===type || (selected===undefined && type===-1)) ? <Remind /> : null
                        }
                        title={
                            <Text style={styles.selectTypeTitle}>{type === -1 ? i18n.t(LANGUAGE_KEYS.TX_ALL) : txTypeNames[i]}</Text>
                        }
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }

    static selectReceivedType(press:Function, selected?: number){
        const { styles } = obStyles
        const txTypes = [0, 2];
        const txTypeNames = [
            i18n.t(LANGUAGE_KEYS.TX_TRANSFER), 
            i18n.t(LANGUAGE_KEYS.TX_LEASE)
        ];
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={150}
            visible={true}>
            <View style={styles.importMnemonicContainer}>
                {txTypes.map((type, i)=>{
                    return <ListTitle 
                        key={i}
                        contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                        trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        onPress={()=>{
                            rsb.destroy()
                            press(type)
                        }}
                        trailing={
                            selected===type ? <Remind /> : null
                        }
                        title={
                            <Text style={styles.selectTypeTitle}>{txTypeNames[i]}</Text>
                        }
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }

    static selectLanguage(press:Function, selected?: LANGUAGES){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={150}
            visible={true}>
            <View style={styles.importMnemonicContainer}>
                {Object.keys(languageMaps).map((language,i) =>{
                    const lang = language as TLanguage
                    return <ListTitle 
                        key={language}
                        contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                        trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        onPress={()=>{
                            rsb.destroy()
                            press(lang)
                        }}
                        trailing={
                            selected===lang ? <Remind /> : null
                        }
                        title={
                            <Text style={styles.selectTypeTitle}>{languageMaps[lang].name}</Text>
                        }
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }
    static selectCurrency(press:Function, selected?: CURRENCY){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={180}
            visible={true}>
            <View style={styles.importMnemonicContainer}>
                {[CURRENCY.CNY, CURRENCY.USD, CURRENCY.BTC].map((currency,i) =>{
                    return <ListTitle 
                        key={currency}
                        contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                        trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        onPress={()=>{
                            rsb.destroy()
                            press(currency)
                        }}
                        trailing={
                            selected===currency ? <Remind /> : null
                        }
                        title={
                            <Text style={styles.selectTypeTitle}>{currency}</Text>
                        }
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            selectTypeTitle: {
                fontSize: 18,
                color: colors.textSecondary
            },
            importMnemonicContainer:{
                flex: 1,
                flexDirection: "column",
                paddingLeft: 20,
                paddingRight: 20
            },
            importMnemonicLeading: {
                width: 40,
                height: 40,
                borderRadius: 40,
                backgroundColor: colors.border,
                alignItems: "center",
                justifyContent: "center"
            },
            importMnemonicIcon:{
                fontSize: 25
            },
            importMnemonicText:{
                fontSize: 18
            }
        })
    }
})
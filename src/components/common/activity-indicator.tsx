/* eslint-disable react-native/no-inline-styles */
/**
 * AutoActivityIndicator
 * @file AutoActivityIndicator
 * @module app/components/common/activity-indicator
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { useLayoutEffect, useState } from 'react'
import { ActivityIndicator, View, ViewStyle, TextStyle, StyleProp } from 'react-native'
import { observer } from 'mobx-react'
import { Text } from './text'
import { IS_IOS } from '@app/config'
import colors from '@app/style/colors'
import fonts from '@app/style/fonts'
import mixins from '@app/style/mixins'

export interface IAutoActivityIndicatorProps {
  size?: number | 'small' | 'large'
  style?: ViewStyle
  text?: string
  textStyle?: TextStyle
}

export const AutoActivityIndicator = observer((props: IAutoActivityIndicatorProps): JSX.Element => {

  const getIndicator = (style?: ViewStyle | null) => (
    <ActivityIndicator
      animating={true}
      style={style}
      size={props.size || 'small'}
      color={IS_IOS ? colors.secondary : colors.primary}
    />
  )

  if (props.text) {
    return (
      <View style={[{ ...mixins.colCenter }, props.style]}>
        {getIndicator(null)}
        <Text
          style={[
            {
              ...fonts.small,
              marginTop: 5,
              color: colors.textSecondary
            },
            props.textStyle
          ]}
        >
          {props.text}
        </Text>
      </View>
    )
  }

  return getIndicator(props.style)
})


export const WithActivityIndicator = observer((props: {
  timeout?: number
  style?: StyleProp<ViewStyle>
  enabled?: boolean
  preview?: React.ReactNode | React.ReactNode[]
  children: React.ReactNode | React.ReactNode[]
}): JSX.Element => {
  const { enabled, style, preview, timeout } = props
  const [loading, setLoading] = useState(true);
  useLayoutEffect(() => {
    if (timeout && timeout > 100) {
      setLoading(false)
    } else {
      setTimeout(() => {
        setLoading(false)
      }, timeout || 100)
    }
  })
  if (enabled !== false && loading) {
    return <View style={[{ flex: 1, justifyContent: 'center', alignItems: 'center' }, style]}>
      {preview ?
        preview
        :
        <ActivityIndicator
          animating={true}
        />
      }
    </View>
  }
  return <>
    {props.children}
  </>
})

/**
 * SearchInput
 * @file SearchInput
 * @module app/components/common/searchInput
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, {ReactNode, useState} from 'react'
import { StyleSheet, TextInputProps, TextInput, View, StyleProp, ViewStyle, NativeSyntheticEvent, TextInputTextInputEventData } from 'react-native'
import { observer } from 'mobx-react'
import { observable } from 'mobx'
import colors from '@app/style/colors'
import { optionStore } from '@app/stores/option'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { TouchableView } from './touchable-view'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'

interface TextInputWarpProps {
    viewStyle?: StyleProp<ViewStyle>
}

export const SearchInput = observer((props: TextInputWarpProps & TextInputProps): JSX.Element => {
    const styles = obStyles.styles
    let inputRef: React.RefObject<TextInput> = React.createRef();
    let [trailingVisible, setTrailingVisible] = useState(false)
    const { onChangeText } = props
    const onChange = (e: string)=>{
        setTrailingVisible(!!e)
        if(onChangeText){
            onChangeText(e)
        }
    }
    const onClear=()=>{
        if(inputRef){
            inputRef.current?.clear()
            onChange('')
            inputRef.current?.focus()
        }
    }
    return (
        <View
            style={[
                styles.box,
                props.viewStyle
            ]}
        >
            <View style={[styles.leading]}>
                <FontAwesome5 name="search" size={16} color={colors.textSecondary}/>
            </View>
            <TextInput
                {...props}
                ref={inputRef}
                placeholderTextColor={colors.textSecondary}
                style={[styles.input,props.style]}
                onChangeText={onChange}
                placeholder={i18n.t(LANGUAGE_KEYS.SEARCH)}
            />
            {trailingVisible ?
                <TouchableView style={[styles.trailing]} onPress={onClear}>
                    <FontAwesome5 name="times-circle" size={16} color={colors.textSecondary}/>
                </TouchableView>
            : null}
        </View>
    )
})

export default SearchInput

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        box: {
            backgroundColor: colors.border,
            borderColor: colors.border,
            borderRadius: 4,
            borderWidth: 1,
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center'
        },
        leading: {
            paddingLeft: 15,
            paddingRight: 15
        },
        trailing: {
            paddingLeft: 15,
            paddingRight: 15
        },
        input: {
            minHeight: 30,
            color: colors.textSecondary,
            flex: 1
        }
      })
    }
})
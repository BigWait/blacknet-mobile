/**
 * Remind
 * @file Remind
 * @module app/components/common/remind
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React from 'react'
import { StyleProp, TextStyle } from 'react-native'
import { observer } from 'mobx-react'
import colors from '@app/style/colors'
import Icon from 'react-native-vector-icons/FontAwesome5'

interface IRemindProps {
  size?: number
  color?: string
  style?: StyleProp<TextStyle>
}

export const Remind = observer((props: IRemindProps): JSX.Element => {
  return (
    <Icon
      style={props.style}
      size={props.size || 16}
      color={props.color || colors.primary}
      name="check"
    />
  )
})

/**
 * Scanner
 * @file scanner
 * @module app/components/common/scanner
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */
import React, {useState, Component} from "react";
import { StyleSheet, View, Text, LogBox, Vibration, Button, SafeAreaView, Animated, InteractionManager, Easing } from 'react-native'
import Modal from 'react-native-modal';
import colors from "@app/style/colors";
import i18n from "@app/services/i18n";
import { LANGUAGE_KEYS } from "@app/constants/language";
import { parseQrscanerURL } from '@app/utils/bln'
import { IBlnScanerURL } from '@app/types/bln'
import { RNCamera, BarCodeReadEvent } from 'react-native-camera';
import { IS_IOS } from '@app/config'
import sizes from "@app/style/sizes";
import showToast from "@app/services/toast";
import { observer } from "mobx-react";
import { observable } from "mobx";
import Image from "@app/components/common/image";

export interface ModalScannerProps {
    onSuccess?(uri: IBlnScanerURL): void
}

@observer
export default class ModalScanner extends Component<ModalScannerProps> {
    state = {
        isModalVisible: false,
        animation: new Animated.Value(0)
    };

    constructor(props: ModalScannerProps){
        super(props)
        this.state.animation = new Animated.Value(0)
    }
      
    componentDidMount(){
        LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
        InteractionManager.runAfterInteractions(()=>{
            this.startAnimation()
        });
    }
    
    startAnimation(){
        this.state.animation.setValue(0);
        Animated.timing(this.state.animation, {
            toValue:1,
            duration:1500,
            easing: Easing.linear,
            useNativeDriver: false
        }).start(()=>this.startAnimation());
    }

    toggle = () => {
        this.setState({isModalVisible: !this.state.isModalVisible});
    };
    show = () => {
        this.setState({isModalVisible: true});
    };
    close = () => {
        this.setState({isModalVisible: false});
    };
    barcodeReceived(e: BarCodeReadEvent) {
        if (e) {
            Vibration.vibrate([0, 500], false);
            if(this.props.onSuccess){
                this.props.onSuccess(parseQrscanerURL(e.data))
            }
            this.close()
        } else {
            showToast(i18n.t(LANGUAGE_KEYS.SCAN_QRCODE_ERROR))
        }
    }
    render() {
        const width = sizes.screen.width
        const height = sizes.screen.height
        const { styles } = obStyles
        return (
            <Modal isVisible={this.state.isModalVisible}
                backdropOpacity={1}
            >   
                <SafeAreaView>
                    <View style={styles.header}>
                        <Button color={colors.primary} title={i18n.t(LANGUAGE_KEYS.CANCEL)} onPress={this.close} />
                    </View>
                </SafeAreaView>
                {
                    IS_IOS ? 
                        <RNCamera
                            style={styles.preview}
                            type={RNCamera.Constants.Type.back}
                            barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
                            flashMode={RNCamera.Constants.FlashMode.auto}
                            onBarCodeRead={(e) => this.barcodeReceived(e)}
                            captureAudio={false}
                        >
                            <View style = {{height: (height-264)/3, backgroundColor:'rgba(0,0,0,0.5)'}}></View>
                            <View style={{flexDirection:'row'}}>
                                <View style={styles.itemStyle}/>
                                <View style={styles.rectangle}>
                                    <Image
                                        style={[styles.rectangle, {position:'absolute', left: 0, top: 0}]}
                                        source={require('@app/assets/images/icon_scan_rect.png')}
                                    />
                                    <Animated.View style={[styles.animatedStyle, {
                                        transform: [{
                                        translateY: this.state.animation.interpolate({
                                            inputRange: [0,1],
                                            outputRange: [0,200]
                                        })
                                        }]
                                    }]}>
                                    </Animated.View>
                                </View>
                                <View style={styles.itemStyle}/>
                            </View>
                            <View style={styles.textView}>
                                <Text style={styles.textStyle}>{i18n.t(LANGUAGE_KEYS.SCAN_QRCODE_TEXT)}</Text>
                            </View>
                        </RNCamera>
                    :
                    <RNCamera
                        style={styles.preview}
                        type={RNCamera.Constants.Type.back}
                        googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.QR_CODE}
                        flashMode={RNCamera.Constants.FlashMode.auto}
                        captureAudio={false}
                        onBarCodeRead={(e) => this.barcodeReceived(e)}
                    >
                        <View style = {{height: (height-244)/3, backgroundColor:'rgba(0,0,0,0.5)'}}></View>
                        <View style={{flexDirection:'row'}}>
                            <View style={styles.itemStyle}/>
                            <View style={styles.rectangle}>
                                <Image
                                    style={[styles.rectangle, {position:'absolute', left: 0, top: 0}]}
                                    source={require('@app/assets/images/icon_scan_rect.png')}
                                />
                                <Animated.View style={[styles.animatedStyle, {
                                    transform: [{
                                    translateY: this.state.animation.interpolate({
                                        inputRange: [0,1],
                                        outputRange: [0,200]
                                    })
                                    }]
                                }]}>
                                </Animated.View>
                            </View>
                            <View style={styles.itemStyle}/>
                        </View>
                        <View style={styles.textView}>
                            <Text style={styles.textStyle}>{i18n.t(LANGUAGE_KEYS.SCAN_QRCODE_TEXT)}</Text>
                        </View>
                    </RNCamera>
                    
                }
            </Modal>
        );
    }
}

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: "red",
            // justifyContent: "center",
            // alignItems: "center"
        },
        preview: {
            flex: 1,
        },
        itemStyle:{
            backgroundColor:'rgba(0,0,0,0.5)',
            width: (sizes.screen.width - 240)/2,
            height: 200
        },
        textView:{flex:1,backgroundColor:'rgba(0,0,0,0.5)',alignItems:'center'},
        textStyle:{
            color: colors.primary,
            paddingTop:20,
            fontWeight:'bold',
            fontSize: 14
        },
        animatedStyle:{
            height:2,
            backgroundColor: colors.primary
        },
        rectangle: {
            height: 200,
            width: 200,
        },
        topViewStyle:{
            height: 30,
            backgroundColor: colors.yellow
        },
        header:{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            height: 50,
            alignItems: "center"
        }
    })
    }
})
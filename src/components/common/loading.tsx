/**
 * Loading
 * @file 双击控件
 * @module app/components/common/loading
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */
import React, {
    Component
} from 'react';
import {
    View,
    StyleSheet,
    ActivityIndicator
} from 'react-native';
import RootSiblings from 'react-native-root-siblings';
import Spinner from 'react-native-loading-spinner-overlay';
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { observer } from 'mobx-react';
import { observable } from 'mobx';

@observer
export default class Loading extends Component {
    static displayName = i18n.t(LANGUAGE_KEYS.LOADING);

    static show = (msg: string = '') => {
        const { styles } = obStyles
        return new RootSiblings(<Spinner
            visible={true}
            textContent={msg}
            textStyle={styles.spinnerTextStyle}
        />);
    };

    static hide = (loading: RootSiblings) => {

        if(!loading) return;
        if (loading instanceof RootSiblings) {
            loading.destroy();
        } else {
            console.warn(`Loading.hide expected a \`RootSiblings\` instance as argument.\nBut got \`${typeof loading}\` instead.`);
        }
    };

    private _loading: RootSiblings;

    constructor(props:any) {
        super(props);
        this._loading = new RootSiblings(<Spinner
            visible={true}
        />);
    }

    UNSAFE_componentWillMount = () => {
        this._loading = new RootSiblings(<Spinner
            visible={true}
        />);
    };

    UNSAFE_componentWillReceiveProps = (nextProps:any) => {
        this._loading.update(<ActivityIndicator
            {...nextProps}
        />);
    };

    componentWillUnmount = () => {
        this._loading.destroy();
    };

    render() {
        return null;
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            spinnerTextStyle: {
              color: '#FFF'
            }
        })
    }
})

export {
    RootSiblings as Manager
};
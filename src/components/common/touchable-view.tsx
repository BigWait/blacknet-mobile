/**
 * TouchableView
 * @file TouchableView
 * @module app/components/common/touchable-view
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React from 'react'
import { TouchableOpacity, TouchableOpacityProps } from 'react-native'
import { observer } from 'mobx-react'
import { IChildrenProps } from '@app/types/props'
import sizes from '@app/style/sizes'

export const TouchableView = observer((props: TouchableOpacityProps & IChildrenProps): JSX.Element => {
  return (
    <TouchableOpacity
      activeOpacity={sizes.touchOpacity}
      {...props}
    >
      {props.children}
    </TouchableOpacity>
  )
})

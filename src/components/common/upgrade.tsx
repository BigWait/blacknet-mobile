/**
 * Upgrade
 * @file Upgrade
 * @module app/components/common/upgrade
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */
import React, {useState, Component} from "react";
import { StyleSheet, View, Text, LogBox, Vibration, SafeAreaView, Animated, InteractionManager, Easing, Image, AccessibilityInfo, ActivityIndicator, Linking, DeviceEventEmitter, EmitterSubscription, NativeModules } from 'react-native'
import Modal, { ModalProps } from 'react-native-modal';
import colors from "@app/style/colors";
import i18n from "@app/services/i18n";
import { LANGUAGE_KEYS } from "@app/constants/language";
import { parseQrscanerURL } from '@app/utils/bln'
import { IBlnScanerURL, IBlnVersion } from '@app/types/bln'
import { RNCamera, BarCodeReadEvent } from 'react-native-camera';
import { appleID, IS_IOS } from '@app/config'
import sizes from "@app/style/sizes";
import showToast from "@app/services/toast";
import { observer } from "mobx-react";
import { action, observable } from "mobx";
import RootSiblings from 'react-native-root-siblings';
import PropTypes from 'prop-types';
import { optionStore } from "@app/stores/option";
import BigNumber from "bignumber.js";
import Markdown from "react-native-markdown-display";
import Button from "../ui/button";
import * as Progress from 'react-native-progress';
import { boundMethod } from "autobind-decorator";
import { 
    upgrade,
    openAPPStore,
    addDownListener,
} from 'rn-app-upgrade';

export default {
    update: (data?: IBlnVersion)=>{
        const _root = new RootSiblings(
            <UpgradeContainer 
                onModalHide={()=>_root.destroy()}
                data={data}
            />
        );
        return _root
    }
};

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            content: {
                backgroundColor: colors.background,
                padding: 22,
                borderRadius: 4,
                position: "relative"
            },
            contentTitle: {
                fontSize: 20,
                marginBottom: 15,
                textAlign: "center"
            },
            description: {
                marginBottom: 12
            },
            contentDesc: {
                marginBottom: 15
            },
            progressBar: {
                flex: 1
            },
            progressText: {
                paddingRight: 10,
                color: colors.primary
            },
            progress: {
                marginBottom: 8,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center'
            },
            contentDescText: {
                fontSize: 16
            },
            disableButton: {
                backgroundColor: colors.textMuted,
                borderWidth: 0
            },
            downButton: {

            },
            cancelButton: {
                marginTop: 10
            }
        })
    }
})

export interface UpgradeContainerProps {
    isVisible: boolean
    data?: IBlnVersion
    onModalHide?(): void
}

@observer
export class UpgradeContainer extends Component<UpgradeContainerProps> {
    static defaultProps = {
        isVisible: true
    }
    static propTypes = {
        isVisible: PropTypes.bool
    }
    state = {
        isVisible: true,
        progress: 0,
        download: false
    };
    private subscription?: EmitterSubscription
    componentWillUnmount(){
        this.subscription?.remove()
    }
    componentDidMount(){
        this.subscription = DeviceEventEmitter.addListener('LOAD_PROGRESS',(progress: number)=>{ //监听进度
            this.setState({
                progress: progress / 100
            })
            if(progress >= 100){
                this.reset()
            }
        })
    }
    @boundMethod
    toggle(){
        this.setState({isVisible: !this.state.isVisible});
    };
    @boundMethod
    show(){
        this.setState({isVisible: true});
    };
    @boundMethod
    close(){
        this.setState({isVisible: false});
    };
    @boundMethod
    download(){
        this.setState({download: true, progress: 0});
    }
    @boundMethod
    reset(){
        this.setState({isVisible: true, progress: 0, download: false});
    }
    @boundMethod
    onPress(){
        if(this.props.data?.DownloadUrl){
            if(IS_IOS){
                return Linking.canOpenURL(optionStore.versionUpdateUrl).then(supported => {
                    supported && Linking.openURL(optionStore.versionUpdateUrl);
                }).catch(error => {
                    console.log(`Open url: ${optionStore.versionUpdateUrl} failed:`, error)
                })
                // return openAPPStore(appleID)
            }else{
                this.download()
                upgrade(this.props.data?.DownloadUrl)
            }
        }
    }
    render() {
        const { styles } = obStyles
        return (
            <Modal isVisible={this.state.isVisible}
                onModalHide={this.props.onModalHide}
            >
                <View style={styles.content}>
                    <Text style={styles.contentTitle}>{i18n.t(LANGUAGE_KEYS.UPGRADE_HAS_NEW_VERSION)}</Text>
                    <View style={styles.contentDesc}>
                        <Text style={styles.contentDescText}>{`${i18n.t(LANGUAGE_KEYS.UPGRADE_VERSION)}：${this.props.data?.VersionName}`}</Text>
                        <Text style={styles.contentDescText}>{`${i18n.t(LANGUAGE_KEYS.UPGRADE_SIZE)}：${new BigNumber(this.props.data?.ApkSize || "").dividedBy(1024).toFixed(1).toString()}MB`}</Text>
                    </View>
                    <View style={styles.description}>
                        <Markdown>
                            {this.props.data?.ModifyContent}
                        </Markdown>
                    </View>
                    {/* {this.renderProgressBar()} */}
                    {this.renderButton()}
                    {this.renderCancelButton()}
                </View>
            </Modal>
        );
    }
    renderButton(){
        const { styles } = obStyles
        if(this.state.download){
            return <Button text={parseFloat((this.state.progress * 100).toFixed(2)).toString()+"%"} style={[{width: "100%", height: 40, maxWidth: undefined}, styles.disableButton]}
                onPress={undefined}
            />
        }
        return <Button text={i18n.t(LANGUAGE_KEYS.UPGRADE)} style={[{width: "100%", height: 40, maxWidth: undefined}, styles.downButton]}
            onPress={this.onPress}
        />
    }
    renderCancelButton(){
        const { styles } = obStyles
        if(this.state.download){
            return <Button text={i18n.t(LANGUAGE_KEYS.CANCEL)} style={[{width: "100%", height: 40, maxWidth: undefined, marginTop: 10}, styles.disableButton]}
                onPress={undefined}
            />
        }
        return <Button text={i18n.t(LANGUAGE_KEYS.CANCEL)} style={[{width: "100%", height: 40, maxWidth: undefined, marginTop: 10}, styles.cancelButton]}
            onPress={this.close}
        />
    }
    renderProgressBar(){
        if(!this.state.download){
            return null
        }
        const { styles } = obStyles
        return <View style={styles.progress}>
                    <View style={styles.progressBar}>
                        <Progress.Bar progress={this.state.progress} width={null} color={colors.primary}>
                        
                        </Progress.Bar>
                    </View>
              </View>
    }
}

// if(IS_IOS){
//   return Linking.canOpenURL(optionStore.versionUpdateUrl).then(supported => {
//       supported && Linking.openURL(optionStore.versionUpdateUrl);
//   }).catch(error => {
//       console.log(`Open url: ${optionStore.versionUpdateUrl} failed:`, error)
//   })
// }
// if(optionStore.version?.DownloadUrl){
//   upgrade(optionStore.version?.DownloadUrl)
// }

// class UpgradeStore{
//     @observable isVisible: boolean = true

//     @action.bound
//     update() {
        
//     }
//     @action.bound
//     onModalHide(){
//         this.isVisible = true
//     }
// }

// const upgradeStore = new UpgradeStore()

// export interface UpgradeProps {

// }

// let _upgrade: RootSiblings

// @observer
// export class Upgrade extends Component<UpgradeProps> {

//     @observable isVisible: boolean = false
//     private _upgrade?: RootSiblings; 

//     static update(){
//         return new RootSiblings(<UpgradeContainer onModalHide={upgradeStore.onModalHide} isVisible={upgradeStore.isVisible}/>);
//         if(_upgrade instanceof RootSiblings){
//             _upgrade.update(<UpgradeContainer onModalHide={upgradeStore.onModalHide} isVisible={upgradeStore.isVisible}/>);
//         } else {
//             _upgrade = new RootSiblings(<UpgradeContainer onModalHide={upgradeStore.onModalHide} isVisible={upgradeStore.isVisible}/>);
//         }
//         return _upgrade
//     }
      
//     componentWillMount = () => {
//         console.log(111)
//         this._upgrade = new RootSiblings(<UpgradeContainer
//             isVisible={upgradeStore.isVisible}
//             onModalHide={upgradeStore.onModalHide} 
//             {...this.props}
//         />);
//     };

//     componentWillReceiveProps = (nextProps:any) => {
//         console.log(2222)
//         this._upgrade?.update(<UpgradeContainer
//             onModalHide={upgradeStore.onModalHide} 
//             {...nextProps}
//         />);
//     };

//     componentWillUnmount = () => {
//         console.log(3333)
//         this._upgrade?.destroy();
//     };

//     render() {
//         return null
//     }
// }
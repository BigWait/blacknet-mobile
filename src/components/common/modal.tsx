/**
 * Modal
 * @file Modal
 * @module app/components/common/modal
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React from 'react';
import {
  TouchableWithoutFeedback,
  StyleSheet,
  Modal,
  View,
} from 'react-native';
import t from 'prop-types';
import { StyleProp, ViewStyle, ModalProps } from 'react-native'
import { IChildrenProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes';
import { observer } from 'mobx-react';
import { observable } from 'mobx';

interface MyModalProps {
    dismiss?(): void
    style?: StyleProp<ViewStyle>
}

@observer
class MyModal extends React.Component<MyModalProps & ModalProps & IChildrenProps> {

  static propTypes = {
    children: t.node.isRequired,
    visible: t.bool.isRequired,
    dismiss: t.func.isRequired,
    transparent: t.bool,
    animationType: t.string,
  };

  static defaultProps = {
    animationType: 'fade',
    transparent: true,
  };

  render() {
    const { props } = this;
    const { styles } = obStyles
    return (
        <Modal
            visible={props.visible}
            transparent={props.transparent}
            onRequestClose={props.dismiss}
            animationType={props.animationType}
        >
            <View style={styles.modal}>
                <TouchableWithoutFeedback onPress={props.dismiss}>
                    <View style={styles.modalOverlay} />
                </TouchableWithoutFeedback>
                <View style={[styles.modalContent, props.style]}>
                    {props.children}
                </View>
            </View>
        </Modal>
    );
  }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            modal:{
                flex: 1,
                justifyContent: 'center',
                alignItems: "center"
            },
            modalContent: {
                width: sizes.screen.width - 100,
                backgroundColor: colors.background,
                padding: 10,
                borderRadius: 5
            },
            modalOverlay: {
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                backgroundColor: 'rgba(0,0,0,0.5)'
            }
        })
    }
})

export default MyModal;

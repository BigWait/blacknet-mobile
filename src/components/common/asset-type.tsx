import { BlnTxType, IBlnTransaction } from "@app/types/bln"
import BigNumber from "bignumber.js"

export const txIsTransfer = (tx: IBlnTransaction):boolean=>{
    if(tx.type === BlnTxType.Transfer && !/^(chat|profile|post|new contact): /i.test(tx.message || "")){
        return true
    }
    return false
}
export const txIsLease = (tx: IBlnTransaction):boolean=>{
    if(tx.type === BlnTxType.Lease){
        return true
    }
    return false
}
export const txIsCancelLease= (tx: IBlnTransaction):boolean=>{
    if(tx.type === BlnTxType.CancelLease){
        return true
    }
    return false
}
export const txIsProfile= (tx: IBlnTransaction):boolean=>{
    if(tx.type === BlnTxType.Transfer && /^(profile): /i.test(tx.message || "") && tx.amount == new BigNumber("0.1").multipliedBy(1e8).toString()){
        return true
    }
    return false
}
export const txIsChat= (tx: IBlnTransaction):boolean=>{
    if(tx.type === BlnTxType.Transfer && /^(chat): /i.test(tx.message || "") && tx.amount == new BigNumber("0.005").multipliedBy(1e8).toString()){
        return true
    }
    return false
}
export const txIsPost= (tx: IBlnTransaction):boolean=>{
    if(tx.type === BlnTxType.Transfer && /^(post): /i.test(tx.message || "") && tx.amount == new BigNumber("0.03").multipliedBy(1e8).toString()){
        return true
    }
    return false
}
export const txIsReplyPost= (tx: IBlnTransaction):boolean=>{
    if(tx.type === BlnTxType.Transfer && /^(post): /i.test(tx.message || "") && tx.amount == new BigNumber("0.031").multipliedBy(1e8).toString()){
        return true
    }
    return false
}
export const txIsContact= (tx: IBlnTransaction):boolean=>{
    if(tx.type === BlnTxType.Transfer && /^(new contact): /i.test(tx.message || "") && tx.amount == new BigNumber("0.1").multipliedBy(1e8).toString()){
        return true
    }
    return false
}
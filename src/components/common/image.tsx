/**
 * Image
 * @file 公共图像组件
 * @module app/components/common/image
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { useState } from 'react'
import { ImageProps, Image as RNImage} from 'react-native'
import { Observer } from 'mobx-react'
import FastImage, { FastImageProps } from 'react-native-fast-image'
import { isObject } from 'lodash';
import colors from '@app/style/colors';

export const Image = (props:  ImageProps & FastImageProps): JSX.Element => {
    const [erred, setErred] = useState(false)
    if(isObject(props.source)){
        let source = props.source
        if((props.source as {[key: string]: any}).uri === 'https://blacknet.ninja/images/logo.png'){
            source = require('@app/assets/images/1024.png')
        }
        return <Observer render={()=>
            <FastImage
                {...props}
                source={source}
                style={[props.style, erred ? {
                    backgroundColor: colors.border
                } : {}]}
                onError={()=>setErred(true)}
            />
        }/>
    }
    return <Observer render={()=>
        <RNImage
            {...props}
        />
    }/>
}

export default Image


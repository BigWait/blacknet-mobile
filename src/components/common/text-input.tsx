/**
 * TextInput
 * @file TextInput
 * @module app/components/common/text-input
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, {ReactNode} from 'react'
import { StyleSheet, TextInputProps, TextInput, View, StyleProp, ViewStyle } from 'react-native'
import { observer } from 'mobx-react'
import { observable } from 'mobx'
import colors from '@app/style/colors'
import { optionStore } from '@app/stores/option'

interface TextInputWarpProps {
    trailing?: ReactNode
    leading?: ReactNode
    viewStyle?: StyleProp<ViewStyle>
}

export const TextInputWarp = observer((props: TextInputWarpProps & TextInputProps): JSX.Element => {
    const styles = obStyles.styles
    // props.style
    return (
        <View
            style={[
                styles.inputBox,
                props.viewStyle
            ]}
        >
            {props.leading ?? null}
            <TextInput
                {...props}
                placeholderTextColor={colors.border}
                style={[styles.input,props.style]}
            />
            {props.trailing ?? null}
        </View>
    )
})

export default TextInputWarp

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        inputBox: {
            backgroundColor: colors.cardBackground,
            borderColor: colors.border,
            borderWidth: 1,
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            minHeight: 40
        },
        input: {
            marginLeft: 5,
            backgroundColor: colors.cardBackground,
            height: 40,
            flex: 1
        }
      })
    }
})
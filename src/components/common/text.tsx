/**
 * Text
 * @file 公共文本控件
 * @module app/components/common/text
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React from 'react'
import { Text as RNText, TextProps } from 'react-native'
import { observer } from 'mobx-react'
import { IChildrenProps } from '@app/types/props'
import colors from '@app/style/colors'
import fonts from '@app/style/fonts'

export const Text = observer((props: TextProps & IChildrenProps): JSX.Element => {
  return (
    <RNText
      {...props}
      style={[
        {
          color: colors.textDefault,
          fontFamily: fonts.fontFamily
        },
        props.style
      ]}
    >
      {props.children}
    </RNText>
  )
})

export const BLNText = observer((props: TextProps & IChildrenProps): JSX.Element => {
  return (
    <Text
      {...props}
      style={[
        {
          color: colors.textDefault
        },
        props.style
      ]}
    >
      {props.children}
    </Text>
  )
})

export const TextLink = observer((props: TextProps & IChildrenProps): JSX.Element => {
  return (
    <RNText
      {...props}
      style={[
        props.style,
        {
          color: colors.textLink,
          fontFamily: fonts.fontFamily,
          flexShrink:1
        }
        
      ]}
    >
      {props.children}
    </RNText>
  )
})


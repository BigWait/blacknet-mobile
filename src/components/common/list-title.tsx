/**
 * ListTitle
 * @file ListTitle
 * @module app/components/common/list-title
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, ReactNode } from "react";
import { StyleSheet, View, ViewStyle, StyleProp } from 'react-native'
import colors from '@app/style/colors'
import { TouchableView } from '@app/components/common/touchable-view'
import { observer } from "mobx-react";
import { observable } from "mobx";
import { color } from "react-native-reanimated";
 
export interface ListTitleProps {
    title?: ReactNode
    subtitle?: ReactNode
    trailing?: ReactNode
    leading?: ReactNode
    onPress?(): void
    style?: StyleProp<ViewStyle>
    contentsStyle?: StyleProp<ViewStyle>
    trailingStyle?: StyleProp<ViewStyle>
    leadingStyle?: StyleProp<ViewStyle>
    titleStyle?: StyleProp<ViewStyle>
    subtitleStyle?: StyleProp<ViewStyle>
  }
  
@observer
export default class ListTitle extends Component<ListTitleProps> {

    constructor(props: ListTitleProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.container, this.props.style]}
                onPress={this.props.onPress}
            >
                {this.props.leading ?
                    <View style={[styles.leading, this.props.leadingStyle]}>{this.props.leading}</View>
                : null}
                {this.props.title || this.props.subtitle || this.props.leading || this.props.trailing ?
                    <View style={[styles.content, this.props.contentsStyle]}>
                        {this.props.title ?
                            <View style={[styles.title, this.props.titleStyle]}>{this.props.title}</View>
                        : null}
                        {this.props.subtitle ?
                            <View style={[styles.subtitle, this.props.subtitleStyle]}>{this.props.subtitle}</View>
                        : null}
                    </View>
                : null}
                {this.props.trailing ?
                    <View style={[styles.trailing, this.props.trailingStyle]}>{this.props.trailing}</View>
                : null}
            </TouchableView>
        );
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            container: {
                flexDirection: 'row',
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-start',
                // backgroundColor: colors.cardBackground,
                marginLeft: 10,
                marginRight: 10,
            },
            content: {
                flexDirection: 'column',
                borderTopWidth: 1,
                height: 56,
                flex: 1,
                borderTopColor: colors.border,
                justifyContent: "center",
            },
            leading: {
                borderTopColor: colors.border,
                width: 32,
                height: 56,
                paddingLeft: 5,
                backgroundColor: 'transparent',
                alignItems: "flex-start",
                justifyContent: "center",
                color: colors.textDefault
            },
            trailing: {
                height: 56,
                borderTopColor: colors.border,
                borderTopWidth: 1,
                justifyContent: "center",
            },
            title: {
        
            },
            subtitle: {
        
            },
            listTitleText:{
                fontSize: 14,
                color: colors.textDefault,
                marginRight: 10
            },
        })
    }
})

@observer
export class RowList extends Component<ListTitleProps> {

    constructor(props: ListTitleProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        if(!this.props.onPress){
            return (
                <View
                    style={[{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start'
                    }, this.props.style]}
                >
                    {this.renderContent()}
                </View>
            );
        }
        return (
            <TouchableView
                style={[{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-start'
                }, this.props.style]}
                onPress={this.props.onPress}
            >
                {this.renderContent()}
            </TouchableView>
        );
    }
    renderContent(){
        const { styles } = obStyles
        return <View style={{flexDirection: 'row', position: 'relative' ,paddingTop: 6, paddingBottom: 6, paddingLeft: 36, paddingRight: 8}}>
            {this.props.leading ?
                <View style={[{
                    position: "absolute",
                    left: -4,
                    top: -5,
                    bottom: -6,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderBottomColor: "#ffffff",
                    borderBottomWidth: 1,
                    width: 40
                }, this.props.leadingStyle]}>{this.props.leading}</View>
            : null}
            {this.props.trailing ?
                <View style={[{

                }]}>
                    {this.props.trailing}
                </View>
            : null}
        </View>
    }
}
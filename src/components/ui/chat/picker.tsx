/**
 * pciker
 * @file picker
 * @module app/components/common/ui/chat/picker
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React from 'react'
import { StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native'
import { observer } from 'mobx-react'
import colors from '@app/style/colors'
import EmojiSelector, { Categories, EmojiSelectorProps } from "react-native-emoji-selector";
import { observable } from 'mobx'
import Feather from 'react-native-vector-icons/Feather'
import LinearGradient from 'react-native-linear-gradient';
import { optionStore } from '@app/stores/option'
import { TouchableView } from '@app/components/common/touchable-view';
import { color } from 'react-native-reanimated';
import { isFunction } from 'lodash';
import i18n from '@app/services/i18n';
import { LANGUAGES, LANGUAGE_KEYS } from '@app/constants/language';

export const ChatPlusPicker = observer((props: {
    containerStyle?: StyleProp<ViewStyle>
    acctionStyle?: StyleProp<ViewStyle>
    acctionTextStyle?: StyleProp<ViewStyle>
    onPress?(type: string): void
}): JSX.Element => {
    const { styles } = obStyles
    return (
        <View
        style={[
            {
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                paddingLeft: 10,
                paddingRight: 10
            },
            props.containerStyle
        ]}
        >
             <TouchableView 
                style={[styles.acctionStyle, props.acctionStyle]}
                onPress={()=>{
                    if(isFunction(props.onPress)){
                        props.onPress('album')
                    }
                }}
            >
                <Feather name={"image"} size={40} color={colors.textTitle} />
                <Text style={[styles.acctionTextStyle, props.acctionTextStyle]}>{i18n.t(LANGUAGE_KEYS.ALBUM)}</Text>
            </TouchableView>
            <TouchableView 
                style={[styles.acctionStyle, props.acctionStyle]}
                onPress={()=>{
                    if(isFunction(props.onPress)){
                        props.onPress('camera')
                    }
                }}
            >
                <Feather name={"camera"} size={40} color={colors.textTitle} />
                <Text style={[styles.acctionTextStyle, props.acctionTextStyle]}>{i18n.t(LANGUAGE_KEYS.CAMERA)}</Text>
            </TouchableView>
        </View>
    )
})

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            acctionStyle: {
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 6,
                height: 70,
                width: 70,
                marginRight: 10,
                backgroundColor: optionStore.darkTheme ? colors.border : colors.border,
            },
            acctionTextStyle: {
                textAlign: 'center',
                fontSize: 12,
                marginTop: 2,
                color: colors.textTitle
            }
        })
    }
})


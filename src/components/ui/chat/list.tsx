import React, { Component } from "react";
import { StyleSheet, View, Text, StyleProp, ViewStyle } from 'react-native'
import colors from '@app/style/colors'
import { TouchableView } from '@app/components/common/touchable-view'
import sizes from "@app/style/sizes";
import { showTimeFromNow } from "@app/utils/bln";
import { TextLink } from "@app/components/common/text";
import { observer } from "mobx-react";
import { observable } from "mobx";
import Image from "@app/components/common/image";

export interface ChatMessageListItemProps {
    name?: string
    time?: string
    last?: string
    avatar?: string
    style?: StyleProp<ViewStyle>
    onPress?(): void
}
  
@observer
export class ChatMessageListItem extends Component<ChatMessageListItemProps> {

    constructor(props: ChatMessageListItemProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.listWarp, this.props.style]}
                onPress={this.props.onPress}
            >
                <View style={styles.avatarBox}>
                    {this.props.avatar ?
                        <Image
                            style={styles.avatar}
                            source={{uri: this.props.avatar}}
                        />
                    : 
                        <Image
                            style={styles.avatar}
                            source={require('@app/assets/images/1024.png')}
                        />
                    }
                </View>
                <View style={styles.box}>
                    <View style={styles.left}>
                        {this.props.name ? <TextLink style={styles.leftText} numberOfLines={1}>{this.props.name}</TextLink>: null}
                        {this.props.last ? <Text style={[styles.leftText, {color: colors.textSecondary}]} numberOfLines={1}>{this.props.last}</Text>: null}
                    </View>
                    {this.props.time ?
                        <View style={styles.right}>
                            <Text style={{color: colors.textDefault}}>{showTimeFromNow(this.props.time)}</Text>
                        </View>
                    : null}
                </View>
            </TouchableView>
        );
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            listWarp:{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 3,
                flex: 1
            },
            avatar:{
                width: 40,
                height: 40,
                borderRadius: 40
            },
            avatarBox:{
                marginRight: 15,
                marginLeft: 15
            },
            left: {
                flex: 1,
                paddingRight: 10,
            },
            leftText: {
                paddingVertical: 4,
                
            },
            right: {
                minWidth: 0,
                paddingRight: 10
            },
            box: {
                borderBottomColor: colors.border,
                borderBottomWidth: 1,
                width: sizes.screen.width - 70,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingBottom: 5,
                paddingTop: 5,
            },
            title: {
                flex: 1
            },
            subTitle: {
                flex: 1
            },
            time: {
            }
        })
    }
})





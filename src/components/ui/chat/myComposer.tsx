import { TouchableView } from '@app/components/common/touchable-view';
import colors from '@app/style/colors';
import { shortAddress } from '@app/utils/address';
import { verifyAccount, verifyAlias } from '@app/utils/bln';
import MSG from '@app/utils/msg';
import { boundMethod } from 'autobind-decorator';
import PropTypes from 'prop-types';
import React from 'react';
import { LayoutChangeEvent, NativeSyntheticEvent, Platform, StyleSheet, Text, TextInput, TextInputChangeEventData, View } from 'react-native';
import { ComposerProps, IMessage, utils } from 'react-native-gifted-chat'
import AntDesign from 'react-native-vector-icons/AntDesign'
export const MIN_COMPOSER_HEIGHT = Platform.select({
    ios: 33,
    android: 41,
    web: 34,
});
export const MAX_COMPOSER_HEIGHT = 200;
export const DEFAULT_PLACEHOLDER = 'Type a message...';
const { StylePropType } = utils
const styles = StyleSheet.create({
    textInput: {
        flex: 1,
        marginLeft: 10,
        fontSize: 16,
        lineHeight: 16,
        ...Platform.select({
            web: {
                paddingTop: 6,
                paddingLeft: 4,
            },
        }),
        marginTop: Platform.select({
            ios: 6,
            android: 0,
            web: 6,
        }),
        marginBottom: Platform.select({
            ios: 5,
            android: 3,
            web: 4,
        }),
    },
});
export default class MyComposer extends React.Component<ComposerProps & {
    renderQuote?(props: ComposerProps): React.ReactChild | React.ReactChild[]
    onLayout?:((event: LayoutChangeEvent) => void) | undefined
    clearQuote?(): void
    quoteMessage?: IMessage
    minComposerQuoteHeight: number
}> {
    static contextTypes = {
        actionSheet: PropTypes.func,
    };
    static defaultProps = {
        composerHeight: MIN_COMPOSER_HEIGHT,
        minComposerQuoteHeight: 0,
        text: '',
        placeholderTextColor: '#b2b2b2',
        placeholder: DEFAULT_PLACEHOLDER,
        textInputProps: null,
        multiline: true,
        disableComposer: false,
        textInputStyle: {},
        textInputAutoFocus: false,
        keyboardAppearance: 'default',
        onTextChanged: (text: string) => { },
        onInputSizeChanged: () => { },
    };
    static propTypes = {
        composerHeight: PropTypes.number,
        text: PropTypes.string,
        placeholder: PropTypes.string,
        placeholderTextColor: PropTypes.string,
        textInputProps: PropTypes.object,
        onTextChanged: PropTypes.func,
        onInputSizeChanged: PropTypes.func,
        multiline: PropTypes.bool,
        disableComposer: PropTypes.bool,
        textInputStyle: StylePropType,
        textInputAutoFocus: PropTypes.bool,
        keyboardAppearance: PropTypes.string,
    };

    contentSize: {
        width: number;
        height: number;
    } = {
        height: 0,
        width: 0
    }
    textInputHeight: number = 0
    textInputHeightOffset: number = 0
    quoteHeight: number = 0
    @boundMethod
    onContentSizeChange(e: any){
        const { contentSize } = e.nativeEvent;
        // Support earlier versions of React Native on Android.
        if (!contentSize) {
            return;
        }
        if (!this.contentSize.height && this.textInputHeight) {
            this.textInputHeightOffset = this.textInputHeight - contentSize.height
        }
        if (!this.contentSize ||
            (this.contentSize &&
                (this.contentSize.width !== contentSize.width ||
                    this.contentSize.height !== contentSize.height))) {
                        contentSize.height += this.textInputHeightOffset
                        // contentSize.height += this.quoteHeight
            this.contentSize = contentSize;
            if(this.props.onInputSizeChanged) this.props.onInputSizeChanged(this.contentSize);
        }
    }
    @boundMethod
    onChangeText(text: string){
        if(this.props.onTextChanged) this.props.onTextChanged(text);
    }
    render() {
        return (<View style={{flex: 1}}
                onLayout={this.props.onLayout}
            >
            <TextInput testID={this.props.placeholder} accessible accessibilityLabel={this.props.placeholder} placeholder={this.props.placeholder} placeholderTextColor={this.props.placeholderTextColor} multiline={this.props.multiline} editable={!this.props.disableComposer} onChange={this.onContentSizeChange} onContentSizeChange={this.onContentSizeChange} onChangeText={this.onChangeText} style={[
                styles.textInput,
                this.props.textInputStyle,
                {
                    height: this.props.composerHeight,
                    ...Platform.select({
                        web: {
                            outlineWidth: 0,
                            outlineColor: 'transparent',
                            outlineOffset: 0,
                        },
                    }),
                },
            ]} autoFocus={this.props.textInputAutoFocus} value={this.props.text} enablesReturnKeyAutomatically underlineColorAndroid='transparent' keyboardAppearance={this.props.keyboardAppearance} {...this.props.textInputProps}/>
            <View style={{flex: 1}}
                onLayout={(e)=>{
                    this.quoteHeight = e.nativeEvent.layout.height
                    if(this.props.onInputSizeChanged) this.props.onInputSizeChanged(this.contentSize);
                }}
            >
                {this.props.renderQuote ? this.props.renderQuote(this.props) : this.renderQuote()}
            </View>
        </View>)
    }

    renderQuote(){
        if(!this.props.quoteMessage){
            return null
        }
        let text = MSG.formatText(this.props.quoteMessage.text)
        if(this.props.quoteMessage.image) {
            text = '[image]'
        }
        if(this.props.quoteMessage.video) {
            text = '[video]'
        }
        if(this.props.quoteMessage.audio) {
            text = '[audio]'
        }
        let username = this.props.quoteMessage.user.name || ""
        if(verifyAccount(username)){
            username = shortAddress(username)
        }
        return <View style={{minHeight: this.props.minComposerQuoteHeight - 4, marginTop:4, position: 'relative', padding: 4, paddingLeft: 10, paddingRight: 30, borderRadius: 4, backgroundColor: colors.border}}>
                    <Text style={{fontSize: 16}} numberOfLines={1} >{username}: {text}</Text>
                    <TouchableView
                        style={{position: 'absolute', width: 30, height: this.quoteHeight - 4, right: 0, top: 0, justifyContent: 'center', alignItems: 'center'}}
                        onPress={this.props.clearQuote}
                    >
                        <AntDesign name='closecircle' size={14}/>
                    </TouchableView>
                </View>
    }
}

import { TouchableView } from '@app/components/common/touchable-view';
import { chatStore } from '@app/pages/chat/stores/chat';
import API from '@app/services/api';
import { accountStore } from '@app/stores/account';
import { optionStore } from '@app/stores/option';
import colors from '@app/style/colors';
import { IBlnScanMessage } from '@app/types/bln';
import { boundMethod } from 'autobind-decorator';
import { action, computed, observable } from 'mobx';
import { observer } from 'mobx-react';
import React, { cloneElement } from 'react';
import { ActivityIndicator, StyleSheet, View, Linking, Text, StatusBar } from 'react-native';
import { IMessage, MessageTextProps, utils } from 'react-native-gifted-chat';
import PropTypes from 'prop-types';
import { IS_ANDROID } from '@app/config';
import Lightbox from 'react-native-lightbox-v2';
import Image from '@app/components/common/image';
import Video, { RNVideo } from '@app/components/common/video';
// @ts-ignore

const textStyle = {
    fontSize: 16,
    lineHeight: 20,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 10,
};

const obStyles = observable({
    get left() {
        return StyleSheet.create({
            messageText: {
                borderRadius: 3,
                backgroundColor: colors.textInputBG,
                marginBottom: 5,
                opacity: 0.9
            },
            text: {
                color: colors.textDefault,
                ...textStyle
            },
            link: {
                color: colors.textDefault,
                textDecorationLine: 'underline',
            },
            container: {
                justifyContent: 'flex-start'
            }
        })
    },
    get right() {
        return StyleSheet.create({
            messageText: {
                borderRadius: 3,
                backgroundColor: colors.textInputBG,
                marginBottom: 5,
                opacity: 0.9
            },
            text: {
                color: colors.textDefault,
                ...textStyle,
            },
            link: {
                color: colors.textDefault,
                textDecorationLine: 'underline',
            },
            container: {
                justifyContent: 'flex-end'
            }
        })
    },
    get styles() {
        return StyleSheet.create({
            container: {
                flexDirection: 'row'
            },
            imageActive: {
                flex: 1,
                resizeMode: 'contain',
            },
            videoActive: {
                flex: 1
            }
        })
    }
})

@observer
export default class MyMessageQuote extends React.Component<MessageTextProps<IMessage> & {
    position: string
    formatQuote?(text: string): Promise<{[key: string]: any}>
    formatText?(text: string): any
}> {

    static contextTypes = {
        actionSheet: PropTypes.func,
    };

    player: React.RefObject<RNVideo> = React.createRef()

    @observable private quoteData: {[key: string]: any} = {}

    constructor(props: any){
        super(props)
        const { formatQuote, currentMessage } = this.props
        if(formatQuote && currentMessage?.text){
            formatQuote(currentMessage.text).then(this.updateQuoteData)
        }
    }

    @action.bound
    private updateQuoteData(data: any){
        this.quoteData = data
    }

    render(){
        if(!this.quoteData['text'] && !this.quoteData['name']){
            return null
        }
        const { position} = this.props
        const { styles } = obStyles
        const textStyle = {
            left: {color: colors.textMuted}, 
            right: {color: optionStore.darkTheme ? colors.textMuted : colors.textDefault}
        }
        return <View style={styles.container}>
            <Lightbox
                onOpen={()=>{
                    if(IS_ANDROID){
                        StatusBar.setBackgroundColor('#000000')
                    }
                    StatusBar.setHidden(false, 'fade')
                }}
                onClose={()=>{
                    if(IS_ANDROID){
                        StatusBar.setBackgroundColor('transparent')
                    }
                    StatusBar.setHidden(false, 'fade')
                }}
                renderContent={()=>{
                    if(this.quoteData['image']){
                        return this.renderImage()
                    }
                    if(this.quoteData['video']){
                        return this.renderVideo()
                    }
                    if(this.quoteData['audio']){
                        return this.renderAudio()
                    }
                    return this.renderText()
                }}
                >
                <View style={obStyles[position].messageText}>
                        <View style={[
                            obStyles[this.props.position].container,
                            this.props.containerStyle &&
                                this.props.containerStyle[this.props.position],
                        ]}>
                            <Text
                                style={[
                                    obStyles[this.props.position].text,
                                    textStyle[this.props.position],
                                    this.props.textStyle && this.props.textStyle[this.props.position],
                                    this.props.customTextStyle,
                                    {fontSize: 12, lineHeight: 14}
                                ]}
                            >{this.quoteData['name']}: {this.quoteData['text']}</Text>
                    </View>
                </View>
            </Lightbox>
        </View>
    }

    renderText(){
        if(!this.quoteData['text']){
            return null
        }
        return <Text style={{color: colors.white, padding: 15, fontSize: 18, textAlign: 'center'}}>{this.quoteData['text']}</Text>
    }

    renderImage(){
        if(!this.quoteData['image']){
            return null
        }
        const { styles } = obStyles
        return <Image 
            resizeMode="contain" 
            style={[styles.imageActive]}
            source={{ uri: this.quoteData['image'] }}
        />
    }
    renderVideo(){
        if(!this.quoteData['video']){
            return null
        }
        const { styles } = obStyles
        return <Video
                resizeMode="contain"
                ref={this.player}
                style={[styles.videoActive]}
                paused={false}
                source={{uri: this.quoteData['video'] }}
            />
    }
    renderAudio(){
        if(!this.quoteData['audio']){
            return null
        }
        const { styles } = obStyles
        return <View style={styles.container}>
        </View>
    }
}

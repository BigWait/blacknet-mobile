/**
 * emoji
 * @file emoji
 * @module app/components/common/ui/chat/emoji
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React from 'react'
import { StyleProp, StyleSheet, View, ViewStyle } from 'react-native'
import { observer } from 'mobx-react'
import colors from '@app/style/colors'
import EmojiSelector, { Categories, EmojiSelectorProps } from "react-native-emoji-selector";
import { observable } from 'mobx'
import { TouchableView } from '../../common/touchable-view'
import Feather from 'react-native-vector-icons/Feather'
import LinearGradient from 'react-native-linear-gradient';
import { optionStore } from '@app/stores/option'

export const EmojiPicker = observer((props: {
    containerStyle?: StyleProp<ViewStyle>
    acctionStyle?: StyleProp<ViewStyle>
    acctionDelStyle?: StyleProp<ViewStyle>
    acctionSendStyle?: StyleProp<ViewStyle>
    enableAction?: boolean
    onDelPress?(): void
    onSendPress?(): void
} & EmojiSelectorProps): JSX.Element => {
    const { styles } = obStyles
    return (
        <View
        style={[
            {
                position: 'relative',
                flex: 1,
                marginHorizontal: 15
            },
            props.containerStyle
        ]}
        >
            <EmojiSelector
                category={Categories.all}
                showSearchBar={false}
                showTabs={false}
                showHistory={true}
                showSectionTitles={false}
                columns={9}
                {...props}
            />
            <View style={[styles.acctionStyle, props.acctionStyle]}>
                <LinearGradient colors={[optionStore.darkTheme ? 'rgba(59,59,59, 0.1)' : 'rgba(255,255,255, 0.1)', colors.background, colors.background, colors.background]} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} style={styles.linearGradient}>
                    <TouchableView 
                        style={[styles.acctionDelStyle, props.acctionDelStyle, props.enableAction ? {} : {}]}
                        onPress={props.enableAction ? props.onDelPress : undefined}
                        disabled={!props.enableAction}
                    >
                        <Feather name={"delete"} size={24} />
                    </TouchableView>
                    <TouchableView 
                        style={[styles.acctionSendStyle, props.acctionSendStyle, props.enableAction ? {backgroundColor: colors.primary} : {}]}
                        onPress={props.enableAction ? props.onSendPress : undefined}
                        disabled={!props.enableAction}
                    >
                        <Feather name={"send"} size={24} />
                    </TouchableView>
                </LinearGradient>
            </View>
        </View>
    )
})

export default EmojiPicker

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            linearGradient: {
                flexDirection: 'row',
                padding: 15,
                borderTopLeftRadius: 5,
                borderTopRightRadius: 5
            },
            acctionStyle: {
                position: 'absolute',
                zIndex: 1,
                bottom: 0,
                right: 0
            },
            acctionDelStyle: {
                borderRadius: 4,
                padding: 8,
                paddingLeft: 15,
                paddingRight: 15,
                backgroundColor: optionStore.darkTheme ? colors.border : colors.background,
                elevation: 5,
                shadowColor: optionStore.darkTheme ? colors.background : colors.border,
                shadowOpacity: 0.6,
                shadowOffset: { width: 0, height: 5},
            },
            acctionSendStyle: {
                borderRadius: 4,
                marginLeft: 10,
                padding: 8,
                paddingLeft: 15,
                paddingRight: 15,
                backgroundColor: optionStore.darkTheme ? colors.border : colors.background,
                elevation: 5,
                shadowColor: optionStore.darkTheme ? colors.background : colors.border,
                shadowOpacity: 0.6,
                shadowOffset: { width: 0, height: 5},
            }
        })
    }
})


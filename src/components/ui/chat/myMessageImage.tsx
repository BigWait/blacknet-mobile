import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { ActivityIndicator, ImageLoadEventData, NativeSyntheticEvent, StatusBar, StyleSheet, Text, View, } from 'react-native';

import Lightbox from 'react-native-lightbox-v2';
import { IMessage, MessageImageProps, utils } from 'react-native-gifted-chat'
import sizes from '@app/style/sizes';
import colors from '@app/style/colors';
import { observer } from 'mobx-react';
import { action, computed, observable } from 'mobx';
import { boundMethod } from 'autobind-decorator';
import API from '@app/services/api';
import { accountStore } from '@app/stores/account';
import {chatStore} from '@app/pages/chat/stores/chat';
import { IBlnScanMessage } from '@app/types/bln';
import { TouchableView } from '@app/components/common/touchable-view';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { optionStore } from '@app/stores/option';
import Image from '@app/components/common/image';
import { IS_ANDROID } from '@app/config';
import i18n from '@app/services/i18n';
import { LANGUAGE_KEYS } from '@app/constants/language';
import MSG from '@app/utils/msg';
const { StylePropType } = utils
const obStyles = observable({
    get left() {
        return StyleSheet.create({
            container: {
                justifyContent: 'flex-start'
            }
        })
    },
    get right() {
        return StyleSheet.create({
            container: {
                justifyContent: 'flex-end'
            }
        })
    },
    get styles(){
        return StyleSheet.create({
            container: {
                flexDirection: 'row',
                alignItems: 'center'
            },
            imageView: {
                position: 'relative'
            },
            statusView: {
                padding: 10
            },
            image: {
                borderRadius: 3,
                margin: 5,
                marginLeft: 0,
                resizeMode: 'cover'
            },
            imageActive: {
                flex: 1,
                resizeMode: 'contain',
            },
            loadingView: {
                position: 'absolute',
                backgroundColor: colors.background,
                opacity: 0.5,
                flex: 1,
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
            },
            loading: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'
            },
            preloadView: {
                position: 'relative',
                overflow: 'hidden',
                justifyContent: 'center',
                alignItems: 'center'
            },
            preloadImg: {
                position: 'absolute',
                width: sizes.screen.width,
                height: sizes.screen.height,
                zIndex: -1,
                opacity: 0
            },
            loadingText: {
                marginTop: 2,
                color: optionStore.darkTheme ? colors.white : colors.textDefault
            }
        })
    }
})

const DEFAULT_OPTION_TITLES = [i18n.t(LANGUAGE_KEYS.QUOTE), i18n.t(LANGUAGE_KEYS.CANCEL)];

@observer 
export default class MyMessageImage extends Component<MessageImageProps<IMessage> & {
    onQuotePress?(message: IMessage): void
    position: string
    extendObject?(txid: string): any
}> {
    static contextTypes = {
        actionSheet: PropTypes.func,
    };
    static defaultProps = {
        position: 'left'
    };
    render() {
        const { containerStyle, lightboxProps, position } = this.props;
        const { styles } = obStyles
        if (!this.message) return null;
        return (
            <View style={[styles.container, obStyles[position].container ,containerStyle]}>
                {!this.sent && !this.pending  ? 
                    <View style={[styles.statusView]}>
                        <TouchableView
                            onPress={this.reSend}
                        >
                            <FontAwesome5 name={'exclamation-circle'}  size={24} color={colors.transferOut} />
                        </TouchableView>
                    </View>
                : null}
                <View style={[styles.imageView]}>
                    {!this.state.loaded ?
                        <View style={[styles.preloadView, {position: 'relative', width: this.state.imageWidth, height: this.state.imageHeight}]}>
                            <Image 
                                style={styles.preloadImg}
                                source={{ uri: this.message.image }}
                                onLoad={(evt: any)=>{
                                    let height = evt.nativeEvent.height
                                    let width = evt.nativeEvent.width
                                    if(width > this.state.maxWidth){
                                        height = this.state.maxWidth / width * height
                                        width = this.state.maxWidth
                                    }
                                    this.setState({
                                        imageHeight: height,
                                        imageWidth: width,
                                        loaded: true
                                    }, ()=>{
                                        if( this.pending && !this.sent ){
                                            this.animation()
                                            this.upload()
                                        }
                                    })
                                }}
                            />
                            <FontAwesome5 name={'spinner'}  size={45} color={colors.textMuted} />
                        </View>
                    :
                        <Lightbox 
                            activeProps={{
                                style: styles.imageActive,
                            }} 
                            onOpen={()=>{
                                if(IS_ANDROID){
                                    StatusBar.setBackgroundColor('#000000')
                                }
                                StatusBar.setHidden(false, 'fade')
                            }}
                            onClose={()=>{
                                if(IS_ANDROID){
                                    StatusBar.setBackgroundColor('transparent')
                                }
                                StatusBar.setHidden(false, 'fade')
                            }}
                            onLongPress={this.onLongPress}
                            {...lightboxProps}
                        >
                            <Image 
                                resizeMode="contain" 
                                style={[styles.image, {width: this.state.imageWidth, height: this.state.imageHeight}]}
                                source={{ uri: this.message?.image }}
                            />
                        </Lightbox>
                    }
                    {this.state.loaded && !this.sent && this.pending && this.process !== 100 ? 
                        <View style={styles.loadingView}>
                            <View style={styles.loading}>
                                <ActivityIndicator color={optionStore.darkTheme ? colors.white : colors.textDefault}/>
                                <Text style={styles.loadingText}>{this.percent}</Text>
                            </View>
                        </View>
                    : null}
                </View>
        </View>
        );
    }

    @boundMethod
    onLongPress(){
        const { currentMessage, onQuotePress } = this.props;
        if (currentMessage && currentMessage.image) {
            const options = DEFAULT_OPTION_TITLES
            const cancelButtonIndex = options.length - 1;
            this.context.actionSheet().showActionSheetWithOptions({
                options,
                cancelButtonIndex,
            }, (buttonIndex: number) => {
                switch (buttonIndex) {
                    case 0:
                        this.props.onQuotePress && this.props.onQuotePress(currentMessage)
                        break;
                    default:
                        break;
                }
            });
        }
    };
    
    state = {
        imageHeight: 45,
        imageWidth: 45,
        loaded: false,
        maxWidth: sizes.screen.width / 3
    }

    componentDidMount(){
        this.message = this.props.currentMessage
    }

    @observable private process: number = 0
    @observable private message: IMessage | undefined

    @computed
    get sent(){
        return this.message?.sent
    }

    @computed
    get pending(){
        return this.message?.pending
    }

    @boundMethod
    @action
    updateProcess(n: number){
        this.process = n
    }

    @computed
    get percent(){
        return `${this.process}%`
    }

    @boundMethod
    @action
    private reSend(){
        if(this.message){
            this.message.sent = false
            this.message.pending = true
        }
        this.process = 0
        this.animation()
        this.upload()
    }

    @action
    private update(msg: IBlnScanMessage, id: string, success: boolean = false){
        let message = this.message
        if(message){
            message.pending = msg.pending
            message.sent = msg.sent
            message.received = msg.received
        }
        if(this.props.extendObject){
            message = {...message, ...this.props.extendObject(id)}
        }
        chatStore.replace(accountStore.currentAddress, msg.to, id, msg)
        if(success){
            chatStore.upsertsList(accountStore.currentAddress, [msg])
        }
        this.message = message
    }

    @boundMethod
    private upload(){
        const { currentMessage } = this.props;
        const id = (currentMessage?._id || '').toString()
        let info = id.split(':')
        const isGroup = /^true$/i.test(info[2]);
        const address = info[3]
        const isPrivate = /^true$/i.test(info[4]);
        API.upload(currentMessage?.image || '', info[1], id).then((res)=>{
            if(res){
                return `[image: ${res.cid}]`
            }
            return null
        }).then((text)=>{
            if(text){
                let quote = MSG.getQuote(currentMessage?.text || '') || ''
                if(quote){
                    quote = `[txid:${quote}]`
                }
                if(isGroup){
                    return API.sendGroupMessage(
                        accountStore.currentMnemonic,
                        accountStore.currentAddress,
                        address,
                        text,
                        isPrivate,
                        quote
                    )
                }
                return API.sendMessage(
                    accountStore.currentMnemonic,
                    accountStore.currentAddress,
                    address,
                    text,
                    quote
                )
            }
            return null
        }).then((msg)=>{
            if(msg){
                // success
                this.updateProcess(100)
                msg.pending = false
                msg.received = false
                msg.sent = true
                this.update(msg, id, true)
            } else {
                // fail
                return Promise.reject(msg)
            }
        }).catch((e)=>{
            // fail
            let msg = { 
                from: accountStore.currentAddress,
                to: address,
                time: Math.floor(new Date(currentMessage?.createdAt || '').getTime() / 1000).toString(),
                txid: id,
                group: isGroup,
                sent: false,
                received: false,
                pending: false,
                text: `[image: ${currentMessage?.image}]`
            } as IBlnScanMessage
            this.update(msg, id)
        })
        .finally(() => {
            accountStore.refreshBalance()
        })
    }

    animation(){
        const hide = (n: number)=>{
            if(n < this.process){
                return
            }
            if(n < 99){
                requestAnimationFrame(()=>hide(n + 1))   
            }
            this.updateProcess(n)
        }
        return hide(0)
    }
}
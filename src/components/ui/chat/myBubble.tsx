import { LANGUAGE_KEYS } from '@app/constants/language';
import i18n from '@app/services/i18n';
import colors from '@app/style/colors';
import { boundMethod } from 'autobind-decorator';
import { observable } from 'mobx';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Text, StyleSheet, TouchableWithoutFeedback, View, } from 'react-native';
import { utils, Time, MessageImage, MessageText, BubbleProps, IMessage, QuickReplies, RenderMessageTextProps } from 'react-native-gifted-chat'
const { StylePropType, isSameUser, isSameDay } = utils
import Clipboard from '@react-native-community/clipboard'
import sizes from '@app/style/sizes';
import MyMessageImage from './myMessageImage';
import MyMessageVideo from './myMessageVideo';
import MyMessageText from './myMessageText';
import MyMessageQuote from './myMessageQuote';
import MSG from '@app/utils/msg';

const styles = observable({
    get left() {
        return StyleSheet.create({
            container: {
                flex: 1,
                alignItems: 'flex-start',
            },
            wrapper: {
                minHeight: 20,
                marginRight: 60,
                justifyContent: 'flex-end',
            },
            containerToNext: {
                borderBottomLeftRadius: 3,
            },
            containerToPrevious: {
                borderTopLeftRadius: 3,
            },
            bottom: {
                flexDirection: 'row',
                justifyContent: 'flex-start',
            },
            messageText: {
                borderRadius: 3,
                backgroundColor: colors.border,
                marginBottom: 5
            }
        })
    },
    get right() {
        return StyleSheet.create({
            container: {
                flex: 1,
                alignItems: 'flex-end',
            },
            wrapper: {
                minHeight: 20,
                marginLeft: 60,
                justifyContent: 'flex-end',
            },
            containerToNext: {
                borderBottomRightRadius: 3,
            },
            containerToPrevious: {
                borderTopRightRadius: 3,
            },
            bottom: {
                flexDirection: 'row',
                justifyContent: 'flex-end',
            },
            messageText: {
                borderRadius: 3,
                backgroundColor: colors.primary,
                marginBottom: 5
            }
        })
    },
    get content() {
        return StyleSheet.create({
            tick: {
                fontSize: 10,
                backgroundColor: 'transparent',
                color: colors.white,
            },
            tickView: {
                flexDirection: 'row',
                marginRight: 10,
            },
            username: {
                top: 0,
                left: 0,
                maxWidth: sizes.screen.width - 100,
                fontSize: 12,
                backgroundColor: 'transparent',
                color: '#aaa',
            },
            usernameView: {
                flexDirection: 'row',
                marginHorizontal: 0,
                marginBottom: 5,
            }
        })
    }
})

const DEFAULT_OPTION_TITLES = [i18n.t(LANGUAGE_KEYS.QUOTE), i18n.t(LANGUAGE_KEYS.COPY), i18n.t(LANGUAGE_KEYS.CANCEL)];

export default class MyBubble extends PureComponent<BubbleProps<IMessage> & {
    onQuotePress?(message: IMessage): void
    renderMessageQuote?(props: RenderMessageTextProps<IMessage>): React.ReactNode;
    formatQuote?(text: string): Promise<{[key: string]: any}>
    formatText?(text: string): any
    extendObject?(txid: string): any
}> {

    static contextTypes = {
        actionSheet: PropTypes.func,
    };

    @boundMethod
    onLongPress(){
        const { currentMessage } = this.props;
        if (this.props.onLongPress) {
            this.props.onLongPress(this.context, this.props.currentMessage);
        }
        else if (currentMessage && currentMessage.text) {
            const { optionTitles } = this.props;
            const options = optionTitles && optionTitles.length > 0
                ? optionTitles.slice(0, 2)
                : DEFAULT_OPTION_TITLES;
            const cancelButtonIndex = options.length - 1;
            this.context.actionSheet().showActionSheetWithOptions({
                options,
                cancelButtonIndex,
            }, (buttonIndex: number) => {
                switch (buttonIndex) {
                    case 0:
                        this.props.onQuotePress && this.props.onQuotePress(currentMessage)
                        break;
                    case 1:
                        Clipboard.setString(currentMessage.text);
                        break;
                    default:
                        break;
                }
            });
        }
    };
    styledBubbleToNext() {
        const { currentMessage, nextMessage, position, containerToNextStyle, } = this.props;
        if (currentMessage &&
            nextMessage &&
            position &&
            isSameUser(currentMessage, nextMessage) &&
            isSameDay(currentMessage, nextMessage)) {
            return [
                styles[position].containerToNext,
                containerToNextStyle && containerToNextStyle[position],
            ];
        }
        return null;
    }
    styledBubbleToPrevious() {
        const { currentMessage, previousMessage, position, containerToPreviousStyle, } = this.props;
        if (currentMessage &&
            previousMessage &&
            position &&
            isSameUser(currentMessage, previousMessage) &&
            isSameDay(currentMessage, previousMessage)) {
            return [
                styles['left'].containerToPrevious,
                containerToPreviousStyle && containerToPreviousStyle[position],
            ];
        }
        return null;
    }

    renderMessageQuote() {
        const { position } = this.props
        if (this.props.currentMessage && this.props.currentMessage.text ) {
            if(MSG.hasQuote(this.props.currentMessage.text)){
                const { containerStyle, wrapperStyle, optionTitles, ...messageTextProps } = this.props;
                if (this.props.renderMessageQuote) {
                    return this.props.renderMessageQuote(messageTextProps);
                }
                return <MyMessageQuote postion={position} {...messageTextProps}/>
            }
        }
        return null;
    }

    renderMessageText() {
        const { position } = this.props
        if (this.props.currentMessage && this.props.currentMessage.text && MSG.hasText(this.props.currentMessage.text)) {
            const { containerStyle, wrapperStyle, optionTitles, ...messageTextProps } = this.props;
            if (this.props.renderMessageText) {
                return this.props.renderMessageText(messageTextProps);
            }
            return <MyMessageText postion={position} {...messageTextProps} formatText={this.props.formatText} extendObject={this.props.extendObject}/>
        }
        return null;
    }

    renderMessageImage() {
        const { position } = this.props
        if (this.props.currentMessage && this.props.currentMessage.image) {
            const { containerStyle, wrapperStyle, ...messageImageProps } = this.props;
            if (this.props.renderMessageImage) {
                return this.props.renderMessageImage(messageImageProps);
            }
            return <MyMessageImage {...messageImageProps} postion={position} onQuotePress={this.props.onQuotePress} extendObject={this.props.extendObject}/>;
        }
        return null;
    }

    renderMessageVideo() {
        const { position } = this.props
        if (this.props.currentMessage && this.props.currentMessage.video) {
            const { containerStyle, wrapperStyle, ...messageVideoProps } = this.props;
            if (this.props.renderMessageVideo) {
                return this.props.renderMessageVideo(messageVideoProps);
            }
            return <MyMessageVideo {...messageVideoProps} postion={position} onQuotePress={this.props.onQuotePress} extendObject={this.props.extendObject}/>;
        }
        return
    }

    renderTicks() {
        const { currentMessage, renderTicks, user } = this.props;
        if (renderTicks && currentMessage) {
            return renderTicks(currentMessage);
        }
        if (currentMessage &&
            user &&
            currentMessage.user &&
            currentMessage.user._id !== user._id) {
            return null;
        }
        if (currentMessage &&
            (currentMessage.sent || currentMessage.received || currentMessage.pending)) {
            return (<View style={styles.content.tickView}>
          {/* ticks 暂时不要 */}
          {/* {!!currentMessage.sent && (<Text style={[styles.content.tick, this.props.tickStyle]}>✓</Text>)} */}
          {/* {!!currentMessage.received && (<Text style={[styles.content.tick, this.props.tickStyle]}>✓</Text>)} */}
          {/* {!!currentMessage.pending && (<Text style={[styles.content.tick, this.props.tickStyle]}>🕓</Text>)} */}
        </View>);
        }
        return null;
    }
    renderTime() {
        return null;
        // if (this.props.currentMessage && this.props.currentMessage.createdAt) {
        //     const { containerStyle, wrapperStyle, textStyle, ...timeProps } = this.props;
        //     if (this.props.renderTime) {
        //         return this.props.renderTime(timeProps);
        //     }
        //     return <Time {...timeProps}/>;
        // }
        // return null;
    }
    renderUsername() {
        const { currentMessage, user } = this.props;
        if (this.props.renderUsernameOnMessage && currentMessage) {
            if (user && currentMessage.user._id === user._id) {
                return null;
            }
            return (<View style={styles.content.usernameView}>
          <Text style={[styles.content.username, this.props.usernameStyle]}>
            {currentMessage.user.name}
          </Text>
        </View>);
        }
        return null;
    }
    renderCustomView() {
        if (this.props.renderCustomView) {
            return this.props.renderCustomView(this.props);
        }
        return null;
    }
    renderBubbleContent() {
        return this.props.isCustomViewBottom ? (<View>
        {this.renderMessageImage()}
        {this.renderMessageVideo()}
        {this.renderMessageText()}
        {this.renderMessageQuote()}
        {this.renderCustomView()}
      </View>) : (<View>
        {this.renderCustomView()}
        {this.renderMessageImage()}
        {this.renderMessageVideo()}
        {this.renderMessageText()}
        {this.renderMessageQuote()}
      </View>);
    }
    render() {
        const { position, containerStyle, wrapperStyle, bottomContainerStyle, } = this.props;
        return (<View style={[
            styles[position].container,
            containerStyle && containerStyle[position],
        ]}>
        {/* name */}
        {this.renderUsername()}
        {/* content */}
        <View style={[
            styles[position].wrapper,
            this.styledBubbleToNext(),
            this.styledBubbleToPrevious(),
            wrapperStyle && wrapperStyle[position],
        ]}>
            <TouchableWithoutFeedback onLongPress={this.onLongPress} accessibilityTraits='text' {...this.props.touchableProps}>
                <View>
                    {this.renderBubbleContent()}
                    <View style={[
                        styles[position].bottom,
                        bottomContainerStyle && bottomContainerStyle[position],
                    ]}>
                        {this.renderTime()}
                        {this.renderTicks()}
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </View>
      </View>);
    }
}


export const formatMessageText = (message:string) =>{
    const res = message.match(/\[(\w+:\w+)\]/ig)
    const obj: {[key: string]: any} = {}
    if(res && res.length){
        res.forEach((data)=>{
            const [key, value] = data.replace('[', '').replace(']', '').split(":")
            obj[key] = value
        })
    }
    return obj
}
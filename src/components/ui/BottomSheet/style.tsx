import colors from "@app/style/colors";
import { StyleSheet } from "react-native";

export const verticalPadding = 8
export const borderRadius = 16
export const marginTop = 56

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: "flex-end"
    },
    backdrop: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: '#111111',
        opacity: 0.4
    },
    content: {
        borderTopLeftRadius: borderRadius,
        borderTopRightRadius: borderRadius
    },
    contentHeader: {
        height: borderRadius,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center'
    },
    handle: {
        width: 31,
        height: 4,
        backgroundColor: '#666666',
        opacity: 0.4,
        borderRadius: 2
    }
})

export default styles
export const contentContainer = (windowHeight: number)=>{
    return {
        backgroundColor: colors.background,
        borderTopLeftRadius: borderRadius,
        borderTopRightRadius: borderRadius,
        height: windowHeight * 2
    }
}
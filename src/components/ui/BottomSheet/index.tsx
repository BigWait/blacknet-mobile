import React, { memo, useCallback, useEffect, useMemo, useState } from "react";
import PropTypes from "prop-types";
import {
  View,
  Modal,
  Platform,
  useWindowDimensions,
  ModalProps,
  Keyboard,
} from "react-native";
import styles, { borderRadius, contentContainer, marginTop, verticalPadding } from "./style";
import { useKeyboard } from '@react-native-community/hooks';
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { PanGestureHandler, State as GestureHandlerStates, TapGestureHandler } from 'react-native-gesture-handler';
import Animated from "@app/components/animated";
import { IChildrenProps } from "@app/types/props";

const springConfig = {
    damping: 15,
    mass: 0.7,
    stiffness: 150,
    overshootClamping: false,
    restSpeedThreshold: 0.1,
    restDisplacementThreshold: 0.1
}
const bottomSheetHooks = function(props: {
    onCancel?(): void
    disableDrag?: boolean
    showHandle?: boolean
    visible: boolean
    backdropDismiss?: boolean
    backButtonCancel?: boolean
    children?: React.ReactNode | React.ReactNode[]
}){
    let { 
        onCancel = undefined,
        showHandle = true,
        visible = false,
        backdropDismiss = true,
        backButtonCancel = true,
        disableDrag = false
    } = props
    let bodyRef: React.RefObject<any>= React.createRef(),
    masterRef: React.RefObject<any>= React.createRef(),
    [height, setHeight] = useState(0),
    { height: windowHeight } = useWindowDimensions(),
    { keyboardShown, keyboardHeight } = useKeyboard(),
    safeArea = useSafeAreaInsets(),
    minHeight = verticalPadding * 2 + safeArea.bottom,
    maxHeight = windowHeight - safeArea.top - marginTop,
    // [visible, setVisible] = useState(false),
    masterTranslationY = Animated.useValue(0),
    masterVelocityY = Animated.useValue(GestureHandlerStates.UNDETERMINED),
    masterState = Animated.useValue(GestureHandlerStates.UNDETERMINED),
    tapState = Animated.useValue(0),
    manualOpen = Animated.useValue(0),
    manualClose = Animated.useValue(0),
    offset = Animated.useValue(0),
    dragOver = Animated.useValue(1),
    clock = Animated.useClock(),
    tapGestureHandler = Animated.useGesture({state: tapState}),
    onMasterEvent = Animated.useGesture({
        translationY: masterTranslationY,
        state: masterState,
        velocityY: masterVelocityY
    }),
    onBodyEvent = onMasterEvent,
    sheetHeight = Math.min(maxHeight, height + borderRadius),
    openSnapPoint = Animated.useValue(0),
    closeSnapPoint = 0,
    onClose = ()=>{
        console.log('onClose', visible);
        if(visible){
            // setVisible(false)
            visible = !visible
            setHeight(0)
            if(onCancel){
                onCancel()
            }
        }
    },
    closeSheet = ()=>{
        console.log(777777);
        Animated.setValue(manualClose, 1)
    },
    onSnap = (pos:any)=>{
        console.log("1onSnap", pos, closeSnapPoint)
        if(pos[0] === closeSnapPoint){
            onClose()
        }
    },
    interrupted = Animated.and(Animated.eq(masterState, GestureHandlerStates.BEGAN), Animated.clockRunning(clock)),
    translateY = useMemo(()=>{
        console.log("translateY", visible)
        return Animated.withEasing({
            value: Animated.cond(
                Animated.lessOrEq(masterTranslationY, 0),
                Animated.divide(masterTranslationY, 2),
                masterTranslationY
            ),
            velocity: masterVelocityY,
            offset: offset,
            state: masterState,
            animationOver: dragOver,
            snapPoints: [openSnapPoint, closeSnapPoint]
        })
    }, []),
    opacity = useMemo(()=>{
        console.log("opacity", visible)
        return Animated.cond(
            openSnapPoint, 
            Animated.interpolate(
                translateY,
                {
                    inputRange: [Animated.multiply(openSnapPoint, opacityCoeff), 0],
                    outputRange: [1, 0],
                    extrapolate: Animated.Extrapolate.CLAMP
                }
            )
        )
    }, [])

    // Animated.useCode(()=>{
    //     console.log("onSnap", visible)
    //     return Animated.cond(
    //         Animated.and(
    //             Animated.eq(masterState, GestureHandlerStates.END),
    //             Animated.not(dragOver)
    //         ),
    //         Animated.call([translateY], onSnap)
    //     )
    // }, [onSnap])

    // 条件运行动画
    Animated.useCode(()=>{
        return Animated.block([
            Animated.cond(
                Animated.and(interrupted, manualOpen),
                [
                    Animated.call([], ()=>console.log(11111)),
                    Animated.set(manualOpen, 0),
                    Animated.set(offset, openSnapPoint),
                    Animated.stopClock(clock)
                ]
            ),
            Animated.cond(
                Animated.and(manualOpen, Animated.not(manualClose)),
                [
                    Animated.call([], ()=>console.log(200000)),
                    Animated.set(offset, Animated.reSpring({
                        from: offset,
                        to: openSnapPoint,
                        clock: clock,
                        config: springConfig
                    })),
                    Animated.cond(
                        Animated.not(Animated.clockRunning(clock)),
                        Animated.set(manualOpen, 0)
                    ),
                    Animated.cond(
                        manualOpen,
                        Animated.call([], ()=>console.log(21)),
                        Animated.call([], ()=>console.log(23)),
                    )
                ]
            )
        ])
    }, [])

    // 关闭动画
    Animated.useCode(()=>{
        return Animated.block([
            Animated.cond(
                Animated.and(interrupted, manualClose),
                [
                    Animated.call([], ()=>console.log(300000)),
                    Animated.set(manualClose, 0),
                    Animated.set(offset, closeSnapPoint),
                    Animated.call([], onClose),
                    Animated.stopClock(clock)
                ]
            ),
            Animated.cond(
                Animated.eq(tapState, GestureHandlerStates.END),
                [
                    Animated.call([], ()=>console.log(31)),
                    Animated.cond(
                        Animated.not(manualClose),
                        [
                            Animated.call([], ()=>console.log(32)),
                            Animated.stopClock(clock),
                            Animated.set(manualClose, 1)
                        ]
                    ),
                    Animated.set(tapState, GestureHandlerStates.UNDETERMINED)
                ]
            ),
            Animated.cond(
                manualClose,
                [
                    Animated.call([], ()=>console.log(33)),
                    Animated.set(offset, Animated.reTiming({
                        from: offset,
                        to: closeSnapPoint,
                        clock: clock,
                        easing: Animated.easings.easeOut,
                        duration: closeDuration
                    })),
                    Animated.cond(
                        Animated.not(Animated.clockRunning(clock)),
                        [
                            Animated.call([], ()=>console.log(34)),
                            Animated.set(manualClose, 0),
                            Animated.set(manualOpen, 0),
                            Animated.call([], onClose)
                        ]
                    )
                ]
            )
        ])
    }, [onCancel])

    // 监听height和visible 
    Animated.useCode(()=>{
        console.log("height visible", height, visible)
        return Animated.cond(
            Animated.and(
                Animated.not(manualClose),
                visible ? 1 : 0,
                height > minHeight ? 1 : 0
            ),
            [
                Animated.call([], ()=>console.log(123456)),
                Animated.stopClock(clock),
                Animated.set(openSnapPoint, sheetHeight * -1),
                Animated.set(manualOpen, 1)
            ]
        )
    }, [height, visible])

    // useCallback(()=>{
    //     console.log("useCallback", visible)
    //     if(backButtonCancel){
    //         closeSheet()
    //     }
    // }, [visible])
    
    useEffect(()=>{
        console.log("default", visible)
        // if(props.visible){
        //     Keyboard.dismiss()
        // } else {
        //     if(visible){
        //         closeSheet()
        //     }
        // }
    }, [visible])

    return (
        <CustomModal
            transparent={true}
            visible={visible}
            hardwareAccelerated={true}
            presentationStyle={"overFullScreen"}
            onRequestClose={() => {
                if(backButtonCancel){
                    closeSheet()
                }
            }}
        >
            <View 
                style={styles.container}
                pointerEvents={'box-none'}
            >
                <TapGestureHandler enabled={backdropDismiss} {...tapGestureHandler} >
                    <Animated.View
                        style={[styles.backdrop, {opacity: opacity}]}
                    >
                    </Animated.View>
                </TapGestureHandler>
                <Animated.View
                    style={[contentContainer(windowHeight), {
                        transform: [
                            {translateY: translateY},
                            {translateY: windowHeight * 2}
                        ]
                    }]}
                >
                    <PanGestureHandler {...onMasterEvent} ref={masterRef} waitFor={bodyRef} enabled={!!disableDrag}>
                        <Animated.View style={styles.contentHeader}>
                            {showHandle ?
                                <Animated.View  style={styles.handle}>
                                </Animated.View>
                            : null}
                        </Animated.View>
                    </PanGestureHandler>
                    <PanGestureHandler {...onBodyEvent} ref={bodyRef} waitFor={masterRef} 
                        enabled={!!disableDrag && sheetHeight != maxHeight}
                    >
                        <Animated.View style={{height: sheetHeight}}>
                            <Animated.ScrollView bounces={false}
                                scrollEnabled={sheetHeight === maxHeight}
                            >
                                <Animated.View  
                                    style={{
                                        paddingTop: verticalPadding,
                                        paddingBottom: verticalPadding + (Object.is(Platform.OS, 'ios') && keyboardShown ? keyboardHeight : safeArea.bottom)
                                    }}
                                    onLayout={(event)=>{
                                        setHeight(event.nativeEvent.layout.height)
                                    }}
                                >
                                    {props.children}
                                </Animated.View>
                            </Animated.ScrollView>
                        </Animated.View>
                    </PanGestureHandler>
                </Animated.View>
            </View>
        </CustomModal>
    )
}

bottomSheetHooks.defaultProps = {
    showHandle: true
}

const BottomSheet = memo(bottomSheetHooks)
const opacityCoeff = 0.8
const closeDuration = 150
const CustomModal = (props: ModalProps & IChildrenProps): JSX.Element => {
    if(Object.is(Platform.OS, 'ios')){
        return <Modal {...props}>{props.children}</Modal>
    } else {
        return <View 
                style={styles.container}
                pointerEvents={props.visible ? "box-none" : "none"}
               >
                   {props.visible && props.children}
               </View>
    }    
}

export default BottomSheet;
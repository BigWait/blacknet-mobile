import React, { Component } from "react";
import { StyleSheet, View, Text, ViewStyle, StyleProp, Platform } from 'react-native'
import colors from '@app/style/colors'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { TouchableView } from '@app/components/common/touchable-view'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { Text as RNText, TextProps } from 'react-native'
import { observer } from 'mobx-react'
import { IChildrenProps, Navigation } from '@app/types/props'
import fonts from '@app/style/fonts'
import { IBlnScanForumPost } from "@app/types/bln";
import { showTimeFromNow } from "@app/utils/bln";
import { userStore } from "@app/stores/users";
import { action, computed, observable } from "mobx";
import { TextLink } from "../common/text";
import API from "@app/services/api";
import { HomeRoutes } from "@app/routes";
import { Image } from "react-native";

export const PostListName = observer((props: TextProps & IChildrenProps): JSX.Element => {
    return (
        <RNText
            numberOfLines={1}
            {...props}
            style={[
                {
                    color: colors.textLink,
                    fontFamily: fonts.fontFamily
                },
                props.style
            ]}
        >
            {props.children}
        </RNText>
    )
})

export const PostListTitle = observer((props: TextProps & IChildrenProps): JSX.Element => {
    return (
        <RNText
            {...props}
            style={[
                {
                    color: colors.textTitle,
                    fontWeight: "bold",
                    fontFamily: fonts.fontFamily,
                    overflow: 'hidden'
                },
                props.style
            ]}
        >
            {props.children}
        </RNText>
    )
})

export const PostListSubtitle = observer((props: TextProps & IChildrenProps): JSX.Element => {
    return (
        <RNText
            numberOfLines={5}
            {...props}
            style={[
                {
                    color: colors.textDefault,
                    fontFamily: fonts.fontFamily
                },
                props.style
            ]}
        >
            {props.children}
        </RNText>
    )
})

export const PostListTime = observer((props: TextProps & IChildrenProps): JSX.Element => {
    return (
        <RNText
            {...props}
            style={[
                {
                    color: '#7997C7',
                    fontFamily: fonts.fontFamily
                },
                props.style
            ]}
        >
            {props.children}
        </RNText>
    )
})


export interface PostProps {
    data: IBlnScanForumPost
    floor?: number
    style?: StyleProp<ViewStyle>
    onPress?(): void
    onBottomPress?(): void
    onNamePress?(): void
    onReplyPress?(): void
    onCommentReplyPress?(post: IBlnScanForumPost): void
}

@observer
export class PostList extends Component<PostProps> {

    constructor(props: PostProps) {
        super(props)
    }

    render() {
        const { data } = this.props
        const { styles } = obStyles
        return (
            <View style={[
                styles.quanziContainer,
                this.props.style
            ]}>
                <View style={styles.quanziHeader}>

                    <TouchableView
                        onPress={this.props.onNamePress}
                    >
                        <View style={styles.quanziHeaderLeft}>
                            <Image style={styles.avatar} source={{ uri: userStore.getUserImage(data.from) }} />
                        </View>
                    </TouchableView>

                    <TouchableView
                        onPress={this.props.onPress}
                    >
                        <View style={styles.quanziName}>
                            <PostListName>{userStore.getUserName(data.from)}</PostListName>
                        </View>
                        <View style={styles.quanziContent}>
                            <PostListSubtitle>{data.data.content}</PostListSubtitle>
                        </View>
                    </TouchableView>

                </View>
                <TouchableView
                    onPress={this.props.onPress}
                >
                    <View style={styles.quanziTime}>
                        <View style={styles.timing}>
                            <PostListTime>{showTimeFromNow(data.data.lastreply)}</PostListTime>
                        </View>
                        <FontAwesome5 style={styles.trailing} name="paper-plane" color={colors.textDefault} />
                    </View>
                </TouchableView>
            </View>
        );
    }
}

@observer
export class PostListDetail extends Component<PostProps> {

    constructor(props: PostProps) {
        super(props)
    }

    render() {
        const { data } = this.props
        const { styles } = obStyles
        return (
            <View style={[
                styles.postListContainer,
                this.props.style
            ]}>
                <View style={[styles.postListHeader]}>
                    <View style={styles.postListHeaderLeft}>
                        <TouchableView
                            onPress={this.props.onReplyPress}
                        >
                            <Text style={{ fontWeight: 'bold', color: colors.textDefault }}>#1</Text>
                        </TouchableView>
                    </View>
                    <View style={styles.postListHeaderRight}>
                        <PostListTime>{data.time && showTimeFromNow(data.time)}</PostListTime>
                    </View>
                </View>
                <View>
                    <View style={[{
                        paddingBottom: 0,
                        //paddingTop:1 
                    }]}>
                        <PostListTitle style={{ fontSize: 18 }}>{data.data.title}</PostListTitle>
                    </View>
                    <View style={styles.postListContent}>
                        <Text
                            style={{ fontSize: 14, lineHeight: 20, color: colors.textDefault }
                            }
                            selectable={true}
                        >
                            {data.data.content}
                        </Text>
                    </View>
                </View>
                <View style={styles.postListFooter}>
                    <View
                        style={{ flex: 1 }}
                    >
                        <TouchableView
                            onPress={this.props.onNamePress}
                        >
                            <PostListName>{userStore.getUserName(data.from)}</PostListName>
                        </TouchableView>
                    </View>
                    <View
                        style={{ minWidth: 60, flexDirection: 'row', justifyContent: "flex-end" }}
                    >
                        <TouchableView
                            onPress={this.props.onReplyPress}
                        >
                            <PostListName>{i18n.t(LANGUAGE_KEYS.FORUM_REPLY)}</PostListName>
                        </TouchableView>
                    </View>
                </View>
            </View>
        );
    }
}

@observer
export class PostListReply extends Component<PostProps & { navigation?: Navigation }> {

    @observable.shallow private comments: IBlnScanForumPost[] = []
    @observable private maps: Map<string, string> = new Map()

    @action.bound
    private updateCommets(comments: IBlnScanForumPost[]) {
        comments.forEach((c) => {
            this.maps.set(c.txid, c.from)
        })
        this.comments = comments
    }

    @computed
    private get CommentLen() {
        return this.comments.length
    }

    componentDidMount() {
        // set txid address map
        this.maps.set(this.props.data.txid, this.props.data.from)
        // fetch commets
        if (this.props.data.data.hasComment) {
            API.fetchPostComment(this.props.data.txid).then((comments) => this.updateCommets(comments))
        }
    }

    render() {
        const { data } = this.props
        const { styles } = obStyles
        return (
            <View style={[
                styles.postListContainer,
                this.props.style
            ]}>
                <View style={[styles.postListHeader, styles.postListHeaderReply]}>
                    <View style={styles.postListHeaderLeft}>
                        <TouchableView
                            onPress={this.props.onReplyPress}
                        >
                            <Text style={{ color: colors.textSecondary }}>#{this.props.floor}</Text>
                        </TouchableView>
                    </View>
                    <View style={styles.postListHeaderRight}>
                        <PostListTime>{showTimeFromNow(data.time)}</PostListTime>
                    </View>
                </View>
                <View style={styles.postListContent}>
                    <Text
                        style={{ fontSize: 14, lineHeight: 24, color: colors.textDefault }}
                        selectable={true}
                    >
                        {data.data.content}
                    </Text>
                </View>
                <View style={styles.postListFooterReply}>
                    <View
                        style={{ flex: 1 }}
                    >
                        <TouchableView
                            onPress={this.props.onNamePress}
                        >
                            <PostListName>{userStore.getUserName(data.from)}</PostListName>
                        </TouchableView>
                    </View>
                    <View
                        style={{ minWidth: 60, flexDirection: 'row', justifyContent: "flex-end" }}
                    >
                        <TouchableView
                            onPress={this.props.onReplyPress}
                        >
                            <PostListName>{i18n.t(LANGUAGE_KEYS.FORUM_REPLY)}</PostListName>
                        </TouchableView>
                    </View>
                </View>
                {this.renderComments()}
            </View>
        );
    }

    renderComments() {
        if (!this.CommentLen) {
            return null
        }
        const { styles } = obStyles
        return <View style={styles.postListComments}>
            {this.comments.slice().map((tx, key) => {
                return <PostListReplyComment
                    key={key}
                    txMap={this.maps}
                    data={tx}
                    navigation={this.props.navigation}
                    onCommentReplyPress={this.props.onCommentReplyPress}
                />
            })}
        </View>
    }
}

@observer
export class PostListReplyComment extends Component<PostProps & { txMap: Map<string, string>, navigation?: Navigation }> {
    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.postListCommentsView, { paddingTop: 2, paddingBottom: 2 }]}
                onPress={() => {
                    this.props.onCommentReplyPress && this.props.onCommentReplyPress(this.props.data)
                }}
            >
                {this.renderName()}
                {this.renderReply()}
                {this.renderReplyName()}
                {this.renderContent()}
            </TouchableView>
        );
    }
    renderName() {
        const { styles } = obStyles
        return <TouchableView
            onPress={() => {
                this.props.navigation && this.props.navigation.navigate(HomeRoutes.ExplorerDetail, {
                    type: "account",
                    id: this.props.data.from
                })
            }}
            style={[styles.postListCommentsView, {}]}
        >
            {/* {`${userStore.getUserName(this.props.data.from)}${this.props.data.data.quotereplyid ? '' : ': '}`.split("").map(t => <TextLink>{t}</TextLink>)} */}
            <TextLink style={{ flexShrink: 1 }}>{`${userStore.getUserName(this.props.data.from)}${this.props.data.data.quotereplyid ? '' : ': '}`}</TextLink>
        </TouchableView>
    }
    renderReplyName() {
        const { data } = this.props
        if (!data.data.quotereplyid) {
            return null
        }
        const { styles } = obStyles
        const { txMap } = this.props
        const name = userStore.getUserName(txMap.get(data.data.quotereplyid)) || data.data.quotereplyid
        return <TouchableView
            onPress={() => {
                const address = txMap.get(data.data.quotereplyid || "")
                this.props.navigation && address && this.props.navigation.navigate(HomeRoutes.ExplorerDetail, {
                    type: "account",
                    id: address
                })
            }}
            style={[styles.postListCommentsView]}
        >
            {/* {`${name}: `.split("").map(t => <TextLink>{t}</TextLink>)} */}
            {<TextLink style={{ flexShrink: 1 }}>{`${name}: `}</TextLink>}
        </TouchableView>
    }
    renderReply() {
        if (!this.props.data.data.quotereplyid) {
            return null
        }
        const { styles } = obStyles
        return (
            <View style={[styles.postListCommentsView, { paddingRight: 4, paddingLeft: 4 }]}>
                {/* {i18n.t(LANGUAGE_KEYS.FORUM_REPLY).split('').map(t => <Text>{t}</Text>)} */}
                <Text style={{ flexShrink: 1 }}>{i18n.t(LANGUAGE_KEYS.FORUM_REPLY)}</Text>
            </View>
        )
    }
    renderContent() {
        return <Text style={{ flexShrink: 1 }}>{this.props.data.data.content}</Text>
        // return this.props.data.data.content.split('').map((t)=>{
        //     return <Text style={{color: colors.textDefault}}>{t}</Text>
        // })
    }
}

export interface TransactionsProcessingProps {
    data?: number
    style?: StyleProp<ViewStyle>
    onPress?(): void
}
@observer
export class TransactionsProcessing extends Component<TransactionsProcessingProps> {

    constructor(props: TransactionsProcessingProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        if (!this.props.data) {
            return null
        }
        return <TouchableView
            onPress={this.props.onPress}
        >
            <View style={styles.transactionProcessing}>
                <Text>{this.props.data}</Text>
                <Text style={{ paddingLeft: 4, paddingRight: 4 }}>{i18n.t(LANGUAGE_KEYS.TX_TRANSACTIONS)}</Text>
                <Text>{i18n.t(LANGUAGE_KEYS.TX_WAITING_CONFIRMED)}</Text>
            </View>
        </TouchableView>
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            postListCommentsView: {
                flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center'
            },
            postListComments: {
                backgroundColor: colors.border,
                padding: 8,
                marginTop: 4,
                marginBottom: 8
            },
            transactionProcessing: {
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                height: 30
            },
            postListContainer: {
                backgroundColor: colors.cardBackground,
                padding: 10,
                flex: 1,
                marginHorizontal: Platform.OS == 'android' ? 10 : 0,
                borderColor: colors.border,
                borderWidth: 1
            },
            postListHeader: {
                paddingBottom: 15,
                borderBottomColor: colors.border,
                borderBottomWidth: 1,
                flexDirection: "row",
                justifyContent: 'space-between',
            },
            postListHeaderLeft: {
                flex: 1
            },
            postListHeaderRight: {

            },
            postListContent: {
                paddingTop: 10,
                paddingBottom: 10,
                lineHeight: 20
            },
            postListFooter: {
                borderTopColor: colors.border,
                borderTopWidth: 0,
                paddingTop: 10,
                flexDirection: "row",
                justifyContent: 'space-between'
            },
            postListFooterReply: {
                borderBottomColor: colors.border,
                borderBottomWidth: 0,
                paddingTop: 10,
                paddingBottom: 10,
                flexDirection: "row",
                justifyContent: 'space-between'
            },
            postListHeaderReply: {
                borderBottomWidth: 0
            }
            ,
            avatar: {
                height: 45,
                width: 45,
                marginLeft: -10,
            },
            quanziName: {
                marginLeft: 15,
            },
            quanziContainer: {
                backgroundColor: colors.cardBackground,
                padding: 5,
                flex: 1,
                marginHorizontal: Platform.OS == 'android' ? 10 : 0,
                borderBottomColor: colors.border,
                borderBottomWidth: 0.5,
            },
            quanziHeader: {
                paddingBottom: 5,
                flexDirection: "row",
            },
            quanziContent: {
                paddingTop: 10,
                paddingBottom: 10,
                padding: 10,
                paddingRight: 50,
                marginLeft: 5,
            },
            quanziHeaderLeft: {
                flex: 1,
            },
            quanziTime: {
                padding: 5,
                marginTop: 15,
                flexDirection: "row",
                justifyContent: 'space-between'
            },
            timing: {
                marginLeft: 40,
            },
            trailing: {
                fontSize: 16,
                marginRight: 10
            }
        })
    }
})


import React, { Component } from "react";
import { StyleSheet, Text } from 'react-native'
import { TouchableView } from '@app/components/common/touchable-view'
import { observer } from "mobx-react";
import { observable } from "mobx";

export interface BLNBlockProps {
    onPress?(): void
}

@observer
export default class BLNBlock extends Component<BLNBlockProps> {

    constructor(props: any) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return <TouchableView onPress={this.props.onPress}>
            <Text style={styles.address}>{this.props.children}</Text>
        </TouchableView>;
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            address: {
                color: '#3CA2A2',
                fontSize: 16,
                textAlign: "right"
            }
        })
    }
})








import React, { Component } from "react";
import { StyleSheet, StyleProp, TextStyle } from 'react-native'
import { TouchableView } from '@app/components/common/touchable-view'
import { TextLink } from '@app/components/common/text'
import colors from "@app/style/colors";
import { observer } from "mobx-react";
import { observable } from "mobx";

export interface BLNAddressProps {
    onPress?(): void
    style?: StyleProp<TextStyle>
}

@observer
export default class BLNAddress extends Component<BLNAddressProps> {

    constructor(props: any) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return this.props.onPress ?
            <TouchableView onPress={this.props.onPress}>
                <TextLink style={[styles.address, this.props.style]}>{this.props.children}</TextLink>
            </TouchableView>
            : <TextLink style={[styles.address, this.props.style]}>{this.props.children}</TextLink>;
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            address: {
                fontSize: 16,
                color: colors.textLink
            }
        })
    }
})






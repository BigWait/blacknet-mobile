import { observer } from 'mobx-react'
import { BlnTxType, IBlnTransaction } from "@app/types/bln";
import { formatChatContent, formatGroupChatContent, formatPostTx, showBalance, showContactName } from "@app/utils/bln";
import colors from '@app/style/colors';
import React from "react";
import { StyleSheet, View, Text, ViewStyle, StyleProp } from 'react-native'
import i18n from '@app/services/i18n';
import { LANGUAGE_KEYS } from '@app/constants/language';
import { TouchableView } from '../common/touchable-view';
import { accountStore } from '@app/stores/account';
import BigNumber from 'bignumber.js';
import { userStore } from '@app/stores/users';
import { observable } from 'mobx';
import { stashStore } from '@app/stores/stash';

export interface StashListItemProps {
    data: IBlnTransaction
    style?: StyleProp<ViewStyle>
    status?: boolean
    onPress?(): void
}

export const StashListItem = observer((props: StashListItemProps): JSX.Element => {
    let result;
    let tx = props.data;
    if (txIsCancelLease(tx)) {
        result = renderFor(tx)
    }
    if (txIsProfile(tx)) {
        result = renderProfile(tx)
    }
    if (txIsChat(tx)) {
        result = renderChat(tx)
    }
    if (txIsPost(tx)) {
        result = renderPost(tx)
    }
    if (txIsReplyPost(tx)) {
        result = renderReplyPost(tx)
    }
    if (txIsContact(tx)) {
        result = renderContact(tx)
    }
    if (txIsTransfer(tx) || txIsLease(tx)) {
        result = renderTo(tx)
    }
    if (!result) {
        return <></>
    }
    const { styles } = obStyles
    return (
        <TouchableView
            onPress={props.onPress}
        >
            <View
                // {...props}
                style={[
                    styles.itemContainer,
                    props.style
                ]}
            >
                {props.status == false ? null :
                    <View style={styles.status}>
                        {statusText(props.data)}
                    </View>
                }
                {result}
            </View>
        </TouchableView>
    )
})

const statusText = (tx: IBlnTransaction): JSX.Element => {
    const { styles } = obStyles

    if (stashStore.status(tx) == 'pending') {

        return <Text style={styles.pending}>Pending</Text>
    }

    if (stashStore.status(tx) == 'success') {

        return <Text style={styles.success}>Confirmed</Text>
    }


    return <Text style={styles.failed}>FAILED</Text>


}

const renderTo = (tx: IBlnTransaction): JSX.Element => {
    const { styles } = obStyles
    return (<>
        <View style={styles.itemType}>
            <Text style={styles.itemTextType}>{showType(tx)}</Text>
            <Text style={styles.message}>{showBalance(tx.amount)} {accountStore.blnSymbol}</Text>
            <Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TX_TO)}</Text>
            <Text style={styles.itemText}>{userStore.getUserName(tx.to)}</Text>
        </View>
    </>)
}

const renderFor = (tx: IBlnTransaction): JSX.Element => {
    const { styles } = obStyles
    return (<>
        <View style={styles.itemType}>
            <Text style={styles.itemTextType}>{showType(tx)}</Text>
            <Text style={styles.message}>{showBalance(tx.amount)} {accountStore.blnSymbol}</Text>
            <Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TX_FOR)}</Text>
            <Text style={styles.itemText}>{userStore.getUserName(tx.to)}</Text>
        </View>

    </>)
}

const renderProfile = (tx: IBlnTransaction): JSX.Element => {
    const { styles } = obStyles
    return (<>
        <View style={styles.itemType}>
            <Text style={styles.itemTextType}>{showType(tx)}</Text>
            <Text style={styles.message}>{showBalance(tx.amount)} {accountStore.blnSymbol}</Text>
        </View>
    </>)
}

const renderChat = (tx: IBlnTransaction): JSX.Element => {
    let text = tx.message, address = accountStore.currentAddress
    let mymessage = tx.to == address || tx.from == address;
    if (text) {
        if (txIsGroupChat(tx)) {
            text = formatGroupChatContent(text)
        } else if (mymessage) {
            if (tx.to == address) {
                text = formatChatContent(accountStore.currentMnemonic, tx.from || "", text)
            } else {
                text = formatChatContent(accountStore.currentMnemonic, tx.to || "", text)
            }
        } else {
            text = 'encrypted message'
        }
    }
    const { styles } = obStyles
    return (<>
        <View style={styles.itemType}>
            <Text style={styles.itemTextType}>{showType(tx)}</Text>
            <Text style={styles.message}>{text} <Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TX_TO)} {userStore.getUserName(tx.to)}</Text></Text>

        </View>
    </>)
}

const renderContact = (tx: IBlnTransaction): JSX.Element => {
    const { styles } = obStyles
    return (<>
        <View style={styles.itemType}>
            <Text style={styles.itemTextType}>{showType(tx)}</Text>
            <Text style={styles.message}>{showContactName(accountStore.currentMnemonic, accountStore.currentAddress, tx.message || "")}</Text>
            <Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TX_FOR)}</Text>
            <Text style={styles.itemText}>{userStore.getUserName(tx.to)}</Text>
        </View>
    </>)
}

const renderPost = (tx: IBlnTransaction): JSX.Element => {
    const postData = formatPostTx(tx.message || "");
    const { styles } = obStyles
    return (<>
        <View style={styles.itemType}>
            <Text style={styles.itemTextType}>{showType(tx)}</Text>
            <Text style={styles.message}>{postData.title || postData.content}</Text>
            <Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TX_FROM)}</Text>
        </View>
        <Text style={styles.itemText}>{userStore.getUserName(tx.to)}</Text>
    </>)
}

const renderReplyPost = (tx: IBlnTransaction): JSX.Element => {
    const postData = formatPostTx(tx.message || "");
    const { styles } = obStyles
    return (<>
        <View style={styles.itemType}>
            <Text style={styles.itemTextType}>{showType(tx)}</Text>
            <Text style={styles.message}>{postData.content}</Text>
            <Text style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TX_TO)}</Text>
        </View>
        <Text style={styles.itemText}>{postData.replyid}</Text>
    </>)
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            itemContainer: {
                padding: 15,
                paddingTop: 10,
                paddingBottom: 10,
                paddingRight: 130,
                borderBottomWidth: 1,
                borderBottomColor: colors.border,
                position: 'relative'
            },
            status: {
                position: 'absolute',
                right: 4,
                top: 5,
            },
            success: {
                color: '#fff',
                backgroundColor: colors.transferIn,
                width: 77,
                textAlign: 'center',
                padding: 3,
                fontSize: 12,
            },
            pending: {
                color: '#fff',
                backgroundColor: colors.textDefault,
                width: 77,
                textAlign: 'center',
                padding: 3,
                fontSize: 12,
            },
            failed: {
                color: '#fff',
                backgroundColor: colors.transferOut,
                width: 77,
                textAlign: 'center',
                padding: 3,
                fontSize: 12,
            },
            itemType: {
                flexDirection: "row",
                alignItems: "center",
            },
            message: {
                color: colors.primary,
                fontSize: 15,
                marginHorizontal: 10,
            },
            itemText: {
                lineHeight: 20,
                color: colors.textDefault,
            },
            itemTextType: {
                lineHeight: 20,
                // backgroundColor: colors.sbgcolor,
                borderColor: colors.textDefault,
                borderWidth: 1,
                color: colors.textDefault,
                borderRadius: 3,
                paddingLeft: 4,
                paddingRight: 4,
                fontSize: 12
            }
        })
    }
})

export const showType = (tx: IBlnTransaction): string => {
    if (txIsProfile(tx)) {
        return i18n.t(LANGUAGE_KEYS.STASH_OP_SETPROFILE);
    }
    if (txIsChat(tx)) {
        return i18n.t(LANGUAGE_KEYS.STASH_OP_CHAT);
    }
    if (txIsPost(tx)) {
        return i18n.t(LANGUAGE_KEYS.STASH_OP_POST);
    }
    if (txIsReplyPost(tx)) {
        return i18n.t(LANGUAGE_KEYS.STASH_OP_REPLY_POST);
    }
    if (txIsContact(tx)) {
        return i18n.t(LANGUAGE_KEYS.STASH_OP_CONTACT);
    }
    if (txIsTransfer(tx)) {
        return i18n.t(LANGUAGE_KEYS.TX_TRANSFER);
    }
    if (txIsLease(tx)) {
        return i18n.t(LANGUAGE_KEYS.TX_LEASE_OUT);
    }
    if (txIsCancelLease(tx)) {
        return i18n.t(LANGUAGE_KEYS.TX_CANCEL_LEASE)
    }
    return ""
}

export const txIsTransfer = (tx: IBlnTransaction): boolean => {
    if (tx.type === BlnTxType.Transfer && !/^(chat|profile|post|new contact): /i.test(tx.message || "")) {
        return true
    }
    return false
}
export const txIsLease = (tx: IBlnTransaction): boolean => {
    if (tx.type === BlnTxType.Lease) {
        return true
    }
    return false
}
export const txIsCancelLease = (tx: IBlnTransaction): boolean => {
    if (tx.type === BlnTxType.CancelLease) {
        return true
    }
    return false
}
export const txIsProfile = (tx: IBlnTransaction): boolean => {
    if (tx.type === BlnTxType.Transfer && /^(profile): /i.test(tx.message || "") && tx.amount == new BigNumber("0.1").multipliedBy(1e8).toString()) {
        return true
    }
    return false
}
export const txIsChat = (tx: IBlnTransaction): boolean => {
    if (tx.type === BlnTxType.Transfer && /^(chat): /i.test(tx.message || "") && tx.amount == new BigNumber("0.005").multipliedBy(1e8).toString()) {
        return true
    }
    if (tx.type === BlnTxType.Transfer && /^(chat): /i.test(tx.message || "") && tx.amount == new BigNumber("0.01").multipliedBy(1e8).toString()) {
        return true
    }
    return false
}
export const txIsGroupChat = (tx: IBlnTransaction): boolean => {
    if (tx.type === BlnTxType.Transfer && /^(chat): /i.test(tx.message || "") && tx.amount == new BigNumber("0.01").multipliedBy(1e8).toString()) {
        return true
    }
    return false
}
export const txIsPost = (tx: IBlnTransaction): boolean => {
    if (tx.type === BlnTxType.Transfer && /^(post): /i.test(tx.message || "") && tx.amount == new BigNumber("0.03").multipliedBy(1e8).toString()) {
        return true
    }
    return false
}
export const txIsReplyPost = (tx: IBlnTransaction): boolean => {
    if (tx.type === BlnTxType.Transfer && /^(post): /i.test(tx.message || "") && tx.amount == new BigNumber("0.0031").multipliedBy(1e8).toString()) {
        return true
    }
    return false
}
export const txIsContact = (tx: IBlnTransaction): boolean => {
    if (tx.type === BlnTxType.Transfer && /^(new contact): /i.test(tx.message || "") && tx.amount == new BigNumber("0.1").multipliedBy(1e8).toString()) {
        return true
    }
    return false
}
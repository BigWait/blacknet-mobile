import React, { Component } from "react";
import { StyleProp, StyleSheet, Text, TextStyle, ViewProps, ViewStyle } from 'react-native'
import colors from '@app/style/colors'
import { TouchableView } from '@app/components/common/touchable-view'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { observer } from "mobx-react";
import { observable } from "mobx";
 
export interface ButtonProps {
    text?: String
    onPress?(): void
    style?: StyleProp<ViewStyle>
    textStyle?: StyleProp<TextStyle>
}

@observer
export default class Button extends Component<ButtonProps> {

    constructor(props: ButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.button, this.props.style]}
                onPress={this.props.onPress}
            >
                <Text style={[styles.bottomButtonText, this.props.textStyle]}>{this.props.text}</Text>
            </TouchableView>
        );
    }
}

@observer
export class DeleteButton extends Component<ButtonProps> {

    constructor(props: ButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
            style={styles.deleteButton}
            onPress={this.props.onPress}
            >
                <Text style={styles.deleteButtonText}>{this.props.text}</Text>
            </TouchableView>
        );
    }
}

@observer
export class CopyButton extends Component<ButtonProps> {

    constructor(props: ButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
            style={styles.copyButton}
            onPress={this.props.onPress}
            >
                <Text style={styles.copyButtonText}>{this.props.text}</Text>
            </TouchableView>
        );
    }
}


export interface AddButtonProps {
    style?: StyleProp<ViewStyle>
    size?: number
}
@observer
export class AddButton extends Component<AddButtonProps & ButtonProps> {

    constructor(props: ButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.addButton, this.props.style]}
                onPress={this.props.onPress}
            >
                <FontAwesome5 name={"comment-dots"} size={this.props.size} style={styles.addButtonIcon}></FontAwesome5>
            </TouchableView>
        );
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            addButton: {
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: colors.deep,
            },
            addButtonIcon: {
        
            },
            copyButton: {
                height: 40,
                borderRadius: 2,
                flexDirection:"column",
                alignItems: "center",
                backgroundColor: colors.deep,
                justifyContent: "center"
            },
            copyButtonText: {
                lineHeight: 40,
                color: colors.pure
            },
            deleteButton: {
                height: 40,
                backgroundColor: colors.red,
                borderRadius: 2,
                flexDirection:"column",
                alignItems: "center",
                justifyContent: "center"
            },
            deleteButtonText: {
                lineHeight: 40,
                color: colors.primary
            },
            button: {
                height: 50,
                borderStyle: "solid",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                borderWidth: 1,
                borderColor: colors.deep,
                backgroundColor: colors.deep,
                borderRadius: 5,
                maxWidth: 150
            },
            bottomButtonText: {
                fontSize: 16,
                color: colors.textDefault
            }
        })
    }
})
  







import React, { Component } from "react";
import { StyleSheet, View, Text } from 'react-native'
import colors from '@app/style/colors'
import { observable } from 'mobx'
import { TouchableView } from '@app/components/common/touchable-view'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { observer } from "mobx-react";
 
export interface FilterTypeProps {
    onPress?(): void
    type: number
    all?: boolean
  }
  
@observer
export class FilterType extends Component<FilterTypeProps> {
    private txTypeNames: Array<string> =[]
    private txTypes: Array<number> =[]
    constructor(props: FilterTypeProps) {
        super(props)
        if(props.all){
            this.txTypeNames.push(...[
                i18n.t(LANGUAGE_KEYS.TX_ALL), 
                i18n.t(LANGUAGE_KEYS.TX_TRANSFER), 
                i18n.t(LANGUAGE_KEYS.TX_LEASE), 
                i18n.t(LANGUAGE_KEYS.TX_CANCEL_LEASE), 
                i18n.t(LANGUAGE_KEYS.TX_POS_GENERATED)
            ])
            this.txTypes.push(...[-1, 0, 2, 3, 254])
        } else {
            this.txTypeNames.push(...[
                i18n.t(LANGUAGE_KEYS.TX_TRANSFER), 
                i18n.t(LANGUAGE_KEYS.TX_LEASE)
            ])
            this.txTypes.push(...[0, 2])
        }
    }
    render() {
        const { styles } = obStyles
        return (
            <View style={styles.filterView}>
                <TouchableView
                style={styles.filterViewItem}
                onPress={this.props.onPress}
                >
                    <Text style={styles.filterViewItemText}>{this.props.type === -1 ? i18n.t(LANGUAGE_KEYS.TX_ALL) : this.txTypeNames[this.txTypes.indexOf(this.props.type)]}</Text>
                    <AntDesign
                        name="caretdown"
                        style={{
                            color: colors.textDefault
                        }}
                    />
                </TouchableView>
            </View>
        );
    }
}
  
const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        filterView:{
            // backgroundColor: colors.grey,
            // borderBottomColor: colors.border,
            // borderBottomWidth: 1,
            height: 40
        },
        filterViewItem:{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
        },
        filterViewItemText:{
            fontSize: 16,
            marginRight: 5,
            color: colors.textDefault
        }
      })
    }
})




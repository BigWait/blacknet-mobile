import React, { Component } from "react";
import { StyleSheet, Text, StyleProp, ViewStyle } from 'react-native'
import colors from '@app/style/colors'
import { TouchableView } from '@app/components/common/touchable-view'
import { observer } from "mobx-react";
import { observable } from "mobx";

 
export interface BottomButtonProps {
    text?: String
    onPress?(): void
    style?: StyleProp<ViewStyle>
}
 
@observer
export default class BottomButton extends Component<BottomButtonProps> {

    constructor(props: BottomButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
            style={[styles.bottomButton, this.props.style]}
            onPress={this.props.onPress}
            >
                <Text style={styles.bottomButtonText}>{this.props.text}</Text>
            </TouchableView>
        );
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            bottomButton: {
                height: 40,
                flex: 1,
                borderStyle: "solid",
                // borderTopWidth: 0.5,
                // borderTopColor: colors.border,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                borderWidth: 1,
                borderColor: colors.deep,
                backgroundColor: colors.deep,
                borderRadius: 5,
                marginHorizontal: 10,
                maxWidth: 120
        
            },
            bottomButtonText: {
                fontSize: 16,
                color: colors.pure
            }
        })
    }
})







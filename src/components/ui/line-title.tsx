import React, { Component, ReactNode } from "react";
import { StyleSheet, View } from 'react-native'
import colors from '@app/style/colors'
import { TouchableView } from '@app/components/common/touchable-view'
import { observer } from "mobx-react";
import { observable } from "mobx";
 
export interface LineTitleProps {
    title?: ReactNode
    subtitle?: ReactNode
    trailing?: ReactNode
    leading?: ReactNode
    onPress?(): void
    marginDirection?: "left" | "right"
    marginSize?: number
}
  
@observer
export class LineTitle extends Component<LineTitleProps> {

    constructor(props: LineTitleProps) {
        super(props)
    }

    private getCustomSytle(): any{
        if(this.props.marginDirection === "left"){
            return {
                marginLeft: this.props.marginSize || 0
            }
        }
        if(this.props.marginDirection === "right"){
            return {
                marginRight: this.props.marginSize || 0
            }
        }
        return {}
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={styles.listTitle}
                onPress={this.props.onPress}
            >
                <View style={[styles.listWarp, this.getCustomSytle()]}>
                    {this.props.leading ? <View style={styles.leading}>{this.props.leading}</View> : null}
                    <View style={styles.listContent}>
                        {this.props.title ?? this.props.title}
                        {this.props.subtitle ?? this.props.subtitle}
                    </View>
                    {this.props.trailing ? <View style={styles.trailing}>{this.props.trailing}</View> : null}
                </View>
            </TouchableView>
        );
    }
}

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        listWarp:{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingTop: 15,
            paddingBottom: 15,
            borderBottomColor: colors.border,
            borderBottomWidth: 1
        },
        leading: {
            paddingRight: 15,
            // marginBottom: 5
        },
        trailing: {
            paddingLeft: 15
        },
        listTitle: {
            flex: 1
        },
        listContent: {
            flex: 1,
            overflow: "hidden",
        },
        title: {
            fontSize: 18
        },
        subtitle: {
            fontSize: 16,
            color: colors.cardBackground
        }
      })
    }
})






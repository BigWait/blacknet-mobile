import React, { Component, ReactNode } from "react";
import { StyleSheet, View, Text, ViewStyle, Dimensions, Image } from 'react-native'
import colors from '@app/style/colors'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { TouchableView } from '@app/components/common/touchable-view'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import ListTitle from "../common/list-title";

export interface AccountListItemProps {
    title?: string,
    name?: string,
    onPress?(): void,
    balance?: IBlnBalance
}

@observer
export class AccountListItem extends Component<AccountListItemProps> {

    constructor(props: AccountListItemProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        let accountName = ''
        if (this.props.name) {
            accountName = this.props.name?.length != 67 ? this.props.name : '';
        }
        if (accountName?.length < 1) {
            accountName = this.props.title || "";
        }

        return (
            <TouchableView onPress={this.props.onPress}>
                <View style={styles.item}>
                    <View style={styles.itemTitle}>
                        <Image style={styles.avatar} source={{ uri: userStore.getUserImage(this.props.title) }} />
                        <Text style={{ color: colors.textDefault, fontSize: 16 }}>{accountName}</Text>
                    </View>
                    <View style={styles.itemBalance}>
                        <BLNBalance style={{ color: colors.textDefault, fontSize: 18 }}>{this.props.balance?.balance}</BLNBalance>
                    </View>
                </View>
            </TouchableView>
        )
    }
}

export interface LabelListTitleProps {
    trailing?: ReactNode
    leading?: ReactNode
    onPress?(): void
}

@observer
export class LabelListTitle extends Component<LabelListTitleProps> {

    constructor(props: LabelListTitleProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                onPress={this.props.onPress}
                style={styles.labelListTitle}
            >
                <View style={styles.labelListTitleLeading}>
                    {this.props.leading ? this.props.leading : null}
                </View>
                <View style={styles.labelListTitleTrailing}>
                    {this.props.trailing ? this.props.trailing : null}
                </View>

            </TouchableView>
        );
    }
}

export interface RowListTitleProps {
    trailing?: ReactNode
    leading?: ReactNode
    onPress?(): void
    style?: ViewStyle
}

@observer
export class RowListItem extends Component<RowListTitleProps> {
    constructor(props: RowListTitleProps) {
        super(props)
    }
    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.rowListTitle, this.props.style]}
                onPress={this.props.onPress}
            >
                <View style={styles.rowListTitleLeading}>
                    {this.props.leading ?? this.props.leading}
                </View>
                <View style={styles.rowListTitleTrailing}>
                    {this.props.trailing ?? this.props.trailing}
                </View>
            </TouchableView>
        );
    }
}



import { IBlnTransaction, IBlnLease, IBlnBalance } from '@app/types/bln'
import { showTypeBalance, showType, showTypeIcon, showTime, showBalance, showAmountPrefix } from '@app/utils/bln'
import BLNBalance from "./BLNBalance";
import { accountStore } from "@app/stores/account";
export interface TransactionItemProps {
    title?: string,
    name?: string,
    address: string,
    tx: IBlnTransaction,
    symbol?: string,
    onPress?(): void,
    small?: boolean
}

const windowWidth = Dimensions.get('window').width

@observer
export class TransactionItem extends Component<TransactionItemProps> {

    constructor(props: TransactionItemProps) {
        super(props)
    }

    render() {
        let tx = this.props.tx, title = (<Text></Text>);
        const { styles } = obStyles
        if (tx.type === 254 || tx.type === 999) {
            title = (<TextLink>{showType(this.props.address, this.props.tx)}</TextLink>)
        } else {
            title = (<TextLink style={{ maxWidth: windowWidth * 0.6 }}>{tx.txid.toLocaleLowerCase()}</TextLink>)
        }
        return <ListTitle
            title={title}
            leading={
                this.props.small ? null : <View style={styles.txLeading}>
                    <FontAwesome5
                        name={showTypeIcon(this.props.address, this.props.tx)}
                        style={styles.txLeadingIcon}
                    />
                </View>
            }
            trailingStyle={{ minWidth: 0, paddingLeft: 6 }}
            trailing={<BLNBalance origin={true} prefix={showAmountPrefix(this.props.address, this.props.tx)}>{showBalance(tx.amount)}</BLNBalance>}
            onPress={this.props.onPress}
        />
    }
}

import { stashStore } from '@app/stores/stash'
import { TextLink } from "../common/text";
import { observer, Observer } from "mobx-react";
import { observable } from "mobx";
import { userStore } from "@app/stores/users";
export interface LeaseItemProps {
    tx: IBlnLease,
    onPress?(): void
}

@observer
export class LeaseItem extends Component<LeaseItemProps> {

    constructor(props: LeaseItemProps) {
        super(props)
    }

    render() {
        const tx = this.props.tx
        const { styles } = obStyles
        return (
            <Observer render={() =>
                <View style={styles.leaseList}>
                    <Text style={styles.leaseListText}>{showBalance(this.props.tx.amount)} {accountStore.blnSymbol}</Text>
                    <Text style={styles.leaseListText}>{i18n.t(LANGUAGE_KEYS.TX_TO)}</Text>
                    <Text>{this.props.tx.publicKey}</Text>
                    <View style={styles.leaseListButton}>
                        {stashStore.leaseIsPending(accountStore.currentAddress, tx.publicKey, tx.height) ?
                            <View
                                style={styles.leaseButtonProcess}
                            >
                                <Text>{i18n.t(LANGUAGE_KEYS.TX_PROCESSING)}</Text>
                            </View>
                            :
                            <TouchableView
                                style={styles.leaseButton}
                                onPress={this.props.onPress}
                            >
                                <Text>{i18n.t(LANGUAGE_KEYS.TX_WITHDRAW)}</Text>
                            </TouchableView>
                        }
                    </View>
                </View>
            } />
        )
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            avatar: {
                width: 40,
                height: 40,
                borderRadius: 40,
                position: "absolute",
                top: 0
            },
            item: {
                paddingVertical: 10,
                borderColor: colors.deep,
                borderWidth: 1,
                flex: 1,
                marginHorizontal: 10,
                backgroundColor: colors.deep,
                marginTop: 10,
                borderRadius: 5,
                padding: 10
            },
            itemTitle: {
                marginTop: 0,
                height: 40,
                position: "relative",
                paddingLeft: 52,
                justifyContent: 'center'
            },
            itemBalance: {
                alignItems: 'flex-end',
                marginTop: 10,
            },
            leaseList: {
                paddingTop: 15,
                paddingBottom: 15,
                alignItems: 'center',
                borderBottomWidth: 1,
                borderBottomColor: colors.border
            },
            leaseListButton: {
                marginTop: 10
            },
            leaseListText: {
                lineHeight: 25
            },
            leaseListContent: {
                flex: 1,
                marginRight: 15,
                overflow: "hidden",
            },
            leaseListContenth1: {
                height: 20,
                overflow: "hidden"
            },
            leaseListContenth2: {
                flex: 1,
                height: 20,
                flexDirection: 'row',
                justifyContent: 'space-between'
            },
            leaseButton: {
                backgroundColor: colors.primary,
                padding: 6,
                minWidth: 100,
                alignItems: 'center',
                borderRadius: 4
            },
            leaseButtonProcess: {
                backgroundColor: colors.grey,
                padding: 6,
                minWidth: 100,
                alignItems: 'center',
                borderRadius: 4
            },
            txLeading: {
                width: 40,
                height: 32,
                borderRadius: 40,
                alignItems: "center",
                justifyContent: "center"
            },
            txLeadingIcon: {
                fontSize: 20
            },
            labelListTitle: {
                flexDirection: 'row',
                paddingTop: 10,
                paddingBottom: 10,
                alignItems: 'center',
                justifyContent: 'space-between',
            },
            labelListTitleLeading: {
                minWidth: 0
            },
            labelListTitleTrailing: {
                flex: 1,
                flexDirection: 'row',
                justifyContent: "flex-end"
            },
            rowListTitle: {
                flexDirection: 'row',
                paddingTop: 10,
                paddingBottom: 10,
                alignItems: 'center',
                justifyContent: 'flex-end',
                borderBottomWidth: 1,
                borderBottomColor: colors.border
            },
            rowListTitleLeading: {

            },
            rowListTitleTrailing: {
                flex: 1
            },
            logo: {
                width: 40,
                height: 40,
                borderRadius: 40,
                marginLeft: 15
            },
            listTitle: {
                flexDirection: 'row',
                paddingTop: 13,
                paddingBottom: 13,
                alignItems: 'center',
                justifyContent: 'space-between',
                borderBottomWidth: 1,
                borderBottomColor: '#e4e5e4',
                backgroundColor: '#fff'

            },
            listContent: {
                flex: 1,
                // marginLeft: 15,
                // marginRight: 15,
                overflow: "hidden",
            },
            title: {
                fontSize: 14,
                color: colors.textDefault,
                lineHeight: 20
            },
            subtitle: {
                fontSize: 14,
                color: colors.textDefault
            },
            listIcon: {
                fontSize: 24,
                marginRight: 15
            }
        })
    }
})










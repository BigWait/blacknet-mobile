import { accountStore } from "@app/stores/account";
import colors from "@app/style/colors";
import { BigNumber } from "bignumber.js";
import React, { Component } from "react";
import { StyleSheet, Text, View, StyleProp, ViewStyle, TextStyle } from 'react-native'
import { TouchableView } from '@app/components/common/touchable-view'
import { observer } from "mobx-react";
import { observable } from "mobx";

export interface BalanceProps {
    short?: boolean
    origin?: boolean
    onPress?(): void
    style?: StyleProp<TextStyle>
    prefix?: string
}


function toLocaleString(number: any) {

    let str = String(number);

    if (str.indexOf('.') === -1) return str.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    let left = str.split('.')[0], right = str.split('.')[1];

    return left.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.' + right;
}

@observer
export default class BLNBalance extends Component<BalanceProps> {

    constructor(props: any) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        let short = this.props.short || false;
        let number = this.props.children;
        let text = this.props.origin ? toLocaleString(new BigNumber(number as string).toNumber()) : getBalanceText(number, short);

        return <Text style={[styles.address, getBalanceColor(this.props.prefix), this.props.style]}>{this.props.prefix}{text} {accountStore.symbol}</Text>;
    }
}
function getBalanceColor(prefix?: string) {

    switch (prefix) {
        case "+":
            return { color: colors.transferIn }
        case "-":
            return { color: colors.transferOut }
        default:
            return { color: colors.primary }
    }
}

function getBalanceText(i: any, short: boolean) {

    let fixed = 8;

    if (short) fixed = 0;

    return toLocaleString(parseFloat(new BigNumber(i || "0").div(1e8).toFixed(fixed)));
}


export interface HeaderBlnBalanceProps {
    balance: string
    currency?: string
    nickname?: string
    onPress?(): void
    style?: StyleProp<ViewStyle>
}

@observer
export class HeaderBlnBalance extends Component<HeaderBlnBalanceProps> {

    constructor(props: any) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        const currency = this.props.currency ?? parseFloat(new BigNumber(accountStore.blnPrice).multipliedBy(new BigNumber(this.props.balance || "0").div(1e8)).toFixed(8)).toString()
        return <View style={[styles.balance, this.props.style]}>
            <TouchableView
                onPress={this.props.onPress}
            >
                <BLNBalance style={{
                    fontSize: 24
                }}>{this.props.balance}</BLNBalance>
            </TouchableView>
            <Text style={{
                fontSize: 12, color: colors.textSecondary,
                marginTop: 3
            }}>≈ {currency}</Text>
        </View>
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            nickname: {
                color: colors.textTitle,
                fontSize: 18,
                paddingBottom: 10
            },
            address: {
                color: colors.primary,
                fontSize: 14,
                textAlign: 'right'
            },

            balance: {
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: colors.cardBackground
            },
            balanceText: {
                color: colors.primary,
                fontSize: 30
            },
            balanceCNY: {
                fontSize: 14
            }
        })
    }
})






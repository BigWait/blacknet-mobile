import React, { Component, ReactNode } from "react";
import { StyleSheet, View, Text, StyleProp, ViewStyle } from 'react-native'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import fonts from '@app/style/fonts'
import { TouchableView } from '@app/components/common/touchable-view'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { observer } from "mobx-react";
import { observable } from "mobx";

export interface ResultProps {
    title?: ReactNode
    bottom?: ReactNode
    style?: StyleProp<ViewStyle>
}

@observer
export class Result extends Component<ResultProps> {

    constructor(props: ResultProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={[styles.container, this.props.style]}>
                <View style={styles.containerTop}>
                    {this.props.title}
                </View>
                <View style={styles.containerBottom}>
                    {this.props.bottom}
                </View>
            </View>
        );
    }
}

export interface ButtonResultProps {
    title?: string
    button?: string
    style?: StyleProp<ViewStyle>
    onPress?(): void
}

@observer
export class ButtonResult extends Component<ButtonResultProps> {

    constructor(props: ButtonResultProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <Result
                style={this.props.style}
                title={<Text style={styles.normalTitle} >{this.props.title}</Text>}
                bottom={<TouchableView
                    style={styles.button}
                    onPress={this.props.onPress}
                >
                    <Text>{this.props.button}</Text>
                </TouchableView>}
            />
        );
    }
}

export interface NoResultProps {
    title?: String
    style?: StyleProp<ViewStyle>
}

@observer
export class NoResult extends Component<NoResultProps> {

    constructor(props: NoResultProps) {
        super(props)
    }

    render() {
        const commonIconOptions = {
            name: 'angle-down',
            size: 19,
            color: colors.textSecondary
        }
        const { styles } = obStyles
        return (
            <View style={styles.centerContainer}>
                <Text style={styles.normalTitle}>{this.props.title ?? i18n.t(LANGUAGE_KEYS.NO_RESULT_RETRY)}</Text>
                <View style={{ marginTop: sizes.goldenRatioGap }}>
                    {/* <FontAwesome5 {...commonIconOptions} /> */}
                    {/* <FontAwesome5 {...commonIconOptions} style={{ marginTop: -13 }} /> */}
                </View>
            </View>
        );
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            button: {
                backgroundColor: colors.primary,
                borderRadius: 5,
                padding: 10,
                alignItems: 'center',
                minWidth: 100
            },
            container: {
                // justifyContent: 'center',
                // alignItems: 'center',
                padding: sizes.gap
            },
            containerTop: {
                alignItems: 'center'
            },
            containerBottom: {
                marginTop: sizes.goldenRatioGap,
                alignItems: 'center'
            },
            centerContainer: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: sizes.gap
            },
            normalTitle: {
                ...fonts.base,
                color: colors.textDefault,
                textAlign: 'center'
            }
        })
    }
})










import React from "react";
import { StyleSheet,  StyleProp, ViewStyle, View } from 'react-native'
import colors from '@app/style/colors'
import { IChildrenProps } from "@app/types/props";
import { observer } from "mobx-react";
import { safeAreaViewBottom } from "@app/style/sizes";
import { observable } from "mobx";
import { optionStore } from "@app/stores/option";

 
export interface BottomBarProps {
    style?: StyleProp<ViewStyle>
}

export default observer((props: BottomBarProps & IChildrenProps): JSX.Element => {
    const { styles } = obStyles
    return (
        <View style={[styles.container, props.style]}>
            {props.children}
        </View>
    )
})
  
export const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            container:{
                shadowColor: optionStore.darkTheme ? colors.headerBGColor : colors.border,
                shadowOpacity: 0.4,
                shadowOffset: { width: 0, height: -4},
                backgroundColor: colors.cardBackground,
                paddingBottom: safeAreaViewBottom
            }
        })
    }
})







/**
 * App entry.
 * @file App 入口文件
 * @module app/entry
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */
import './shim'
import {AppRegistry} from 'react-native';
import App from './src/app';
import {name as appName} from './app.json';
AppRegistry.registerComponent(appName, () => App);

mobile_version=$(node -e "process.stdout.write((require('./package.json').version));")

echo "Start release $mobile_version"

cp android/app/build/outputs/apk/release/app-release.apk android/app/build/blacknet-mobile-$mobile_version.apk

response=`curl --request POST --header "Private-Token: ${gitlab_private_token}" --form "file=@android/app/build/blacknet-mobile-${mobile_version}.apk" "https://gitlab.COM/api/v4/projects/blacknet-ninja%2Fblacknet-mobile/uploads"`
echo $response
markdown=`echo $response | jq -r '.markdown'`

echo $markdown

